import React from "react";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import Loading from "../components/loading/Loading";
import Login from "../feature/Login/Login";
const Auth = ({ authRoute }) => {
  const authLoading = useSelector((state) => state.auth.authLoading);

  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);
  let body;

  if (authLoading) {
    body = (
      <Loading />
    );
  } else if (isAuthenticated) {
    return <Redirect to="/admin/dashboard" />;
  } else {
    body = <>{authRoute === "login" && <Login />} </>;
  }

  return <>{body}</>;
};

export default Auth;
