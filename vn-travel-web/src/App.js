// app.js
// import "mapbox-gl/dist/mapbox-gl.css";
import React, { useEffect } from "react";
import { Provider, useDispatch } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import store from "./app/store";
import Auth from "./auth/Auth";
import Layout from "./components/layout/Layout";
import ProtectedRoute from "./components/ProtectedRoute";
import Error from "./pages/Error";
import Landing from "./pages/Landing";
import { loadUser } from "./redux/authSlice";

// defaultTheme
const App = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(loadUser());
    //TODO: call to get tag, categories,...

    return () => {};
  }, []);
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path="/" component={Landing} />
          <Route
            exact
            path="/admin/login"
            render={(props) => <Auth {...props} authRoute="login" />}
          />
          <ProtectedRoute path="/admin" component ={Layout} />
          <Route path="" component={Error} />
        </Switch>
      </Router>
    </Provider>
  );
};

export default App;
