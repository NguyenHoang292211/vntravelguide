import React, { useState } from "react";
import "./explore.css";
import ExploreItem from "./ExploreItem";
import Popup from "../../components/popup/Popup";
import ExploreForm from "./ExploreForm";
import { Paper } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { createExplore, updateExplore } from "../../redux/exploreSlide";
import { Snackbar } from "@mui/material";
import MuiAlert from "@mui/material/Alert";
import { Box } from "@mui/system";
import { Skeleton } from "@mui/material";
import { Grid } from "@material-ui/core";
const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});
let SUCCESS_MESSAGE = "Update successfully!";
let ERROR_MESSAGE = "Error happen, try again!";

const Explore = () => {
  const explores = useSelector((state) => state.explore.explores);
  const exploreLoading = useSelector((state) => state.explore.isLoading);
  const dispatch = useDispatch();
  const [recordForEdit, setRecordForEdit] = useState({});
  const [openPopup, setOpenPopup] = useState(false);
  const [openSnackSuccess, setOpenSnackSuccess] = useState(false);
  const [openSnackError, setOpenSnackError] = useState(false);
  const handleCreateNewExplore = () => {
    setRecordForEdit(null);
    setOpenPopup(true);
  };
  /**
   * Function handle submit button
   * @param {*} explore model Explore update
   * @param {*} resetForm function clear form info
   */
  const addOrEdit = async (explore, resetForm) => {
    try {
      if (explore.id === "001") {
        const response = await dispatch(createExplore(explore));
        setOpenPopup(false);

        if (response.payload.success) {
          SUCCESS_MESSAGE = "Create successfully";
          setOpenSnackSuccess(true);
        } else {
          setOpenSnackError(true);
        }
      } else {
        setOpenPopup(false);
        const response = await dispatch(updateExplore(explore));
        if (response.payload.success) {
          SUCCESS_MESSAGE = "Update successfully";
          setOpenSnackSuccess(true);
        } else {
          setOpenSnackError(true);
        }
      }
      resetForm();
    } catch (error) {
      setOpenSnackError(true);
      resetForm();
    }
  };

  const handleEditExplorePlace = (id) => {
    setRecordForEdit(explores.find((item) => item.id === id));
    setOpenPopup(true);
  };

  const handleCloseSnackError = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenSnackError(false);
  };
  const handleCloseSnackSuccess = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenSnackSuccess(false);
  };
  const handleClosePopup = () => {
    setOpenPopup(false);
  };
  return (
    <>
      {exploreLoading ? (
        <Grid container>
          {Array.from(new Array(8)).map((item, index) => (
            <Box key={index} sx={{ width: 300, marginRight: 0.5, my: 5 }}>
              <Skeleton variant="rectangular" width={300} height={175} />
            </Box>
          ))}
        </Grid>
      ) : (
        <>
          <div className="header">
            <i class="bx bxs-exclude"></i>
            Explore
          </div>
          <Paper p={3}>
            <div className="container">
              {explores?.length >= 0 &&
                explores.map((event, index) => (
                  <ExploreItem item={event} onClick={handleEditExplorePlace} />
                ))}
              <div
                className="btnCreate"
                onClick={() => handleCreateNewExplore()}
              >
                New Explore
              </div>
            </div>
            {openPopup && (
              <Popup
                title="Explore form"
                openPopup={openPopup}
                setOpenPopup={setOpenPopup}
              >
                <ExploreForm
                  recordForEdit={recordForEdit}
                  addOrEdit={addOrEdit}
                  closePopup={handleClosePopup}
                />
              </Popup>
            )}
          </Paper>
          {/* /* Snack  */}
          <Snackbar
            anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
            open={openSnackSuccess}
            autoHideDuration={3000}
            onClose={handleCloseSnackSuccess}
          >
            <Alert
              onClose={handleCloseSnackSuccess}
              sx={{ width: "100%" }}
              severity="success"
            >
              {SUCCESS_MESSAGE}
            </Alert>
          </Snackbar>
          {/* /* Snack error */}
          <Snackbar
            anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
            open={openSnackError}
            autoHideDuration={3000}
            onClose={handleCloseSnackError}
          >
            <Alert
              onClose={handleCloseSnackError}
              sx={{ width: "100%" }}
              severity="error"
            >
              {ERROR_MESSAGE}
            </Alert>
          </Snackbar>
        </>
      )}
    </>
  );
};

export default Explore;
