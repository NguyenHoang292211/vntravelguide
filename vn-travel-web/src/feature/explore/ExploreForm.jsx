import { Grid, makeStyles, Typography } from "@material-ui/core";
import React, { useEffect } from "react";
import { Form, useForm } from "../../components/UI/useForm";
import Input from "../../components/UI/Input";
import Button from "../../components/UI/Button";
import MultipleValues from "../../components/UI/MultipleValues";
import { Paper } from "@mui/material";
import { ClassNames } from "@emotion/react";
import { ObjectSchema } from "yup";
import { useSelector } from "react-redux";
import SwitchIcon from "../../components/UI/SwitchIcon";


const initialFValues = {
  id: "001",
  title: "",
  description: "",
  banner: "",
  tags: [],
  isHidden: false,
};

const useStyles = makeStyles((theme) => ({
  paperImageReview: {
    padding: theme.spacing(2),
  },
  imgReview: {
    height: "19rem",
    width: "25rem",
  },
}));

const ExploreForm = (props) => {
  const classes = useStyles();
  const tags = useSelector((state) => state.place.tags);

  const { closePopup, submitError, isLoading, addOrEdit, recordForEdit } =
    props;

  let tagsSelected = recordForEdit != null ? recordForEdit.tags : [];

  // Convert tag(Object) to array of id
  const myTags = tagsSelected?.map((item) => item.id ?? item._id);


  const validate = (fieldValues = values) => {
    let temp = { ...errors };
    if ("title" in fieldValues) {
      temp.title = fieldValues.title ? "" : "This field is required";
    }
    if (fieldValues == values) {
      return Object.values(temp).every((x) => x == "");
    }
  };

  const { values, setValues, errors, setErrors, handleInputChange, resetForm } =
    useForm(initialFValues, true, validate);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validate()) {
      addOrEdit(values, resetForm);
    }
  };

  useEffect(() => {
    if (recordForEdit != null)
      setValues({
        ...recordForEdit,
      });
  }, [recordForEdit]);

  return (
    <Form onSubmit={handleSubmit}>
      <Grid container>
        {/* /* Information */}
        <Grid md={6} xs={12}>
          <Input
            name="title"
            label="Title"
            value={values.title}
            onChange={handleInputChange}
            error={errors.title}
            rows={1}
          />
          <Input
            name="description"
            label="Description"
            value={values.description}
            onChange={handleInputChange}
            error={errors.description}
            multiline
            rows={3}
          />
          <Input
            name="banner"
            label="Banner"
            value={values.banner}
            onChange={handleInputChange}
            error={errors.banner}
          />

          <MultipleValues
            name="tags"
            label="Tag"
            optionsList={tags}
            defaultValue={tags.filter((item) => myTags?.includes(item.id))}
            value={values.tags}
            onChange={handleInputChange}
          />
          <SwitchIcon
            name="isHidden"
            defaultValue={values?.isHidden ? false : true}
            value={values.isHidden}
            onChange={handleInputChange}
            label={values.isHidden ? "Not use" : "Using"}
          />
        </Grid>
        {/* /* Image */}
        <Grid className={classes.paperImageReview} md={6} xs={12}>
          <Typography>Image review</Typography>
          <Paper
            className={classes.imgReview}
            style={{
              backgroundImage: `url(${values.banner}) `,
              backgroundRepeat: "no-repeat",
              backgroundSize: "contain",
            }}
          ></Paper>
          <div>
            <Button type="submit" text="Submit" />
            <Button text="Cancel" onClick={closePopup} color="default" />
          </div>
        </Grid>
      </Grid>
    </Form>
  );
};

export default ExploreForm;
