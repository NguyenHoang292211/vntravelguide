import React from "react";
import "./exploreItem.css";
const ExploreItem = ({ item, onClick }) => {
  const { title, description, banner, tags, id, isHidden } = item;
  return (

    <div
      className="exploreItem"
      style={{ backgroundImage: `url(${banner})`, opacity: isHidden ? 0.3 : 1 }}
    >
      {" "}
      <div className="divBlur">
        <div className="div__description">
          <div className="explore__info">
            <label className="explore__info--txt title">{title}</label>
            <label className="explore__info--txt description">
              {description}
            </label>
          </div>
          <div className="div__edit">
            <i
              class="bx bxs-edit-location icon bx-sm"
              onClick={() => onClick(id)}
            ></i>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ExploreItem;
