import {
  Fab, InputLabel,
  MenuItem,
  Select,
  TextField,
  Tooltip
} from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import InputAdornment from "@material-ui/core/InputAdornment";
import { makeStyles } from "@material-ui/core/styles";
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from "@material-ui/icons/Search";
import React from "react";
import "./navFilter.css"
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },  
  "MuiTextField-root": {
    margin: theme.spacing(2),
    width: "25ch",
  },
}));

const NavFilter = () => {
  const classes = useStyles();

  const [age, setAge] = React.useState("");

  const handleChange = (event) => {
    setAge(event.target.value);
  };
  return (
    <div className="nav">
      <div className="nav__left">
        {/* /* Filter select province */}
        <FormControl className={classes.formControl}>
          <InputLabel id="filter__province--label">Province</InputLabel>
          <Select
            labelId="filter__province--label"
            id="filter_province--select"
            value={age}
            onChange={handleChange}
          >
            <MenuItem value={10}>All</MenuItem>
            <MenuItem value={20}>District 2</MenuItem>
            <MenuItem value={30}>District 3</MenuItem>
          </Select>
        </FormControl>
        {/* /* Filter select category */}
        <FormControl className={classes.formControl}>
          <InputLabel id="filter__category--label">Category</InputLabel>
          <Select
            labelId="filter__category--label"
            id="filter_category--select"
            value={age}
            onChange={handleChange}
          >
            <MenuItem value={10}>All</MenuItem>
            <MenuItem value={20}>Food</MenuItem>
            <MenuItem value={30}>Drink</MenuItem>
          </Select>
        </FormControl>
        {/* /* Filter select tag */}
        <FormControl className={classes.formControl}>
          <InputLabel id="filter__tag--label">Tag</InputLabel>
          <Select
            labelId="filter__tag--label"
            id="filter_tag--select"
            value={age}
            onChange={handleChange}
          >
            <MenuItem value={10}>All</MenuItem>
            <MenuItem value={20}>Tag 1</MenuItem>
            <MenuItem value={30}>Tag 2</MenuItem>
          </Select>
        </FormControl>
        {/* /* Search input */}
        <FormControl className={classes.formControl}>
          <TextField
            defaultValue="Bare"
            InputProps={{
              endAdornment: (
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              ),
            }}
          ></TextField>
        </FormControl>
      </div>
      <div className="nav__right">
        {/*  /* Button add new place */}

        <Tooltip  title="Place" aria-label="Add">
          <Fab color="primary" className={classes.fab}>
            <AddIcon />
          </Fab>
        </Tooltip>
      </div>
    </div>
  );
};

export default NavFilter;
