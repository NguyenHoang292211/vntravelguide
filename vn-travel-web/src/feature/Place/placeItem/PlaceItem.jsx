import { Grid, makeStyles } from "@material-ui/core";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import StarIcon from "@material-ui/icons/Star";
import Card from "@mui/material/Card";
import React from "react";
import { useHistory } from "react-router";
import { CATEGORY, STATUS_PRIVATE } from "../../../utils/constants";
import "./placeItem.css";
import TagLabel from "./tagLabel/TagLabel";

const useStyles = makeStyles((theme) => ({
  item__grid: {
    padding: theme.spacing(1),
    overflow: "hidden",
  },
  item__card: {
    height: "var(--card__img-height)",
  },
}));
const PlaceItem = (props) => {
  const { item, id, handleEditPlace } = props;


  const classes = useStyles();
  let ratingMapper = [];
  //TODO: parse t to value
  for (let i = 1; i <= parseInt(item.rateVoting); i++) {
    ratingMapper.push(
      <StarIcon className="star" md-12 style={{ width: "14px" }} />
    );
  }
  return (
    <Grid item xs={12} sm={6} md={3} key={id} className={classes.item__grid}>
      <Card sx={{ position: "relative" }} className={classes.item__card}>
        <div className="card__image--div">
          <img
            className="card__image--img"
            src={
              item.images.length <= 0
                ? "https://images.unsplash.com/photo-1632516643720-e7f5d7d6ecc9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=411&q=80"
                : item.images[0]
            }
            alt="card-img"
          />
          <button
            className="card__edit"
            onClick={() => {
              handleEditPlace(id);
            }}
          >
            {" "}
            <i className="bx bxs-edit-alt"></i>
          </button>
        </div>
        <div className="card__content">
          <h3 className="card__title">
            {item.name.length > 30
              ? `${item.name.substring(0, 25)}...`
              : item.name}
          </h3>
          <div className="card__rating">
            <div className="card__rating--left">{ratingMapper}</div>
            <div className="card__rating--right">
              <span>({item.reviewCount??'0'} comment)</span>
            </div>
          </div>
          <div className="card__location">
            <LocationOnIcon className="card__location--icon" />
            <h4 className="card__location--address">
              {" "}
              {item.address
                ? item.address.length > 70
                  ? `${item.address.substring(0, 70)}...`
                  : item.address
                : item.province?.name}
            </h4>
          </div>
          <div className="card__tag">
            {item.category && (
              <TagLabel name={item.category.name} type={CATEGORY} />
            )}
            {item.status === STATUS_PRIVATE && (
              <TagLabel name="Close" type="tag" type={STATUS_PRIVATE} />
            )}
            {item.popular && <i class="bx bxs-purchase-tag  bx-tada iconPopular" color="#c4c4"></i>}
          </div>
          <div className="card__time">
            <div>
              <span>Open </span>
              {item.openTime} {item.closeTime}
            </div>
          </div>
        </div>
      </Card>
    </Grid>
  );
};

export default PlaceItem;
