import React from "react";
import {
  CATEGORY,
  PROVINCE,
  STATUS_ACTIVE,
  STATUS_PRIVATE,
  STATUS_DISABLE
} from "../../../../utils/constants";
import "./tagLabel.css";

const TagLabel = (props) => {
  const { name, type } = props;
  return (
    <>
      {type === CATEGORY ? (
        <span className="tag tag--category">{name}</span>
      ) : type === PROVINCE ? (
        <span className="tag tag--province">{name}</span>
      ) : type === STATUS_PRIVATE ? (
        <span className="tag tag--private">{name}</span>
      ) : type === STATUS_ACTIVE ? (
        <span className="tag tag--active">{name}</span>
      ) : type === STATUS_DISABLE ? (
        <span className="tag tag--disable">{name}</span>
      ) : type === STATUS_ACTIVE ? (
        <span className="tag tag--active">{name}</span>
      ) : (
        ""
      )}
    </>
  );
};

export default TagLabel;
