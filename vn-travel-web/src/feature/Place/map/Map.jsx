import "@goongmaps/goong-geocoder/dist/goong-geocoder.css";
import "@goongmaps/goong-js/dist/goong-js.css";
import MapGL, { Marker } from "@goongmaps/goong-map-react";
import { Button, Divider, Grid, InputBase, TextField } from "@material-ui/core";
import ArrowBackOutlinedIcon from "@mui/icons-material/ArrowBackOutlined";
import { default as LocationOn } from "@mui/icons-material/LocationOn";
import ReplayOutlinedIcon from "@mui/icons-material/ReplayOutlined";
import SaveRoundedIcon from "@mui/icons-material/SaveRounded";
import SearchOutlinedIcon from "@mui/icons-material/SearchOutlined";
import CircularProgress from "@mui/material/CircularProgress";
import IconButton from "@mui/material/IconButton";
import Paper from "@mui/material/Paper";
import React, { useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router";
import { updatePlace } from "../../../redux/placeSlice";
import "./map.css";

const Map = (props) => {
  const [marker, setMarker] = useState(null);
  const [viewport, setViewport] = React.useState({
    latitude: 16.4619,
    longitude: 107.59546,
    zoom: 6,
  });
  const { placeId } = useParams();
  let currentPlace = useSelector((state) =>
    state.place.places.find((item) => item.id === placeId)
  );
  const [isSearching, setIsSearching] = useState(false);
  const history = useHistory();

  const dispatch = useDispatch();
  const [searchText, setSearchText] = useState("");
  const [predictedPlaces, setPredictedPlaces] = useState([]);
  const [visiblePrediction, setVisiblePrediction] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const handleGeocoderViewportChange = (viewport) => {
    const geocoderDefaultOverrides = { transitionDuration: 1500 };
    return handleViewportChange({
      ...viewport,
      ...geocoderDefaultOverrides,
    });
  };
  const handleViewportChange = (viewport) => {
    setViewport(viewport);
  };

  const getPredictedPlaces = (input) => {
    if (input !== "") {
      const predictionUrl = `https://rsapi.goong.io/Place/AutoComplete?api_key=${process.env.REACT_APP_GOONG_MAPS_KEY}&input=${input}`;
      setPredictedPlaces([]);
      setIsSearching(true);
      fetch(predictionUrl)
        .then((response) => {
          if (response.ok) {
            return response.json();
          }
          throw response;
        })
        .then((data) => {
          setPredictedPlaces(data.predictions);
          return data;
        })
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {
          setIsSearching(false);
        });
    } else {
      setPredictedPlaces([]);
    }
  };

  //called when input search change
  const onSearchPlace = async (e) => {
    setVisiblePrediction(true);
    setSearchText(e.target.value);
    getPredictedPlaces(e.target.value);
  };

  //call save {lng, lat, address} to database
  const handleSaveLocation = async () => {
    const { lat, lng } = marker;
    const address = searchText;

    if (currentPlace) {
      let newPlace = {
        ...currentPlace,
        ...{ longtitude: lng, lattitude: lat, address: address },
      };
      setIsLoading(true);
      const response = await dispatch(updatePlace(newPlace));
      if (response.payload.success) {
        setIsLoading(false);
        history.goBack();
      }
    }
  };

  //Get place information from goong
  const getPlaceById = (place_id, description) => {
    const url = `https://rsapi.goong.io/Place/Detail?place_id=${place_id}&api_key=${process.env.REACT_APP_GOONG_MAPS_KEY}`;
    setSearchText(description);
    setPredictedPlaces([]);
    fetch(url)
      .then((response) => {
        if (response.ok) return response.json();
      })
      .then((data) => {
        setMarker({
          lat: data.result.geometry.location.lat,
          lng: data.result.geometry.location.lng,
        });
        handleGeocoderViewportChange({
          latitude: data.result.geometry.location.lat,
          longitude: data.result.geometry.location.lng,
          zoom: 10,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const mapRef = useRef();

  const handleReset = () => {
    setMarker(null);
    setSearchText("");
  };

  const goBack = () => {
    history.goBack();
  };

  return (
    <div className="container">
      <IconButton className="back-button" onClick={goBack}>
        <ArrowBackOutlinedIcon style={{ color: "#888", fontSize: 22 }} />
      </IconButton>
      <Grid xs={12} container>
        <Grid item className="search-contaner" md={5} xs={12}>
          <Grid xs={12} className="search-grid">
            {/* Search */}
            <Grid xs={12}>
              <Paper
                component="form"
                sx={{
                  p: "2px 4px",
                  display: "flex",
                  alignItems: "center",
                  width: "100%",
                }}
              >
                <InputBase
                  sx={{ ml: 1, flex: 1 }}
                  placeholder="Search Google Maps"
                  inputProps={{ "aria-label": "search google maps" }}
                  onChange={onSearchPlace}
                  value={searchText}
                  className="search-place-input"
                  // onBlur={() => {
                  //   setVisiblePrediction(false);
                  // }}
                />
                <IconButton
                  className="search-icon"
                  type="button"
                  sx={{ p: "10px" }}
                  aria-label="search"
                >
                  <SearchOutlinedIcon />
                </IconButton>
              </Paper>
              {/* Predicted places */}
              <div
                className={
                  visiblePrediction
                    ? "autocomplete-dropdown-container-visible"
                    : "autocomplete-dropdown-container"
                }
              >
                {isSearching ? (
                  <div className="searchLoading">Loading...</div>
                ) : null}
                {predictedPlaces.map((prediction) => {
                  return (
                    <Grid item xs={12} md={5}>
                      <div className="suggestion-item">
                        <IconButton className="location-icon">
                          <LocationOn fontSize="small" htmlColor="#888" />
                        </IconButton>
                        <div
                          className="desciption"
                          onClick={() =>
                            getPlaceById(
                              prediction.place_id,
                              prediction.description
                            )
                          }
                        >
                          <p className="suggestion-item-text main-text">
                            {prediction.structured_formatting.main_text}
                          </p>
                          <p className="suggestion-item-text secondary-text">
                            {prediction.structured_formatting.secondary_text}
                          </p>
                        </div>
                      </div>
                      <Divider
                        variant="fullWidth"
                        component="div"
                        style={{
                          background: "rgb(225,225,225)",
                        }}
                      />
                    </Grid>
                  );
                })}
              </div>
            </Grid>
            <div className="geometry-description">
              <p className="geometry-text">Geometry information :</p>
            </div>
            <Grid container xs={12} spacing={1} style={{ padding: "10px" }}>
              <Grid item xs={6} md={12}>
                <TextField
                  className="input-text"
                  label="Longtitude"
                  type="text"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  variant="outlined"
                  size="small"
                  name="longtitude"
                  value={marker ? marker.lng : ""}
                />
              </Grid>
              <Grid item xs={6} md={12}>
                <TextField
                  className="input-text"
                  label="Lattitude"
                  type="text"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  placeholder=""
                  variant="outlined"
                  size="small"
                  name="lattitude"
                  value={marker ? marker.lat : ""}
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid xs={12} spacing={2} className="button-container">
            <Grid md={12} className="button-container">
              <Button
                className="action-button"
                variant="contained"
                color="secondary"
                size="small"
                endIcon={<ReplayOutlinedIcon />}
                onClick={handleReset}
              >
                Reset
              </Button>
            </Grid>
            <Grid item md={12} className="button-container">
              <Button
                className="action-button"
                variant="outlined"
                color="primary"
                size="small"
                endIcon={<SaveRoundedIcon />}
                onClick={handleSaveLocation}
                disabled={isLoading}
              >
                Save location
              </Button>
              {isLoading && (
                <CircularProgress
                  size={23}
                  sx={{
                    position: "absolute",
                    alignItems: "center",
                    justifyContent: "center",
                    display: "flex",
                    // marginTop: "-12px",
                    // marginLeft: "-12px",
                  }}
                />
              )}
            </Grid>
          </Grid>
        </Grid>
        <Grid
          container
          item
          className="map-container"
          xs={12}
          md={7}
          style={{ position: "relative" }}
        >
          <MapGL
            {...viewport}
            ref={mapRef}
            width="100%"
            height="90vh"
            onViewportChange={handleViewportChange}
            goongApiAccessToken={process.env.REACT_APP_GOONG_MAPTILES_KEY}
          >
            {marker && (
              <Marker
                latitude={marker.lat}
                longitude={marker.lng}
                offsetLeft={-20}
                offsetTop={-10}
              >
                <img
                  src="/location_icon.png"
                  height="50"
                  width="50"
                  alt="location"
                />
              </Marker>
            )}
          </MapGL>
        </Grid>
      </Grid>
    </div>
  );
};

export default Map;
