import { Card, Fab, Grid, makeStyles, Tooltip } from "@material-ui/core";
import InputAdornment from "@material-ui/core/InputAdornment";
import { Search } from "@material-ui/icons";
import AddIcon from "@material-ui/icons/Add";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import Popup from "../../components/popup/Popup";
import Input from "../../components/UI/Input";
import Select from "../../components/UI/Select";
import { createPlace } from "../../redux/placeSlice";
import { ERROR_MESSAGE, FILTER_POPULAR } from "../../utils/constants";
import "./place.css";
import PlaceForm from "./placeForm/PlaceForm";
import PlaceList from "./placeList/PlaceList";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
  },
  selectFilter: {
    width: "40rem",
    marginRight: theme.spacing(2),
    paddingRight: "1rem",
  },
  "MuiTextField-root": {
    margin: theme.spacing(2),
    width: "25ch",
  },
  searchInput: {
    width: "20rem",
    // [theme.breakpoints.down("sm")]:{
    //   marginTop:theme.spacing(1)
    // }
  },
  card: {
    margin: theme.spacing(2),
    padding: theme.spacing(2),
    paddingRight: theme.spacing(0),
  },
  fab: {
    paddingRight: theme.spacing(-1),
  },
}));

const PlacesView = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const [openPopup, setOpenPopup] = useState(false);
  const [error, setError] = useState("");
  const [filterFn, setFilterFn] = useState([]);
  const places = useSelector((state) => state.place.places)
    .slice()
    .sort((a, b) => Date.parse(b.updatedAt) - Date.parse(a.updatedAt));
  const categories = useSelector((state) => state.place.categories);
  const provinces = useSelector((state) => state.place.provinces);
  const categoriesFilter = [
    { id: "All", name: "All" },
    {
      id: FILTER_POPULAR,
      name: FILTER_POPULAR,
    },
    ...categories.map((e) => {
      return { id: e.id, name: e.name };
    }),
  ];
  const provincesFilter = [
    { id: "All", name: "All" },
    {
      id: FILTER_POPULAR,
      name: FILTER_POPULAR,
    },
    { id: "" },
    ...provinces.map((e) => {
      return { id: e.id, name: e.name };
    }),
  ];

  const isLoading = useSelector((state) => state.place.isLoadipng);
  const [keyWord, setKeyWord] = useState("");
  const [filterProvince, setFilterProvince] = useState("All");
  const [filterCategory, setFilterCategory] = useState("All");
  const openInPopup = (item) => {
    setOpenPopup(true);
  };
  const closePopup = (item) => {
    setOpenPopup(false);
  };

  const handleCreate = async (place) => {
    try {
      const resultCreate = await dispatch(createPlace(place));

      if (String(resultCreate.payload.success) === "true") {
        const placeId = resultCreate.payload.place.id;
        history.push(`places/edits/${placeId}`);
      } else {
        setError(ERROR_MESSAGE);
        setTimeout(() => {
          setError("");
        }, 2000);
      }
    } catch (error) {
      setError(ERROR_MESSAGE);
      setTimeout(() => {
        setError("");
      }, 2000);
    }
  };

  const handleSearch = (keyWord, items) => {
    if (keyWord === "") {
      return items;
    } else
      return [
        ...items.filter((x) =>
          x.name
            .toLowerCase()
            .normalize("NFD")
            .replace(/([\u0300-\u036f]|[^0-9a-zA-Z])/g, "")
            .includes(
              keyWord
                .toLowerCase()
                .normalize("NFD")
                .replace(/([\u0300-\u036f]|[^0-9a-zA-Z])/g, "")
            )
        ),
      ];
  };
  const handleFilterProvince = (filter, items) => {
    if (filter === "All") return items;
    if (filter === FILTER_POPULAR) return items.filter((x) => x.popular);
    else return [...items.filter((x) => x.province._id.includes(filter))];
  };
  const handleFilterCategory = (filter, items) => {
    if (filter === "All") return items;
    if (filter === FILTER_POPULAR) return items.filter((x) => x.popular);
    else {
      return [...items.filter((x) => x.category._id.includes(filter))];
    }
  };

  const filter = (key, province, category) => {
    let filterPlaces = handleSearch(key, places);
    filterPlaces = handleFilterProvince(province, filterPlaces);
    filterPlaces = handleFilterCategory(category, filterPlaces);
    setFilterFn([...filterPlaces]);
  };
  const handleFilter = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    if (name === "province") {
      setFilterProvince(value);
      filter(keyWord, value, filterCategory);
    } else if (name === "category") {
      setFilterCategory(value);
      filter(keyWord, filterProvince, value);
    } else {
      setKeyWord(value);
      filter(value, filterProvince, filterCategory);
    }
  };
  useEffect(() => {
    filter(keyWord, filterProvince, filterCategory);
    return () => {};
  }, []);

  return (
    <div>
      {/* /* Start - Nav filter */}
      <div className={`nav ${classes.card}`}>
        <div className="nav__left">
          <Grid container spacing={1}>
            <Grid item md={3}>
              {/* /* Filter select province */}
              <Select
                className={classes.selectFilter}
                name="province"
                label="Province"
                onChange={handleFilter}
                options={provincesFilter}
                value={filterProvince}
              ></Select>
            </Grid>
            <Grid item md={3}>
              {/* /* Filter select category */}
              <Select
                className={classes.selectFilter}
                name="category"
                label="Category"
                options={categoriesFilter}
                onChange={handleFilter}
                value={filterCategory}
              ></Select>
            </Grid>
            <Grid item md={6} xs={6}>
              <Input
                name="search"
                label="Search places"
                className={classes.searchInput}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start">
                      <Search />
                    </InputAdornment>
                  ),
                }}
                onChange={handleFilter}
              ></Input>
            </Grid>
          </Grid>
        </div>
        <div className="nav__right">
          {/*  /* Button add new place */}

          <Tooltip title="Place" aria-label="Add">
            <Fab
              color="primary"
              className={classes.fab}
              onClick={() => openInPopup()}
            >
              <AddIcon />
            </Fab>
          </Tooltip>
        </div>
      </div>
      {/*  /* End - Nav filter */}
      <Card className={classes.card}>
        <PlaceList places={filterFn} />
      </Card>
      <Popup
        openPopup={openPopup}
        setOpenPopup={setOpenPopup}
        title="Create new place"
      >
        <PlaceForm
          handleCreate={handleCreate}
          closePopup={closePopup}
          submitError={error}
          isLoading={isLoading}
        />
      </Popup>
    </div>
  );
};

export default PlacesView;
