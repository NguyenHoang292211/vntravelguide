import { CircularProgress, Grid } from "@material-ui/core";
import React from "react";
import NumberFormat from "react-number-format";
import { useSelector } from "react-redux";
import Button from "../../../components/UI/Button";
import Input from "../../../components/UI/Input";
import Select from "../../../components/UI/Select";
import TimePicker from "../../../components/UI/TimePicker";
import { Form, useForm } from "../../../components/UI/useForm";
import "./placeForm.css";

const PlaceForm = (props) => {
  const { handleCreate, closePopup, submitError, isLoading } = props;
  //TODO: change to truely code get tags,...
  const categories = useSelector((state) => state.place.categories);
  const provinces = useSelector((state) => state.place.provinces);

  const validate = (fieldValues = values) => {
    let temp = { ...errors };
    if ("name" in fieldValues)
      temp.name = fieldValues.name ? "" : "This field is not empty";
    if ("description" in fieldValues)
      temp.description = fieldValues.description
        ? ""
        : "This field is not empty";

    setErrors({
      ...temp,
    });

    if (fieldValues === values) {
      return Object.values(temp).every((x) => x === "");
    }
  };

  const initialFValues = {
    id: "0",
    name: "",
    category: categories[0] ? categories[0].id : "",
    province: provinces[0] ? provinces[0].id : "",
    description: "",
    openTime: "07:30",
    closeTime: "07:00",
    startPrice: "",
    endPrice: "",
  };

  const { values, errors, setErrors, handleInputChange } = useForm(
    initialFValues,
    true,
    validate
  );

  const handleSubmit = (e) => {
    e.preventDefault();

    if (validate()) {
      handleCreate(values);
    }
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Grid container>
        <Grid item xs={12} >
          <Input
            name="name"
            label="Name"
            value={values.name}
            onChange={handleInputChange}
            error={errors.name}
            rows={1}
          ></Input>

          <Input
            name="description"
            label="Description"
            value={values.description}
            onChange={handleInputChange}
            error={errors.description}
            multiline
            rows={4}
          ></Input>
        </Grid>
        {/* /* Selection Left */}
        <Grid item md={6} xs={12}>
          <Select
            name="province"
            label="Province"
            value={values.province}
            onChange={handleInputChange}
            options={provinces}
          ></Select>

          <TimePicker
            name="openTime"
            label="Open time"
            value={values.openTime}
            onChange={handleInputChange}
          ></TimePicker>
          <NumberFormat
            name="startPrice"
             customInput={Input}
            label="Lowest Price"
            thousandSeparator={true}
            prefix={"$ "}
            error={errors.startPrice}
            onValueChange={(v) => {
              values.startPrice = v.value;
            }}
          />
        </Grid>
        {/* /* Selection Right */}
        <Grid item md={6} xs={12}>
          <Select
            name="category"
            label="Category"
            value={values.category}
            options={categories}
            onChange={handleInputChange}
          ></Select>

          <TimePicker
            name="closeTime"
            label="Close time"
            value={values.closeTime}
            onChange={handleInputChange}
          ></TimePicker>
     
          <NumberFormat
            customInput={Input}
            name="endPrice"
            label="Highest Price"
            thousandSeparator={true}
            prefix={"$ "}
            error={errors.endPrice}
            onValueChange={(v) => {
              values.endPrice = v.value;
            }}
          />
        </Grid>
        <div className="popup__button">
          <Button type="submit" text="Submit  "></Button>
          <Button text="Cancel" color="default" onClick={closePopup} />
          {isLoading && <CircularProgress />}

          {submitError && (
            <span className="place--form__error">{submitError}</span>
          )}
        </div>
      </Grid>
    </Form>
  );
};

export default PlaceForm;
