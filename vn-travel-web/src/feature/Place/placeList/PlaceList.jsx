import { Skeleton } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import PlaceItem from "../placeItem/PlaceItem";
import "./placeList.css";
import empty_place from "../../../assets/images/empty_place.svg";
import Pagination from "../../../components/pagination/Pagination";
const PlaceList = (props) => {
  const history = useHistory();
  const handleEditPlace = (id) => {
    history.push(`/admin/places/edits/${id}`);
  };

  const loading = useSelector((state) => state.place.isLoading);
  const { places } = props;
  const placePerPage = 12;
  const pageLimit = Math.ceil(places.length / placePerPage);

  return (
    <>
      {loading ? (
        <div className="place--list__container">
          <>
            {Array.from(new Array(8)).map((item, index) => (
              <Box key={index} sx={{ width: 280, marginRight: 0.5, my: 5 }}>
                <Skeleton variant="rectangular" width={280} height={188} />
                <Box sx={{ pt: 0.5 }}>
                  <Skeleton />
                  <Skeleton width="60%" />
                </Box>
              </Box>
            ))}
          </>
        </div>
      ) : places.length <= 0 ? (
        <div className="empty__container">
          <img alt="empty-place" src={empty_place} className="empty" />{" "}
        </div>
      ) : (
    
        <>
          {places.length > 0 ? (
            <>
              <Pagination
                data={places}
                RenderComponent={PlaceItem}
                title="Posts"
                pageLimit={pageLimit}
                dataLimit={placePerPage}
                handleEditPlace={handleEditPlace}
              />
            </>
          ) : (
            <h1>No Posts to display</h1>
          )}
        </>
      
      )}
    </>
  );
};

export default PlaceList;
