import React from "react";
import { Route, Switch } from "react-router-dom";
import PlaceEdit from "./PlaceEdit";
import PlacesView from "./PlacesView";
import Map from "./map/Map";

const Places = (props) => {
  return (
    <Switch>
      <Route  path="/admin/places/edits/:placeId/map" component={Map} />
      <Route  path="/admin/places/edits/:placeId" component={PlaceEdit} />
      <Route path="/" component={PlacesView} />
    </Switch>
  );
};

export default Places;
