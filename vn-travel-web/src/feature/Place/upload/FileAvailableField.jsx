import React from "react";
import "./singleImageReview.css";

const FileAvailableField = (props) => {
  const { imageUrl, onDelete } = props;

  return (
    <>
      <div className="review__container--main">
        <div className="review__container--image">
          <img className="review__image" src={imageUrl} alt="places" />
          <button
            className="review__button--remove"
            onClick={(e) => {
              e.preventDefault();
              onDelete(imageUrl);
            }}
          >
            <i class="bx bx-x"></i>
          </button>
          <div className="review__div--opacity"></div>
        </div>
      </div>
    </>
  );
};

export default FileAvailableField;
