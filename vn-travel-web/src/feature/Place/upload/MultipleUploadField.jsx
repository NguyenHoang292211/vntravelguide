import React, { useCallback, useEffect, useState } from "react";
import { Grid, makeStyles } from "@material-ui/core";
import { useField } from "formik";
import { useDropzone } from "react-dropzone";
import UploadError from "./UploadError";
import SingleFileUploadWithProgress from "./SingleFileUploadField";
import FileAvailableField from "./FileAvailableField";
import { useDispatch } from "react-redux";
import { updateImagesInPlace } from "../../../redux/placeSlice";
import "./multipleUploadField.css";

const randomIdInTime = () => {
  return new Date().toISOString().replace(/:/g, "-");
};
const useStyles = makeStyles((theme) => ({
  dropzone: {
    border: `2px dashed ${theme.palette.primary.main}`,
    borderRadius: theme.shape.borderRadius,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    background: theme.palette.background.default,
    height: theme.spacing(10),
    outline: "none",
  },
  drop__review: {
    justifyContent: "flex-start",
    height: "auto",
    padding: "8px",
  },
}));
const MultipleUploadField = (props) => {
  //id of place in this place
  let { id, name, images } = props;
  const [,, helpers] = useField(name);
  const [files, setFiles] = useState([]);
  const [fileErrors, setFileErrors] = useState([]);
  const classes = useStyles();
  const dispatch = useDispatch();
  const onDrop = useCallback((acceptedFiles, rejectedFiles) => {
    setFiles([]);
    //Do something with file here
    const mappedAcc = acceptedFiles.map((file,index) => ({
      key:{index},
      file: file,
      errors: [],
      id: randomIdInTime,
    }));
    const mappedRej = rejectedFiles.map((rej) => ({
      ...rej,
      id: randomIdInTime,
    }));
    setFiles((curr) => [...curr, ...mappedAcc]);
     setFileErrors([...mappedRej]);
  }, []);

  //Images include image available and upload Image
  const saveImages = () => {
    let filesUrl = [];
    files.forEach((f) => {
      filesUrl.push(f.url);
    });
    let sumImages = [...images, ...filesUrl];
    dispatch(
      updateImagesInPlace({
        id: id,
        images: sumImages,
      })
    );
  };
  useEffect(() => {
    //Helpers.setValue wil primitive change the value and trigger validation (Not to call event change)
    helpers.setValue(files);
    let emptyFiles = [];
    emptyFiles = files.filter((item) => !("url" in item));
    if (files.length > 0 && emptyFiles.length <= 0) {
      saveImages();
      setFiles([]);
      setFileErrors([]);
    }
     helpers.setTouched(true);
  }, [files]);

  const onUpload = (file, url) => {
    setFiles((curr) =>
      curr.map((fw) => {
        if (fw.file === file) {
          return { ...fw, url };
        }
        return fw;
      })
    );
  };

  const onDelete = (fileUrl) => {
    setFiles((curr) => curr.filter((fw) => fw.url !== fileUrl));
    saveImages();
  };

  function onDeleteFileAvailable(fileUrl) {
    images = images.filter((fu) => fu!==fileUrl);
    saveImages();
    //dispatch(updateImagesInPlace(filesUrl ));
  }
  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: ["image/*", "video/*"],
   
  });

  return (
    <React.Fragment>
      <Grid item>
        <div {...getRootProps({ className: classes.dropzone })}>
          <input {...getInputProps()} />
          <p>Drag 'n' drop some files here, or click to select files</p>
        </div>
      </Grid>
      <Grid item>
        <Grid
          container
          className={`${classes.dropzone} ${classes["drop__review"]}`}
        >
          <Grid
            key={randomIdInTime}
            container
            className={`${classes["drop__review--contain"]}`}
          >
            {images.map((imageUrl) => (
              <FileAvailableField
                onDelete={onDeleteFileAvailable}
                imageUrl={imageUrl}
                // file={fileWrapper.file}
              />
            ))}
          </Grid>
          <Grid container>
            {files.map((fileWrapper) => (
              <>
                {fileWrapper.errors.length ? (
                  <UploadError
                    key={`randomIdInTime+${fileWrapper.file.name}`}
                    file={fileWrapper.file}
                    errors={fileWrapper.errors}
                    onDelete={onDelete}
                  />
                ) : (
                  <SingleFileUploadWithProgress
                    onDelete={onDelete}
                    onUpload={onUpload}
                    file={fileWrapper.file}
                  />
                )}
              </>
            ))}
            <Grid container className={`${classes["drop__review--contain"]}`}>
              {fileErrors.map((fileWrapper) => (
                <UploadError
                  key={`randomIdInTime+${fileWrapper.file.name}`}
                  file={fileWrapper.file}
                  errors={fileWrapper.errors}
                  onDelete={onDelete}
                />
              ))}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </React.Fragment>
  );
};

export default MultipleUploadField;
