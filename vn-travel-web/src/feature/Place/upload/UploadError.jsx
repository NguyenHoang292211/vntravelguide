import { createStyles, LinearProgress, withStyles } from "@material-ui/core";
import { Snackbar } from "@mui/material";
import MuiAlert from "@mui/material/Alert";
import React, { useState } from "react";
import "./singleImageReview.css";
const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const ErrorLinearProgress = withStyles((theme) =>
  createStyles({
    bar: {
      backgroundColor: theme.palette.error.main,
    },
  })
)(LinearProgress);

const UploadError = (props) => {
  // const { file, onDelete, onUpload, errors } = props;
  const { file, errors } = props;
  const [message, setMessage] = useState(false);
  const [open, setOpen] = useState(false);
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };
  const openMessage = () => {
    setMessage(`${file.name}: ${errors[0].message}`);
    setOpen(true);
  };
  openMessage();

  return (
    <div className="review__container">
      <Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        open={open}
        autoHideDuration={3000}
        onClose={handleClose}
      >
        <Alert onClose={handleClose} severity="error" sx={{ width: "100%" }}>
          {message}
        </Alert>
      </Snackbar>
      <div className="review__container--main">
        <div className="review__container--image">
          <img
            alt="error"
            className="review__image"
            src={"https://cdn-icons-png.flaticon.com/512/1179/1179186.png"}
          />
        </div>
      </div>

      <ErrorLinearProgress
        className="review__progress"
        variant="determinate"
        value={100}
      />
    </div>
  );
};

export default UploadError;
