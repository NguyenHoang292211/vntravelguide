import { LinearProgress } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import "./singleImageReview.css";
const SingleFileUploadField = (props) => {
  const [progress, setProgress] = useState(0);
  let [url, setUrl] = useState("");
  let { file, onUpload } = props;
  useEffect(() => {
    if (!("url" in file)) {
      async function upload() {
        let tmpUrl = await uploadFile(file, setProgress);

        setUrl(tmpUrl);
        onUpload(file, tmpUrl);
      }
      upload();
    }
  }, []);

  return (
    <div className="review__container">
      <div className="review__container--main">
        <div className="review__container--image">
          <img alt="place_image"
            className="review__image"
            src={
              url || "https://cdn-icons-png.flaticon.com/512/2820/2820324.png"
            }
          />

          <div className="review__div--opacity"></div>
        </div>
      </div>

      <LinearProgress
        className="review__progress"
        variant="determinate"
        value={progress}
      />
    </div>
  );
};

function uploadFile(file, onProgress) {
  //TODO: change url here
  //const url = "https://api.cloudinary.com/v1_1/demo/image/upload";
  const url = "https://api.cloudinary.com/v1_1/vntravel285366/image/upload";
  //const key = "docs_upload_example_us_preset";
  const key = "h9egsmwb";
  return new Promise((res, rej) => {
    const xhr = new XMLHttpRequest();
    xhr.open("POST", url);

    xhr.onload = () => {
      const resp = JSON.parse(xhr.responseText);
      //* Secure url is url get after upload
      return res(resp.secure_url);
    };
    xhr.onerror = (evt) => rej(evt);
    xhr.upload.onprogress = (event) => {
      if (event.lengthComputable) {
        const percentage = (event.loaded / event.total) * 100;
        onProgress(Math.round(percentage));
      }
    };

    const formData = new FormData();
    //formData.append("upload_preset", "h9egsmwb");
    formData.append("file", file);
    formData.append("cloud_name", "vntravel285366");
    formData.append("upload_preset", key);
    xhr.send(formData);
  });
}
export default SingleFileUploadField;
