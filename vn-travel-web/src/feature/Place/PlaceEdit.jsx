import { yupResolver } from "@hookform/resolvers/yup";
import {
  CircularProgress,
  Grid,
  InputAdornment,
  Paper,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import EditLocationIcon from "@mui/icons-material/EditLocation";
import { Snackbar } from "@mui/material";
import MuiAlert from "@mui/material/Alert";
import Skeleton from "@mui/material/Skeleton";
import Stack from "@mui/material/Stack";
import { Form, Formik } from "formik";
import React, { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import NumberFormat from "react-number-format";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router";
import * as yup from "yup";
import Button from "../../components/UI/Button";
import Input from "../../components/UI/Input";
import MultipleValues from "../../components/UI/MultipleValues";
import Rate from "../../components/UI/Rate";
import Select from "../../components/UI/Select";
import SwitchIcon from "../../components/UI/SwitchIcon";
import TimePicker from "../../components/UI/TimePicker";
import {
  setCurrentPlace,
  startLoadDataPlace,
  updatePlace,
} from "../../redux/placeSlice";
import {
  MESSAGE_NORMAL,
  MESSAGE_POPULAR,
  MESSAGE_PRIVATE,
  MESSAGE_PUBLIC,
  STATUS_PRIVATE,
  STATUS_PUBLIC,
} from "../../utils/constants";
import "./placeEdit.css";
import MultipleUploadField from "./upload/MultipleUploadField";

/**
 * Style use for material
 */
const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiFormControl-root": {
      width: "97.75%",
      margin: theme.spacing(2),
    },
  },
  progress: {
    margin: theme.spacing(2),
  },
  skeleton: {
    "margin-bottom": theme.spacing.unit,
  },
  paperEdit: {
    padding: theme.spacing(2),
  },
  gridEdit: {
    padding: theme.spacing(2),
  },
  rate: {
    marginLeft: "2rem",
  },
}));

/**
 * Use Yup validate data
 */
const schema = yup.object().shape({
  name: yup.string().required("This field is not empty"),
  weight: yup.number(),
  description: yup.string().required("This field is not empty"),
  startPrice: yup.number().positive("Price must be greater than 0"),
  endPrice: yup.number().positive("Price must be greater than 0"),
});

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

/**
 * Use React hook form
 * @returns
 */
const PlaceEdit = () => {
  const dispatch = useDispatch();
  startLoadDataPlace();
  const loading = useSelector((state) => state.place.isLoading);
  const places = useSelector((state) => state.place.places);

  const { placeId } = useParams();
  setCurrentPlace(placeId);
  const classes = useStyles();
  const history = useHistory();
  //Get data for select

  let initialFValues = useSelector((state) =>
    state.place.places.find((item) => item.id === placeId)
  );
  let currentPlace = initialFValues;

  const tags = useSelector((state) => state.place.tags);
  const provinces = useSelector((state) => state.place.provinces);
  const categories = useSelector((state) => state.place.categories);
  const [open, setOpen] = React.useState(false);
  const [openError, setOpenError] = useState(false);
  const [message, setMessage] = useState("s");
  const skeleton = 8;
  const skeletonObject = [];
  for (let i = 0; i < skeleton; i++) {
    skeletonObject.push(<Skeleton variant="rectangular" height={40} />);
  }
  //Use React hook form
  const {
    handleSubmit,
    formState: { errors },
    control,
    setValue,
  } = useForm({
    resolver: yupResolver(schema),
  });

  //TODO: change to receive urlPath
  let initialFiles = [];
  const onUpdatePlaceSubmit = async (data) => {
    data.id = placeId;
    let mapStatus = String(data.status);
    if (mapStatus.includes("false") || mapStatus.includes(STATUS_PRIVATE)) {
      data.status = STATUS_PRIVATE;
    } else {
      data.status = STATUS_PUBLIC;
    }
    let newPlace = {
      ...currentPlace,
      ...data,
    };
    const response = await dispatch(updatePlace(newPlace));
    if (response.payload.success) {
      setMessage("Update successfully");
      setOpen(true);
    } else {
      setMessage("Cannot update now. Try again");
      setOpenError(true);
    }
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const handleCloseError = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenError(false);
  };

  const onBack = () => {
    history.goBack();
  };

  const goSelectPlace = () => {
    history.push(`/admin/places/edits/${placeId}/map`);
  };

  return (
    <>
      {typeof initialFValues === "undefined" ? (
        <div className="notfound__div">
          <Typography padding={5} gutterBottom align="center" variant="h5">
            Place does not exist!
          </Typography>{" "}
        </div>
      ) : typeof tags === "undefined" ||
        typeof categories === "undefined" ||
        typeof provinces === "undefined" ? (
        <Paper>
          <Grid container>
            <Grid item xs={12} className={classes.skeleton}>
              <Stack spacing={4}>
                <Skeleton variant="rectangular" height={40} />
              </Stack>
            </Grid>
            <Grid item xs={6}>
              <Stack spacing={4}>{skeletonObject}</Stack>
            </Grid>
            <Grid item xs={6}></Grid>
          </Grid>
        </Paper>
      ) : (
        <>
          <div className="edit__back">
            {/*  /* Snack success */}
            <Snackbar
              anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
              open={open}
              autoHideDuration={3000}
              onClose={handleClose}
            >
              <Alert
                onClose={handleClose}
                severity="success"
                sx={{ width: "100%" }}
              >
                {message}
              </Alert>
            </Snackbar>
            {/* /* Snack error */}
            <Snackbar
              anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
              open={openError}
              autoHideDuration={3000}
              onClose={handleCloseError}
            >
              <Alert
                onClose={handleCloseError}
                severity="error"
                sx={{ width: "100%" }}
              >
                {message}
              </Alert>
            </Snackbar>
            <div className="edit__back--icon" onClick={onBack}>
              <i class="bx bx-arrow-back"></i>
            </div>
          </div>
          <Paper>
            <div className="edit__main">
              <form
                className={classes.root}
                onSubmit={handleSubmit(onUpdatePlaceSubmit)}
              >
                <Grid container className={classes.gridEdit}>
                  <Grid item xs={6}>
                    {/* /* Name */}
                    <Controller
                      name="name"
                      control={control}
                      defaultValue={initialFValues ? initialFValues.name : ""}
                      render={({
                        field: { onChange, value },
                        fieldState: { error },
                      }) => (
                        <Input
                          name="name"
                          label="Name"
                          value={value}
                          onChange={onChange}
                          error={errors.name?.message}
                          rows={1}
                        ></Input>
                      )}
                    ></Controller>

                    {/* /* Location */}
                    {/* <Controller 
                      name="location"
                      label="Location"
                      control={control}
                      defaultValue={
                        initialFValues.location ? initialFValues.location : ""
                      }
                      render={({
                        field: { onChange, value },
                        fieldState: { error },
                      }) => ( */}
                    <Input
                      name="location"
                      label="Location"
                      value={`${initialFValues.longtitude} - ${initialFValues.lattitude}`}
                      // value={value}
                      // onChange={onChange}
                      InputProps={{
                        readOnly: true,
                        endAdornment: (
                          <InputAdornment
                            position="start"
                            onClick={goSelectPlace}
                            className="edit__icon"
                          >
                            <EditLocationIcon />
                          </InputAdornment>
                        ),
                      }}
                    ></Input>
                    {/* )}
                    ></Controller> */}
                    {/* /* Address */}
                    {/* <Controller
                      name="address"
                      label="Address"
                      control={control}
                      defaultValue={
                        initialFValues.address ? initialFValues.address : ""
                      }
                      render={({
                        field: { onChange, value },
                        fieldState: { error },
                      }) => ( */}
                    <Input
                      name="address"
                      label="Address"
                      value={initialFValues.address}
                      InputProps={{
                        readOnly: true,
                        endAdornment: (
                          <InputAdornment
                            position="start"
                            className="edit__icon"
                            onClick={goSelectPlace}
                          >
                            <EditLocationIcon />
                          </InputAdornment>
                        ),
                      }}
                    ></Input>
                    {/* )}
                    ></Controller> */}

                    <Grid container className={classes["second-root"]}>
                      {/* /* Rating */}
                      <Grid item xs={6}>
                        <Controller
                          name="weight"
                          control={control}
                          defaultValue={
                            initialFValues ? initialFValues.weight : 0
                          }
                          render={({
                            field: { onChange, value },
                            fieldState: { error },
                          }) => (
                            <Input
                              name="weight"
                              label="weight"
                              value={value}
                              onChange={onChange}
                              error={errors.weight?.message}
                            ></Input>
                          )}
                        ></Controller>
                      </Grid>
                      <Grid item xs={6}>
                        <Controller
                          name="rate"
                          control={control}
                          defaultValue={
                            initialFValues ? initialFValues.rate : 0
                          }
                          render={({
                            field: { onChange, value },
                            fieldState: { error },
                          }) => (
                            <Rate
                              label="Rating"
                              name="rate"
                              value={value}
                              onChange={onChange}
                              md-12
                              style={{ width: "1rem", marginLeft: "1.25rem" }}
                            />
                          )}
                        ></Controller>
                      </Grid>
                    </Grid>
                    {/*  /* Tag name */}
                    <Controller
                      name="tags"
                      control={control}
                      defaultValue={initialFValues.tags}
                      render={({
                        field: { onChange, value },
                        fieldState: { error },
                      }) => (
                        <MultipleValues
                          name="tags"
                          label="Tag name"
                          optionsList={tags}
                          defaultValue={initialFValues.tags}
                          value={value}
                          onChange={onChange}
                          id={initialFValues.id}
                        />
                      )}
                    ></Controller>

                    {/*  /* Province */}

                    <Controller
                      name="province"
                      control={control}
                      defaultValue={
                        initialFValues ? initialFValues.province._id : ""
                      }
                      render={({
                        field: { onChange, value },
                        fieldState: { error },
                      }) => (
                        <Select
                          name="province"
                          label="Province"
                          defaultValue={
                            initialFValues ? initialFValues.province._id : ""
                          }
                          value={value}
                          options={provinces}
                          onChange={onChange}
                        ></Select>
                      )}
                    ></Controller>
                    {/*  /* Category */}
                    <Controller
                      name="category"
                      control={control}
                      defaultValue={
                        initialFValues ? initialFValues.category._id : ""
                      }
                      render={({
                        field: { onChange, value },
                        fieldState: { error },
                      }) => (
                        <Select
                          name="category"
                          label="Category"
                          value={value}
                          options={categories}
                          onChange={onChange}
                        ></Select>
                      )}
                    ></Controller>

                    <Controller
                      name="description"
                      control={control}
                      defaultValue={
                        initialFValues ? initialFValues.description : ""
                      }
                      render={({
                        field: { onChange, value },
                        fieldState: { error },
                      }) => (
                        <Input
                          name="description"
                          label="Description"
                          value={value}
                          onChange={onChange}
                          rows={4}
                          error={errors.description?.message}
                        ></Input>
                      )}
                    ></Controller>

                    <Grid container>
                      {/* /* Open - Close time */}
                      <Grid item xs={6}>
                        <Controller
                          name="openTime"
                          control={control}
                          defaultValue={
                            initialFValues ? initialFValues.openTime : "07:30"
                          }
                          render={({
                            field: { onChange, value },
                            fieldState: { error },
                          }) => (
                            <TimePicker
                              name="openTime"
                              label="Open time"
                              value={value}
                              onChange={onChange}
                            ></TimePicker>
                          )}
                        ></Controller>
                        <Controller
                          name="startPrice"
                          control={control}
                          defaultValue={
                            initialFValues ? initialFValues.price.start : "0"
                          }
                          render={({
                            field: { onChange, value },
                            fieldState: { error },
                          }) => (
                            <NumberFormat
                              defaultValue={
                                initialFValues
                                  ? initialFValues.price.start
                                  : "0"
                              }
                              customInput={Input}
                              label="Lowest Price"
                              thousandSeparator={true}
                              suffix={"   vnd"}
                              error={errors.startPrice?.message}
                              onValueChange={(v) => {
                                //value without dollar signe
                                setValue("startPrice", v.value);
                              }}
                            />
                          )}
                        ></Controller>
                      </Grid>
                      {/* /* Selection Right */}
                      <Grid item xs={6}>
                        <Controller
                          name="closeTime"
                          control={control}
                          defaultValue={
                            initialFValues ? initialFValues.closeTime : "10:30"
                          }
                          render={({
                            field: { onChange, value },
                            fieldState: { error },
                          }) => (
                            <TimePicker
                              name="closeTime"
                              label="Close time"
                              value={value}
                              onChange={onChange}
                            ></TimePicker>
                          )}
                        ></Controller>
                        <Controller
                          name="endPrice"
                          control={control}
                          defaultValue={
                            initialFValues ? initialFValues.price.end : ""
                          }
                          render={({
                            field: { onChange, value },
                            fieldState: { error },
                          }) => (
                            <NumberFormat
                              defaultValue={
                                initialFValues ? initialFValues.price.end : ""
                              }
                              customInput={Input}
                              label="Highest Price"
                              thousandSeparator={true}
                              suffix={"   vnd"}
                              error={errors.endPrice?.message}
                              onValueChange={(v) => {
                                setValue("endPrice", v.value);
                              }}
                            />
                          )}
                        ></Controller>
                      </Grid>
                      <Grid item xs={12}>
                        <Controller
                          name="status"
                          control={control}
                          defaultValue={
                            initialFValues
                              ? initialFValues.status === "public"
                                ? true
                                : false
                              : false
                          }
                          render={({
                            field: { onChange, value },
                            fieldState: { error },
                          }) => (
                            <SwitchIcon
                              name="status"
                              defaultValue={
                                initialFValues.status === "public"
                                  ? true
                                  : false
                              }
                              value={value}
                              label={
                                value === "public" || value == true
                                  ? MESSAGE_PUBLIC
                                  : MESSAGE_PRIVATE
                              }
                              onChange={onChange}
                            />
                          )}
                        ></Controller>
                      </Grid>
                      {/* /* set popular  */}

                      <Grid item xs={12}>
                        <Controller
                          name="popular"
                          control={control}
                          defaultValue={initialFValues?.popular}
                          render={({
                            field: { onChange, value },
                            fieldState: { error },
                          }) => (
                            <SwitchIcon
                              name="popular"
                              defaultValue={initialFValues.popular}
                              value={value}
                              label={value ? MESSAGE_POPULAR : MESSAGE_NORMAL}
                              onChange={onChange}
                            />
                          )}
                        ></Controller>
                      </Grid>

                      <Grid item xs={12}>
                        <Button text="Save" type="submit"></Button>
                        {loading && <CircularProgress />}
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid item xs={6}>
                    {/* /* Upload image */}
                    <div className="place--edit__container--image">
                      <Formik
                        initialValues={initialFiles}
                        onSubmit={(values) => {}}
                      >
                        {(formikProps) => {
                          //Do something here

                          return (
                            <Form>
                              <Grid container spacing={2} direction="column">
                                <MultipleUploadField
                                  id={initialFValues.id}
                                  name="files"
                                  images={
                                    initialFValues ? initialFValues.images : []
                                  }
                                />
                              </Grid>
                            </Form>
                          );
                        }}
                      </Formik>
                    </div>
                  </Grid>
                </Grid>
              </form>
            </div>
          </Paper>
        </>
      )}
    </>
  );
};

export default PlaceEdit;
