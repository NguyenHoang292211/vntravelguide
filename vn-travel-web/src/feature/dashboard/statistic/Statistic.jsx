import { Card, CardHeader } from "@mui/material";
import React from "react";
import statusCards from '../../../assets/JsonData/status-card-data.json';
import { DAILY_VISIT, TOTAL_PLACES, TOTAL_QUERIES, TOTAL_USERS } from "../../../utils/constants";
import './statistic.css';
import StatusCard from "./StatusCard";

const Statistic = (props) => {
  let countStatistic=[0,0,0,0];
  const {totalUser,totalPlace, totalCountView}=props;
  countStatistic[TOTAL_USERS]=totalUser;
  countStatistic[TOTAL_PLACES]=totalPlace;
  countStatistic[TOTAL_QUERIES]=totalCountView;
  countStatistic[DAILY_VISIT] =totalCountView
  return (
    <Card sx={{ margin: 1 }}>
      <CardHeader
        title="Summary"
        sx={{ fontSize: 10 }}
        subheader="Summary of the month"
      />
      <Card className='card__statistic'>
      {statusCards.map((item, index) => (
        <div className="col-6" key={index}>
          <StatusCard icon={item.icon} count={countStatistic[index]} title={item.title} color={item.color} />
        </div>
      ))}
      </Card>
    </Card>
  );
};

export default Statistic;
