import CheckCircleRoundedIcon from "@mui/icons-material/CheckCircleRounded";
import DoneIcon from "@mui/icons-material/Done";
import EditIcon from "@mui/icons-material/Edit";
import MoreVertOutlinedIcon from "@mui/icons-material/MoreVertOutlined";
import PersonIcon from "@mui/icons-material/Person";
import RemoveCircleIcon from "@mui/icons-material/RemoveCircle";
import {
  Avatar,
  IconButton,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from "@mui/material";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { alpha, styled } from "@mui/material/styles";
import React from "react";
import Moment from "react-moment";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";

const StyledMenu = styled((props) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "right",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "right",
    }}
    {...props}
  />
))(({ theme }) => ({
  "& .MuiPaper-root": {
    borderRadius: 5,
    borderWidth: 0,
    marginTop: theme.spacing(1),
    minWidth: 180,
    color:
      theme.palette.mode === "light"
        ? "rgb(55, 65, 81)"
        : theme.palette.grey[300],
    // boxShadow:
    //   "rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px",
    boxShadow: "1px 1px 5px 0px rgba(0,0,0,0.2)",
    "& .MuiMenu-list": {
      padding: "4px 0",
    },
    "& .MuiMenuItem-root": {
      "& .MuiSvgIcon-root": {
        fontSize: 18,
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(1.5),
      },
      "&:active": {
        backgroundColor: alpha(
          theme.palette.primary.main,
          theme.palette.action.selectedOpacity
        ),
      },
    },
  },
}));

const ContributeItem = (props) => {
  const { contribute, handleUpdateContribute } = props;
  const history = useHistory();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const isUpdating = useSelector((state) => state.report.isUpdating);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  //Handle update status of contribute
  const updateHiddenContribute = (e) => {
    e.preventDefault();
    let contribute_ = {
      id: contribute.id,
      isHidden: !contribute.isHidden,
      isSeen: contribute.isSeen,
    };
    handleUpdateContribute(contribute_);
  };

  const updateSeenContribute = (e) => {
    e.preventDefault();
    let contribute_ = {
      id: contribute.id,
      isHidden: contribute.isHidden,
      isSeen: !contribute.isSeen,
    };

    handleUpdateContribute(contribute_);
  };

  const handleEditPlaceNavigation = () => {
    handleClose();

    history.push(
      `places/edits/${
        props.contribute.place.id
          ? props.contribute.place.id
          : props.contribute.place._id
      }`
    );
  };
  const handleViewContributor = () => {
    handleClose();
    const userId = props.contribute.user._id;
    history.push(`users/${userId}/info`);
  };

  return (
    <React.Fragment>
      <ListItem
        disableRipple
        button
        secondaryAction={
          <Typography component="div">
            <IconButton
              edge="end"
              aria-label="More"
              id="demo-customized-button"
              aria-controls="demo-customized-menu"
              aria-haspopup="true"
              aria-expanded={open ? "true" : undefined}
              variant="contained"
              onClick={handleClick}
            >
              <MoreVertOutlinedIcon />
            </IconButton>
            <StyledMenu
              id="demo-customized-menu"
              MenuListProps={{
                "aria-labelledby": "demo-customized-button",
              }}
              anchorEl={anchorEl}
              open={open}
              onClose={handleClose}
            >
              {contribute.isSeen ? (
                <MenuItem onClick={updateSeenContribute}>
                  <DoneIcon />
                  Mark as unread
                </MenuItem>
              ) : (
                <MenuItem onClick={updateSeenContribute}>
                  <CheckCircleRoundedIcon />
                  Mark as read
                </MenuItem>
              )}

              <MenuItem onClick={updateHiddenContribute}>
                <RemoveCircleIcon />
                Hide this contribute
              </MenuItem>
              <MenuItem onClick={handleEditPlaceNavigation}>
                <EditIcon />
                Go to this place
              </MenuItem>
              <MenuItem onClick={handleViewContributor}>
                <PersonIcon />
                Contributor Info
              </MenuItem>
            </StyledMenu>
          </Typography>
        }
        sx={{ display: "flex" }}
        alignItems="flex-start"
      >
        <ListItemAvatar>
          <Avatar src={contribute.user.image} alt="P" size="medium" />
        </ListItemAvatar>

        <ListItemText
          primary={
            <React.Fragment>
              <Typography
                sx={{
                  display: "flex",
                  fontWeight: 400,
                  color: "#6A5C5C",
                  fontSize: 12,
                  fontStyle: "italic",
                  justifyContent: "space-between",
                }}
                variant="subtitle2"
              >
                <span
                  style={{
                    fontSize: 13,
                    color: "#444",
                  }}
                >
                  {contribute.user.fullName}
                </span>
                <Moment format="YYYY-MM-DD HH:mm">
                  {contribute.createdAt}
                </Moment>
              </Typography>
              <Typography
                sx={{
                  display: "block",
                  fontWeight: 500,
                  paddingTop: 1,
                  paddingBottom: 1,
                  color: "#433838",
                  fontSize: 16,
                }}
                variant="body2"
              >
                {contribute.place.name}
              </Typography>
            </React.Fragment>
          }
          secondary={contribute.content}
        />
      </ListItem>
    </React.Fragment>
  );
};

export default ContributeItem;
