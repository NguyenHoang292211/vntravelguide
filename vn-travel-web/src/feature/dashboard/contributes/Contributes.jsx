import {
  Avatar,
  Card,
  List,
  ListItem,
  ListSubheader,
  Skeleton,
  Typography,
} from "@mui/material";
import React from "react";
import { useSelector } from "react-redux";
import ContributeItem from "./ContributeItem";
import "./styles.css";

const Contributes = (props) => {
  const { contributes } = props;
  const unSeenContributes = contributes
    .filter((contribute) => contribute.isSeen === false)
    .sort((a, b) => (b.createdAt - a.createdAt ? -1 : 1));
  const seenContributes = contributes
    .filter((contribute) => contribute.isSeen === true)
    .sort((a, b) => (b.createdAt - a.createdAt ? -1 : 1));

  const isLoading = useSelector((state) => state.contribute.loading);

  const skelotonRows = [0, 1, 2];
  //render Skeleton
  const renderSkeleton = (field) => {
    return (
      <div className="skeleton-div">
        <Skeleton
          className="skeleton-circle"
          variant="circular"
          height={40}
          animation="wave"
          sx={{ bgcolor: "rgba(202, 190, 190, 0.616)" }}
        >
          <Avatar />
        </Skeleton>
        <Skeleton
          className="skeleton-res"
          variant="rectangular"
          height={40}
          animation="wave"
          sx={{ bgcolor: "rgba(202, 190, 190, 0.616)" }}
        ></Skeleton>
      </div>
    );
  };

  return (
    <Card className="CommentReport-card">
      <div>
        <Typography
          variant="h6"
          gutterBottom
          component="div"
          sx={{ p: 1, pb: 0, fontSize: 18, fontWeight: 500 }}
        >
          Contributes
        </Typography>
        <List sx={{ mb: 2 }}>
          {contributes ? (
            <>
              <ListSubheader sx={{ bgcolor: "#EEE" }}>Newest</ListSubheader>
              {isLoading ? (
                skelotonRows.map(() => {
                  return (
                    <React.Fragment>
                      <ListItem>{renderSkeleton()}</ListItem>
                    </React.Fragment>
                  );
                })
              ) : unSeenContributes.length > 0 ? (
                unSeenContributes.map((contribute, index) => (
                  <ContributeItem
                  key={index}
                    contribute={contribute}
                    handleUpdateContribute={props.handleUpdateContribute}
                  />
                ))
              ) : (
                <Typography
                  variant="subtitle2"
                  textAlign="center"
                  fontSize={15}
                  padding={3}
                >
                  No contribute here!
                </Typography>
              )}
              <ListSubheader sx={{ bgcolor: "#EEE" }}>Seen</ListSubheader>
              {isLoading ? (
                skelotonRows.map(() => {
                  return (
                    <React.Fragment>
                      <ListItem>{renderSkeleton()}</ListItem>
                    </React.Fragment>
                  );
                })
              ) : seenContributes.length > 0 ? (
                seenContributes.map((contribute, index) => (
                  <ContributeItem
                    contribute={contribute}
                    handleUpdateContribute={props.handleUpdateContribute}
                  />
                ))
              ) : (
                <Typography
                  variant="subtitle2"
                  textAlign="center"
                  fontSize={15}
                  padding={3}
                >
                  No contribute here!
                </Typography>
              )}
            </>
          ) : (
            <>
              <Typography variant="body1" textAlign="center">
                Contribute not found
              </Typography>
            </>
          )}
        </List>
      </div>
    </Card>
  );
};

export default Contributes;
