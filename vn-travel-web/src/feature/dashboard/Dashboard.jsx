import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Grid } from "@material-ui/core";
import Statistic from "./statistic/Statistic";
import TopPlaces from "./top-places/TopPlaces";
import NewUsers from "./new-users/NewUsers";
import CommentReport from "./comment-reports/CommentReport";
import Contributes from "./contributes/Contributes";
import {
  loadReports,
  updateReport,
  updateReviewStatus,
} from "../../redux/reportSlice";
import { loadContributes, updateContribute } from "../../redux/contributeSlice";
import { Snackbar } from "@mui/material";
import MuiAlert from "@mui/material/Alert";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});
const Doashboard = () => {
  const places = useSelector((state) => state.place.places);
  const dispatch = useDispatch();
  const users = useSelector((state) => state.user.users);
  const TOP_PLACE = 7;

  let sortPlaces = places.slice().sort((a, b) => b.rateVoting - a.rateVoting);
  sortPlaces = sortPlaces.slice(0, TOP_PLACE);

  //Report
  const reports = useSelector((state) => state.report.reports);
  const reportError = useSelector((state) => state.report.error);
  const isUpdatingReport = useSelector((state) => state.report.isUpdating);

  //Contribute
  const contributes = useSelector((state) => state.contribute.contributes);
  const contributeError = useSelector((state) => state.contribute.error);
  const isUpdatingContribute = useSelector(
    (state) => state.contribute.isUpdating
  );
  //Snack bar
  const [openSnackSuccess, setOpenSnackSuccess] = useState(false);
  const [openSnackError, setOpenSnackError] = useState(false);
  let SUCCESS_MESSAGE = "Update report status successfully!";
  let ERROR_MESSAGE = "Error happen, try again!";

  const handleCloseSnackError = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenSnackError(false);
  };

  const handleCloseSnackSuccess = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenSnackSuccess(false);
  };

  /**
   * Handle report
   */
  const handleUpdateReport = async (report) => {
    try {
      await dispatch(updateReport(report));
      if (reportError === "" && isUpdatingReport === false) {
        setOpenSnackSuccess(true);
      } else if (reportError !== "") {
        setOpenSnackError(true);
        ERROR_MESSAGE = reportError;
      }
    } catch (error) {
      setOpenSnackError(true);
      ERROR_MESSAGE = "Some error happen.Please try again!";
    }
  };

  const updateReview = async (params) => {
    try {
      await dispatch(updateReviewStatus(params));
      if (reportError === "" && isUpdatingReport === false) {
        setOpenSnackSuccess(true);
      } else if (reportError !== "") {
        setOpenSnackError(true);
        ERROR_MESSAGE = reportError;
      }
    } catch (error) {
      console.log(error);
      setOpenSnackError(true);
      ERROR_MESSAGE = "Some error happen.Please try again!";
    }
  };

  /**
   * Handle contribute
   */
  const handleUpdateContribute = async (params) => {
    try {
      await dispatch(updateContribute(params));
      if (contributeError === "" && isUpdatingContribute === false) {
        setOpenSnackSuccess(true);
      } else if (contributeError !== "") {
        setOpenSnackError(true);
        ERROR_MESSAGE = contributeError;
      }
    } catch (error) {
      console.log(error);
      setOpenSnackError(true);
      ERROR_MESSAGE = "Some error happen.Please try again!";
    }
  };

  useEffect(() => {
    dispatch(loadReports());
    dispatch(loadContributes());
  }, []);
  const countView=()=>{
    let count=0;
    places.forEach(place => {
      count+=place.viewCount;
    });
    return count;
  }
  return (
    <div>
      {/*  /* Snack success */}
      <Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        open={openSnackSuccess}
        autoHideDuration={3000}
        onClose={handleCloseSnackSuccess}
      >
        <Alert
          onClose={handleCloseSnackSuccess}
          sx={{ width: "100%" }}
          severity="success"
        >
          {SUCCESS_MESSAGE}
        </Alert>
      </Snackbar>
      {/* /* Snack error */}
      <Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        open={openSnackError}
        autoHideDuration={3000}
        onClose={handleCloseSnackError}
      >
        <Alert
          onClose={handleCloseSnackError}
          sx={{ width: "100%" }}
          severity="error"
        >
          {ERROR_MESSAGE}
        </Alert>
      </Snackbar>
      <Grid container>
        <Grid item xs={12} sm={6} md={8}>
          <Grid item xs={12}>
            <Statistic
              totalUser={users.length}
              totalPlace={places.length}
              totalCountView={countView()}
            />
          </Grid>
          <Grid item xs={12}>
            <NewUsers places={places} users={users} />
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <TopPlaces places={sortPlaces} />
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={12} md={6}>
          <CommentReport
            reports={reports}
            handleUpdateReport={handleUpdateReport}
            updateReview={updateReview}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <Contributes
            contributes={contributes}
            handleUpdateContribute={handleUpdateContribute}
          />
        </Grid>
      </Grid>
    </div>
  );
};

export default Doashboard;
