import { Card, CardHeader, CardContent, Paper } from "@mui/material";
import React from "react";
import Chart from "react-apexcharts";
import { CREATED_AT_DEFAULT } from "../../../utils/constants";

const NewUsers = (props) => {
  const { users, places } = props;
  //Sort by date create
  let sortPlacesByDate = places
    .slice()
    .sort((a, b) => Date.parse(a.createdAt) - Date.parse(b.createdAt));
  let sortUsersByDate = users
    .slice()
    .sort((a, b) => Date.parse(a.createdAt) - Date.parse(b.createdAt));

  //Start period in Place and User
  const startPlace =
    places.length > 0
      ? places[0]
      : new Object({ createdAt: CREATED_AT_DEFAULT });
  const startUser =
    users.length > 0 ? users[0] : new Object({ createdAt: CREATED_AT_DEFAULT });
  //End period in Place and User
  const endPlace =
    places.length > 0
      ? places[places.length - 1]
      : new Object({ createdAt: CREATED_AT_DEFAULT });
  const endUser =
    users.length > 0
      ? users[users.length - 1]
      : new Object({ createdAt: CREATED_AT_DEFAULT });

  let arrayPeriod = [startPlace, startUser, endPlace, endUser];
  let sortPeriodByDate = arrayPeriod
    .slice()
    .sort((a, b) => Date.parse(a.createdAt) - Date.parse(b.createdAt));

  let groupPlaces = Object.values(
    mapObjectToChart(sortPlacesByDate, arrayPeriod[0], arrayPeriod[3])
  );
  let groupUsers = Object.values(
    mapObjectToChart(sortUsersByDate, arrayPeriod[0], arrayPeriod[3])
  );
  //Sort by date create
  groupPlaces = standardizedToTwelve(sortPeriodByDate[0], groupPlaces);
  groupUsers = standardizedToTwelve(sortPeriodByDate[0], groupUsers);
  const dataPlaces = {
    series: [
      {
        name: "Place/Month",
        data: groupPlaces?.map((item) => item.count),
      },
      {
        name: "User/Month",
        data: groupUsers?.map((item) => item.count),
      },
    ],
    options: {
      color: ["#6ab04c", "#2980b9"],
      chart: {
        background: "transparent",
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: "smooth",
      },
      xaxis: { categories: groupPlaces?.map((item) => item.key) },
    },
    legend: {
      position: "top",
    },
    grid: {
      show: false,
    },
  };

  return (
    <Card sx={{ margin: 1, minHeight: 250 }}>
      <CardHeader
        title="Recent User, Places"
        subheader="Ratio new user and place per month"
      />
      <CardContent>
        <Paper>
          <Chart
            series={dataPlaces.series}
            options={dataPlaces.options}
            height="179px"
          />
        </Paper>
      </CardContent>
    </Card>
  );
};
const mapObjectToChart = (data, startPeriod, endPeriod) => {
  let dictionary = {};
  //Get month and year;
  const startMonth = startPeriod.createdAt.split("-")[1];
  const startYear = startPeriod.createdAt.split("-")[0];

  const endMonth = endPeriod.createdAt.split("-")[1];
  const endYear = startPeriod.createdAt.split("-")[0];
  for (let i = startYear; i <= endYear; i++) {
    let endJ = i === endYear ? endMonth : 12;
    let key = "";
    for (let j = 1; j <= endJ; j++) {
      key = `${j}/${i}`;
      dictionary[key] = new Object({
        count: 0,
        key: key,
      });
    }
  }

  const groupBy = (item) => {
    let dateCreated = item.createdAt;
    dateCreated = String(dateCreated);

    let key =
      parseInt(dateCreated.split("-")[1]) +
      "/" +
      parseInt(dateCreated.split("-")[0]);

    if (dictionary[key] === undefined) {
      dictionary[key] = new Object({
        count: 0,
        key: key,
      });
    }
    dictionary[key].count++;
  };
  data.forEach((item) => {
    groupBy(item);
  });

  return dictionary;
};

const standardizedToTwelve = (startItem, groupArray) => {
  if (groupArray.length < 12) {
    //Fill 12 month ago
    const startMonth = parseInt(startItem.createdAt.split("-")[1]);
    const startYear = parseInt(startItem.createdAt.split("-")[0]);
    for (let i = startMonth - 1; i > 0; i--) {
      let newObject = new Object({ key: `${i}/${startYear}`, count: 0 });
      groupArray = [newObject, ...groupArray];
    }
    if (!groupArray) return [];
    return groupArray;
  } else {
    return groupArray;
  }
};
export default NewUsers;
