import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    title: {
        transition: "all .5s ease",
        display: (props) => (props.open ? "flex" : "none"),
    },
    nav: {
        transition: "all 1.5s ease",
        [theme.breakpoints.down("sm")]: {
            width: (props) => (props.open ? "auto" : "0px"),
        },
    },
    spanTransition: {
        transition: "all 1s ease",
        width: (props) => (props.open ? "112px" : "0px"),
        [theme.breakpoints.down("sm")]: {
            width: "112",
        },
    },
}));
