import React from "react";
import {
  Chip,
  Avatar,
  Card,
  CardHeader,
  CardContent,
  Rating,
} from "@mui/material";
import Divider from "@mui/material/Divider";
import "./style.css";

const TopPlaces = (props) => {
  return (
    <Card sx={{ margin: 1 }}>
      <CardHeader
        aria-setsize={12}
        title="Places"
        subheader="Top places of month"
      />
      <CardContent>
        {props.places.map((place, index) => (
          <PlaceItem place={place} index={index + 1} key={index} />
        ))}
      </CardContent>
    </Card>
  );
};

const PlaceItem = (props) => {
  const { place, index } = props;
  return (
    <div>
      <div className="TopPlaceRow">
        <div className="TopPlaceCol--4">
          <div className="TopPlaceAvatar-div">
            <span className="TopPlaceAvatar-span">{index}</span>
            <Avatar
              alt="place_img"
              src={place.images[0]}
              sx={{ width: 65, height: 65 }}
              variant="square"
            />
          </div>
        </div>
        <div className="TopPlaceCol--8">
          <div>
            <p className="TopPlaceName-p">{place.name}</p>
            <div className="TopPlaceRating-div">
              <Rating
                name="read-only"
                size="small"
                value={Math.round(place.rateVoting)}
                readOnly
              />
              <p className="TopPlaceRating-span">
                ( {place.rateVoting?.toFixed(2) ?? 0} )
              </p>
            </div>
            <div className="TopPlaceReview-div">
              <p className="TopPlaceReview-p"> {place.reviewCount} reviews</p>
              <Chip
                label={place.province?.name}
                variant="outlined"
                size="small"
                sx={{
                  fontSize: 12,
                  color: place.province ? place.province?.color : "#555",
                }}
              ></Chip>
            </div>
          </div>
        </div>
      </div>
      <Divider variant="middle" />
    </div>
  );
};
export default TopPlaces;
