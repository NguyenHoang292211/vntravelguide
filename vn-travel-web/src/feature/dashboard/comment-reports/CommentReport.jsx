import CheckCircleOutlinedIcon from "@mui/icons-material/CheckCircleOutlined";
import CheckCircleRoundedIcon from "@mui/icons-material/CheckCircleRounded";
import DoneIcon from "@mui/icons-material/Done";
import EditIcon from "@mui/icons-material/Edit";
import MoreVertOutlinedIcon from "@mui/icons-material/MoreVertOutlined";
import PersonIcon from "@mui/icons-material/Person";
import RemoveCircleIcon from "@mui/icons-material/RemoveCircle";
import RemoveRedEyeOutlinedIcon from "@mui/icons-material/RemoveRedEyeOutlined";
import VisibilityOffOutlinedIcon from "@mui/icons-material/VisibilityOffOutlined";
import {
  Avatar,
  Card,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  ListSubheader,
  Skeleton, Tooltip,
  Typography
} from "@mui/material";
import Divider from "@mui/material/Divider";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { alpha, styled } from "@mui/material/styles";
import React from "react";
import Moment from "react-moment";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import "./styles.css";

const StyledMenu = styled((props) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "right",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "right",
    }}
    {...props}
  />
))(({ theme }) => ({
  "& .MuiPaper-root": {
    borderRadius: 5,
    borderWidth: 0,
    marginTop: theme.spacing(1),
    minWidth: 180,
    color:
      theme.palette.mode === "light"
        ? "rgb(55, 65, 81)"
        : theme.palette.grey[300],
    boxShadow: "1px 1px 5px 0px rgba(0,0,0,0.2)",
    "& .MuiMenu-list": {
      padding: "4px 0",
    },
    "& .MuiMenuItem-root": {
      "& .MuiSvgIcon-root": {
        fontSize: 18,
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(1.5),
      },
      "&:active": {
        backgroundColor: alpha(
          theme.palette.primary.main,
          theme.palette.action.selectedOpacity
        ),
      },
    },
  },
}));

const CommentReport = (props) => {
  const { reports, handleUpdate } = props;
  const isLoading = useSelector((state) => state.report.loading);

  const reports_ = reports;
  const seenReports = reports
    .filter((report) => report.isSeen === true)
    .sort((a, b) => (b.createdAt - a.createdAt ? -1 : 1));
  const unSeenReports = reports_
    .filter((report) => report.isSeen === false)
    .sort((a, b) => (b.createdAt - a.createdAt ? -1 : 1));

  const skelotonRows = [0, 1, 2];

  //render Skeleton
  const renderSkeleton = (field) => {
    return (
      <div className="skeleton-div">
        <Skeleton
          className="skeleton-circle"
          variant="circular"
          height={40}
          animation="wave"
          sx={{ bgcolor: "rgba(202, 190, 190, 0.616)" }}
        >
          <Avatar />
        </Skeleton>
        <Skeleton
          className="skeleton-res"
          variant="rectangular"
          height={40}
          animation="wave"
          sx={{ bgcolor: "rgba(202, 190, 190, 0.616)" }}
        ></Skeleton>
      </div>
    );
  };

  return (
    <>
      <Card className="CommentReport-card">
        <div>
          <Typography
            variant="h6"
            gutterBottom
            component="div"
            sx={{ p: 1, pb: 0, fontSize: 18, fontWeight: 500 }}
          >
            Reported reviews
          </Typography>
          <List sx={{ mb: 2 }}>
            <ListSubheader sx={{ bgcolor: "#DDD", fontSize: 15 }}>
              Newest
            </ListSubheader>
            {isLoading ? (
              skelotonRows.map(() => {
                return (
                  <React.Fragment>
                    <ListItem>{renderSkeleton()}</ListItem>
                  </React.Fragment>
                );
              })
            ) : unSeenReports.length > 0 ? (
              unSeenReports.map((report, index) => (
                <ReportItem
                  report={report}
                  index={index}
                  key={index}
                  handleUpdateReport={props.handleUpdateReport}
                  updateReview={props.updateReview}
                />
              ))
            ) : (
              <Typography
                variant="subtitle2"
                textAlign="center"
                fontSize={15}
                padding={3}
              >
                No report here!
              </Typography>
            )}

            <ListSubheader sx={{ bgcolor: "#DDD", fontSize: 15 }}>
              Seen
            </ListSubheader>
            {isLoading ? (
              skelotonRows.map(() => {
                return (
                  <React.Fragment>
                    <ListItem>{renderSkeleton()}</ListItem>
                  </React.Fragment>
                );
              })
            ) : seenReports.length > 0 ? (
              seenReports.map((report, index) => (
                <ReportItem
                  report={report}
                  key={index}
                  handleUpdateReport={props.handleUpdateReport}
                  updateReview={props.updateReview}
                />
              ))
            ) : (
              <Typography
                variant="subtitle2"
                textAlign="center"
                fontSize={15}
                padding={3}
              >
                No report here!
              </Typography>
            )}
          </List>
        </div>
      </Card>
    </>
  );
};

const ReportItem = (props) => {
  const history = useHistory();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const { report } = props;
  const isUpdating = useSelector((state) => state.report.isUpdating);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  //update isHidden field of report
  const handleClickHideReport = (e) => {
    e.preventDefault();

    const updateReport = {
      id: props.report.id,
      isSeen: props.report.isSeen,
      isHidden: !props.report.isHidden,
    };
    handleClose();
    props.handleUpdateReport(updateReport);
  };

  //update isSeen field of report
  const updateIsSeenReport = (e) => {
    e.preventDefault();
    const updateReport = {
      id: props.report.id,
      isSeen: !props.report.isSeen,
      isHidden: props.report.isHidden,
    };
    props.handleUpdateReport(updateReport);
    handleClose();
  };

  //update reiview status
  const updateReviewStatus = (e) => {
    e.preventDefault();
    const params = {
      reportId: report.id,
      reviewId: report.review._id ? report.review._id : report.review.id,
      status: !report.review.isHidden,
    };
    handleClose();
    props.updateReview(params);
  };

  const handleEditPlaceNavigation = () => {
    handleClose();
    history.push(`places/edits/${props.report.review.place}`);
  };

  const handleViewReviewer = () => {
    handleClose();
    const userId = props.report.review.user;
    history.push(`users/${userId}/info`);
  };

  return (
    <React.Fragment>
      <ListItem
        disableRipple
        button
        secondaryAction={
          <Typography component="div">
            <IconButton
              edge="end"
              aria-label="More"
              id="demo-customized-button"
              aria-controls="demo-customized-menu"
              aria-haspopup="true"
              aria-expanded={open ? "true" : undefined}
              variant="contained"
              onClick={handleClick}
            >
              <Tooltip title="More">
                <MoreVertOutlinedIcon />
              </Tooltip>
            </IconButton>
            <StyledMenu
              id="demo-customized-menu"
              MenuListProps={{
                "aria-labelledby": "demo-customized-button",
              }}
              anchorEl={anchorEl}
              open={open}
              onClose={handleClose}
            >
              <MenuItem onClick={updateReviewStatus} disabled={isUpdating}>
                {props.report.review.isHidden ? (
                  <>
                    <RemoveRedEyeOutlinedIcon />
                    Show review
                  </>
                ) : (
                  <>
                    <VisibilityOffOutlinedIcon />
                    Hide review
                  </>
                )}
              </MenuItem>
              {props.report.isSeen ? (
                <MenuItem onClick={updateIsSeenReport}>
                  <CheckCircleRoundedIcon />
                  Mark as unread
                </MenuItem>
              ) : (
                <MenuItem onClick={updateIsSeenReport}>
                  <DoneIcon />
                  Mark as read
                </MenuItem>
              )}
              <Divider sx={{ my: 0.5 }} />
              <MenuItem onClick={handleClickHideReport}>
                <RemoveCircleIcon />
                Never show this report
              </MenuItem>
              <MenuItem onClick={handleEditPlaceNavigation}>
                <EditIcon />
                Edit this place
              </MenuItem>
              <MenuItem onClick={handleViewReviewer}>
                <PersonIcon />
                Reviewer Info
              </MenuItem>
            </StyledMenu>
          </Typography>
        }
        alignItems="center"
      >
        <ListItemAvatar>
          <Avatar src={props.report.reporter.image} alt="P" />
        </ListItemAvatar>
        <ListItemText
          sx={{
            paddingLeft: 1,
          }}
          primary={
            <React.Fragment>
              <Typography
                sx={{
                  display: "block",
                  fontSize: 12,
                  fontWeight: 400,
                  color: "#6A5C5C",
                  fontStyle: "italic",
                }}
                variant="subtitle2"
              >
                By {props.report.reporter.fullName}{" "}
                <Moment format="YYYY-MM-DD HH:mm">
                  {props.report.createdAt}
                </Moment>
              </Typography>
              <Typography
                sx={{
                  display: "block",
                  fontWeight: 500,
                  paddingTop: 1,
                  paddingBottom: 1,
                  color: "#433838",
                  fontSize: 15,
                }}
                variant="body2"
              >
                {props.report.reason}
              </Typography>
            </React.Fragment>
          }
          secondary={
            <React.Fragment>
              <Typography
                sx={{ display: "inline", color: "#514B4B" }}
                component="span"
                variant="body2"
              >
                {props.report.review.content}
              </Typography>
              
              <Typography
                component="div"
                style={{ display: "flex", justifyContent: "flex-end" }}
              >
                <IconButton>
                  {props.report.review.isHidden ? (
                    <Tooltip title="User can not see this review" arrow>
                      <VisibilityOffOutlinedIcon
                        sx={{ fontSize: 20, color: "#AAA" }}
                      />
                    </Tooltip>
                  ) : (
                    <Tooltip title="User can see this review" arrow>
                      <RemoveRedEyeOutlinedIcon
                        sx={{ fontSize: 20, color: "#999" }}
                      />
                    </Tooltip>
                  )}
                </IconButton>
                <IconButton>
                  {props.report.isSeen ? (
                    <CheckCircleRoundedIcon
                      sx={{ fontSize: 20, color: "#32A5E5" }}
                    />
                  ) : (
                    <CheckCircleOutlinedIcon
                      sx={{ fontSize: 20, color: "#64727A" }}
                    />
                  )}
                </IconButton>
              </Typography>
            </React.Fragment>
          }
        />
      </ListItem>
    </React.Fragment>
  );
};

export default CommentReport;
