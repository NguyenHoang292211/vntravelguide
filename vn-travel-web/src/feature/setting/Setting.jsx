import AppBar from "@material-ui/core/AppBar";
import Box from "@material-ui/core/Box";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import SwipeableViews from "react-swipeable-views";
import {
  createCategory,
  loadCategories,
  updateCategory,
} from "../../redux/categorySlice";
import {
  createProvince,
  deleteProvince,
  loadProvinces,
  updateProvince,
} from "../../redux/provinceSlice";
import tagReducer, {
  createTag,
  deleteTag,
  loadTags,
  updateTag,
} from "../../redux/tagSlice";
import "./setting.css";
import { provinceTableColumn, tagTableColumn } from "./TableUiData";
import TabPanelItem from "./tabPanelItem/TabPanelItem";
import { Snackbar } from "@mui/material";
import MuiAlert from "@mui/material/Alert";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});
//Setting UI
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <>{children}</>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
  },
}));

const Settings = () => {
  const dispatch = useDispatch();

  let provinces = useSelector((state) => state.province.provinces)
    .slice()
    .sort((a, b) => Date.parse(b.updatedAt) - Date.parse(a.updatedAt));
  const loadingProvince = useSelector((state) => state.province.loading);
  const provinceError = useSelector((state) => state.province.error);
  const provinceUpdating = useSelector((state) => state.province.updating);

  let tags = useSelector((state) => state.tag.tags)
    .slice()
    .sort((a, b) => Date.parse(b.updatedAt) - Date.parse(a.updatedAt));
  const loadingTag = useSelector((state) => state.tag.loading);
  const tagError = useSelector((state) => state.tag.error);
  const tagUpdating = useSelector((state) => state.tag.updating);

  let categories = useSelector((state) => state.category.categories)
    .slice()
    .sort((a, b) => Date.parse(b.updatedAt) - Date.parse(a.updatedAt));
  const loadingCategory = useSelector((state) => state.category.loading);
  const categoryError = useSelector((state) => state.category.error);
  const categoryUpdating = useSelector((state) => state.category.updating);

  const tabsList = ["Provinces", "Tags", "Categories"];
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = useState(0);
  const provinceColumn = provinceTableColumn;
  const tagColumn = tagTableColumn;

  //*Snack bar
  const [openSnackSuccess, setOpenSnackSuccess] = useState(false);
  const [openSnackError, setOpenSnackError] = useState(false);
  let SUCCESS_MESSAGE = "Update successfully!";
  let ERROR_MESSAGE = "Error happen, try again!";

  const handleCloseSnackError = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenSnackError(false);
  };

  const handleCloseSnackSuccess = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenSnackSuccess(false);
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  //*Handle province

  const loadProvince = () => {
    try {
      dispatch(loadProvinces());
    } catch (error) {
      console.log(error.message);
    }
  };

  const saveProvince = async (province) => {
    try {
      await dispatch(createProvince(province));
      if (provinceError === "" && provinceUpdating === false) {
        setOpenSnackSuccess(true);
      } else if (provinceError !== "") {
        setOpenSnackError(true);
        ERROR_MESSAGE = provinceError;
      }
    } catch (error) {
      setOpenSnackError(true);
      ERROR_MESSAGE = "Some error happen.Please try again!";
    }
  };

  const deleteProvince_ = async (id) => {
    try {
      await dispatch(deleteProvince(id));
      if (provinceError === "" && provinceUpdating === false) {
        setOpenSnackSuccess(true);
      } else if (provinceError !== "") {
        setOpenSnackError(true);
        ERROR_MESSAGE = provinceError;
      }
    } catch (error) {
      setOpenSnackError(true);
      ERROR_MESSAGE = "Some error happen.Please try again!";
    }
  };

  const updateProvince_ = async (province) => {
    try {
      await dispatch(updateProvince(province));
      if (provinceError === "" && provinceUpdating === false) {
        setOpenSnackSuccess(true);
      } else if (provinceError !== "") {
        setOpenSnackError(true);
        ERROR_MESSAGE = provinceError;
      }
    } catch (error) {
      setOpenSnackError(true);
      ERROR_MESSAGE = "Some error happen.Please try again!";
    }
  };

  //* Handle tag
  const loadTag = () => {
    try {
      dispatch(loadTags());
    } catch (error) {
      console.log(error.message);
    }
  };

  const saveTag_ = async (tag) => {
    try {
      await dispatch(createTag(tag));
      if (tagError === "" && tagUpdating === false) {
        setOpenSnackSuccess(true);
      } else if (tagError !== "") {
        setOpenSnackError(true);
        ERROR_MESSAGE = tagError;
      }
    } catch (error) {
      setOpenSnackError(true);
      ERROR_MESSAGE = "Some error happen.Please try again!";
    }
  };

  const updateTag_ = async (tag) => {
    try {
      await dispatch(updateTag(tag));
      if (tagError === "" && tagUpdating === false) {
        setOpenSnackSuccess(true);
      } else if (tagError !== "") {
        setOpenSnackError(true);
        ERROR_MESSAGE = tagError;
      }
    } catch (error) {
      setOpenSnackError(true);
      ERROR_MESSAGE = "Some error happen.Please try again!";
    }
  };

  //* Handle category
  const loadCategory = () => {
    try {
      dispatch(loadCategories());
    } catch (error) {
      console.log(error.message);
    }
  };

  const saveCategory = async (category) => {
    try {
      await dispatch(createCategory(category));
      if (categoryError === "" && categoryUpdating === false) {
        setOpenSnackSuccess(true);
      } else if (categoryError !== "" && categoryUpdating === false) {
        setOpenSnackError(true);
        ERROR_MESSAGE = categoryError;
      }
    } catch (error) {
      setOpenSnackError(true);
      ERROR_MESSAGE = "Some error happen.Please try again!";
    }
  };

  const updateCategory_ = async (category) => {
    try {
      await dispatch(updateCategory(category));
      if (categoryError === "" && categoryUpdating === false) {
        setOpenSnackSuccess(true);
      } else if (categoryError !== "" && categoryUpdating === false) {
        setOpenSnackError(true);
        ERROR_MESSAGE = categoryError;
      }
    } catch (error) {
      setOpenSnackError(true);
      ERROR_MESSAGE = "Some error happen.Please try again!";
    }
  };

  useEffect(() => {
    loadProvince();
    loadTag();
    loadCategory();
  }, [dispatch]);

  return (
    <div className={classes.root}>
      <Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        open={openSnackSuccess}
        autoHideDuration={3000}
        onClose={handleCloseSnackSuccess}
      >
        <Alert
          onClose={handleCloseSnackSuccess}
          sx={{ width: "100%" }}
          severity="success"
        >
          {SUCCESS_MESSAGE}
        </Alert>
      </Snackbar>
      {/* /* Snack error */}
      <Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        open={openSnackError}
        autoHideDuration={3000}
        onClose={handleCloseSnackError}
      >
        <Alert
          onClose={handleCloseSnackError}
          sx={{ width: "100%" }}
          severity="error"
        >
          {ERROR_MESSAGE}
        </Alert>
      </Snackbar>
      <AppBar position="static" color="inherit">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          {tabsList.map((tab, index) => (
            <Tab
              style={{ fontWeight: 600 }}
              label={tab}
              {...a11yProps(index)}
              key={index}
            />
          ))}
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === "rtl" ? "x-reverse" : "x"}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction} key={0}>
          <TabPanelItem
            columns={provinceColumn}
            rows={provinces ? provinces : []}
            loading={loadingProvince}
            tabTitle="province"
            saveItem={saveProvince}
            handleDelete={deleteProvince_}
            updateItem={updateProvince_}
          />
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction} key={1}>
          <TabPanelItem
            columns={tagColumn}
            rows={tags ? tags : []}
            tabTitle="tag"
            loading={loadingTag}
            saveItem={saveTag_}
            updateItem={updateTag_}
          />
        </TabPanel>
        <TabPanel value={value} index={2} dir={theme.direction} key={2}>
          <TabPanelItem
            columns={provinceColumn}
            rows={categories ? categories : []}
            tabTitle="category"
            loading={loadingCategory}
            saveItem={saveCategory}
            updateItem={updateCategory_}
          />
        </TabPanel>
      </SwipeableViews>
    </div>
  );
};

export default Settings;
