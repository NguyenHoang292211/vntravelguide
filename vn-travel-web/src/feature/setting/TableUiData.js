export const provinceTableColumn = [
    {
        field: "name",
        headerName: "Name",
        width: 200,
        editable: false,
    },
    {
        field: "color",
        headerName: "Color",
        width: 70,
    },
    {
        field: "isHidden",
        headerName: "Status",
        width: 100,
    },
    {
        field: "updatedAt",
        headerName: "Last edited",
        width: 150,
        editable: false,
    },
    {
        field: "action",
        headerName: "Action",
        width: 150,
        editable: false,
    },
];

export const tagTableColumn = [
    {
        field: "name",
        headerName: "Name",
        width: 200,
        editable: false,
    },
    {
        field: "isHidden",
        headerName: "Status",
        width: 100,
    },
    {
        field: "updatedAt",
        headerName: "Last edited",
        width: 150,
        editable: false,
    },
    {
        field: "action",
        headerName: "Action",
        width: 150,
        editable: false,
    },
];

