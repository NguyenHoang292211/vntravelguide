import React, { useEffect, useState } from "react";

import { makeStyles } from "@material-ui/core/styles";
import "./tabPanelItem.css";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Fab from "@material-ui/core/Fab";
import Moment from "react-moment";
import Skeleton from "@material-ui/lab/Skeleton";
import { Grid } from "@material-ui/core";
import { Chip } from "@mui/material";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Button from "@material-ui/core/Button";
import Tooltip from "@material-ui/core/Tooltip";

//import icon
import EditIcon from "@material-ui/icons/Edit";
import AddCircleRounded from "@material-ui/icons/AddCircleRounded";
import SearchOutlined from "@material-ui/icons/SearchOutlined";
import CreationDialog from "../dialog/CreationDialog";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  container: {
    maxHeight: 400,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  tableRow: {
    "&:hover": {
      backgroundColor: "#E1F5FE !important",
    },
  },
}));

const TabPanelItem = (props) => {
  const classes = useStyles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const skelotonRows = [0, 1, 2, 3];
  const [openedDialog, setOpenedDialog] = useState(false);
  const [openedEditDialog, setOpenEditDialog] = useState(false);
  const statusFilterItems = ["All", "Public", "Private"];
  const [itemsDisplayed, setItemsDisplayed] = useState([]); // save item after filter
  const [statusFilterValue, setStatusFilterValue] = useState(
    statusFilterItems[0]
  );
  const [keyWord, setKeyWord] = useState(""); //for search
  const [rows, setRows] = useState([]); //props value
  const [data, setData] = useState({
    id: "",
    name: "",
    color: "#FFFFF",
    isHidden: true,
  });

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleOpenDialog = () => {
    setOpenedDialog(true);
    setData({
      id: "",
      name: "",
      color: "#FFFFF",
      isHidden: false,
      image: "",
    });
  };

  const handleCloseUpdateDialog = () => {
    setOpenEditDialog(false);
    setData({
      id: "",
      name: "",
      color: "#FFFFF",
      isHidden: false,
      image: "",
    });
  };

  const handleOpenUpdateDialog = (selectedId) => {
    //get selected item
    const currentItem = props.rows.find((item) => item.id === selectedId);

    setData({
      id: currentItem.id,
      color: currentItem.color,
      name: currentItem.name,
      isHidden: currentItem.isHidden,
      image: currentItem.image ? currentItem.image : "",
    });

    setOpenEditDialog(true);
  };

  const handleCloseDialog = () => {
    setOpenedDialog(false);
  };

  //Render cell of table with specified field
  const renderTableCell = (field, value, id) => {
    switch (field) {
      case "isHidden":
        if (value) return <Chip label="Private" color="warning" size="small" />;
        else
          return (
            <Chip label="Public" variant="outlined" size="small" color="info" />
          );
      case "color":
        return (
          <div className="color-container">
            <div
              className="color-tag"
              style={{
                background: value,
              }}
            ></div>
          </div>
        );
      case "action":
        return (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
            }}
          >
            <Tooltip title={"Edit " + props.tabTitle} aria-label="edit" arrow>
              <Fab
                color="primary"
                aria-label="edit"
                style={{ marginRight: 20, height: 35, width: 35 }}
                onClick={() => handleOpenUpdateDialog(id)}
              >
                <EditIcon fontSize="small" />
              </Fab>
            </Tooltip>
          </div>
        );
      case "updatedAt":
        return <Moment format="YYYY-MM-DD HH:mm">{value}</Moment>;
      default:
        return value;
    }
  };

  const handleFilterStatus = (status, items) => {
    if (status === statusFilterItems[0])
      //all
      return items;
    else if (status === statusFilterItems[1]) {
      //public
      return [...items.filter((item) => item.isHidden === false)];
    }
    //private
    else return [...items.filter((item) => item.isHidden === true)];
  };

  const handleSearch = (keyWord, items) => {
    if (keyWord === "") {
      return items;
    } else
      return [
        ...items.filter((x) =>
          x.name
            .toLowerCase()
            .normalize("NFD")
            .replace(/([\u0300-\u036f]|[^0-9a-zA-Z])/g, "")
            .includes(
              keyWord
                .toLowerCase()
                .normalize("NFD")
                .replace(/([\u0300-\u036f]|[^0-9a-zA-Z])/g, "")
            )
        ),
      ];
  };

  const filterAll = (keyWord, status, list) => {
    let items = handleSearch(keyWord, list);
    items = handleFilterStatus(status, items);
    setItemsDisplayed(items);
  };

  const handleFilter = (e) => {
    const value = e.target.value;
    const name = e.target.name;
    if (name === "status") {
      setStatusFilterValue(value);
      filterAll(keyWord, value, rows);
    } else if (name === "search") {
      setKeyWord(value);
      filterAll(value, statusFilterValue, rows);
    }
  };

  useEffect(() => {
    setRows(props.rows);
    filterAll(keyWord, statusFilterValue, props.rows);
  }, [props.rows]);

  //render Skeleton
  const renderSkeleton = (field) => {
    if (field === "action")
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Skeleton
            className="skeleton"
            variant="circle"
            height={30}
            animation="wave"
          ></Skeleton>
          <Skeleton
            className="skeleton"
            variant="circle"
            height={30}
            animation="wave"
          ></Skeleton>
        </div>
      );
    return <Skeleton width="100%" variant="text" animation="wave"></Skeleton>;
  };

  return (
    <div className="tab-container">
      <Grid container className="tab-head">
        <Grid item xs={12} sm={6} className="head-left">
          <FormControl variant="outlined" className={classes.formControl}>
            <InputLabel id="filter" style={{ fontSize: 13 }}>
              Status
            </InputLabel>
            <Select
              value={statusFilterValue}
              labelId="filter"
              id="filter-select-outlined"
              label="Filter"
              name="status"
              style={{ fontSize: 13 }}
              className="select-filter"
              onChange={handleFilter}
            >
              {statusFilterItems.map((item, index) => {
                return (
                  <MenuItem key={index} style={{ fontSize: 14 }} value={item}>
                    {item}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
          <Button
            variant="contained"
            color="primary"
            className="add-button"
            endIcon={<AddCircleRounded fontSize="small" />}
            onClick={handleOpenDialog}
          >
            New {props.tabTitle}
          </Button>
          <CreationDialog
            open={openedDialog}
            openDialog={handleOpenDialog}
            closeDialog={handleCloseDialog}
            title={props.tabTitle}
            saveItem={props.saveItem}
            edit={false}
            data={data}
          />
          <CreationDialog
            open={openedEditDialog}
            openDialog={handleOpenUpdateDialog}
            closeDialog={handleCloseUpdateDialog}
            title={props.tabTitle}
            updateItem={props.updateItem}
            edit={true}
            data={data}
          />
        </Grid>
        <Grid item xs={12} sm={6} className="head-right">
          <div className="search-field">
            <input
              className="search-input"
              type="text"
              placeholder="Search"
              name="search"
              value={keyWord}
              onChange={handleFilter}
            />
            <SearchOutlined className="search-icon" fontSize="large" />
          </div>
        </Grid>
      </Grid>
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {props.columns.map((column) => (
                  <TableCell
                    key={column.field}
                    align="center"
                    style={{ width: column.width }}
                    color="#039be5"
                  >
                    {column.headerName}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {!props.loading
                ? itemsDisplayed
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((item) => {
                      return (
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                          key={item.id}
                          className={classes.tableRow}
                        >
                          {props.columns.map((column) => {
                            const value = item[column.field];
                            const id = item["id"];
                            return (
                              <TableCell
                                key={column.field}
                                align="center"
                                style={{ paddingTop: 8, paddingBottom: 8 }}
                              >
                                {renderTableCell(column.field, value, id)}
                              </TableCell>
                            );
                          })}
                        </TableRow>
                      );
                    })
                : skelotonRows.map((index) => {
                    return (
                      <TableRow tabIndex={-1} key={index}>
                        {props.columns.map((column) => {
                          return (
                            <TableCell key={column.field} align="center">
                              <div
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                  justifyContent: "center",
                                }}
                              >
                                {renderSkeleton(column.field)}
                              </div>
                            </TableCell>
                          );
                        })}
                      </TableRow>
                    );
                  })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 15, 20, 25, 50, 100]}
          component="div"
          count={itemsDisplayed.length}
          rowsPerPage={rowsPerPage}
          page={page}
          style={{ overflow: "hidden" }}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
};

export default TabPanelItem;
