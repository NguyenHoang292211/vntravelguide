import { Divider, Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import { useTheme } from "@material-ui/core/styles";
import Switch from "@material-ui/core/Switch";
import TextField from "@material-ui/core/TextField";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import React, { useEffect, useState } from "react";
import "./creationDialog.css";
import notFound from "../../../assets/images/notFound.jpg";

const CreationDialog = (props) => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("xs"));
  const formInitialState = {
    id: "",
    name: "",
    color: "#ffffff",
    isHidden: false,
    image: "",
  };
  const [formValue, setFormValue] = useState(formInitialState);

  const [status, setStatus] = useState(true);

  const [errorImage, setErrorImage] = useState(false);
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormValue({
      ...formValue,
      [name]: value,
    });
  };

  const handleSave = (e) => {
    e.preventDefault();
    props.saveItem(formValue);
    props.closeDialog();
  };

  const handleEdit = (e) => {
    e.preventDefault();
    props.updateItem(formValue);
    props.closeDialog();
  };

  useEffect(() => {
    setFormValue({
      ...formValue,
      id: props.data.id,
      name: props.data.name,
      color: props.data.color,
      isHidden: props.data.isHidden,
      image: props.data.image ? props.data.image : "",
    });
    setStatus(!props.data.isHidden);
  }, [props.data]);

  return (
    <div>
      <Dialog
        maxWidth="lg"
        fullScreen={fullScreen}
        open={props.open}
        onClose={props.closeDialog}
        aria-labelledby="responsive-dialog-title"
      >
        <Typography className="dialog-title" variant="h5">
          {props.edit ? "Edit" : "New"} {props.title}
        </Typography>
        <Divider variant="middle" className="divider" />

        <DialogContent>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
            }}
          >
            <div
              style={{
                minWidth: 400,
                paddingLeft: 25,
                paddingRight: 25,
                paddingTop: 25,
              }}
            >
              <TextField
                className="input-text"
                label="Name"
                type="text"
                InputLabelProps={{
                  shrink: true,
                }}
                placeholder={`${props.title} name`}
                variant="outlined"
                fullWidth
                size="medium"
                name="name"
                onChange={(e) => handleInputChange(e)}
                value={formValue.name}
              />

              {props.title === "tag" ? (
                <></>
              ) : (
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "flex-start",
                    marginTop: 15,
                    alignItems: "center",
                  }}
                >
                  <label className="label">Color</label>
                  <input
                    type="color"
                    name="color"
                    className="color-picker"
                    value={formValue.color}
                    onChange={handleInputChange}
                  />
                  <input
                    type="text"
                    name="color-value"
                    className="color-text"
                    value={formValue.color}
                  />
                </div>
              )}
              {props.title === "province" ? (
                <div style={{ marginTop: 15 }}>
                  <TextField
                    className="input-text"
                    label=" Image URL"
                    type="text"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    placeholder="https://image/url"
                    variant="outlined"
                    fullWidth
                    size="medium"
                    name="image"
                    onChange={(e) => handleInputChange(e)}
                    value={formValue.image}
                  />
                </div>
              ) : (
                <></>
              )}

              <div className="status-setting">
                <label className="label">Status</label>
                <Switch
                  color="primary"
                  name="isHidden"
                  inputProps={{ "aria-label": "primary checkbox" }}
                  checked={status}
                  onChange={() => {
                    setFormValue({
                      ...formValue,
                      isHidden: status,
                    });
                    setStatus(!status);
                  }}
                />
              </div>
            </div>
            {props.title === "province" && (
              <div
                style={{
                  paddingLeft: 20,
                  display: "block",
                }}
              >
                <label className="label">Banner image</label>
                {formValue.image && formValue.image !== "" && (
                  <img
                    title="Banner"
                    style={{
                      maxHeight: 300,
                      maxWidth: 350,
                    }}
                    src={formValue.image}
                    // alt={formValue.banner}
                    onError={(e) => {
                      e.target.onerror = null;
                      e.target.src = notFound;
                    }}
                  />
                )}
              </div>
            )}
          </div>
        </DialogContent>
        <DialogActions>
          <Button
            autoFocus
            onClick={props.edit ? handleEdit : handleSave}
            color="primary"
            style={{
              fontSize: 15,
              fontWeight: 600,
            }}
          >
            {props.edit ? "Edit" : "Save"}
          </Button>
          <Button
            onClick={() => {
              props.closeDialog();
              setFormValue(props.data);
            }}
            style={{
              fontSize: 15,
              fontWeight: 600,
            }}
            color="secondary"
            autoFocus
          >
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default CreationDialog;
