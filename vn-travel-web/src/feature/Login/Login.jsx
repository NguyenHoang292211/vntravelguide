import React from "react";
import "./login.css";
import googleLogo from "../../assets/images/google.svg";
import logo from "../../assets/images/logoName.png";
import { useDispatch } from "react-redux";
import { loginAdminWithGoogle } from "../../redux/authSlice";
import { GoogleLogin } from "react-google-login";

import LoginForm from "./LoginForm/LoginForm";

const Login = () => {
  const dispatch = useDispatch();
  const body = (
    <>
      <LoginForm />
    </>
  );
  const onSuccess = (res) => {
    dispatch(loginAdminWithGoogle(res.tokenId));
  };

  const onFailure = (res) => {};

  return (
    <div
      className="main-div"
      style={{
        backgroundImage: `url(https://preview.colorlib.com/theme/bootstrap/login-form-20/images/xbg.jpg.pagespeed.ic.tiVxeakBSd.webp)`,
      }}
    >
      <div className="main-blur"></div>
      <div className="display-login">
        <div className="login" alt="app-logo">
          <div className="div-logo">
            <img src={logo} className="logo" alt="logo" />
          </div>
          {body}
          <div className="label-choose">
            <span className="label-or">or</span>
          </div>

          <GoogleLogin
            clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
            buttonText="Login"
            onSuccess={onSuccess}
            onFailure={onFailure}
            cookiePolicy={`single_host_origin`}
            render={(renderProps) => (
              <button
                onClick={renderProps.onClick}
                disabled={renderProps.disabled}
                className="btn-google-login btn"
              >
                <img
                  src={googleLogo}
                  alt="google-logo"
                  className="google-logo"
                ></img>
                Login with google{" "}
              </button>
            )}
          ></GoogleLogin>
        </div>
      </div>
    </div>
  );
};

export default Login;
