import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginUser } from "../../../redux/authSlice";
import "./loginForm.css";
const LoginForm = () => {
  const dispatch = useDispatch();

  const error = useSelector((state) => state.auth.error);
  //Local state
  const [loginForm, setLoginForm] = useState({
    email: "",
    password: "",
  });

  const { email, password } = loginForm;
  const onChangeLoginForm = (event) =>
    setLoginForm({ ...loginForm, [event.target.name]: event.target.value });
    
  const login = async (event) => {
    try {
      event.preventDefault();
      dispatch(loginUser(loginForm));
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <form onSubmit={login}>
        <div className="login-field">
          <i className="bx bx-envelope"></i>
          <input
            required
            type="text"
            className="login-input"
            placeholder="Email"
            name="email"
            value={email}
            onChange={onChangeLoginForm}
          ></input>
        </div>
        <div className="login-field">
          <i className="bx bx-lock-open"></i>
          <input
            required
            type="password"
            name="password"
            className="login-input"
            placeholder="Password"
            value={password}
            onChange={onChangeLoginForm}
          ></input>
        </div>
        <p className="error"> {error}</p>

        <button type="submit" className="btn btn-normal-login">
          Login
        </button>
      </form>
    </>
  );
};

export default LoginForm;
