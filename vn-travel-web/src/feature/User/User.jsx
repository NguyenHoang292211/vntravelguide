import { InputAdornment, makeStyles, Paper } from "@material-ui/core";
import { Search } from "@material-ui/icons";
import {
  Alert,
  Avatar,
  Card,
  Snackbar,
  Stack,
  Switch,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import Moment from "react-moment";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";

import DialogConfirm from "../../components/dialog/DialogConfirm";
import Loading from "../../components/loading/Loading";
import Input from "../../components/UI/Input";
import { updateUserStatus } from "../../redux/userSlice";
import { STATUS_ACTIVE, STATUS_DISABLE } from "../../utils/constants";
import TagLabel from "../Place/placeItem/tagLabel/TagLabel";
import SearchNotFound from "./searchNotFound/SearchNotFound";
const useStyles = makeStyles((theme) => ({
  searchInput: {
    width: "20rem",
  },
  root: {
    backgroundColor: theme.palette.background.paper,
  },
  card: {
    margin: theme.spacing(2),
    padding: theme.spacing(2),
  },
  tableRow: {
    "&:hover": {
      backgroundColor: "#E1F5FE !important",
    },
  },
}));

const TABLE_HEAD = [
  { id: "name", label: "Name", alignRight: false },
  { id: "email", label: "Email", alignRight: false },
  { id: "status", label: "Status", alignRight: false },
  { id: "createdAt", label: "Attendance", alignRight: false },
  { id: "update", label: "Updated", alignRight: false },
];

const User = () => {
  const classes = useStyles();
  const [page, setPage] = useState(0);
  const [filterName, setFilterName] = useState("");
  const [rowsPerPage, setRowsPerPage] = useState(8);

  const { userId } = useParams();

  const users = useSelector((state) => state.user.users)
    .slice()
    .sort((a, b) => Date.parse(b.updatedAt) - Date.parse(a.updatedAt));
  const dispatch = useDispatch();
  const [userSelected, setUserSelected] = useState({
    id: "",
    message: "",
    title: "Confirmation",
    isHidden: "true",
  });
  const [openSnackSuccess, setOpenSnackSuccess] = useState(false);
  const [openSnackError, setOpenSnackError] = useState(false);
  const [openDialog, setOpenDialog] = useState(false);
  const SUCCESS_MESSAGE = "Update user state successfully!";
  const ERROR_MESSAGE = "Error happen, try again!";
  const [loading, setLoading] = useState(false);

  const handleOpenDialog = (id, email, isHidden) => {
    let message = "";
    if (isHidden) {
      message = `Are your sure to active user ${email}?`;
    } else {
      message = `Are your sure to disable user ${email}?`;
    }
    setUserSelected({
      id: id,
      title: "Confirmation",
      message: message,
      isHidden: isHidden,
    });
    setOpenDialog(true);
  };

  const handleCloseSnackError = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenSnackError(false);
  };

  const handleCloseSnackSuccess = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenSnackSuccess(false);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const handleConfirmChange = async (id) => {
    setLoading(true);
    const response = await dispatch(
      updateUserStatus({ id: id, isHidden: !userSelected.isHidden })
    );
    try {
      if (response.payload.success) {
        setOpenSnackSuccess(true);
        resetDialog();
      } else {
        setOpenSnackError(true);
        resetDialog();
      }
    } catch (error) {
      resetDialog();
      setOpenSnackError(true);
    }
  };
  const resetDialog = () => {
    setLoading(false);
    setOpenDialog(false);
  };

  // const isLoading = useSelector((state) => state.user.isLoading);
  const handleFindByName = (array, query) => {
    if (query === "") {
      return array;
    } else {
      return [
        ...array.filter((x) =>
          x.fullName
            .toLowerCase()
            .normalize("NFD")
            .replace(/([\u0300-\u036f]|[^0-9a-zA-Z])/g, "")
            .includes(
              query
                .normalize("NFD")
                .replace(/([\u0300-\u036f]|[^0-9a-zA-Z])/g, "")
            )
        ),
      ];
    }
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - users.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const filteredUsers = userId
    ? users
      ? users.filter((user) => user.id === userId)
      : []
    : users
    ? handleFindByName(users, filterName)
    : [];
  const isUserNotFound = filteredUsers.length === 0;
  const handleFilter = (e) => {
    //TODO: filter Here

    setFilterName(e.target.value);
  };
  const handleUpdateStatus = (id) => {
    //TODO:Show dialog
    //TODO:Handle in slide
  };

  //TODO: get user in database
  return (
    <>
      {typeof users == "undefined" ? (
        <Loading />
      ) : (
        <>
          <DialogConfirm
            open={openDialog}
            id={userSelected.id}
            handleClose={handleCloseDialog}
            message={userSelected.message}
            handleConfirm={handleConfirmChange}
            isLoading={loading}
          />
          {/*  /* Snack success */}
          <Snackbar
            anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
            open={openSnackSuccess}
            autoHideDuration={3000}
            onClose={handleCloseSnackSuccess}
          >
            <Alert
              onClose={handleCloseSnackSuccess}
              severity="success"
              sx={{ width: "100%" }}
            >
              {SUCCESS_MESSAGE}
            </Alert>
          </Snackbar>

          {/* /* Snack error */}
          <Snackbar
            anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
            open={openSnackError}
            autoHideDuration={3000}
            onClose={handleCloseSnackError}
          >
            <Alert
              onClose={handleCloseSnackError}
              severity="error"
              sx={{ width: "100%" }}
            >
              {ERROR_MESSAGE}
            </Alert>
          </Snackbar>

          <Card className={classes.card}>
            <div className="nav">
              <div className="nav__left">
                <Input
                  name="search"
                  label="Search user name"
                  className={classes.searchInput}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Search />
                      </InputAdornment>
                    ),
                  }}
                  onChange={(e) => {
                    handleFilter(e);
                  }}
                ></Input>
              </div>
              <div className="nav__right"></div>
            </div>
            {/* <div className="content"> */}
            <Paper>
              <TableContainer sx={{ maxHeight: 400 }}>
                <Table>
                  <TableHead>
                    <TableRow>
                      {TABLE_HEAD.map((headCell) => (
                        <TableCell
                          key={headCell.id}
                          align={headCell.alignRight ? "right" : "left"}
                        >
                          {" "}
                          {headCell.label}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {filteredUsers
                      .slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                      .map((row) => {
                        //TODO: update isHidden user in mg
                        const {
                          id,
                          fullName,
                          email,
                          image,
                          isHidden,
                          createdAt,
                        } = row;
                        return (
                          <TableRow
                            hover
                            className={classes.tableRow}
                            key={id}
                            style={{
                              height: "12px",
                              paddingTop: 8,
                              paddingBottom: 8,
                            }}
                          >
                            <TableCell
                              component="th"
                              scope="row"
                              padding="none"
                              align="center"
                              style={{
                                height: "12px",
                                paddingTop: 8,
                                paddingBottom: 8,
                                paddingLeft: 16,
                              }}
                            >
                              <Stack
                                direction="row"
                                alignItems="center"
                                spacing={2}
                              >
                                <Avatar
                                  alt={fullName}
                                  src={image}
                                  style={{ height: "35px", width: "35px" }}
                                />
                                <Typography variant="subtitle2" noWrap>
                                  {fullName}
                                </Typography>
                              </Stack>
                            </TableCell>
                            <TableCell
                              align="left"
                              style={{
                                height: "12px",
                                paddingTop: 8,
                                paddingBottom: 8,
                              }}
                            >
                              {email}{" "}
                            </TableCell>
                            <TableCell
                              align="left"
                              style={{
                                height: "12px",
                                paddingTop: 8,
                                paddingBottom: 8,
                              }}
                            >
                              <Moment format="YYYY-MM-DD HH:mm">
                                {createdAt}
                              </Moment>{" "}
                            </TableCell>
                            <TableCell
                              align="left"
                              style={{
                                height: "12px",
                                paddingTop: 8,
                                paddingBottom: 8,
                              }}
                            >
                              <TagLabel
                                type={isHidden ? STATUS_DISABLE : STATUS_ACTIVE}
                                name={isHidden ? STATUS_DISABLE : STATUS_ACTIVE}
                              />
                            </TableCell>
                            <TableCell
                              style={{
                                height: "12px",
                                paddingTop: 8,
                                paddingBottom: 8,
                              }}
                            >
                              <Switch
                                checked={isHidden ? false : true}
                                onClick={(e) => {
                                  e.preventDefault();
                                  handleOpenDialog(id, email, isHidden);
                                }}
                              />
                            </TableCell>
                          </TableRow>
                        );
                      })}
                    {emptyRows > 0 && (
                      <TableRow style={{ height: 53 * emptyRows }}>
                        <TableCell colSpan={6} />
                      </TableRow>
                    )}
                  </TableBody>
                  {isUserNotFound && (
                    <TableBody>
                      <TableRow>
                        <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                          <SearchNotFound searchQuery={filterName} />
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  )}
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[8, 10, 25]}
                component="div"
                count={filteredUsers.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </Paper>

            {/* </div> */}
          </Card>
        </>
      )}
    </>
  );
};

export default User;
