import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { baseUrl, ERROR_UN_SAVING } from "../utils/constants";

const initialState = {
  places: [],
  error: "",
  isLoading: false,
  tags: [],
  categories: [],
  provinces: [],
  currPlace: null,
};

/**
 * Get all place
 */
export const getAllPlaces = createAsyncThunk(
  "places/getPlaces",
  async (params, { rejectWithValue }) => {
    try {
      const response = await axios.get(
        `${baseUrl}/places/private?populate=true`
      );
      if (response.data.success) {
        
        return {
          places: response.data.places,
        };
      } else {
        return {
          places: [],
        };
      }
    } catch (error) {
      console.log(error.message);
      return rejectWithValue(error.message);
    }
  }
);

/**
 * Create new place
 */
export const createPlace = createAsyncThunk(
  "place/create",
  async (place, { rejectWithValue }) => {
    try {
      const response = await axios.post(`${baseUrl}/places`, place);
      if (response.data.success) {
        return {
          success: response.data.success,
          place: response.data.place,
        };
      }
    } catch (error) {
      console.log(error.response.data.message);
      return rejectWithValue(error.response.data.message);
    }
  }
);
/**
 * Get all tag
 */
export const getTags = createAsyncThunk(
  "tags/getTags",
  async (params, { rejectWithValue }) => {
    try {
      const response = await axios.get(`${baseUrl}/tags`);

      if (response.data.success) {
        return {
          success: true,
          tags: response.data.tags,
        };
      } else {
        return {
          tags: [],
        };
      }
    } catch (error) {
      console.log(error.message);
      return rejectWithValue(error.message);
    }
  }
);
/**
 * Get all province
 */
export const getProvinces = createAsyncThunk(
  "provinces/getProvinces",
  async (params, { rejectWithValue }) => {
    try {
      const response = await axios.get(`${baseUrl}/provinces`);
      if (response.data.success) {
        return {
          success: response.data.success,
          provinces: response.data.provinces,
        };
      } else {
        return {
          provinces: [],
        };
      }
    } catch (error) {
      console.log(error.message);
      return rejectWithValue(error.message);
    }
  }
);
/**
 * Get all category
 */

export const getCategories = createAsyncThunk(
  "categories/getCategories",
  async (params, { rejectWithValue }) => {
    try {
      const response = await axios.get(`${baseUrl}/categories`);
      if (response.data.success) {
        return {
          categories: response.data.categories,
        };
      } else {
        return {
          categories: [],
        };
      }
    } catch (error) {
      console.log(error.message);
      return rejectWithValue(error.message);
    }
  }
);
/**
 * Update image to server
 */
export const updateImagesInPlace = createAsyncThunk(
  "places/updateImages",
  async (place, { rejectWithValue }) => {
    //TODO:update to server
    try {
      const response = await axios.put(
        `${baseUrl}/places/${place.id}/images`,
        place
      );
      if (response.data.success) {
        return {
          success: response.data.success,
          place: response.data.place,
        };
      }
    } catch (error) {
      console.log(error.response.data.message);
      return rejectWithValue(error.response.data.message);
    }
  }
);

/**
 * Update place
 */
export const updatePlace = createAsyncThunk(
  "places/updateField",
  async (place, { rejectWithValue }) => {
    try {
      const response = await axios.put(`${baseUrl}/places/${place.id}`, place);
      if (response.data.success) {
        return {
          success: response.data.success,
          place: response.data.place,
        };
      }
    } catch (error) {
      console.log(error.response.data.message);
      return rejectWithValue(error.response.data.message);
    }
  }
);

/**
 * Get place current
 */
export const getPlaceCurrent = createAsyncThunk(
  "place/get",
  async (id, { rejectWithValue }) => {
    try {
      const response = await axios.get(`${baseUrl}/places/${id}?populate=true`);
      if (response.data.success) {
        return {
          place: response.data.place,
        };
      } else {
        return {
          place: null,
        };
      }
    } catch (error) {
      console.log(error.message);
      return rejectWithValue(error.message);
    }
  }
);

const placeSlice = createSlice({
  name: "place",
  initialState,
  reducers: {
    startLoadDataPlace: (state) => {
      state.isLoading = true;
    },
    endLoadingDataPlace: (state) => {
      state.isLoading = false;
    },
    setCurrentPlace: (state, action) => {
      state.currPlace = state.places.find((item) => item.id === action.payload);
    },
    updateCategory: (state, action) => {
      state.places.forEach((e) => {
        if (e.id === action.payload.placeID) {
          //Get category info
          // let tmpCategory = state.categories.find((e) => e.category.id == []);
        }
      });
    },
  },
  extraReducers: {
    [getAllPlaces.pending]: (state, action) => {
      state.isLoading = true;
    },
    [getAllPlaces.fulfilled]: (state, action) => {
      state.places = action.payload.places;
      state.isLoading = false;
    },
    [getAllPlaces.rejected]: (state, action) => {
      state.isLoading = false;
    },
    [createPlace.pending]: (state, action) => {
      state.isLoading = true;
    },
    [createPlace.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.places.push(action.payload.place);
    },
    [createPlace.rejected]: (state, action) => {
      state.error = action.payload;
      state.isLoading = false;
    },
    [getTags.fulfilled]: (state, action) => {
      state.tags = action.payload.tags;
    },
    [getTags.rejected]: (state, action) => {
      state.tags = [];
    },

    [getProvinces.fulfilled]: (state, action) => {
      state.provinces = action.payload.provinces;
    },
    [getProvinces.rejected]: (state, action) => {
      state.provinces = [];
    },

    [getCategories.fulfilled]: (state, action) => {
      state.categories = action.payload.categories;
      state.isLoading = false;
    },
    [getCategories.rejected]: (state, action) => {
      state.categories = [];
    },
    [updateImagesInPlace.fulfilled]: (state, action) => {
      state.places.forEach((e) => {
        if (e.id === action.payload.place.id) {
          e.images = action.payload.place.images;
        }
      });
      state.isLoading = false;
    },
    [updateImagesInPlace.pending]: (state, action) => {
      state.isLoading = true;
    },
    [updateImagesInPlace.rejected]: (state, action) => {
      state.error = ERROR_UN_SAVING;
      state.isLoading = false;
    },
    [updatePlace.pending]: (state, action) => {
      state.isLoading = true;
    },
    [updatePlace.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.places.forEach((e) => {
        if (e.id === action.payload.place.id) {
          e.status = action.payload.place.status;
          e.description = action.payload.place.description;
          e.address = action.payload.place.address;
          e.name = action.payload.place.name;
          e.longtitude = action.payload.place.longtitude;
          e.lattitude = action.payload.place.lattitude;
          e.tags = action.payload.place.tags;
          e.rate = action.payload.place.rate;
          e.province = action.payload.place.province;
          e.weight = action.payload.place.weight;
          e.category = action.payload.place.category;
          e.status = action.payload.place.status;
          e.closeTime = action.payload.place.closeTime;
          e.openTime = action.payload.place.openTime;
          e.price.start = action.payload.place.price.start;
          e.price.end = action.payload.place.price.end;
          e.rateVoting = action.payload.place.rateVoting;
          e.popular = action.payload.place.popular;
        }
      });
    },
    [updatePlace.rejected]: (state, action) => {
      state.isLoading = false;
    },
  },
});
// Action creators are generated for each case reducer function
export const { startLoadDataPlace, setCurrentPlace } = placeSlice.actions;
export default placeSlice.reducer;
