import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
// import {baseUrl} from '../utils/constants';

//TODO: Change primary base url
const url = "https://vntravel-api.herokuapp.com/app/api/v1";
/**
 * Get all unhidden reports
 */
export const loadReports = createAsyncThunk(
  "reports/load",
  async (params, { rejectWithValue }) => {
    try {
      const response = await axios.get(`${url}/reports`);
      if (response.data.success) {
        return {
          reports: response.data.reports,
        };
      }
    } catch (error) {
      console.log(error.message);
      return rejectWithValue("Some error happen.Please try again!");
    }
  }
);

/**
 * Update report
 */
export const updateReport = createAsyncThunk(
  "reports/update",
  async (report, { rejectWithValue }) => {
    try {
      const response = await axios.put(`${url}/reports/${report.id}`, report);
      if (response.data.success) {
        return {
          report: response.data.report,
        };
      }
    } catch (error) {
      console.log(error.message);
      return rejectWithValue("Some error happen.Please try again!");
    }
  }
);

export const updateReviewStatus = createAsyncThunk(
  "reports/review/status",
  async (params, { rejectWithValue }) => {
    const body = {
      isHidden: params.status,
    };
    try {
      const response = await axios.put(
        `${url}/reviews/delete/${params.reviewId}`,
        body
      );
      if (response.data.success) {
        return {
          review: response.data.review,
          reportId: params.reportId,
        };
      }
    } catch (error) {
      console.log(error.message);
      return rejectWithValue("Some error happen.Please try again!");
    }
  }
);

const initialState = {
  loading: false,
  reports: [],
  error: "",
  isUpdating: false,
};

const reportSlice = createSlice({
  name: "report",
  initialState: initialState,
  reducers: {},
  extraReducers: {
    //get all reports
    [loadReports.rejected]: (state) => {
      state.loading = false;
      state.reports = [];
      state.error = "Some wrong here. Please try again!";
    },
    [loadReports.pending]: (state) => {
      state.loading = true;
      state.reports = [];
      state.error = "";
    },
    [loadReports.fulfilled]: (state, action) => {
      state.loading = false;
      // const list = action.payload.reports;
      state.reports = action.payload.reports;
      // list.sort((a, b) => (b.createdAt - a.createdAt ? 1 : -1));
      state.error = "";
    },

    [updateReport.fulfilled]: (state, action) => {
      state.isUpdating = false;
      state.error = "";
      if (action.payload.report.isHidden === true) {
        const reports_ = [];
        state.reports.forEach((report) => {
          if (report.id !== action.payload.report.id) reports_.push(report);
        });

        state.reports = reports_;
      } else {
        state.reports.forEach((report) => {
          if (report.id === action.payload.report.id) {
            report.isSeen = action.payload.report.isSeen;
            report.isHidden = action.payload.report.isHidden;
          }
        });
      }
    },
    [updateReport.pendding]: (state) => {
      state.isUpdating = true;
      state.error = "";
    },
    [updateReport.rejected]: (state) => {
      state.error = "Update report failed.Please try again!";
      state.isUpdating = false;
    },
    [updateReviewStatus.pending]: (state) => {
      state.isUpdating = true;
      state.error = "";
    },
    [updateReviewStatus.rejected]: (state) => {
      state.error = "Update reivew status failed. Please try again.";
      state.isUpdating = false;
    },
    [updateReviewStatus.fulfilled]: (state, action) => {
      state.reports.forEach((report) => {
        if (report.id === action.payload.reportId) {
          report.review = action.payload.review;
        }
      });
      state.isUpdating = false;
      state.error = "";
    },
  },
});

const { reducer: reportReducer } = reportSlice;
export default reportReducer;
