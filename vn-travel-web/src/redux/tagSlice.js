import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { baseUrl } from "../utils/constants";

//get all tags
export const loadTags = createAsyncThunk(
  "tags/load",
  async (params, { rejectWithValue }) => {
    try {
      const response = await axios.get(`${baseUrl}/tags`);
      if (response.data.success) {
        return {
          isLoading: false,
          tags: response.data.tags,
        };
      }
    } catch (error) {
      return rejectWithValue("Some error happen.Please try again!");
    }
  }
);

export const deleteTag = createAsyncThunk(
  "tags/delete",
  async (params, { rejectWithValue }) => {
    try {
      const response = await axios.delete(`${baseUrl}/tags/${params}`);
      if (response.data.success) {
        return {
          isLoading: false,
          tag: response.data.tag,
        };
      }
    } catch (error) {
      return rejectWithValue("Some error happen.Please try again!");
    }
  }
);

/**
 * Create Tag
 */
export const createTag = createAsyncThunk(
  "tags/create",
  async (tag, { rejectWithValue }) => {
    try {
      const response = await axios.post(`${baseUrl}/tags`, tag);
      if (response.data.success) {
        return {
          tag: response.data.tag,
        };
      }
    } catch (error) {
      return rejectWithValue("Some error happen.Please try again!");
    }
  }
);

//update tags
export const updateTag = createAsyncThunk(
    "tags/update",
    async (tag, { rejectWithValue }) => {
        try {
            const response = await axios.put(`${baseUrl}/tags/${tag.id}`, tag);
            if (response.data.success) {
                return {
                    isLoading: false,
                    tag: response.data.tag
                }
            }
        } catch (error) {
            console.log(error.message);
            return rejectWithValue("Some error happen.Please try again!")
        }
    }
  
);

const initialState = {
  loading: true,
  tags: [],
  updating: false,
  error: "",
};
const tagSlice = createSlice({
  name: "tag",
  initialState: initialState,
  reducers: {},
  extraReducers: {
    [loadTags.pending]: (state) => {
      state.loading = true;
      state.error = "";
    },
    [loadTags.rejected]: (state) => {
      state.loading = false;
      state.tags = [];
      state.error = "Some wrong here. Please try again! ";
    },
    [loadTags.fulfilled]: (state, action) => {
      state.loading = false;
      state.tags = action.payload.tags;
      state.error = "";
    },
    [deleteTag.pending]: (state) => {
      state.updating = true;
      state.error = "";
    },
    [deleteTag.rejected]: (state, action) => {
      state.updating = false;
      state.error = "Delete failed.Please try again!";
    },
    [deleteTag.fulfilled]: (state, action) => {
      state.updating = false;
      state.tags.filter((tag) => tag.id !== action.payload.tag.id);
      state.error = "";
    },
    [updateTag.pending]: (state) => {
      state.error = "";
      state.updating = true;
    },
    [updateTag.rejected]: (state) => {
      state.error = "Update failed.Please try again!";
      state.updating = false;
    },
    [updateTag.fulfilled]: (state, action) => {
      state.error = "";
      state.updating = false;
      state.tags.forEach((tag) => {
        if (tag.id === action.payload.tag.id) {
          tag.updatedAt = action.payload.tag.updatedAt;
          tag.isHidden = action.payload.tag.isHidden;
          tag.name = action.payload.tag.name;
        }
      });
    },
    [createTag.pending]: (state) => {
      state.error = "";
      state.updating = true;
    },
    [createTag.rejected]: (state) => {
      state.error = "Create fail. Please try again.";
      state.updating = false;
    },
    [createTag.fulfilled]: (state, action) => {
      state.error = "";
      state.tags.push(action.payload.tag);
      state.updating = false;
    },
  },
});

const { reducer: tagReducer } = tagSlice;

export default tagReducer;
