import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import jwt_decode from "jwt-decode";
import {
  ACCESS_TOKEN,
  baseUrl,
  CUR_CODE,
  CUR_EMAIL,
  CUR_EXP,
  CUR_IAT,
  CUR_IMAGE,
  CUR_NAME,
} from "../utils/constants";
import setAuthToken, { clearAuthToken } from "../utils/setAuthToken";

const saveSection = (data) => {
  console.log("user ", data)
  localStorage.setItem(CUR_EMAIL, data.email);
  localStorage.setItem(CUR_IMAGE, data.image);
  localStorage.setItem(CUR_NAME, data.fullName);
  // localStorage.setItem(CUR_IAT, data.iat);
  // localStorage.setItem(CUR_EXP, data.exp);
  // localStorage.setItem(CUR_CODE, data.azp);
};

//Auto login when token still valid
export const loadUser = createAsyncThunk(
  "user/getUser",
  async (params, { rejectWithValue }) => {
    //thunkAPI.dispath(...)
    //     const currentUser = await userApi.getMe();
    //   return currentUser;
    try {
      if (localStorage[ACCESS_TOKEN]) {
        setAuthToken(localStorage[ACCESS_TOKEN]);

        const response = await axios.get(`${baseUrl}/auths/verify`);
        if (response.data.success) {
          return {
            isAuthenticated: true,
            user: response.data.user,
          };
        } else {
          return {
            isAuthenticated: false,
            user: null,
          };
        }
      } else {
        return {
          isAuthenticated: false,
          user: null,
        };
      }
    } catch (error) {
      localStorage.clear();
      console.log(error.message);
      return rejectWithValue(error.message);
    }
  }
);
//Login by email and password
export const loginUser = createAsyncThunk(
  "user/logins",
  async (params, { rejectWithValue }) => {
    const userForm = params;
    try {
      await axios
        .post(`${baseUrl}/auths/login/users?userRole=false`, userForm)
        .then((response) => {
          //Set token for axio
          setAuthToken(response.data.token);
          //Save token
          localStorage.setItem(ACCESS_TOKEN, response.data.token);
          //Decode user in token
          let user = response.data.user;
         
          saveSection(user);
          return {
            isAuthenticated: true,
            user: user,
          };
        });
    } catch (error) {
      // console.log(error.response.status);
      // console.log(error.response.data);
      // console.log(error.config);

      // if (error.response.data.status == 401) {
      return rejectWithValue(error.response.data.message);
      // } else {
      //   return rejectWithValue(ERROR_MESSAGE);
      // }
    }
  }
);

export const loginAdminWithGoogle = createAsyncThunk(
  "admin/googleLogin",
  async (token, { rejectWithValue }) => {
    try {
      setAuthToken(token);
      await axios
        .post(`${baseUrl}/auths/login/google?userRole=false`, {})
        .then((response) => {
          //Set token for axio
          setAuthToken(response.data.token);
          //Save token
          let token = response.data.token.split(" ")[1];
          localStorage.setItem(ACCESS_TOKEN, token);

          //Decode user in token
          let data = jwt_decode(response.data.token);
          saveSection(data);
          return {
            isAuthenticated: true,
            user: {
              email: data.email,
              username: data.name,
              imageUrl: data.picture,
            },
          };
        });
    } catch (error) {
      return rejectWithValue("Error happen. Try again");
    }
  }
);

const initialState = {
  authLoading: true,
  isAuthenticated: false,
  user: null,
  error: "",
  isOpened: true,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setToggleSideBar: (state) => {
      state.isOpened = !state.isOpened;
    },
    setCloseSideBar: (state) => {
      state.isOpened = false;
    },
    logOut: (state) => {
      window.localStorage.clear();
      clearAuthToken();
      state.isAuthenticated = false;
    },
  },
  extraReducers: {
    [loadUser.pending]: (state, actions) => {
      state.authLoading = true;
    },
    [loadUser.fulfilled]: (state, action) => {
      state.authLoading = false;
      state.isAuthenticated = action.payload.isAuthenticated;
      // state.user = action.payload.user;
    },
    [loadUser.rejected]: (state, actions) => {
      state.authLoading = false;
      state.isAuthenticated = false;
      state.user = null;
    },
    [loginUser.fulfilled]: (state, action) => {
      state.authLoading = false;
      state.isAuthenticated = true;
      //  state.user = action.payload.user;
    },
    [loginUser.pending]: (state, actions) => {
      state.authLoading = true;
    },
    [loginUser.rejected]: (state, actions) => {
      state.authLoading = false;
      state.isAuthenticated = false;
      state.error = actions.payload;
      state.user = null;
    },
    [loginAdminWithGoogle.pending]: (state, actions) => {
      state.authLoading = true;
    },
    [loginAdminWithGoogle.rejected]: (state, actions) => {
      state.authLoading = false;
      state.isAuthenticated = false;
      state.user = null;
    },
    [loginAdminWithGoogle.fulfilled]: (state, action) => {
      state.authLoading = false;
      state.isAuthenticated = true;
      // state.user = action.payload.user;
    },
  },
});

export const { setToggleSideBar, setCloseSideBar, logOut } = authSlice.actions;
export default authSlice.reducer;
