import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { baseUrl } from "../utils/constants";

const initialState = {
  users: [],
  isLoading: true,
  error: "",
};

export const loadUsers = createAsyncThunk(
  "users/load",
  async (params, { rejectWithValue }) => {
    try {
      const response = await axios.get(`${baseUrl}/users`);
      if (response.data.success) {
        return {
          isLoading: false,
          users: response.data.users,
        };
      }
    } catch (error) {
      console.log(error.message);
      return rejectWithValue("Some error happen.Please try again!");
    }
  }
);

export const updateUserStatus = createAsyncThunk(
  "users/status",
  async (params, { rejectWithValue }) => {
    try {
      const response = await axios.put(
        `${baseUrl}/users/${params.id}/status`,
        params
      );

      if (response.data.success) {
        return {
          success: true,
          user: response.data.user,
        };
      }
    } catch (error) {
      console.log(error.response.data.message);
      return rejectWithValue(error.response.data.message);
    }
  }
);

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers: {
    [loadUsers.pending]: (state) => {
      state.isLoading = true;
      state.error = "";
    },
    [loadUsers.rejected]: (state) => {
      state.isLoading = false;
      state.error = "Some wrong here. Please try again!";
    },
    [loadUsers.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.users = action.payload.users;
    },
    [updateUserStatus.pending]: (state) => {
      state.isLoading = true;
    },
    [updateUserStatus.rejected]: (state) => {
      state.isLoading = false;
    },
    [updateUserStatus.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.users.forEach((element) => {
        if (element.id === action.payload.user.id) {
          element.isHidden = action.payload.user.isHidden;
        }
      });
    },
  },
});

export const {} = userSlice.actions;
export default userSlice.reducer;
