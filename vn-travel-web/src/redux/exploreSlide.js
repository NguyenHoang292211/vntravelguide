import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { baseUrl } from "../utils/constants";

const initialState = {
  explores: [],
  error: "",
  isLoading: false,
  updating: false,
};

/**
 * * Get all explore
 */
export const loadExplores = createAsyncThunk(
  "explores/getExplores",
  async (params, { rejectWithValue }) => {
    try {
      const response = await axios.get(`${baseUrl}/explorers/private`);
      if (response.data.success) {
        return {
          isLoading: false,
          explores: response.data.explorers,
        };
      }
    } catch (error) {
      console.log(error.message);
      return rejectWithValue("Some error happen. Place try again!");
    }
  }
);

/**
 * * Create Explore
 */
export const createExplore = createAsyncThunk(
  "explores/create",
  async (explore, { rejectWithValue }) => {
    try {
      const response = await axios.post(`${baseUrl}/explorers`, explore);
      if (response.data.success) {
        return {
          explore: response.data.explorer,
          success: true,
        };
      }
    } catch (error) {
      console.log(error.message);
      return rejectWithValue("Some error happen. Place try again!");
    }
  }
);

/**
 * * Update explore
 */
export const updateExplore = createAsyncThunk(
  "explores/update",
  async (explore, { rejectWithValue }) => {
    try {
      const response = await axios.put(
        `${baseUrl}/explorers/${explore.id}`,
        explore
      );
      if (response.data.success) {
        return {
          success: true,
          isLoading: false,
          explore: response.data.explorer,
        };
      }
    } catch (error) {
      console.log(error.message);
      return rejectWithValue("Some error happen. Place try again!");
    }
  }
);

const exploreSlide = createSlice({
  name: "sliceName",
  initialState,
  reducers: {},
  extraReducers: {
    [loadExplores.pending]: (state) => {
      state.isLoading = true;
      state.error = "";
    },
    [loadExplores.rejected]: (state) => {
      state.isLoading = false;
      state.explores = [];
      state.error = "Some wrong here. Please try again!";
    },
    [loadExplores.fulfilled]: (state, actions) => {
      state.isLoading = false;
      state.error = "";
      state.explores = actions.payload.explores;
    },
    [updateExplore.pending]: (state) => {
      state.error = "";
      state.updating = true;
    },
    [updateExplore.rejected]: (state) => {
      state.error = "Update failed.Please try again!";
      state.updating = false;
    },
    [updateExplore.fulfilled]: (state, action) => {
      state.error = "";
      state.updating = false;
      state.explores.forEach((mExplore) => {
        if (mExplore.id === action.payload.explore.id) {
          mExplore.title = action.payload.explore.title;
          mExplore.description = action.payload.explore.description;
          mExplore.banner = action.payload.explore.banner;
          mExplore.tags = action.payload.explore.tags;
          mExplore.isHidden = action.payload.explore.isHidden;
        }
      });
    },
    [createExplore.pending]: (state) => {
      state.error = "";
      state.updating = true;
    },
    [createExplore.rejected]: (state) => {
      state.error = "Create fail. Please try again.";
      state.updating = false;
    },
    [createExplore.fulfilled]: (state, action) => {
      state.error = "";
      state.explores.push(action.payload.explore);
      state.updating = false;
    },
  },
});

export const {} = exploreSlide.actions;
export default exploreSlide.reducer;
