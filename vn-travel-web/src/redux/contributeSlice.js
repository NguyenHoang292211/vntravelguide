import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
// import { baseUrl } from "../utils/constants";

/**
 * Load all contributes
 */
const url = "https://vntravel-api.herokuapp.com/app/api/v1";
export const loadContributes = createAsyncThunk(
  "contributes/load",
  async (params, { rejectWithValue }) => {
    try {
      const response = await axios.get(`${url}/contributes`);
      if (response.data.success) {
        return {
          contributes: response.data.contributes,
        };
      }
    } catch (error) {
      console.log(error.message);
      return rejectWithValue("Some error happen.Please try again!");
    }
  }
);

export const updateContribute = createAsyncThunk(
  "contributes/update",
  async (params, { rejectWithValue }) => {
    try {
      const response = await axios.put(
        `${url}/contributes/${params.id}`,
        params
      );
      if (response.data.success) {
        return {
          contribute: response.data.contribute,
        };
      }
    } catch (error) {}
  }
);

const initialState = {
  loading: false,
  contributes: [],
  error: "",
  isUpdating: false,
};

const contributeSlice = createSlice({
  name: "contribute",
  initialState: initialState,
  procedures: {},
  extraReducers: {
    [loadContributes.pending]: (state) => {
      state.loading = true;
    },
    [loadContributes.rejected]: (state) => {
      state.loading = false;
      state.error = "Load contributes failed. Please reload page to try again.";
    },
    [loadContributes.fulfilled]: (state, action) => {
      state.loading = false;
      state.contributes = action.payload.contributes;
      state.error = "";
    },
    [updateContribute.fulfilled]: (state, action) => {
      state.isUpdating = false;
      state.error = "";
      if (action.payload.contribute.isHidden === true) {
        const contributes_ = [];
        state.contributes.forEach((contribute) => {
          if (contribute.id !== action.payload.contribute.id)
            contributes_.push(contribute);
        });

        // reports_.filter((report) => report.id !== action.payload.report.id);
        state.contributes = contributes_;
      } else {
        state.contributes.forEach((contribute) => {
          if (contribute.id === action.payload.contribute.id) {
            contribute.isSeen = action.payload.contribute.isSeen;
            contribute.isHidden = action.payload.contribute.isHidden;
          }
        });
      }
    },
    [updateContribute.pendding]: (state) => {
      state.isUpdating = true;
      state.error = "";
    },
    [updateContribute.rejected]: (state) => {
      state.error = "Update contribute failed.Please try again!";
      state.isUpdating = false;
    },
  },
});

const { reducer: contributeReducer } = contributeSlice;
export default contributeReducer;
