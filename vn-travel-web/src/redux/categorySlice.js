import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { baseUrl } from "../utils/constants";

/**
 * get all categories
 **/
export const loadCategories = createAsyncThunk(
  "categories/load",
  async (params, { rejectWithValue }) => {
    try {
      const response = await axios.get(`${baseUrl}/categories`);
      if (response.data.success) {
        return {
          isLoading: false,
          categories: response.data.categories,
        };
      }
    } catch (error) {
      console.log(error.message);
      return rejectWithValue("Some error happen.Please try again!");
    }
  }
);

/**
 * Create Category
 */
export const createCategory = createAsyncThunk(
  "categories/create",
  async (category, { rejectWithValue }) => {
    try {
      const response = await axios.post(`${baseUrl}/categories`, category);
      if (response.data.success) {
        return {
          category: response.data.category,
        };
      }
    } catch (error) {
      return rejectWithValue("Some error happen.Please try again!");
    }
  }
);

//update category
export const updateCategory = createAsyncThunk(
  "categories/update",
  async (category, { rejectWithValue }) => {
    try {
      const response = await axios.put(
        `${baseUrl}/categories/${category.id}`,
        category
      );
      if (response.data.success) {
        return {
          isLoading: false,
          category: response.data.category,
        };
      }
    } catch (error) {
      console.log(error.message);
      return rejectWithValue("Some error happen.Please try again!");
    }
  }
);

const initialState = {
  loading: false,
  categories: [],
  error: "",
  updating: false,
};
const categorySlice = createSlice({
  name: "category",
  initialState: initialState,
  reducers: {},
  extraReducers: {
    [loadCategories.pending]: (state) => {
      state.loading = true;
      state.error = "";
    },
    [loadCategories.rejected]: (state, action) => {
      state.loading = false;
      state.categories = [];
      state.error = "Some wrong here. Please try again!";
    },
    [loadCategories.fulfilled]: (state, action) => {
      state.loading = false;
      state.categories = action.payload.categories;
      state.error = "";
    },
    [updateCategory.pending]: (state) => {
      state.error = "";
      state.updating = true;
    },
    [updateCategory.rejected]: (state) => {
      state.error = "Update failed.Please try again!";
      state.updating = false;
    },
    [updateCategory.fulfilled]: (state, action) => {
      state.error = "";
      state.updating = false;
      state.categories.forEach((category) => {
        if (category.id === action.payload.category.id) {
          category.updatedAt = action.payload.category.updatedAt;
          category.isHidden = action.payload.category.isHidden;
          category.name = action.payload.category.name;
          category.color = action.payload.category.color;
        }
      });
    },
    [createCategory.pending]: (state) => {
      state.error = "";
      state.updating = true;
    },
    [createCategory.rejected]: (state) => {
      state.error = "Create fail. Please try again.";
      state.updating = false;
    },
    [createCategory.fulfilled]: (state, action) => {
      state.error = "";
      state.categories.push(action.payload.category);
      state.updating = false;
    },
  },
});

const { reducer: categoryReducer } = categorySlice;

export default categoryReducer;
