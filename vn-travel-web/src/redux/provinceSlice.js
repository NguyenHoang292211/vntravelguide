import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { baseUrl } from "../utils/constants";
//get all provinces
export const loadProvinces = createAsyncThunk(
  "provinces/load",
  async (params, { rejectWithValue }) => {
    try {
      const response = await axios.get(`${baseUrl}/provinces`);
      if (response.data.success) {
        return {
          isLoading: false,
          provinces: response.data.provinces,
        };
      }
    } catch (error) {
      console.log(error.message);
      return rejectWithValue("Some error happen.Please try again!");
    }
  }
);

export const createProvince = createAsyncThunk(
  "provinces/create",
  async (province, { rejectWithValue }) => {
    try {
      const response = await axios.post(`${baseUrl}/provinces`, province);
      console.log(response);
      if (response.data.success)
        return {
          province: response.data.province,
        };
    } catch (error) {
      console.log(error.message);
      return rejectWithValue("Some error happen.Please try again!");
    }
  }
);

export const deleteProvince = createAsyncThunk(
  "provinces/delete",
  async (id, { rejectWithValue }) => {
    try {
      const response = await axios.delete(`${baseUrl}/provinces/${id}`);

      if (response.data.success) return { province: response.data.province };
    } catch (error) {
      console.log(error.message);
      return rejectWithValue("Some error happen.Please try again!");
    }
  }
);

export const updateProvince = createAsyncThunk(
  "provinces/update",
  async (province, { rejectWithValue }) => {
    try {
      const response = await axios.put(
        `${baseUrl}/provinces/${province.id}`,
        province
      );
      if (response.data.success)
        return {
          province: response.data.province,
        };
    } catch (error) {
      console.log(error.message);
      return rejectWithValue("Some error happen.Please try again!");
    }
  }
);

const initialState = {
  loading: false,
  provinces: [],
  error: "",
  updating: false,
};

const provinceSlice = createSlice({
  name: "province",
  initialState: initialState,
  reducers: {},
  extraReducers: {
    /**
     * Get all provinces
     **/
    [loadProvinces.pending]: (state) => {
      state.loading = true;
      state.error = "";
    },
    [loadProvinces.rejected]: (state) => {
      state.loading = false;
      state.provinces = [];
      state.error = "Some wrong here. Please try again!";
    },
    [loadProvinces.fulfilled]: (state, action) => {
      state.loading = false;
      state.provinces = action.payload.provinces;
      state.error = "";
    },
    /**
     * Create new province
     **/
    [createProvince.pending]: (state) => {
      state.error = "";
      state.updating = true;
    },
    [createProvince.rejected]: (state) => {
      state.provinces = [];
      state.error = "Some wrong here. Please try again!";
      state.updating = false;
    },
    [createProvince.fulfilled]: (state, action) => {
      state.provinces.push(action.payload.province);
      state.error = "";
      state.updating = false;
    },
    /**
     * Delete province
     * */

    [deleteProvince.pending]: (state) => {
      state.updating = true;
      state.error = "";
    },
    [deleteProvince.rejected]: (state) => {
      state.updating = false;
      state.error = "Delete failed. Please try again!";
    },
    [deleteProvince.fulfilled]: (state, action) => {
      state.updating = false;
      state.error = "";
      state.provinces.filter(
        (province) => province.id !== action.payload.province.id
      );
    },

    /**
     * Update province
     **/
    [updateProvince.pending]: (state) => {
      state.error = "";
      state.updating = true;
    },
    [updateProvince.rejected]: (state) => {
      state.provinces = [];
      state.error = "Some wrong here. Please try again!";
      state.updating = false;
    },
    [updateProvince.fulfilled]: (state, action) => {
      state.provinces.forEach((province) => {
        if (province.id === action.payload.province.id) {
          province.name = action.payload.province.name;
          province.color = action.payload.province.color;
          province.updatedAt = action.payload.province.updatedAt;
          province.isHidden = action.payload.province.isHidden;
          province.image = action.payload.province.image;
        }
      });
      state.error = "";
      state.updating = false;
    },
  },
});
const { reducer: provinceReducer } = provinceSlice;

export default provinceReducer;
