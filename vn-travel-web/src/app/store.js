import { configureStore } from "@reduxjs/toolkit";
import authReducer from "../redux/authSlice";
import placeReducer from "../redux/placeSlice";
import categoryReducer from "../redux/categorySlice";
import provinceReducer from "../redux/provinceSlice";
import tagReducer from "../redux/tagSlice";
import userReducer from "../redux/userSlice";
import reportReducer from "../redux/reportSlice";
import contributeReducer from "../redux/contributeSlice";
import exploreReducer from "../redux/exploreSlide"
export default configureStore({
  reducer: {
    auth: authReducer,
    province: provinceReducer,
    tag: tagReducer,
    category: categoryReducer,
    place: placeReducer,
    user: userReducer,
    report: reportReducer,
    contribute: contributeReducer,
    explore:exploreReducer
  },
});
