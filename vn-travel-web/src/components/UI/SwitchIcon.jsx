import { FormControlLabel, Switch, withStyles } from "@material-ui/core";
import React from "react";

const IOSSwitch = withStyles((theme) => ({
  root: {
    width: 42,
    height: 26,
    padding: 0,
    margin: theme.spacing(1),
  },
  switchBase: {
    padding: 1,
    "&$checked": {
      transform: "translateX(16px)",
      color: theme.palette.common.white,
      "& + $track": {
        backgroundColor: "#0d47a1",
        opacity: 1,
        border: "none",
      },
    },
    "&$focusVisible $thumb": {
      color: "#E0E0E0",
      border: "6px solid #fff",
    },
  },
  thumb: {
    width: 24,
    height: 24,
  },
  track: {
    borderRadius: 26 / 2,
    border: `1px solid ${theme.palette.grey[400]}`,
    backgroundColor: theme.palette.grey[50],
    opacity: 1,
    transition: theme.transitions.create(["background-color", "border"]),
  },
  checked: {},
  focusVisible: {},
}))(({ classes, ...props }) => {
  return (
    <Switch
      focusVisibleClassName={classes.focusVisible}
      disableRipple
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked,
      }}
      {...props}
    />
  );
});

const SwitchIcon = (props) => {
  const { name, checked, value, label, onChange, ...other } = props;
  
  const convertToDefEventPara = (name, value) => ({
    target: {
        name, value
    }
})
  return (
    <FormControlLabel
      control={
        <IOSSwitch
         
          name={name}
          value={value}
          checked={value}
          onChange={e => onChange(convertToDefEventPara(name, e.target.checked))}
        />
      }
      label={label}
      {...other}
    />
  );
};

export default SwitchIcon;
