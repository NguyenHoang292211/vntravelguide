import { makeStyles, TextField } from "@material-ui/core";
import React from "react";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
  },

}));

const TimePicker = (props) => {
  const classes = useStyles();
  const { name, label, value, onChange } = props;
  return (
    <form className={classes.container} noValidate>
      <TextField
        label={label} variant="outlined"
        type="time"
       name={name}
        value={value}
        onChange={onChange}
        className={classes.textField}
        InputLabelProps={{
          shrink: true,
        }}
   
      ></TextField>
    </form>
  );
};

export default TimePicker;
