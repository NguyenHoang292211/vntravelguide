import {
  FormControl, FormHelperText, InputLabel, makeStyles, MenuItem, Select as MuiSelect
} from "@material-ui/core";
import React from "react";

const useStyles = makeStyles((theme) => ({
  formControl: {
    minWidth: 150,
  },
}));

const Select = (props) => {
  const classes = useStyles();
  const { name, label, value, error = null, onChange, options } = props;
  return (
    <FormControl className={classes.formControl} variant="outlined" {...(error && { error: true })}>
      <InputLabel>{label}</InputLabel>
      <MuiSelect label={label} name={name} value={value} onChange={onChange}>
        {options.map((item) => (
          <MenuItem key={item.id} value={item.id}>
            {item.name}
          </MenuItem>
        ))}
      </MuiSelect>
      {error && <FormHelperText>{error}</FormHelperText>}
    </FormControl>
  );
};

export default Select;
