import { Box } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { Rating } from '@material-ui/lab';
import React from 'react';
const Rate = props => {
    const { name, label, value,onChange, ...other } = props;
    return (
      <Box component="fieldset" md={3} borderColor="transparent"
      >
         <Typography component="legend" style={{marginLeft:"1rem"}}>{label}</Typography>
          <Rating 
            name={name}
            defaultValue={value}
            onChange={onChange}
            {...other}
          ></Rating>
      </Box>
    )
}


export default Rate
