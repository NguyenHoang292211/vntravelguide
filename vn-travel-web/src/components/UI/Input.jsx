import { TextField } from "@material-ui/core";
import React from "react";

const Input = (props) => {
  const { name, label, value, error = null, onChange, ...other } = props;
  return (
      <TextField
        variant="outlined"
        label={label}
        name={name}
        defaultValue={value}
        onChange={onChange}
        {...other}
        {...(error && { error: true, helperText: error })}
        InputLabelProps={{
          shrink: true,
        }}
        value={value}
      />
  );
};

export default Input;
