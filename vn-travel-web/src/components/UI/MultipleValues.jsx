import React from "react";
import { makeStyles, TextField } from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(3),
    },
  },
}));

const MultipleValues = (props) => {
  const classes = useStyles();
  const {
    name,
    label,
    value,
    onChange,
    optionsList,
    defaultValue,
    ...other
  } = props;
  const convertToDefEventPara = (name, value) => ({
    target: {
      name,
      value,
    },
  });
  return (
    <div className={classes.root}>
      <Autocomplete
        multiple
        options={optionsList}
        getOptionLabel={(option) => option.name}
        defaultValue={defaultValue}
        filterSelectedOptions
        onChange={(event, value) =>{
          onChange(
            convertToDefEventPara(
              name,
              value.map((item) => item.id?item.id:item._id )
            )
          )
        }
        }
        renderInput={(params) => (
          <TextField {...params} variant="outlined" label={label} />
        )}
        {...other}
      ></Autocomplete>
    </div>
  );
};

export default MultipleValues;
