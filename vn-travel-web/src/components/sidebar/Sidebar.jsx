import { makeStyles } from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import sidebar_item from "../../assets/JsonData/sidebar_routes.json";
import "./sidebar.css";
const useStyles = makeStyles((theme) => ({
  title: {
    transition: "all .5s ease",
    display: (props) => (props.open ? "flex" : "none"),
    fontWeight: 600,
  },
  nav: {
    transition: "all 1.5s ease",
    [theme.breakpoints.down("sm")]: {
      width: (props) => (props.open ? "auto" : "0px"),
    },
  },
  spanTransition: {
    transition: "all 1s ease",
    width: (props) => (props.open ? "112px" : "0px"),
    [theme.breakpoints.down("sm")]: {
      width: "112",
    },
  },
}));

const SidebarItem = (props) => {
  const active = props.active ? "active" : "";
  const open = useSelector((state) => state.auth.isOpened);
  const classes = useStyles({ open });

  return (
    <div className={`sidebar__item `}>
      <div className={`sidebar__item-inner ${active}`}>
        <i className={props.icon} style={{ fontSize: 23 }}></i>
        <div className={classes.spanTransition}>
          <span className={`title`}>{props.title}</span>
        </div>
      </div>
    </div>
  );
};

const Sidebar = (props) => {
  const activeItem = sidebar_item.findIndex((item) =>
    props.location.pathname.includes(`${item.route}`)
  );
  const open = useSelector((state) => state.auth.isOpened);
  const classes = useStyles({ open });

  return (
    <div className={`sidebar ${classes.nav}`}>
      {sidebar_item.map((item, index) => (
        <Link to={`/admin${item.route}`} key={index}>
          <SidebarItem
            title={item.display_name}
            icon={item.icon}
            active={index === activeItem}
          />
        </Link>
      ))}
    </div>
  );
};

export default Sidebar;
