import React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Slide from "@mui/material/Slide";
import { CircularProgress } from "@mui/material";
import { makeStyles } from "@material-ui/core";
import { height } from "@mui/system";
import { ClassNames } from "@emotion/react";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
  spinner: {
    width: "12px",
    height: "12px",
    fontSize:"12px"
  },
}));
const DialogConfirm = (props) => {
  const classes = useStyles();
  const { id, title, open, handleClose, isLoading, message, handleConfirm } =
    props;

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      onClose={handleClose}
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogTitle>{title ? title : "Confirmation"}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-slide-description">
          {message}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={() => handleConfirm(id)}>
          {" "}
          {isLoading ? (
            <CircularProgress md-12 className={ClassNames.spinner} />
          ) : (
            "Confirm"
          )}{" "}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DialogConfirm;
