import React from "react";
import "./loading.css";
import ReactDOM from "react-dom";
const Loading = () => {
  return (
    <React.Fragment>
      {ReactDOM.createPortal(
        <div className="back-drop">
          <div className="lds-ellipsis">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>,
        document.getElementById("loading-root")
      )}
    </React.Fragment>
  );
};

export default Loading;
