import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect, Route } from "react-router";
import { loadUser } from "../redux/authSlice";
import Loading from "./loading/Loading";

const ProtectedRoute = ({ component: Component, ...rest }) => {
  const dispatch = useDispatch();
  const { authLoading, isAuthenticated } = useSelector((state) => state.auth);
  const [isTryLogin, setIsTryLogin] = useState(false);
  //loadUser
  useEffect(async () => {
    const isLoadUser = await dispatch(loadUser());
    if (isLoadUser) {
      setIsTryLogin(true);
    }
    //TODO: call to get tag, categories,...
    return () => {};
  }, []);

  if (!isTryLogin) {
    return <Loading />;
  }

  if (authLoading) return <Loading />;
  return (
    <Route
      {...rest}
      render={(props) =>
        isAuthenticated ? (
          <>
            <Component {...rest} {...props} />
          </>
        ) : (
          <Redirect to="./login" />
        )
      }
    />
  );
};

export default ProtectedRoute;
