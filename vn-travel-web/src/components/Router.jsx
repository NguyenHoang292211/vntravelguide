import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import User from "../feature/User/User";
import Dashboard from "../feature/dashboard/Dashboard";
import Places from "../feature/Place/Places";
import Setting from "../feature/setting/Setting";
import Explore from "../feature/explore/Explore"
import Error from "../pages/Error"
function Router() {
  // const match= useRouteMatch();
  return (
    <Switch>
      <Route path="/admin/dashboard" exact component={Dashboard} />
      <Route exact path={`/admin/users`} component={User} />
      <Route path={`/admin/users/:userId/info`} component={User} />

      <Route path={`/admin/places`} component={Places} />
      <Route path={`/admin/settings`} component={Setting} />
      <Route path={`/admin/explores`} component={Explore} />
      <Route path={`/admin/`} component={Error} />
      {/* <Route path="" render={()=>(<Redirect to="/" />)} /> */}

    </Switch>
  );
}

export default Router;
