import React from 'react'
import  './itemCard.css'
const ItemCard = (props) => {
    return (
        <div className='card-content'>
          <div className='card-symbol'>
          T
          </div>
        <textarea
          name="titleList"
          id="titleList"
          cols="15"
          rows="1"
          value={props.content}
        ></textarea>
        <div className="add-item">
          <i className="bx bxs-edit-alt"></i>
        </div>
      </div>
    )
}

export default ItemCard
