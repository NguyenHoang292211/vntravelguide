import {
  AppBar,
  Avatar,
  ButtonBase,
  makeStyles,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
  useMediaQuery,
  useTheme
} from "@material-ui/core";
import { blue } from "@material-ui/core/colors";
import LogoutIcon from "@mui/icons-material/Logout";
import MenuIcon from "@mui/icons-material/Menu";
import React, { useEffect, useState } from "react";
import { useGoogleLogout } from "react-google-login";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { logOut, setCloseSideBar, setToggleSideBar } from "../../redux/authSlice";
import { CUR_IMAGE, CUR_NAME } from "../../utils/constants";
const useStyles = makeStyles((theme) => ({
  toolbar: {
    display: "flex",
    justifyContent: "space-between",
    background: blue[400],
    color: "#fff",
  },
  header: {
    display: "flex",
    justifyContent:"center",
    alignItems:"center"
  },
  logoLg: {
    display: "none",
    width: "132px",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  name: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
    fontSize:"1rem",
    marginRight:".5rem"
  },
  headerAvatar: {
    ...theme.typography.commonAvatar,
    ...theme.typography.mediumAvatar,
    transition: "all .2s ease-in-out",
    background: blue[100],
    color: blue[400],
    "&:hover": {
      background: "#fff",
      color: blue[400],
    },
    menuIcon: {
      background: blue[100],
      color: blue[400],
      "&:hover": {
        background: "#fff",
        color: blue[400],
      },
    },
  },
}));
let start = true;
const Header = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const theme = useTheme();

  const matchDownMd = useMediaQuery(theme.breakpoints.down("md"));
  useEffect(() => {
    if (!start) {
      dispatch(setCloseSideBar());
    } else {
      start = false;
    }
  }, [matchDownMd]);
  const curr_user = {
    display_name: localStorage.getItem(CUR_NAME),
    image: localStorage.getItem(CUR_IMAGE),
  };
  const onFailure = () => {
    // alert("Error happen. Cannot finish your request");
  };
  const onLogoutSuccess = () => {
    dispatch(logOut());
    //TODO: clean history
    history.push("/")
  };
  const clientId = process.env.REACT_APP_GOOGLE_CLIENT_ID;
  const { signOut } = useGoogleLogout({
    clientId,
    onLogoutSuccess,
    onFailure,
  });

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <AppBar position="static">
      <Toolbar className={classes.toolbar}>
        <div className={classes.header}>
          <Typography variant="h6" className={classes.logoLg} component="div">
            Vn Travel
          </Typography>
          <ButtonBase sx={{ borderRadius: "12px", overflow: "hidden" }}>
            <Avatar
              variant="rounded"
              className={classes.headerAvatar}
              onClick={() => {
                dispatch(setToggleSideBar());
              }}
              color="inherit"
            >
              <MenuIcon
                className={classes.menuIcon}
                stroke={1.5}
                size="1.3rem"
              />
            </Avatar>
          </ButtonBase>
        </div>
        <div className={classes.header}>
          <Typography variant="h6" className={classes.name} component="div">
            {curr_user.display_name}
          </Typography>
          <div>
            <Avatar
              alt="Avatar"
              id="fade-button"
              aria-controls="fade-menu"
              aria-haspopup="true"
              src={
                typeof curr_user.image != "undefined"
                  ? curr_user.image
                  : "https://i.pinimg.com/474x/f6/a5/8e/f6a58ec11b788ce2088463b324f85380.jpg"
              }
              onClick={handleClick}
            />

            <Menu
              id="fade-menu"
              MenuListProps={{
                "aria-labelledby": "fade-button",
              }}
              anchorEl={anchorEl}
              open={open}
              onClose={handleClose}
              anchorOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
            >
              <MenuItem onClick={onLogoutSuccess}>
                {" "}
                <LogoutIcon /> Logout
              </MenuItem>
             
            </Menu>
          </div>
        </div>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
