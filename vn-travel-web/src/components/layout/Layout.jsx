// material-ui
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { BrowserRouter, Route } from "react-router-dom";
import { loadExplores } from "../../redux/exploreSlide";
import { getAllPlaces, getCategories, getProvinces, getTags } from "../../redux/placeSlice";
import { loadUsers } from "../../redux/userSlice";
import Routes from "../Router";
// import Customization from "../Customization";
import Sidebar from "../sidebar/Sidebar";
import Header from "./Header";
import "./layout.css";

const Layout = () => {
    const dispatch=useDispatch();
    //loadUser
    useEffect(() => {
      //TODO: call to get tag, categories,... 
      dispatch(getAllPlaces())
      dispatch(getTags())
      dispatch(getProvinces())
      dispatch(getCategories())
      dispatch(loadUsers())
      dispatch(loadExplores());
      return () => {
      }
    }, [])

  return (
    <BrowserRouter>
      <Route
        render={(props) => (
          <>
            <Header />
            <div className="layout">
              <Sidebar {...props} />
              <div className="layout__content">
                <div className="layout__content-main">
                  <Routes />
                </div>  
              </div>
            </div>
          </>
        )}
      ></Route>
    </BrowserRouter>
  );
};

export default Layout;
