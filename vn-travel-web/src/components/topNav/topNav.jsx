import React from "react";
import "./topNav.css";
import Dropdown from "../dropdown/Dropdown";
import { Link } from "react-router-dom";

import user_menu from "../../assets/JsonData/user_menus.json";
import { CUR_IMAGE, CUR_NAME } from "../../utils/constants";

//* Render Item in navbar (icon image and user name)
const renderUserToggle = (user) => (
  <div className="topnav__right-user">
    <div className="topnav__right-user__image">
      <img src={user.image} alt="" />
    </div>
    <div className="topnav__right-user__name">{user.display_name}</div>
  </div>
);
//* Way render (Icon + Content)
const renderUserMenu = (item, index) => (
  <Link to="/" key={index}>
    <div className="notification-item">
      <i className={item.icon}></i>
      <span>{item.content}</span>
    </div>
  </Link>
);
const topNav = () => {
  const curr_user = {
    display_name: localStorage.getItem(CUR_NAME),
    image: localStorage.getItem(CUR_IMAGE),
  };

  return (
    <div className="topnav">
      {/* //*Search bar */}
      <div className="topnav__search">
        {/* <input type="text" placeholder="Search here" />
        <i className="bx bx-search"></i> */}
      </div>
      {/* //* Avatar and service: logout... */}
      <div className="topnav__right">
        <div className="topnav__right-item">
          {}
          <Dropdown
            customeToggle={() => renderUserToggle(curr_user)}
            contentData={user_menu}
            renderItems={(item, index) => renderUserMenu(item, index)}
          />
        </div>
      </div>
    </div>
  );
};

export default topNav;
