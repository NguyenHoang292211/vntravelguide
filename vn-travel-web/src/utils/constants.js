export const baseUrl = "http://localhost:5000/app/api/v1";
// export const baseUrl = "https://vntravel-api.herokuapp.com/app/api/v1";
export const ACCESS_TOKEN = "accesstoken";

export const CUR_IMAGE = "cur_imageUrl";
export const CUR_EMAIL = "cur_email";
export const CUR_NAME = "cur_name";
export const CUR_IAT = "cur_iat";
export const CUR_EXP = "cur_exp";
export const CUR_CODE = "cur_c";

export const PROVINCE = "province";
export const CATEGORY = "category";
export const STATUS_ACTIVE = "active";
export const STATUS_DISABLE = "block";

export const STATUS_PUBLIC = "public";
export const STATUS_PRIVATE = "private";
export const ERROR_MESSAGE = "Error happen. Try against";
export const ERROR_UN_SAVING = "Can not saving your handle now";

export const MESSAGE_PUBLIC = "User can view this place";
export const MESSAGE_PRIVATE = "Hidden for user";
export const MESSAGE_NORMAL = "Normal";
export const MESSAGE_POPULAR = "Have event here!";

export const drawerWidth = 260;

export const TOTAL_USERS = 0;
export const DAILY_VISIT = 2;
export const TOTAL_PLACES = 1;
export const TOTAL_QUERIES = 3;

export const CREATED_AT_DEFAULT = "2021-09-10T15:56:03.000+00:00";

export const FILTER_POPULAR = "Popular";
