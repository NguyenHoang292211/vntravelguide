import React from 'react'
import errorPage from "../assets/images/404-page.svg"
import "./error.css"
function Error() {
    return (
        <div className="error__container" >
            <img src={errorPage} alt="error" className="error__image" />
        </div>
    )
}

export default Error
