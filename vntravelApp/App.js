// import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import axios from 'axios';
import React from 'react';
//Navigation
import {LogBox} from 'react-native';
import 'react-native-gesture-handler';
import {Provider} from 'react-redux';
import storeApp from './src/app/storeApp';
import Toast from './src/components/Toast';
import Route from './src/navigations/Route';
import {MenuProvider} from 'react-native-popup-menu';

const Stack = createStackNavigator();

LogBox.ignoreAllLogs(true);
const App = () => {
  // return <BottomNavigation />;

  return (
    <Provider store={storeApp}>
      <MenuProvider>
        <Route />
        <Toast />
      </MenuProvider>
    </Provider>
  );
};

export default App;

