import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  Home,
  MainLayout,
  ReviewsView,
  PlaceDetail,
  PlacesView,
  ProvincesView,
  SearchPlace,
  CreateReview,
  CreatePlan,
  PlanDetail,
  TripPlan,
  Collection,
  Plan,
  EditPlan,
  VoiceDetect,
  ExploreView,
  Map,
} from '../screens';
import {TouchableOpacity} from 'react-native-gesture-handler';
import LottieView from 'lottie-react-native';
import SearchStack from './SearchStack';
import {COLORS, FONTS, images, SIZES} from '../constants';
import Icon from 'react-native-vector-icons/FontAwesome';
import ProfileStack from './ProfileStack';
import Lang from '../language';
import {View} from 'react-native-animatable';
const Stack = createNativeStackNavigator();
const MainStack = () => {
  return (
    <Stack.Navigator
      initialRouteName="Home"
      screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="Home"
        options={{headerShown: false}}
        component={MainLayout}
      />

      <Stack.Screen
        name="DetailPlace"
        component={PlaceDetail}
        options={() => ({
          // headerTitle: route.params.item?.name,
          headerTransparent: true,
          headerTintColor: '#fff',
        })}
      />

      {/* Review navigation */}
      <Stack.Screen name="CreateReview" component={CreateReview} />
      <Stack.Screen
        options={({navigation, route}) => ({
          headerShown: true,
          title:
            route.params.item.name.length > 30
              ? route.params.item.name.slice(0, 30) + '...'
              : route.params.item.name,
          headerShadowVisible: false,
          headerTitleAlign: 'center',
          // headerBackImage: route.params.item?.images[0],
          headerTintColor: COLORS.gray,
          headerTitleStyle: {
            color: COLORS.primary,
            ...FONTS.h4,
          },
          headerLeft: () => (
            <TouchableOpacity
              style={{
                marginLeft: SIZES.base,
                height: 40,
                width: 40,
                justifyContent: 'center',
              }}
              onPress={() => navigation.goBack()}>
              <Icon name="angle-left" size={30} color={COLORS.primary} />
            </TouchableOpacity>
          ),
        })}
        name="CommentsView"
        component={ReviewsView}
      />

      <Stack.Screen name="SearchPlace" component={SearchPlace} />
      <Stack.Screen name="Search1" component={SearchStack} />
      <Stack.Screen
        name="Voice"
        component={VoiceDetect}
        options={({navigation, route}) => ({
          headerShown: true,
          headerTitleAlign: 'center',
          title: '',
          // headerBackImage: route.params.item?.images[0],
          headerTintColor: COLORS.gray,
          headerShadowVisible: false,
          headerTransparent: true,

          headerLeft: () => (
            <TouchableOpacity
              style={{
                backgroundColor: COLORS.transparentBlack2,
                height: 40,
                width: 40,
                borderRadius: 25,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => navigation.goBack()}>
              <Icon name="angle-left" size={35} color={COLORS.white} />
            </TouchableOpacity>
          ),
        })}
      />

      <Stack.Screen
        name="PlacesView"
        component={PlacesView}
        options={({navigation, route}) => ({
          headerShown: true,
          title: route.params.title,
          headerTitleAlign: 'center',
          // headerBackImage: route.params.item?.images[0],
          headerTintColor: COLORS.gray,
          headerShadowVisible: false,
          headerTitleStyle: {
            color: COLORS.primary,
            ...FONTS.h4,
            fontSize: 16,
          },
          headerLeft: () => (
            <TouchableOpacity
              style={{
                marginLeft: SIZES.base,
              }}
              onPress={() => navigation.goBack()}>
              <Icon name="angle-left" size={30} color={COLORS.primary} />
            </TouchableOpacity>
          ),
        })}
      />

      <Stack.Screen
        name="ExploreView"
        component={ExploreView}
        options={({navigation, route}) => ({
          headerShown: true,
          title: Lang.t('explore'),
          headerTitleAlign: 'center',
          // headerBackImage: route.params.item?.images[0],
          headerTintColor: COLORS.gray,
          headerShadowVisible: false,
          headerTitleStyle: {
            color: COLORS.primary,
            ...FONTS.h4,
            fontSize: 16,
          },
          headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Icon name="angle-left" size={30} color={COLORS.primary} />
            </TouchableOpacity>
          ),
        })}
      />
      <Stack.Screen
        name="ProvincesView"
        component={ProvincesView}
        options={({navigation, route}) => ({
          headerShown: true,
          title: route.params.title,
          headerTitleAlign: 'center',
          // headerBackImage: route.params.item?.images[0],
          headerShadowVisible: false,
          headerTintColor: COLORS.gray,
          headerTitleStyle: {
            color: COLORS.primary,
            ...FONTS.h4,
            fontSize: 16,
          },
          headerLeft: () => (
            <TouchableOpacity
              style={{
                marginLeft: SIZES.base,
              }}
              onPress={() => navigation.goBack()}>
              <Icon name="angle-left" size={30} color={COLORS.primary} />
            </TouchableOpacity>
          ),
        })}
      />
      {/* Plan navigation */}
      <Stack.Screen
        name="Plan"
        component={Plan}
        options={() => ({
          // headerTitle: route.params.item?.name,
          headerTransparent: true,
          headerTintColor: '#fff',
        })}
      />
      <Stack.Screen
        name="DetailPlan"
        component={PlanDetail}
        options={() => ({
          // headerTitle: route.params.item?.name,
          headerTransparent: true,
          headerTintColor: '#fff',
        })}
      />
      <Stack.Screen
        name="EditPlan"
        component={EditPlan}
        options={({navigation}) => ({
          headerShown: true,
          title: Lang.t('edit'),
          headerShadowVisible: false,
          headerTitleAlign: 'center',
          // headerBackImage: route.params.item?.images[0],
          headerTintColor: COLORS.gray,
          headerTitleStyle: {
            color: COLORS.primary,
            ...FONTS.h4,
            fontSize: 17,
          },
          headerLeft: () => (
            <TouchableOpacity
              style={{
                marginLeft: SIZES.base,
              }}
              onPress={() => navigation.goBack()}>
              <Icon name="angle-left" size={30} color={COLORS.primary} />
            </TouchableOpacity>
          ),
        })}
      />
      <Stack.Screen
        name="CreatePlan"
        component={CreatePlan}
        options={({navigation}) => ({
          headerShown: true,
          title: Lang.t('new_plan'),
          headerShadowVisible: false,
          headerTitleAlign: 'center',
          // headerBackImage: route.params.item?.images[0],
          headerTintColor: COLORS.gray,
          headerTitleStyle: {
            color: COLORS.primary,
            ...FONTS.h4,
            fontSize: 17,
          },
          headerLeft: () => (
            <TouchableOpacity
              style={{
                marginLeft: SIZES.base,
              }}
              onPress={() => navigation.goBack()}>
              <Icon name="angle-left" size={30} color={COLORS.primary} />
            </TouchableOpacity>
          ),
        })}
      />
      <Stack.Screen name="map" component={Map} />

      <Stack.Screen name="MainProfile" component={ProfileStack} />
      <Stack.Screen
        name="TripPlan"
        component={TripPlan}
        options={() => ({
          // headerTitle: route.params.item?.name,
          headerTransparent: true,
          headerTintColor: '#fff',
        })}
      />
      <Stack.Screen
        name="Collection"
        component={Collection}
        options={() => ({
          // headerTitle: route.params.item?.name,

          headerTransparent: true,
          headerTintColor: '#fff',
        })}
      />
      {/* Search navigation */}
    </Stack.Navigator>
  );
};

export default MainStack;
