import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import {View, Text} from 'react-native';
import {Icon} from 'react-native-elements';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {COLORS, FONTS, icons, SIZES} from '../constants';
import ChangePassword from '../screens/profile/ChangePassword';
import EditProfile from '../screens/profile/EditProfile';
import MyFavorites from '../screens/profile/MyFavorites';
import ProfileScreen from '../screens/profile/ProfileScreen';
import {ChangeLanguage, FriendScreen} from '../screens';
import Lang from '../language';
import FriendProfileScreen from '../screens/friend/FriendsProfileScreen';

const Stack = createNativeStackNavigator();

const ProfileStack = () => {
  return (
    <Stack.Navigator
      initialRouteName="Profile"
      screenOptions={{headerShown: false}}>
      <Stack.Screen
        options={({navigation, route}) => ({
          headerShown: true,
          title: '',
          headerShadowVisible: false,
          headerTransparent: true,
          headerLeft: () => (
            <TouchableOpacity
              style={{
                height: 40,
                width: 30,
                justifyContent: 'center',
              }}
              onPress={() => navigation.goBack()}>
              <IconFontAwesome
                name={icons.angleLeft}
                size={30}
                color={COLORS.white}
              />
            </TouchableOpacity>
          ),
        })}
        name="EditProfile"
        component={EditProfile}></Stack.Screen>
      <Stack.Screen name="Profile" component={ProfileScreen} />
      <Stack.Screen
        name="MyFavorites"
        component={MyFavorites}
        options={({navigation, route}) => ({
          headerShown: true,
          title: Lang.t('screen_favorite'),
          headerShadowVisible: false,
          headerTitleAlign: 'center',
          headerTintColor: COLORS.gray,
          headerTitleStyle: {
            color: COLORS.primary,
            ...FONTS.h4,
            fontSize: 16,
          },

          headerLeft: () => (
            <TouchableOpacity
              style={{
                height: 40,
                width: 30,
                justifyContent: 'center',
              }}
              onPress={() => navigation.goBack()}>
              <IconFontAwesome
                name={icons.angleLeft}
                size={30}
                color={COLORS.primary}
              />
            </TouchableOpacity>
          ),
        })}
      />
      <Stack.Screen
        name="ChangeLanguage"
        component={ChangeLanguage}
        options={({navigation, route}) => ({
          headerShown: true,
          title: Lang.t('change_language'),
          headerShadowVisible: false,
          headerTitleAlign: 'center',
          headerTintColor: COLORS.gray,
          headerTitleStyle: {
            color: COLORS.primary,
            ...FONTS.h4,
            fontSize: 16,
          },

          headerLeft: () => (
            <TouchableOpacity
              style={{
                height: 40,
                width: 30,
                justifyContent: 'center',
              }}
              onPress={() => navigation.goBack()}>
              <IconFontAwesome
                name={icons.angleLeft}
                size={30}
                color={COLORS.primary}
              />
            </TouchableOpacity>
          ),
        })}
      />
      <Stack.Screen
        name="ChangePassword"
        component={ChangePassword}
        options={({navigation, route}) => ({
          headerShown: true,
          headerShadowVisible: false,
          title: Lang.t('screen_change_password'),
          headerTitleAlign: 'center',
          headerTintColor: COLORS.gray,
          headerTitleStyle: {
            ...FONTS.h4,
            color: COLORS.white,
          },
          headerStyle: {
            backgroundColor: COLORS.primary,
          },
          headerLeft: () => (
            <TouchableOpacity
              style={{
                marginLeft: SIZES.base,
                marginRight: SIZES.base,
              }}
              onPress={() => navigation.goBack()}>
              <IconFontAwesome
                name="angle-left"
                size={30}
                color={COLORS.white}
              />
            </TouchableOpacity>
          ),
        })}
      />
      <Stack.Screen
        name="Friends"
        component={FriendScreen}
        options={({navigation, route}) => ({
          headerShown: true,
          headerShadowVisible: false,
          title: Lang.t('screen_search'),
          headerTitleAlign: 'center',
          headerTintColor: COLORS.gray,
          headerTitleStyle: {
            color: COLORS.primary,
            ...FONTS.h4,
            fontSize: 17,
          },
          headerTransparent: true,
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={{
                marginLeft: SIZES.base,
                marginRight: SIZES.base,
              }}>
              <IconFontAwesome
                name="angle-left"
                size={30}
                color={COLORS.black}
              />
            </TouchableOpacity>
          ),
        })}
      />
      <Stack.Screen
        name="ViewUserProfile"
        component={FriendProfileScreen}
        options={({navigation, route}) => ({
          headerShown: true,
          headerShadowVisible: false,
          title: Lang.t('screen_view_profile'),
          headerTransparent: true,
          headerTitleAlign: 'center',
          headerTintColor: COLORS.gray,
          headerTitleStyle: {
            color: COLORS.black,
            ...FONTS.h4,
            fontSize: 17,
          },
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={{
                marginLeft: SIZES.base,
                marginRight: SIZES.base,
              }}>
              <IconFontAwesome
                name="angle-left"
                size={30}
                color={COLORS.black}
              />
            </TouchableOpacity>
          ),
        })}
      />
    </Stack.Navigator>
  );
};

export default ProfileStack;
