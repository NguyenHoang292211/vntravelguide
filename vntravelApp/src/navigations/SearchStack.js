import {createNativeStackNavigator} from '@react-navigation/native-stack';

import React from 'react';
import {COLORS, FONTS, icons} from '../constants';
import Icon from 'react-native-vector-icons/AntDesign';
import {useNavigation} from '@react-navigation/core';
import {FindPlace, Search, ChooseLocation} from '../screens';
import Lang from "../language"
const Stack = createNativeStackNavigator();
const SearchStack = () => {
  const navigation = useNavigation();
  const goBack = () => {
    navigation.goBack();
  };
  return (
    <Stack.Navigator
      initialRouteName="Search"
      screenOptions={{headerShown: true}}>
      <Stack.Screen
        name="Search"
        component={Search}
        options={({route}) => ({
          headerShown: false,
        })}
      />
      <Stack.Screen
        name="FindPlace"
        component={FindPlace}
        options={({route}) => ({
          headerShown: true,
          headerStyle: {
            backgroundColor: COLORS.white,
          },
          headerLeft: () => (
            <Icon
              name={icons.arrowleft}
              style={{marginLeft: 3, marginRight: 10, color: COLORS.primary}}
              size={22}
              onPress={() => goBack()}
            />
          ),
          headerTitleAlign: 'center',
          headerTintColor: COLORS.primary,
          title: Lang.t('screen_pick_location'),
          headerTitleStyle:{
            ...FONTS.h3
          }
        })}
      />
      <Stack.Screen
        name="PickupLocation"
        component={ChooseLocation}
        options={({route}) => ({
          headerShown: true,
          title: Lang.t('screen_specific_place'),
          headerTitleStyle: {
            ...FONTS.h3,
          },
          headerStyle: {
            backgroundColor: COLORS.white,
          },
          headerTintColor: COLORS.primary,
          headerBackVisible: true,
          headerTitleAlign: 'center',
        })}
      />
    </Stack.Navigator>
  );
};

export default SearchStack;
