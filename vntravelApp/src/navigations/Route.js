import NetInfo, {useNetInfo} from '@react-native-community/netinfo';
// import moduleName from '@react-native-community/';
import {NavigationContainer, useRoute} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import LottieView from 'lottie-react-native';
import React, {useEffect, useState} from 'react';
import {TouchableOpacity} from 'react-native';
import {View} from 'react-native';
import I18n from 'react-native-i18n';
import IonIcon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import {InformModal, TextButton} from '../components';
import {COLORS, constants, icons, images, SIZES} from '../constants';
import {AuthCtrl} from '../controller';
import {saveUserData, updateLanguage} from '../stores/authSlice';
import setAuthToken from '../utils/setAuthToken';
import {clearStorage, getItem, setItem} from '../utils/Utils';
import AuthStack from './AuthStack';
import MainStack from './MainStack';
import {updateStatus} from '../stores/voiceBotSlice';
const Stack = createStackNavigator();

const Route = () => {
  const dispatch = useDispatch();
  const [initialRoute, setInitialRoute] = useState('');
  const [isConnected, setIsConnected] = useState(true);
  const [isExpireSession, setIsExpireSession] = useState(false);
  const loadToken = () => {
    try {
      getItem('token').then(value => {
        if (value !== null) {
          //Check if token exist, verify account
          AuthCtrl.verifyAccount(value).then(res => {
            if (res.data.success) {
              setAuthToken(value);
              setInitialRoute('Main');
              // dispatch(updateStatus({showBot: true}));
              let data = {
                user: res.data.user,
                token: value,
              };

              dispatch(saveUserData(data));
            } else if (res.status === 401) {
              setIsExpireSession(true);
              console.log('401');
            } else if (res.status === 403) {
              setInitialRoute('Auth');
              // dispatch(updateStatus({showBot: false}));
            }
          });
        } else {
          setInitialRoute('Auth');
          // dispatch(updateStatus({showBot: false}));
        }
      });
    } catch (error) {
      console.log(error);
    }
  };

  // useEffect(() => {
  //   console.log(route.name);
  // }, [route]);

  useEffect(() => {
    NetInfo.fetch()
      .then(state => {
        if (state.isConnected) {
          loadToken();
        } else {
          console.log('Not connect');
        }
      })
      .catch(err => {
        console.log('Error', err);
      });

    NetInfo.addEventListener(state => {
      if (!state.isConnected) setIsConnected(false);
      else setIsConnected(true);
    });

    //Get language
    getItem('language')
      .then(lang => {
        if (lang) {
          dispatch(updateLanguage(lang));
          I18n.locale = lang.t;
        } else {
          dispatch(updateLanguage(constants.LANGUAGE[0]));
          setItem('language', JSON.stringify(constants.LANGUAGE[0]));
          I18n.locale = constants.LANGUAGE[0].t;
        }
      })
      .catch(err => {
        dispatch(updateLanguage(constants.LANGUAGE[0]));
        I18n.locale = constants.LANGUAGE[0].t;
        console.error(err);
      });
  }, []);

  const flashLoading = () => {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <LottieView
          source={images.loading}
          autoPlay
          loop
          style={{
            height: 250,
            width: 300,
          }}
        />
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: COLORS.white,
      }}>
      {initialRoute === '' ? (
        flashLoading()
      ) : (
        <NavigationContainer>
          <Stack.Navigator
            initialRouteName={initialRoute}
            screenOptions={{headerShown: false}}>
            <Stack.Screen name="Main" component={MainStack} />
            <Stack.Screen name="Auth" component={AuthStack} />
          </Stack.Navigator>
        </NavigationContainer>
      )}

      <InformModal
        isVisible={!isConnected}
        setIsVisible={null}
        description="Please, check your internet connected and reload."
        title="OPP! You're offline"
        source={images.failGif}
        imageStyle={{
          height: 110,
          width: 110,
          marginTop: 8,
          marginBottom: 15,
        }}
        titleStyle={{
          fontSize: 27,
          color: COLORS.red,
        }}
        descriptionStyle={{
          fontSize: 15,
          paddingHorizontal: 2,
          color: COLORS.black,
        }}
        actionComponent={
          <View
            style={{
              alignItems: 'center',
            }}>
            <TextButton
              label="Reload"
              onPress={() => loadToken()}
              startIcon={
                <IonIcon name={icons.reload} size={25} color={COLORS.white} />
              }
              buttonContainerStyle={{
                height: 50,
                width: 200,
                marginTop: 30,
                elevation: 3,
              }}
            />
          </View>
        }
      />

      {/* Inform sesion expired */}
      <InformModal
        isVisible={isExpireSession}
        setIsVisible={null}
        description="Your session is expired. Sign in again?"
        title="Announce"
        source={images.signin}
        imageStyle={{
          height: 100,
          width: 100,
          marginTop: 8,
          marginBottom: 15,
        }}
        titleStyle={{
          fontSize: 23,
          fontFamily: 'LexendDeca-SemiBold',
          color: COLORS.secondary,
        }}
        descriptionStyle={{
          fontSize: 15,
          paddingHorizontal: 4,
          color: COLORS.black,
        }}
        actionComponent={
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 20,
            }}>
            <TextButton
              label="Yes"
              onPress={() => {
                setIsExpireSession(false);
                setInitialRoute('Auth');
              }}
              buttonContainerStyle={{
                height: 50,
                width: SIZES.width * 0.8,
                marginTop: 5,
                elevation: 3,
              }}
            />
            <TextButton
              label="Not now"
              onPress={() => {
                setIsExpireSession(false);
                setInitialRoute('Main');
                clearStorage();
              }}
              labelStyle={{
                color: COLORS.black,
              }}
              buttonContainerStyle={{
                height: 50,
                width: SIZES.width * 0.8,
                marginTop: 8,
                elevation: 3,
                backgroundColor: COLORS.white,
              }}
            />
          </View>
        }
      />
    </View>
  );
};

export default Route;
