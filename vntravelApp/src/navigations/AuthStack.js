import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import {
  Signin,
  Signup,
  CheckMail,
  ForgotPassword,
  Onboarding,
} from '../screens';
const Stack = createNativeStackNavigator();
const AuthStack = () => {
  return (
    <Stack.Navigator
      initialRouteName="Onboarding"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="Checkmail" component={CheckMail} />
      <Stack.Screen name="Forgotpassword" component={ForgotPassword} />
      <Stack.Screen name="Signin" component={Signin} />
      <Stack.Screen name="Signup" component={Signup} />
      <Stack.Screen name="Onboarding" component={Onboarding} />
    </Stack.Navigator>
  );
};

export default AuthStack;
