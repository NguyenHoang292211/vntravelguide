const reviews = [
  {
    id: 0,
    user: {
      name: 'Nathan Lee',
      avatar: require('../assets/images/avatar.jpg'),
      contributation: 5,
    },
    visitDate: '22-11-2021',
    writtenDate: '23-11-2021',
    title: 'Title in here',
    content:
      'The build command uses this as the build target when no environment is specified. You can add further variables.',
    likeCount: 5,
    rating: 4,
    images: [
      require('../assets/images/back_1.jpg'),
      require('../assets/images/back_2.jpg'),
      require('../assets/images/back_3.jpg'),
      require('../assets/images/back_4.jpg'),
    ],
  },
  {
    id: 1,
    user: {
      name: 'Linh Anh Ha',
      avatar: require('../assets/images/avatar.jpg'),
      contributation: 7,
    },
    visitDate: '22-2-2020',
    writtenDate: '23-3-2020',
    title: 'Title in here',
    content:
      'The build command uses this as the build target when no environment is specified. You can add further variables.',
    likeCount: 5,
    rating: 3,
  },
  {
    id: 2,
    user: {
      name: 'Hoang Nguyen',
      avatar: require('../assets/images/avatar.jpg'),
      contributation: 5,
    },
    visitDate: '22-11-2021',
    writtenDate: '23-11-2021',
    title: 'Title in here',
    content:
      'The build command uses this as the build target when no environment is specified. You can add further variables.',
    likeCount: 4,
    rating: 4,
  },
  {
    id: 3,
    user: {
      name: 'Nathan Lee',
      avatar: require('../assets/images/avatar.jpg'),
      contributation: 5,
    },
    visitDate: '22-11-2021',
    writtenDate: '23-11-2021',
    title: 'Title in here',
    content:
      'The build command uses this as the build target when no environment is specified. You can add further variables.',
    likeCount: 5,
    rating: 4,
    images: [
      require('../assets/images/back_1.jpg'),
      require('../assets/images/back_2.jpg'),
      require('../assets/images/back_3.jpg'),
    ],
  },
  {
    id: 4,
    user: {
      name: 'Nathan Lee',
      avatar: require('../assets/images/avatar.jpg'),
      contributation: 5,
    },
    visitDate: '22-11-2021',
    writtenDate: '23-11-2021',
    title: 'Title in here',
    content:
      'The build command uses this as the build target when no environment is specified. You can add further variables.',
    likeCount: 5,
    rating: 4,
    images: [
      require('../assets/images/back_3.jpg'),
      require('../assets/images/back_4.jpg'),
    ],
  },
];

export default reviews;
