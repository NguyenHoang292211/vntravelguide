export default COLORS = {
  mainBlue: '#2D5F73',
  white: '#fff',
  black: '#000',
  descText: '#151515',
  headerTitle: '#413F3F',
  backgroundDefault: '#F6F7FB',
  mist: '#90AFC5',
  stone: '#336B87',
  shadow: '#2A3132',
  autumn: '#763626',
  seafoam: '#C4DFE6',

  //New
};
