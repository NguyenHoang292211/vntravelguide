const background1 = require('../assets/images/back_1.jpg');
const background2 = require('../assets/images/back_2.jpg');
const background3 = require('../assets/images/back_3.jpg');
const background4 = require('../assets/images/back_4.jpg');
const background6 = require('../assets/images/back_6.jpg');
const forgotPassword = require('../assets/images/forgot_password.png');
const avatar = require('../assets/images/avatar.jpg');
const sendEmail = require('../assets/images/send_email.png');
const success = require('../assets/images/success.png');
const logo = require('../assets/images/logo.png');
const menu = require('../assets/images/menu.png');
const background5 = require('../assets/images/back_5.png');
const google = require('../assets/images/google.png');
const photo = require('../assets/images/photo.png');
const filter = require('../assets/images/filter.png');
const flashLoading = require('../assets/images/loadingFlash.gif');
const loginModal = require('../assets/images/loginModal.png');
const run = require('../assets/images/runman.gif');
const empty = require('../assets/images/empty.json');
const loading = require('../assets/images/loading.json');
const signin = require('../assets/images/signin.json');
const successGif = require('../assets/images/success.json');
const failGif = require('../assets/images/failGif.json');
const calendarGif = require('../assets/images/calendar.json');
const circle = require('../assets/images/circle.png');
const rectangle = require('../assets/images/Rectangle.png');
const location = require('../assets/images/location.png');
const placeNotSupportImage = require('../assets/images/Place_Not_Support.jpg');
const instruction = require('../assets/images/instruction.png');

const fillChangePassword = require('../assets/images/fillChangePassword.png');
const successUpdate = require('../assets/images/successUpdate.json');
const smartBot = require('../assets/images/smartbot.json');

const empty_friend = require('../assets/images/empty_friend.png');
const empty_favorite = require('../assets/images/empty_favorite.png');
export default {
  circle,
  instruction,
  run,
  background2,
  background1,
  background3,
  background4,
  background5,
  background6,
  forgotPassword,
  avatar,
  sendEmail,
  success,
  logo,
  menu,
  google,
  photo,
  filter,
  flashLoading,
  loginModal,
  empty,
  loading,
  signin,
  placeNotSupportImage,
  successGif,
  failGif,
  rectangle,
  calendarGif,
  location,
  fillChangePassword,
  successUpdate,
  smartBot,
  empty_friend,
  empty_favorite,
};
