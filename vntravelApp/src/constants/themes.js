import {Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');

export const COLORS = {
  // primary: '#81CAFF',
  // primary: '#0052A2',

  primary: '#02386E',
  secondary: '#763626',
  blue: '#0064C0',
  lightBlue: '#C4DFE6',
  lightBlue1: '#90AFC5',
  supperLightBlue: '#DAEAEF',
  gray: '#898B9A',
  lightGray: '#F5F5F8',
  lightGray1: '#E9E9E9',
  lightGray2: '#C0C0C0',
  darkGray1: '#525C67',
  darkGray: '#757D85',
  gray2: '#BBBDC1',
  gray3: '#CFD0D7',
  white2: '#FBFBFB',
  white3: '#F6F6F6',
  white: '#FFFFFF',
  black: '#000000',
  orange: '#FFC700',
  pink: '#FF7B7B',
  green: '#27AE60',
  red: '#C60202',
  blueLight: '#3F7FE9',
  yellow: '#cccc02',
  //Transparent
  transparent: 'rgba(0, 0, 0, 0)',
  transparentBlack1: 'rgba(0, 0, 0, 0.4)',
  transparentBlack2: 'rgba(0, 0, 0, 0.1)',
  transparentBlack7: 'rgba(0, 0, 0, 0.7)',
  transparentBlack2: 'rgba(0, 0, 0, 0.2)',
  transparentWhite7: 'rgba(255, 255, 255, 0.67)',
  transparentWhite4: 'rgba(255, 255, 255, 0.4)',
  transparentWhite8: 'rgba(255, 255, 255, 0.8)',

  transparentPrimary3: 'rgba(91, 179, 242,0.3)',
};

export const SIZES = {
  // global sizes
  base: 8,
  font: 14,
  radius: 12,
  padding: 24,

  // font sizes
  largeTitle: 40,
  h1: 34,
  h2: 24,
  h3: 17,
  h4: 15,
  h5: 13,
  h6: 11,
  body1: 30,
  body2: 24,
  body3: 16,
  body4: 13,
  body5: 11,

  // app dimensions
  width,
  height,
};
export const FONTS = {
  special: {
    fontFamily: 'Bangers-Regular',
    fontSize: 33,
    lineHeight: 40,
  },
  largeTitle: {fontFamily: 'LexendDeca-Bold', fontSize: SIZES.largeTitle},
  h1: {fontFamily: 'LexendDeca-SemiBold', fontSize: SIZES.h1, lineHeight: 36},
  h2: {
    fontFamily: 'LexendDeca-SemiBold',
    fontSize: SIZES.h2,
    lineHeight: 30,
  },
  h3: {
    fontFamily: 'LexendDeca-Medium',
    fontSize: SIZES.h3,
    lineHeight: 22,
  },
  h4: {
    fontFamily: 'LexendDeca-Medium',
    fontSize: SIZES.h4,
    lineHeight: 22,
  },
  h5: {fontFamily: 'LexendDeca-Medium', fontSize: SIZES.h5, lineHeight: 22},
  body1: {
    fontFamily: 'LexendDeca-Regular',
    fontSize: SIZES.body1,
    lineHeight: 36,
  },
  body2: {
    fontFamily: 'LexendDeca-Regular',
    fontSize: SIZES.body2,
    lineHeight: 30,
  },
  body3: {
    fontFamily: 'LexendDeca-Light',
    fontSize: SIZES.body3,
    lineHeight: 22,
  },
  body4: {
    fontFamily: 'LexendDeca-Light',
    fontSize: SIZES.body4,
    lineHeight: 20,
  },
  body5: {
    fontFamily: 'LexendDeca-Light',
    fontSize: SIZES.body5,
    lineHeight: 22,
  },
  body6: {
    fontFamily: 'LexendDeca-Regular',
    fontSize: SIZES.body5,
    lineHeight: 17,
  },
};

const appTheme = {COLORS, SIZES, FONTS};

export default appTheme;
