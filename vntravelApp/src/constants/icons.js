//AntDesign  icons
const rightCheck = 'checkcircle';
const wrongCheck = 'closecircle';
const solidHeart = 'heart';
const outlineHeart = 'hearto';
const solidLike = 'like1';
const outlineLike = 'like2';
const home = 'home';
const search = 'search1';
const calendar = 'calendar';
const profile = 'user';
const arrowright = 'arrowright';
const direct = 'address';
const arrowleft = 'arrowleft';
const solidStar = 'star';
const outlineStar = 'staro';
const close = 'close';
const check = 'check';
const clock = 'clockcircleo';
const logout = 'logout';
const changePassword = 'meh';
const language = 'earth';
const more_app = 'appstore-o';
const caretdown = 'caretdown';
const delete_ = 'delete';
//MaterialIcons
const pen = 'create';
const done = 'done';
const public_ = 'public';
//Font awesome
const angleLeft = 'angle-left';
const camera = 'camera';
const dollar = 'dollar';
const quote_left = 'quote-left';
const quote_right = 'quote-right';
//Ionicons
const eye = 'eye-outline';
const eyeOff = 'eye-off-outline';
const locationFill = 'location-sharp';
const location = 'location-outline';
const tag = 'pricetags-sharp';
const car = 'car';
//MaterialCommunityIcons
const mapCheck = 'map-check-outline';
// const dollar = 'dollar';
const reload = 'reload';
const note = 'document-text-outline';
const plane = 'paper-plane';
const add = 'add';
const route = 'routes';

//Feather
const more = 'more-vertical';

//Font awesome5

const add_friend = 'user-plus';
const friend = 'user-friends';

export default {
  public_,
  friend,
  add_friend,
  quote_left,
  quote_right,
  more_app,
  close,
  pen,
  rightCheck,
  wrongCheck,
  solidHeart,
  outlineHeart,
  solidLike,
  outlineLike,
  home,
  calendar,
  search,
  profile,
  arrowright,
  arrowleft,
  eye,
  eyeOff,
  direct,
  outlineStar,
  solidStar,
  location,
  angleLeft,
  locationFill,
  mapCheck,
  clock,
  dollar,
  more,
  tag,
  logout,
  changePassword,
  check,
  car,
  reload,
  camera,
  note,
  route,
  plane,
  caretdown,
  add,
  delete_,
  language,
  done,
};
