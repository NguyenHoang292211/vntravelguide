import icons from './icons';
import Lang from '../language';

const LANGUAGE = [
  {
    t: 'vi-VN',
    name: 'vi',
  },
  {
    t: 'en-US',
    name: 'en',
  },
];
const screens = {
  main_layout: 'main_layout',
  home: 'home',
  search: 'search',
  my_plan: 'my_plan',
  favourite_place: 'my_favorite',
  profile: 'profile',
};

const onboarding_screens = [
  // {
  //   id: 1,
  //   backgroundImage: require('../assets/images/back_1.jpg'),
  //   title: 'Welcome',
  //   subtitle: 'Explore our amazing country, new advantures are waiting...',
  // },
  {
    id: 2,
    backgroundImage: require('../assets/images/back_3.jpg'),
    title: 'Discovery',
    subtitle: 'Explore our amazing country, new advantures are waiting...',
  },
  {
    id: 3,
    backgroundImage: require('../assets/images/back_1.jpg'),
    title: 'Your Journey',
    subtitle: 'Create your own trip’s plan , tell us your destination.',
  },
  {
    id: 4,
    backgroundImage: require('../assets/images/back_4.jpg'),
    title: 'Start New Things',
    subtitle: 'Explore our amazing country, new advantures are waiting...',
  },
];

const bottom_tabs = [
  {
    id: 0,
    label: screens.home,
    icon: icons.home,
  },
  {
    id: 1,
    label: screens.search,
    icon: icons.search,
  },
  {
    id: 2,
    label: screens.my_plan,
    icon: icons.calendar,
  },
  {
    id: 3,
    label: screens.profile,
    icon: icons.profile,
  },
];
const baseSuggestUrl = 'https://api.mapbox.com/directions/v5/mapbox';
const defaultVehicle = 'driving';
const GoongApiKey =
  'pk.eyJ1IjoidGhhb2xlMzY2IiwiYSI6ImNrcnpydnJwaDAxOG8ydXFtOXdwNWR0eGsifQ.FCbrlFuxwWzIghMVU_T0iA';

const autoCompleteAPI =
  'https://rsapi.goong.io/Place/AutoComplete?api_key=hZAfEidENJjFpVHpI5RQPsaqAcfV4tyRiTgCxUkh&input';
const infoPlaceAPI = 'https://rsapi.goong.io/Place/Detail?place_id';
const styleIcon = 1;
const styleAnnotation = 2;
const APIPermissionKey = 'hZAfEidENJjFpVHpI5RQPsaqAcfV4tyRiTgCxUkh';

const SHOW_TOAST_MESSAGE = 'SHOW_TOAST_MESSAGE';
const NOTIFY_EMPTY_PICKUP_LOCATION = Lang.t('mess_empty_pickup_location');
const NOTIFY_EMPTY_DESTINATION_LOCATION = Lang.t(
  'mess_empty_destination_location',
);
let NOTIFY_EMPTY_DIRECT = Lang.t('mess_choose_place_first');
const NUMBER_WORD_OF_REVIEW = 120;
const tripDescription = [
  {
    icon: 'heart',
    textEng: Lang.t('trip_desc_1'),
  },
  {
    icon: 'routes',
    textEng: Lang.t('trip_desc_2'),
  },
  {
    icon: 'file-document-outline',
    textEng: Lang.t('trip_desc_3'),
  },
  {
    icon: 'camera',
    textEng: Lang.t('trip_desc_4'),
  },
];
let profiles = {
  favorites: Lang.t('screen_favorite'),
  change_password: Lang.t('mess_password_changed'),
  language: Lang.t('change_language'),
  logout: Lang.t('logout'),
};
const MESS_ERROR_GOOGLE_SERVICE = Lang.t('mess_error_google_service');
const MESS_ERROR_INTERVAL = Lang.t('mess_error_interval');
const METHOD_SIGN_IN = 'methodSignIn';
const methodSignIn = {
  google: 'Google',
  user: 'User',
};
const forgotPasswordMode = {
  email: 0,
  code: 1,
  resetPassword: 2,
  finishChangePassword: 3,
};
const onForgotPassword = [
  {id: 0, name: Lang.t('forgot_pw_st_1')},
  {id: 1, name: Lang.t('forgot_pw_st_2')},
  {id: 2, name: Lang.t('forgot_pw_st_3')},
  {id: 3, name: Lang.t('forgot_pw_st_4')},
];

const reportSuggestion = ['use_word', 'infor', 'picture', 'violent'];
const verifyCodeLength = 6;
export default {
  reportSuggestion,
  screens,
  bottom_tabs,
  onboarding_screens,
  baseSuggestUrl,
  defaultVehicle,
  GoongApiKey,
  styleIcon,
  styleAnnotation,
  autoCompleteAPI,
  infoPlaceAPI,
  APIPermissionKey,
  profiles,
  SHOW_TOAST_MESSAGE,
  NOTIFY_EMPTY_DESTINATION_LOCATION,
  NOTIFY_EMPTY_PICKUP_LOCATION,
  NOTIFY_EMPTY_DIRECT,
  NUMBER_WORD_OF_REVIEW,
  tripDescription,
  MESS_ERROR_GOOGLE_SERVICE,
  MESS_ERROR_INTERVAL,
  METHOD_SIGN_IN,
  methodSignIn,
  forgotPasswordMode,
  onForgotPassword,
  verifyCodeLength,
  LANGUAGE,
};
