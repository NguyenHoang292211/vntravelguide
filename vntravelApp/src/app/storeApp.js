import {configureStore} from '@reduxjs/toolkit';
import searchReducer from '../stores/searchSlide';
import tabReducer from '../stores/tabSlide';
import placeReducer from '../stores/placeSlice';
import authReducer from '../stores/authSlice';
import screenReducer from '../stores/screenSlice';
import planReducer from '../stores/planSlice';
import voiceBotSlice from '../stores/voiceBotSlice';
export default configureStore({
  reducer: {
    search: searchReducer,
    tab: tabReducer,
    place: placeReducer,
    auth: authReducer,
    screen: screenReducer,
    plan: planReducer,
    voice: voiceBotSlice,
  },
});
