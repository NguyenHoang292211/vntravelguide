// export const baseUrl = 'https://vntravel-api.herokuapp.com/app/api/v1';

export const baseUrl = 'http://192.168.1.7:5000/app/api/v1';
export const VIEW_SCREEN_TYPE = {
  PROVINCE: 'province',
  CATEGORY: 'category',
  EXPLORE: 'explore',
  POPULAR: 'popular',
};

export const COLOR_RANDOM = [
  '#fcba03',
  '#ff5147',
  '#114df2',
  '#638dff',
  '#7c87a6',
  '#132a6e',
  '#408a16',
  '#76ff29',
  '#61ad36',
  '#cfd91a',
  '#f1fa57',
  '#1bf791',
  '#087e9c',
  '#4c00ff',
  '#594094',
  '#7750d4',
  '#8676ad',
  '#9a70ff',
  '#2f244a',
  '#d333ff',
  '#b791c2',
  '#8c046a',
  '#a16893',
  '#c9002f',
  '#54acff',
  '#424b54',
  '#6c9642',
  '#8da814',
  '#f78b1e',
  '#f2ae68',
  '#784d22',
];
