import AsyncStorage from '@react-native-async-storage/async-storage';
function isValidEmail(value) {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(value).toLowerCase());
}

export function validateEmail(value, setEmailError) {
  if (value === '') {
    setEmailError('Email must be filled.');
    return false;
  } else if (!isValidEmail(value)) {
    setEmailError('Invalid Email');
    return false;
  } else {
    setEmailError('');
  }
  return true;
}

export function validatePassword(value, setPasswordError) {
  if (value === '') {
    setPasswordError('Password must be filled.');
    return false;
  } else if (value.length < 4) {
    {
      setPasswordError('Password must be more than 7 characters');
      return false;
    }
  } else {
    setPasswordError('');
  }
  return true;
}

//Action with Async storage
export const getItem = key => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem(key).then(data => {
      resolve(JSON.parse(data));
    });
  });
};
export const setItems = dataAndKey => {
  //   data = JSON.stringify(data);
  return AsyncStorage.multiSet(dataAndKey);
};

export const setItem = (key, value) => {
  return AsyncStorage.setItem(key, value);
};

export const removeItem = key => {
  return AsyncStorage.removeItem(key);
};

export const clearStorage = () => {
  return AsyncStorage.clear();
};
