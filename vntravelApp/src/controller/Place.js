import axios from 'axios';
import {baseUrl} from '../utils/constants';

/**
 * Get All places
 * @returns places
 */
const getAllPlaces = async () => {
  try {
    const response = await axios.get(`${baseUrl}/places?populate=true`);

    if (response.data.success) {
      return {
        places: response.data.places,
      };
    } else {
      return {
        places: [],
      };
    }
  } catch (error) {
    console.log(error);
  }
};
/**
 * Get all places of a province
 * @param {String} provinceId
 * @returns places
 */
const getPlaceOfProvince = async provinceId => {
  try {
    const response = await axios.get(
      `${baseUrl}/places/province/${provinceId}`,
    );

    if (response.data.success) {
      return {
        places: response.data.places,
      };
    } else {
      return {
        places: [],
      };
    }
  } catch (error) {
    console.log(error);
    return [];
  }
};

/**
 * Get all places of category
 * @param {String} categoryId
 * @returns places
 */
const getPlaceOfCategory = async categoryId => {
  try {
    const response = await axios.get(
      `${baseUrl}/places/category/${categoryId}`,
    );

    if (response.data.success) {
      return {
        places: response.data.places,
      };
    } else {
      return {
        places: [],
      };
    }
  } catch (error) {
    console.log(error);
    return [];
  }
};

/**
 * Get all provinces
 * @returns provinces
 */
const getAllProvinces = async () => {
  try {
    const response = await axios.get(`${baseUrl}/provinces/public`);

    if (response.data.success) {
      return {
        provinces: response.data.provinces,
      };
    } else {
      return {
        provinces: [],
      };
    }
  } catch (error) {
    console.log(error);
    return [];
  }
};

/**
 * Search places
 * @param {String} text
 * @returns places
 */
const searchPlace = async text => {
  try {
    const response = await axios.post(`${baseUrl}/places/suggestion`, {
      text: text,
    });

    if (response.data.success) {
      return {
        places: response.data.places,
      };
    } else {
      return {
        places: [],
      };
    }
  } catch (error) {
    console.log(error);
    return [];
  }
};

/**
 * Gell all categories
 * @returns categories
 */
const getAllCategories = async () => {
  try {
    const response = await axios.get(`${baseUrl}/categories/public`);

    if (response.data.success) {
      return {
        categories: response.data.categories,
      };
    } else {
      return {
        categories: [],
      };
    }
  } catch (error) {
    console.log(error);
  }
};

const getExploreBanner = async () => {
  try {
    const response = await axios.get(`${baseUrl}/explorers`);

    if (response.data.success) {
      return {
        explorers: response.data.explorers,
      };
    } else {
      return {
        explorers: [],
      };
    }
  } catch (error) {
    console.log(error);
  }
};

const getPlaceByTags = async listTags => {
  try {
    const response = await axios.put(`${baseUrl}/places/explores/tags`, {
      tags: listTags,
    });

    if (response.data.success) {
      return {
        places: response.data.places,
      };
    } else {
      return {
        places: [],
      };
    }
  } catch (error) {
    console.log(error);
    return {
      places: [],
    };
  }
};

const getPopularPlaces = async () => {
  try {
    const response = await axios.get(`${baseUrl}/places/popular/topRating`);

    if (response.data.success) {
      return {
        places: response.data.places,
      };
    } else {
      return {
        places: [],
      };
    }
  } catch (error) {
    console.log(error);
    return {
      places: [],
    };
  }
};

const getBestRating = async () => {
  try {
    const response = await axios.get(`${baseUrl}/places/best-rating/top`);

    if (response.data.success) {
      return {
        places: response.data.places,
      };
    } else {
      return {
        places: [],
      };
    }
  } catch (error) {
    console.log(error);
    return {
      places: [],
    };
  }
};

const getNearbyPlaces = async (
  longtitude,
  lattitude,
  category = '',
  distance = 10000,
) => {
  try {
    //Distance = 2km :)) maybe I dunno
    let url = `${baseUrl}/places/nearBy/${longtitude}/${lattitude}`;
    if (category === '') url = url + `?distance=${distance}`;
    else url = url + `?distance=${distance}&category=${category}`;

    const response = await axios.get(url);
    if (response.data.success) {
      return {
        places: response.data.places,
      };
    } else {
      return {
        places: [],
      };
    }
  } catch (error) {
    console.log(error);
    return {
      places: [],
    };
  }
};

const getPlaceById = async placeId => {
  try {
    let response = null;
    await axios
      .get(`${baseUrl}/places/${placeId}?populate=true`)
      .then(res => {
        response = res;
      })
      .catch(err => {
        console.log(err);
        response = err.response;
      });

    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};
/**
 * Get all favorite of user
 * @returns favorite[]
 */
const getAllFavorite = async () => {
  try {
    const response = await axios.get(`${baseUrl}/users/favorite`);

    if (response.data.success) {
      return {
        favorite: response.data.favorite,
      };
    } else {
      return {
        favorite: [],
      };
    }
  } catch (error) {
    console.log(error);
    return [];
  }
};

export default {
  getPlaceOfProvince,
  getPlaceOfCategory,
  getAllProvinces,
  searchPlace,
  getAllCategories,
  getAllPlaces,
  getExploreBanner,
  getPopularPlaces,
  getBestRating,
  getNearbyPlaces,
  getPlaceById,
  getAllFavorite,
  getPlaceByTags,
};
