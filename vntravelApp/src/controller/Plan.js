import axios from 'axios';
import {baseUrl} from '../utils/constants';

/**
 * Get All plans of a user
 * @returns places
 */
const getAllPlanOfUser = async token => {
  try {
    let response;
    await axios
      .get(`${baseUrl}/plans`, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(res => {
        response = res;
      })
      .catch(err => {
        response = err.response;
      });
    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 * Create plan of a user
 * @returns places
 */
const createPlan = async (token, data) => {
  try {
    let response;
    await axios
      .post(`${baseUrl}/plans`, data, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(res => {
        response = res;
      })
      .catch(err => {
        response = err.response;
      });
    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 * Update plan of a user
 * @returns places
 */
const updatePlan = async (token, data) => {
  try {
    let response;
    await axios
      .put(`${baseUrl}/plans/${data.id}`, data, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(res => {
        response = res;
      })
      .catch(err => {
        response = err.response;
      });
    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};
/***
 * Create plan of a user
 * @returns places
 */
const createSection = async (token, data) => {
  try {
    let response;
    await axios
      .post(`${baseUrl}/sections`, data, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(res => {
        response = res;
      })
      .catch(err => {
        response = err.response;
      });
    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 * Get all sections of plan by planId
 * @returns places
 */
const getSectionOfPlan = async (token, planId) => {
  try {
    let response;
    await axios
      .get(`${baseUrl}/sections/plan/${planId}`, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(res => {
        response = res;
      })
      .catch(err => {
        response = err.response;
      });
    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 * Get all sections of plan by planId
 * @returns places
 */
const updateSection = async (token, data, sectionId) => {
  try {
    let response;
    await axios
      .put(`${baseUrl}/sections/${sectionId}`, data, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(res => {
        response = res;
      })
      .catch(err => {
        response = err.response;
      });
    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 * Delete section of plan by sectionId
 * @returns section
 */
const deleteSection = async (token, sectionId) => {
  try {
    let response;
    await axios
      .delete(`${baseUrl}/sections/${sectionId}`, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(res => {
        response = res;
      })
      .catch(err => {
        response = err.response;
      });
    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 * Delete plan by planId
 * @returns section
 */
const deletePlan = async (token, planId) => {
  try {
    let response;
    await axios
      .delete(`${baseUrl}/plans/${planId}`, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(res => {
        response = res;
      })
      .catch(err => {
        response = err.response;
      });
    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

export default {
  getAllPlanOfUser,
  createPlan,
  getSectionOfPlan,
  updateSection,
  createSection,
  deleteSection,
  deletePlan,
  updatePlan,
};
