import axios from 'axios';
import {baseUrl} from '../utils/constants';

/**
 * Get All plans of a user
 * @returns places
 */
const translate = async (lang, text) => {
  let response;
  await axios
    .post(`${baseUrl}/users/translate`, {
      lang: lang,
      q: text,
    })
    .then(res => {
      response = res;
    })
    .catch(err => {
      response = err.response;
    });
  return response;
};

export default {
  translate,
};
