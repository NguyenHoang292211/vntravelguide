import axios from 'axios';
import {baseUrl} from '../utils/constants';
import {getItem} from '../utils/Utils';
import {useSelector} from 'react-redux';
import {revokeScope} from 'immer/dist/internal';
const getRecentView = async (placeId, token) => {
  try {
    let response = null;
    await axios
      .get(`${baseUrl}/users/recentSearch/${placeId}`, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(res => {
        response = res;
      })
      .catch(err => {
        console.log('Get recent place fail', err);
        response = err.response;
      });
  } catch (error) {
    console.log(error);
    return null;
  }
};

const saveRecentSearch = async (placeId, token) => {
  try {
    let response = null;
    await axios
      .put(`${baseUrl}/users/recentSearch/${placeId}`, null, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(res => {
        response = res;
      })
      .catch(err => {
        console.log('Save recent place fail', err.response.data);
        response = err.response;
      });
    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

const updateProfile = async profile => {
  try {
    return axios.post(`${baseUrl}/users/profiles`, profile);
  } catch (error) {
    console.log(error);
    return null;
  }
};

const getFavoritePlaces = async token => {
  try {
    let response = null;
    await axios
      .get(`${baseUrl}/users/favorite`, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(res => {
        response = res;
      })
      .catch(err => {
        console.log('Get favorite fail : ', err.response.data);
        response = err.response;
      });
    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};
const checkMethodLogin = async email => {
  try {
    let isCanChangePassword = null;
    await axios
      .get(`${baseUrl}/users/loginPassword/${email}`)
      .then(res => {
        isCanChangePassword = res.data.isCanChangePassword;
      })
      .catch(err => {
        isCanChangePassword = false;
      });
    return isCanChangePassword;
  } catch (error) {
    console.log(err);
    return null;
  }
};
const changePassword = async changePasswordInfo => {
  try {
    return axios.put(`${baseUrl}/users/password`, changePasswordInfo);
  } catch (error) {
    console.log(error);
    return null;
  }
};

const updateFavorite = async placeId => {
  try {
    let response = null;
    await axios
      .put(`${baseUrl}/users/favorite/${placeId}`)
      .then(res => {
        response = res;
      })
      .catch(err => {
        console.log('Get favorite fail : ', err.response.data);
        response = err.response;
      });
    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

const verifyEmail = async email => {
  try {
    let response = null;
    await axios
      .get(`${baseUrl}/auths/exists/${email}`)
      .then(res => {
        response = res;
      })
      .catch(err => {
        console.log('Interval error ', err.response.data);
        response = err.response;
      });
    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};
const requestVerifyCode = async (email, generateCode) => {
  try {
    let response = null;

    await axios
      .get(`${baseUrl}/auths/send-email/${email}/${generateCode}`)
      .then(res => {
        response = res;
      })
      .catch(err => {
        console.log('Interval error ', err.response.data);
        response = err.response;
      });
    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

const verifyUserCode = async (generateCode, userCode) => {
  try {
    let response = null;

    await axios
      .get(`${baseUrl}/auths/passwords/verify/${generateCode}/${userCode}`)
      .then(res => {
        response = res;
      })
      .catch(err => {
        console.log('Interval error ', err.response.data);
        response = err.response;
      });
    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

const resetPassword = async infoReset => {
  try {
    let response = null;

    await axios
      .put(`${baseUrl}/auths/passwords`, infoReset)
      .then(res => {
        response = res;
      })
      .catch(err => {
        console.log('Interval error ', err.response.data);
        response = err.response;
      });
    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};
/**
 * Get all friends of user
 * @returns favorite[]
 */
const getAllFriend = async () => {
  try {
    const response = await axios.get(`${baseUrl}/users/friends`);

    if (response.data.success) {
      return {
        friends: response.data.friends,
      };
    } else {
      return {
        friends: [],
      };
    }
  } catch (error) {
    console.log(error);
    return [];
  }
};
/**
 * Get email by keyword
 * @returns users[]
 */
const getSuggestionEmail = async strSearch => {
  try {
    const response = await axios.get(`${baseUrl}/search?email=${strSearch}`);

    if (response.data.success) {
      return {
        users: response.data.users,
      };
    } else {
      return {
        users: [],
      };
    }
  } catch (error) {
    console.log(error);
    return [];
  }
};

/**
 * Get friend selected
 * return user
 */
const getInfoUser = async userId => {
  try {
    const response = await axios.get(`${baseUrl}/search/${userId}/friends`);

    if (response.data.success) {
      return {
        user: response.data.user,
      };
    } else {
      return {
        user: [],
      };
    }
  } catch (error) {
    console.log(error);
    return [];
  }
};
/**
 * Get trip public of friend
 */
const getTripPublicOfUser = async userId => {
  try {
    const response = await axios.get(`${baseUrl}/search/${userId}/plans`);

    if (response.data.success) {
      return {
        plans: response.data.plans,
      };
    } else {
      return {
        plans: [],
      };
    }
  } catch (error) {
    console.log(error);
    return [];
  }
};
/**
 * Get All plans of a user
 * @returns places
 */
const getAllPlanOfUser = async () => {
  try {
    const response = await axios.get(`${baseUrl}/plans`);

    if (response.data.success) {
      return {
        plans: response.data.plans,
      };
    } else {
      return {
        plans: [],
      };
    }
  } catch (error) {
    console.log(error);
    return [];
  }
};
export default {
  getRecentView,
  saveRecentSearch,
  updateProfile,
  getFavoritePlaces,
  updateFavorite,
  checkMethodLogin,
  changePassword,

  verifyEmail,
  requestVerifyCode,
  verifyUserCode,
  resetPassword,

  getAllFriend,

  getSuggestionEmail,
  getInfoUser,
  getTripPublicOfUser,
  getAllPlanOfUser
};
