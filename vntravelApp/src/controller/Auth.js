import axios from 'axios';
import {baseUrl} from '../utils/constants';

const signin = async data => {
  try {
    //TODO: Try with error
    let response;
    await axios
      .post(`${baseUrl}/auths/login/users?userRole=true`, data)
      .then(res => {
        response = res;
      })
      .catch(err => {
        response = err.response;
      });
    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

const signUp = async data => {
  try {
    //TODO: Try with error
    let response;
    await axios
      .post(`${baseUrl}/auths/register`, data)
      .then(res => {
        response = res;
      })
      .catch(err => {
        response = err.response;
      });
    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 *  Verify user by token
 * @param {*} token String
 * @returns response.data
 */
const verifyAccount = async token => {
  try {
    let response;
    await axios
      .get(`${baseUrl}/auths/verify`, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(res => {
        response = res;
      })
      .catch(err => {
        response = err.response;
      });

    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};
/**
 *
 * @param {*} data Sign in with google
 * @returns
 */
const signInWithGoogle = async token => {
  try {
    let response;
    await axios
      .post(`${baseUrl}/auths/login/google?userRole=true`, null, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(res => {
        response = res;
      })
      .catch(err => {
        response = err.response;
      });
    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 *  Verify email exist 
 * @param {*} email String
 * @returns response.data
 */
 const verifyEmailExist = async email => {
  try {
    let response;
    await axios
      .get(`${baseUrl}/auths/exists/${email}`)
      .then(res => {
        response = res;
      })
      .catch(err => {
        response = err.response;
      });

    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 *  Send code to verify email
 * @param {*} email String
 * @param {*} code String
 * @returns response.data
 */
 const requestVerifyCode = async (email, code) => {
  try {
    let response;
    // const code=Math.floor(100000 + Math.random() * 900000);
    await axios
      .get(`${baseUrl}/auths/send-email/${email}/${code}`)
      .then(res => {
        response = res;
      })
      .catch(err => {
        response = err.response;
      });

    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 *  Verify cod
 * @param {*} code String
 * @param {*} verifyCode String
 * @returns response.data
 */
 const verifyCode = async (code, verifyCode) => {
  try {
    let response;
    
    await axios
      .get(`${baseUrl}/auths/passwords/verify/${code}/${verifyCode}`)
      .then(res => {
        response = res;
      })
      .catch(err => {
        response = err.response;
      });

    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 *  Change password
 * @param {*} infoChange 
 * @returns response.data
 */
 const changeForgotPassword = async (infoChange) => {
  try {
    let response;
    // const code=Math.floor(100000 + Math.random() * 900000);
    await axios
      .put(`${baseUrl}/auths/passwords`,infoChange)
      .then(res => {
        response = res;
      })
      .catch(err => {
        response = err.response;
      });

    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};


export default {
  signin,
  verifyAccount,
  signInWithGoogle,
  signUp,

  verifyEmailExist,
  requestVerifyCode,

  verifyCode,
  changeForgotPassword
};
