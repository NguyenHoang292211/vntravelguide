import axios from 'axios';
import {baseUrl} from '../utils/constants';

/**
 * Create contribute
 * @returns places
 */
const createContribute = async (token, data) => {
  try {
    let response = null;
    await axios
      .post(`${baseUrl}/contributes`, data, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(res => {
        response = res;
      })
      .catch(err => {
        response = err.response;
      });

    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

export default {
  createContribute,
};
