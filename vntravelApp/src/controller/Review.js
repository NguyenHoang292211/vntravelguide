import axios from 'axios';
import {baseUrl} from '../utils/constants';

/**
 * Get All places
 * @returns places
 */
const getReviewsOfPlace = async placeId => {
  try {
    const response = await axios.get(`${baseUrl}/reviews/${placeId}`);

    if (response.data.success) {
      return {
        reviews: response.data.reviews,
      };
    } else {
      return {
        reviews: [],
      };
    }
  } catch (error) {
    console.log(error);
    return {
      reviews: [],
    };
  }
};

/**
 * Get All places
 * @returns places
 */
const createReview = async (token, data) => {
  try {
    let response = null;
    await axios
      .post(`${baseUrl}/reviews`, data, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(res => {
        response = res;
      })
      .catch(err => {
        response = err.response;
      });

    return response;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 * Update likeCount and users
 * @returns places
 */
const updateLikeStatus = async (reviewId, token) => {
  try {
    const response = await axios.put(
      `${baseUrl}/reviews/liked/${reviewId}`,
      {},
      {
        headers: {Authorization: `Bearer ${token}`},
      },
    );

    if (response.data.success) {
      return {
        review: response.data.review,
      };
    } else {
      return {
        review: null,
      };
    }
  } catch (error) {
    console.log(error);
    return {
      review: null,
    };
  }
};

/**
 * Update likeCount and users
 * @returns places
 */
const createReport = async (data, token) => {
  try {
    const response = await axios.post(`${baseUrl}/reports`, data, {
      headers: {Authorization: `Bearer ${token}`},
    });

    return response.data;
  } catch (error) {
    console.log(error);
    return {
      review: null,
    };
  }
};

export default {
  getReviewsOfPlace,
  updateLikeStatus,
  createReview,
  createReport,
};
