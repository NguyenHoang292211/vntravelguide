import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  fromScreen: null,
  goBack: false,
  previousScreen: null,
};

const screenSlice = createSlice({
  name: 'screenBack',
  initialState: initialState,
  reducers: {
    setGoBack: (state, action) => {
      if (action.payload.goBack != undefined)
        state.goBack = action.payload.goBack;
      if (action.payload.fromScreen)
        state.fromScreen = action.payload.fromScreen;
      if (action.payload.previousScreen)
        state.previousScreen = action.payload.previousScreen;
    },
    clearGoBack: state => {
      state.goBack = false;
      state.fromScreen = null;
      state.previousScreen = null;
    },
  },
});

export const {setGoBack, clearGoBack} = screenSlice.actions;
export default screenSlice.reducer;
