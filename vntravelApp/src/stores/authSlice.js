import {createSlice} from '@reduxjs/toolkit';
import {constants} from '../constants';
const initialState = {
  isLoading: false,
  userData: null,
  isAuthorized: false,
  token: null,
  language: constants.LANGUAGE[0],
};

const authSlice = createSlice({
  name: 'auth',
  initialState: initialState,
  reducers: {
    saveUserData: (state, action) => {
      let user = action.payload.user;

      state.isLoading = false;
      state.isAuthorized = true;
      state.token = action.payload.token;

      let favorite = user.favorite;
      user.recentSearch.forEach(place => {
        if (favorite.includes(place.id)) {
          place.isFavorite = true;
        } else {
          place.isFavorite = false;
        }
      });

      state.userData = user;
    },
    updateUserData: (state, action) => {
      state.userData = action.payload.user;
    },
    updateLanguage: (state, action) => {
      state.language = action.payload;
    },
    removeUserData: (state, action) => {
      state.userData = null;
      state.isAuthorized = false;
      state.token = null;
    },
    updateFavorite: (state, action) => {
      state.userData.recentSearch.forEach(place => {
        if (place.id === action.payload.placeId) {
          place.isFavorite = action.payload.favoriteValue;
        }
      });
    },
    updateInRecentPlace: (state, action) => {},
  },
});
export const {
  saveUserData,
  removeUserData,
  updateLanguage,
  updateUserData,
  updateFavorite,
  updateInRecentPlace,
} = authSlice.actions;
export default authSlice.reducer;
