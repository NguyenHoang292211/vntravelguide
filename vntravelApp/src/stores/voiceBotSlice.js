import {createSlice} from '@reduxjs/toolkit';
import {constants} from '../constants';

const initialState = {
  currentRoute: '',
  showBot: false,
};

const voiceBotSlice = createSlice({
  name: 'virtualBot',
  initialState,
  reducers: {
    updateStatus: (state, action) => {
      state.showBot = action.payload.showBot;
    },
  },
});

export const {updateStatus} = voiceBotSlice.actions;
export default voiceBotSlice.reducer;
