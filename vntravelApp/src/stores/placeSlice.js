import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import axios from 'axios';
import {baseUrl} from '../utils/constants';

export const getAllPlaces = createAsyncThunk(
  'place/Places',
  async (params, {rejectWithValue}) => {
    try {
      const response = await axios.get(`${baseUrl}/places?populate=true`);

      if (response.data.success) {
        return {
          places: response.data.places,
        };
      } else {
        return {
          places: [],
        };
      }
    } catch (error) {
      console.log(error);
      return rejectWithValue(error.message);
    }
  },
);

//Get all categories

export const getAllCategories = createAsyncThunk(
  'place/categories',
  async (params, {rejectWithValue}) => {
    try {
      const response = await axios.get(`${baseUrl}/categories/public`);

      if (response.data.success) {
        return {
          categories: response.data.categories,
        };
      } else {
        return {
          categories: [],
        };
      }
    } catch (error) {
      console.log(error);
      return rejectWithValue(error.message);
    }
  },
);

//Get all provinces
export const getAllProvinces = createAsyncThunk(
  'place/provinces',
  async (params, {rejectWithValue}) => {
    try {
      const response = await axios.get(`${baseUrl}/provinces/public`);

      if (response.data.success) {
        return {
          provinces: response.data.provinces,
        };
      } else {
        return {
          provinces: [],
        };
      }
    } catch (error) {
      console.log(error);
      return rejectWithValue(error.message);
    }
  },
);

const initialState = {
  places: [],
  popularPlaces: [],
  recommendedProvinces: [],
  recentPlaces: [],
  categories: [],
  provinces: [],
  currentCategory: null, //ID
  isLoading: false,
};

const placeSlice = createSlice({
  name: 'place',
  initialState: initialState,
  reducers: {
    setCurrentCategory: (state, action) => {
      state.currentCategory = action.payload;
    },
  },
  extraReducers: {
    //*Place
    [getAllPlaces.pending]: (state, action) => {
      state.isLoading = true;
    },
    [getAllPlaces.fulfilled]: (state, action) => {
      state.places = action.payload.places;

      isLoading = false;
    },
    [getAllPlaces.rejected]: (state, action) => {
      isLoading = false;
    },

    //*Categories
    [getAllCategories.pending]: (state, action) => {
      state.isLoading = true;
    },
    [getAllCategories.fulfilled]: (state, action) => {
      state.categories = action.payload.categories;

      isLoading = false;
    },
    [getAllCategories.rejected]: (state, action) => {
      isLoading = false;
    },
    //*Province
    [getAllProvinces.pending]: (state, action) => {
      state.isLoading = true;
    },
    [getAllProvinces.fulfilled]: (state, action) => {
      state.provinces = action.payload.provinces;
      isLoading = false;
    },
    [getAllProvinces.rejected]: (state, action) => {
      isLoading = false;
    },
  },
});
export const {setCurrentCategory} = placeSlice.actions;
export default placeSlice.reducer;
