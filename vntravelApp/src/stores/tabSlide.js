import {createSlice} from '@reduxjs/toolkit';
import {constants} from '../constants';

const initialState = {
  selectedTab: constants.screens.home,
};

const tabSlide = createSlice({
  name: 'sliceName',
  initialState,
  reducers: {
    setSelectedTab: (state, actions) => {
      state.selectedTab = actions.payload;
    },
  },
});

export const {setSelectedTab} = tabSlide.actions;
export default tabSlide.reducer;
