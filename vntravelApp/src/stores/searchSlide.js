import {createSlice} from '@reduxjs/toolkit';

import {mockupCategories, mockupPlacesByCategory} from '../assets/mockupData';
const initialState = {
  selectedPlace: {},
  destinationPlace: {},
  currentLocation: {},
  categories: [],
  places: [mockupPlacesByCategory],
};

const searchSlide = createSlice({
  name: 'SearchSlide',
  initialState,
  reducers: {
    setSelectedPlace: (state, actions) => {
      console.log('From action ', actions.payload);
      state.selectedPlace = actions.payload;
    },
    setDestinationPlace: (state, actions) => {
      state.destinationPlace = actions.payload;
    },
    setCurrentLocation: (state, actions) => {
      state.currentLocation = actions.payload;
    },
    resetSearchPlace: state => {
      (state.currentLocation = {}),
        (state.destinationPlace = {}),
        (state.selectedPlace = {});
    },
  },
});

export const {
  setSelectedPlace,
  setDestinationPlace,
  setCurrentLocation,
  resetSearchPlace,
} = searchSlide.actions;
export default searchSlide.reducer;
