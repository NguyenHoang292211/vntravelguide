import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  allPlan: [],
  currentPlan: null,
  isLoading: false,
};

const placeSlice = createSlice({
  name: 'plan',
  initialState: initialState,
  reducers: {
    setAllPlan: (state, action) => {
      state.allPlan = action.payload;
    },
    updatePlans: (state, action) => {
      let plans = state.allPlan.map(el =>
        el.id === action.payload.id ? {...el, ...action.payload} : el,
      );
      state.allPlan = plans;
    },
    deletePlan: (state, action) => {
      state.allPlan = state.allPlan.filter(
        item => item.id !== action.payload.id,
      );
    },
    addNewPlan: (state, action) => {
      state.allPlan.push(action.payload);
    },
    setCurrentPlan: (state, action) => {
      state.currentPlan = action.payload;
    },
  },
});

export const {setAllPlan, updatePlans, deletePlan, addNewPlan, setCurrentPlan} =
  placeSlice.actions;
export default placeSlice.reducer;
