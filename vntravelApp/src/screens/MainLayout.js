import React, {useEffect, useRef} from 'react';
import {FlatList, Image, Text, StatusBar, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';
import LottieView from 'lottie-react-native';
// import {setSelectedTab} from '../stores/tab/TabAction';
import {useDispatch, useSelector} from 'react-redux';
import {Header, TabButton} from '../components';
import {COLORS, constants, FONTS, icons, images, SIZES} from '../constants';
import {Home, Plan, Profile, Search} from '../screens';
import {setSelectedTab} from '../stores/tabSlide';
import Lang from '../language';
import {updateStatus} from '../stores/voiceBotSlice';

const MainLayout = ({navigation}) => {
  const flatListRef = useRef();
  const showBot = useSelector(state => state.voice.showBot);

  //Tab bottom with animate
  const homeTabFlex = useSharedValue(1);
  const homeTabColor = useSharedValue(COLORS.primary);

  const searchTabFlex = useSharedValue(1);
  const searchTabColor = useSharedValue(COLORS.white);

  const planTabFlex = useSharedValue(1);
  const planTabColor = useSharedValue(COLORS.white);

  const profileTabFlex = useSharedValue(1);
  const profileTabColor = useSharedValue(COLORS.white);
  const selectedTab = useSelector(state => state.tab.selectedTab);
  const dispatch = useDispatch();
  const user = useSelector(state => state.auth.userData);
  const lang = useSelector(state => state.auth.language);

  const isAuthorized = useSelector(state => state.auth.isAuthorized);
  //Reanimated animated styled
  const homeFlexStyle = useAnimatedStyle(() => {
    return {
      flex: homeTabFlex.value,
    };
  });

  const homeColorStyle = useAnimatedStyle(() => {
    return {
      backgroundColor: homeTabColor.value,
    };
  });

  const searchFlexStyle = useAnimatedStyle(() => {
    return {
      flex: searchTabFlex.value,
    };
  });

  const searchColorStyle = useAnimatedStyle(() => {
    return {
      backgroundColor: searchTabColor.value,
    };
  });

  const planFlexStyle = useAnimatedStyle(() => {
    return {
      flex: planTabFlex.value,
    };
  });

  const planColorStyle = useAnimatedStyle(() => {
    return {
      backgroundColor: planTabColor.value,
    };
  });

  const profileFlexStyle = useAnimatedStyle(() => {
    return {
      flex: profileTabFlex.value,
    };
  });

  const profileColorStyle = useAnimatedStyle(() => {
    return {
      backgroundColor: profileTabColor.value,
    };
  });

  useEffect(() => {
    dispatch(setSelectedTab(constants.screens.home));
  }, []);

  useEffect(() => {
    Lang.locale = lang.t;
  }, [lang]);

  useEffect(() => {
    //Animated Bottom tab
    if (selectedTab === constants.screens.home) {
      homeTabColor.value = withTiming(COLORS.primary);

      flatListRef?.current?.scrollToIndex({
        index: 0,
        animated: false,
      });
      homeTabFlex.value = withTiming(2, {duration: 500});
    } else {
      homeTabFlex.value = withTiming(1, {
        duration: 500,
      });
      homeTabColor.value = withTiming(COLORS.white);
    }

    if (selectedTab === constants.screens.search) {
      flatListRef?.current?.scrollToIndex({
        index: 1,
        animated: false,
      });
      searchTabFlex.value = withTiming(2, {duration: 500});
      searchTabColor.value = withTiming(COLORS.primary);
    } else {
      searchTabFlex.value = withTiming(1, {
        duration: 500,
      });
      searchTabColor.value = withTiming(COLORS.white);
    }

    if (selectedTab === constants.screens.my_plan) {
      flatListRef?.current?.scrollToIndex({
        index: 2,
        animated: false,
      });
      planTabFlex.value = withTiming(2, {duration: 500});
      planTabColor.value = withTiming(COLORS.primary, {
        duration: 500,
      });
    } else {
      planTabFlex.value = withTiming(1, {
        duration: 500,
      });
      planTabColor.value = withTiming(COLORS.white, {
        duration: 500,
      });
    }

    if (selectedTab === constants.screens.profile) {
      flatListRef?.current?.scrollToIndex({
        index: 3,
        animated: false,
      });
      profileTabFlex.value = withTiming(2, {duration: 500});
      profileTabColor.value = withTiming(COLORS.primary, {
        duration: 500,
        animated: false,
      });
    } else {
      profileTabFlex.value = withTiming(1, {
        duration: 500,
        animated: false,
      });
      profileTabColor.value = withTiming(COLORS.white2, {
        duration: 500,
        animated: false,
      });
    }
  }, [selectedTab]);

  const BottomTab = () => {
    return (
      <View>
        <View
          style={{
            flexDirection: 'row',
            borderRadius: 35,
            backgroundColor: COLORS.white,
            elevation: 7,
          }}>
          <TabButton
            label={constants.screens.home}
            icon={icons.home}
            isFocused={selectedTab === constants.screens.home}
            onPress={() => dispatch(setSelectedTab(constants.screens.home))}
            outerContainerStyle={homeFlexStyle}
            innerContainerStyle={homeColorStyle}
          />
          <TabButton
            label={constants.screens.search}
            icon={icons.search}
            outerContainerStyle={searchFlexStyle}
            innerContainerStyle={searchColorStyle}
            isFocused={selectedTab === constants.screens.search}
            onPress={() => dispatch(setSelectedTab(constants.screens.search))}
          />
          <TabButton
            label={constants.screens.my_plan}
            icon={icons.calendar}
            isFocused={selectedTab === constants.screens.my_plan}
            onPress={() => dispatch(setSelectedTab(constants.screens.my_plan))}
            outerContainerStyle={planFlexStyle}
            innerContainerStyle={planColorStyle}
          />
          <TabButton
            label={constants.screens.profile}
            icon={icons.profile}
            isFocused={selectedTab === constants.screens.profile}
            onPress={() => dispatch(setSelectedTab(constants.screens.profile))}
            outerContainerStyle={profileFlexStyle}
            innerContainerStyle={profileColorStyle}
          />
        </View>
      </View>
    );
  };
  return (
    <Animated.View
      style={{
        flex: 1,
        backgroundColor: COLORS.white,
      }}>
      {/* Header */}
      <StatusBar barStyle="default" backgroundColor={COLORS.lightBlue1} />
      {selectedTab !== constants.screens.profile && (
        <Header
          title={Lang.t(selectedTab)}
          titleStyle={{color: COLORS.primary}}
          containerStyle={{
            height: 60,
            paddingHorizontal: SIZES.radius,
            marginTop: 5,
          }}
          leftComponent={
            <TouchableOpacity
              style={{
                width: 60,
                height: 80,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: COLORS.white,
                flexDirection: 'row',
              }}
              onPress={() => navigation.navigate('Voice')}>
              <LottieView
                resizeMode="cover"
                source={images.smartBot}
                autoPlay
                loop
                autoSize
                style={{
                  height: 80,
                  // marginRight: 50,
                }}
              />
            </TouchableOpacity>
          }
          rightComponent={
            <TouchableOpacity
              onPress={() =>
                dispatch(setSelectedTab(constants.screens.profile))
              }
              style={{
                borderRadius: 23,
                alignItems: 'center',
                justifyContent: 'center',
                borderWidth: 2,

                borderColor: COLORS.white2,
                width: 45,
                height: 45,
              }}>
              <Image
                source={isAuthorized ? {uri: user.image} : images.avatar}
                style={{
                  borderRadius: 22,
                  width: 43,
                  height: 43,
                }}
              />
            </TouchableOpacity>
          }
        />
      )}

      {/* Content */}
      <View
        style={{
          flex: 1,
        }}>
        <FlatList
          ref={flatListRef}
          horizontal
          scrollEnabled={false}
          pagingEnabled
          snapToAlignment="center"
          snapToInterval={SIZES.width}
          showsHorizontalScrollIndicator={false}
          onScrollToIndexFailed={info => {
            const wait = new Promise(resolve => setTimeout(resolve, 500));
            wait.then(() => {
              flatListRef.current?.scrollToIndex({
                index: 0,
                animated: true,
              });
            });
          }}
          data={constants.bottom_tabs}
          keyExtractor={item => `${item.id}`}
          renderItem={({item}) => {
            return (
              <View
                style={{
                  height: SIZES.height,
                  width: SIZES.width,
                }}>
                {item.label === constants.screens.home && (
                  <Home navigation={navigation} />
                )}
                {item.label === constants.screens.search && (
                  <Search navigation={navigation} />
                )}
                {item.label === constants.screens.my_plan && (
                  <Plan navigation={navigation} />
                )}
                {item.label === constants.screens.profile && (
                  <Profile navigation={navigation} />
                )}
              </View>
            );
          }}
        />
      </View>
      {/* Bottom Tab */}

      <View
        style={{
          position: 'absolute',
          right: 0,
          left: 0,
          top: SIZES.height - 70,
          paddingVertical: 5,
          height: 70,
          marginHorizontal: SIZES.radius,
        }}>
        {BottomTab()}
      </View>
    </Animated.View>
  );
};

export default MainLayout;
