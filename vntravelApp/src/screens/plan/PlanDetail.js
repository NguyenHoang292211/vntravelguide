import MapboxGL from '@react-native-mapbox-gl/maps';
import LottieView from 'lottie-react-native';
import moment from 'moment';
import React, {useEffect, useRef, useState} from 'react';
import {
  Animated,
  Image,
  ImageBackground,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {Tooltip} from 'react-native-elements';
import {Divider} from 'react-native-elements/dist/divider/Divider';
import Spinner from 'react-native-spinkit';
import AntIcon from 'react-native-vector-icons/AntDesign';
import FeaIcon from 'react-native-vector-icons/Feather';
import AwesIcon from 'react-native-vector-icons/FontAwesome';
import IonIcon from 'react-native-vector-icons/Ionicons';
import MateIcon from 'react-native-vector-icons/MaterialIcons';
import {useDispatch, useSelector} from 'react-redux';
import {InformModal, Section, TextButton} from '../../components';
import {COLORS, FONTS, icons, images, SIZES} from '../../constants';
import {PlanCtrl} from '../../controller';
import Lang from '../../language';
import {deletePlan} from '../../stores/planSlice';
import {clearGoBack, setGoBack} from '../../stores/screenSlice';
import {setDestinationPlace} from '../../stores/searchSlide';

MapboxGL.setAccessToken(
  'pk.eyJ1IjoidGhhb2xlMzY2IiwiYSI6ImNrcnpydnJwaDAxOG8ydXFtOXdwNWR0eGsifQ.FCbrlFuxwWzIghMVU_T0iA',
);

const InfoPlace = (place, onNavigation) => {
  const goDirect = () => {
    onNavigation(place);
  };

  return (
    <View>
      <View style={{flexDirection: 'row', flex: 1}}>
        <TouchableOpacity
          style={{
            padding: SIZES.base * 2,
            borderRadius: SIZES.base,
            width: 55,
            height: 55,
            alignContent: 'center',
            justifyContent: 'center',
            backgroundColor: COLORS.transparentPrimary3,
          }}>
          <IonIcon name={icons.car} color={COLORS.primary} size={26} />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            padding: SIZES.base * 2,
            borderRadius: SIZES.base,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: COLORS.primary,
            marginLeft: SIZES.base,
            flex: 1,
          }}
          onPress={goDirect}>
          <Text style={{...FONTS.body3, color: COLORS.white}}>More Direct</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const CARD_WIDTH = SIZES.width * 0.9;
const CARD_HEIGHT = 220;
const PlanDetail = ({navigation, route}) => {
  const token = useSelector(state => state.auth.token);
  const goBack = useSelector(state => state.screen.goBack);
  const fromScreen = useSelector(state => state.screen.fromScreen);
  const currentPlan = useSelector(state => state.plan.currentPlan);
  const dispatch = useDispatch();
  const [alertMessage, setAlertMessage] = useState('');
  const [alert, setAlert] = useState(false);
  const [plan, setPlan] = useState(route.params.item);
  const [sections, setSections] = useState([]);
  const [index, setIndex] = useState(0);
  const [loadingSection, setLoadingSection] = useState(true);
  const [isDeleting, setIsDeleting] = useState(false);
  const [deleteSuccess, setDeleteSuccess] = useState(false);
  const [isShowBottomInfo, setIsShowBottomInfo] = useState(false);
  const [comfirmDelete, setComfirmDelete] = useState(false);
  const [places, setPlaces] = useState([]);
  const _map = useRef(null);
  const scrollX = useRef(new Animated.Value(0)).current;
  const flatListRef = useRef(null);
  const viewConfigRef = React.useRef({viewAreaCoveragePercentThreshold: 20});
  const [currentIndex, setCurrentIndex] = useState(0);
  const onViewChangeRef = useRef(({viewableItems, changed}) => {
    setCurrentIndex(viewableItems[0].index);
  });
  //Map animated scroll
  let mapAnimation = new Animated.Value(0);

  const [focusPoint, setFocusPoint] = useState([
    places[0] ? places[0].place.longtitude : 106.816487,
    places[0] ? places[0].place.lattitude : 10.781625,
  ]);
  const deletePlan_ = () => {
    // deleteSuccess(false);
    setIsDeleting(true);
    PlanCtrl.deletePlan(token, plan.id)
      .then(res => {
        if (res) {
          if (res.data.success) {
            setAlert(true);
            setAlertMessage(Lang.t('delete_success'));
            setDeleteSuccess(true);
            dispatch(deletePlan(plan));
          } else {
            setAlert(true);
            setAlertMessage(Lang.t('delete_fail'));
          }
        } else {
          setAlert(true);
          setAlertMessage(Lang.t('delete_fail'));
        }
      })
      .catch(err => {
        setAlert(true);
        setAlertMessage(err);
      })
      .finally(() => setIsDeleting(false));
  };

  const setPlaceList = sections => {
    let places_ = [];
    sections.forEach(section => {
      section.places.forEach(place => {
        places_.push(place);
      });
    });
    setPlaces(places_);
  };

  const interpolations = places.map((place, index) => {
    const inputRange = [
      (index - 1) * CARD_WIDTH,
      index * CARD_WIDTH,
      (index + 1) * CARD_WIDTH,
    ];

    const scale = mapAnimation.interpolate({
      inputRange,
      outputRange: [1, 1.5, 1],
      extrapolate: 'clamp',
    });
    return {scale};
  });

  useEffect(() => {
    if (currentPlan) {
      PlanCtrl.getSectionOfPlan(token, currentPlan.id)
        .then(res => {
          if (res.data.success) {
            setSections(
              res.data.sections.sort(
                (a, b) => new Date(a.start) - new Date(b.start),
              ),
            );
            setPlaceList(res.data.sections);
          } else if (res.status === 400) {
            setAlert(true);
            setAlertMessage(Lang.t('plan_not_exist'));
            navigation.goBack();
          } else {
            setAlert(true);
            setAlertMessage(Lang.t('some_error'));
            navigation.goBack();
          }
        })
        .catch(err => {
          setAlert(true);
          setAlertMessage(err);
        })
        .finally(() => {
          setLoadingSection(false);
        });
    }
    return () => {};
  }, []);

  useEffect(() => {
    if (goBack === true && fromScreen === 'TripPlan' && plan) {
      PlanCtrl.getSectionOfPlan(token, plan.id)
        .then(res => {
          if (res.data.success) {
            setSections(
              res.data.sections.sort(
                (a, b) => new Date(a.start) - new Date(b.start),
              ),
            );
            setPlaceList(res.data.sections);
          } else {
            setAlert(true);
            setAlertMessage(Lang.t('some_error'));
          }
        })
        .then(err => console.log(err))
        .finally(() => {
          setLoadingSection(false);
        });
      dispatch(clearGoBack());
    }
    return () => {};
  }, [goBack, fromScreen]);
  /* -------------------------------------------------------------------------- */
  /*                                  RENDER UI                                 */
  /* -------------------------------------------------------------------------- */

  /* ----------------------------------- TAB BAR---------------------------------- */

  const renderTab = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
          height: 50,
          justifyContent: 'space-around',
        }}>
        <TouchableOpacity
          onPress={() => setIndex(0)}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Animatable.Text
            animation="zoomIn"
            easing="ease-in-out-quad"
            style={{
              textAlign: 'left',
              color: index === 0 ? COLORS.primary : COLORS.gray2,
              ...FONTS.h4,
              fontSize: 16,
            }}>
            {Lang.t('overview')}
          </Animatable.Text>
          <Animatable.View
            transition="backgroundColor"
            // animation={tab1Animation}
            duration={600}
            easing="ease-in-out-back"
            style={{
              justifyContent: 'flex-end',
              width: 30,
              height: 6,
              borderRadius: 5,
              backgroundColor: index == 0 ? COLORS.primary : COLORS.white,
              marginTop: 3,
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setIndex(1)}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Animatable.Text
            animation="zoomIn"
            easing="ease-in-out-quad"
            style={{
              textAlign: 'left',
              color: index === 1 ? COLORS.primary : COLORS.gray2,
              ...FONTS.h4,
              fontSize: 16,
            }}>
            {Lang.t('route')}
          </Animatable.Text>
          <Animatable.View
            transition="backgroundColor"
            // animation={tab1Animation}
            duration={600}
            easing="ease-in-out-back"
            style={{
              justifyContent: 'flex-end',
              width: 30,
              height: 6,
              borderRadius: 5,
              backgroundColor: index === 1 ? COLORS.primary : COLORS.white,
              marginTop: 3,
            }}
          />
        </TouchableOpacity>
      </View>
    );
  };

  /* -------------------------------- OVERVIEW -------------------------------- */
  const renderOverview = () => {
    return (
      <Animatable.View
        animation="slideInRight"
        duration={400}
        easing="ease-in-sine"
        style={{
          width: SIZES.width,
          backgroundColor: COLORS.white,
          flex: 1,
        }}>
        {/* NOTES */}
        <View>
          {/* Title */}
          <View
            style={{
              flexDirection: 'row',
              paddingVertical: 10,
              paddingHorizontal: SIZES.radius,
              backgroundColor: COLORS.lightGray,
              alignItems: 'center',
            }}>
            <IonIcon name={icons.note} size={30} color={COLORS.darkGray1} />
            <Text
              style={{
                ...FONTS.h2,
                color: COLORS.black,
                marginLeft: 3,
                fontSize: 15,
              }}>
              {Lang.t('description')}
            </Text>
          </View>
          {/* Note text */}
          <TouchableOpacity
            onPress={() => navigation.navigate('EditPlan', {plan: currentPlan})}
            style={{
              padding: 10,
            }}>
            <Text
              style={{
                ...FONTS.body4,
                color: plan.note === '' ? COLORS.gray2 : COLORS.darkGray1,
                textAlign: 'center',
              }}>
              {currentPlan.note === '' || !currentPlan.note
                ? Lang.t('note_plan')
                : currentPlan.note}
            </Text>
          </TouchableOpacity>
        </View>
        {/* PLACES TO VISIT */}
        <View>
          {/* Title */}

          {/* Note text */}
          <View
            style={{
              paddingHorizontal: SIZES.base,
              backgroundColor: COLORS.white,
              paddingVertical: 15,
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-around',
            }}>
            <TextButton
              onPress={() => navigation.navigate('TripPlan', {item: plan})}
              label={Lang.t('trip_plan')}
              labelStyle={{
                fontSize: 15,
              }}
              buttonContainerStyle={{
                width: '32%',
                height: 55,
                backgroundColor: COLORS.lightBlue1,
                borderRadius: 12,
              }}
            />
            <TextButton
              label={Lang.t('collection')}
              onPress={() => navigation.navigate('Collection')}
              labelStyle={{
                fontSize: 15,
              }}
              buttonContainerStyle={{
                width: '32%',
                height: 55,
                backgroundColor: COLORS.lightBlue1,
                borderRadius: 12,
              }}
            />
            <TextButton
              label={Lang.t('memories')}
              labelStyle={{
                fontSize: 15,
              }}
              buttonContainerStyle={{
                width: '32%',
                height: 55,
                backgroundColor: COLORS.lightBlue1,
                borderRadius: 10,
              }}
            />
          </View>
          {/* Plan overview */}
          <View
            style={{
              marginVertical: SIZES.radius,
              alignItems: 'center',
            }}>
            {loadingSection ? (
              <View
                style={{
                  height: 320,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Spinner color={COLORS.primary} size={32} type="9CubeGrid" />
              </View>
            ) : (
              <View>
                {sections &&
                  sections.map((item, index) => {
                    return (
                      <Section
                        navigation={navigation}
                        item={item}
                        index={index}
                        key={index}
                        planStartDate={plan.start}
                      />
                    );
                  })}
                {!sections ||
                  (sections.length === 0 && (
                    <View
                      style={{
                        height: SIZES.height * 0.45,
                        alignItems: 'center',
                      }}>
                      <LottieView
                        source={images.calendarGif}
                        autoPlay
                        loop
                        style={{
                          height: 120,
                          width: 120,
                        }}
                      />
                      <Text
                        style={{
                          textAlign: 'center',
                          color: COLORS.darkGray,
                          ...FONTS.body4,
                          padding: 15,
                        }}>
                        {Lang.t('no_section')}
                      </Text>
                    </View>
                  ))}
              </View>
            )}
          </View>
        </View>
      </Animatable.View>
    );
  };

  /* ---------------------------------- ROUTE --------------------------------- */
  const renderRoute = () => {
    const onNavigation = place => {
      dispatch(setDestinationPlace(place));
      navigation.navigate('Search1', {
        screen: 'FindPlace',
        params: {
          isSearchDirect: true,
          isFromOriginStack: false,
        },
      });
    };
    return (
      <Animatable.View
        animation="slideInLeft"
        easing="ease-in-sine"
        duration={400}
        style={{
          width: SIZES.width,
          backgroundColor: COLORS.white,
          flex: 1,
        }}>
        <View
          style={{
            backgroundColor: COLORS.lightGray,
            height: SIZES.height * 0.93,
            width: SIZES.width,
          }}>
          <>
            <MapboxGL.MapView
              style={{height: '100%', width: '100%'}}
              zoomEnabled={true}
              // ref={mapRef}

              logoEnabled={false}
              animated={true}
              tintColor={COLORS.lightBlue1}>
              <MapboxGL.Camera
                animationMode="flyTo"
                allowUpdates={true}
                followUserMode="compass"
                ref={_map}
                zoomLevel={8}
                centerCoordinate={[
                  places.length === 0
                    ? 106.816487
                    : places[currentIndex]?.place.longtitude,
                  places.length === 0
                    ? 10.781625
                    : places[currentIndex]?.place.lattitude,
                ]}
              />
              {/* Render place point */}
              {places?.map((place, i) => {
                let day =
                  new Date(place.visitedTime).getDate() -
                  new Date(plan.start).getDate() +
                  1;
                return (
                  <MapboxGL.PointAnnotation
                    id={
                      place.place.id +
                      i +
                      (Math.random() + 1).toString(36).substring(7)
                    }
                    key={
                      place.place.id +
                      (Math.random() + 1).toString(36).substring(7)
                    }
                    title={place.place.name}
                    coordinate={[place.place.longtitude, place.place.lattitude]}
                    selected={true}
                    onSelected={e => {
                      flatListRef?.current?.scrollToIndex({
                        index: i,
                        animated: true,
                      });
                      // setCurrentIndex(i);
                    }}>
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <TouchableOpacity
                        onPress={() => console.log(place.place.name)}
                        style={{
                          width: 50,
                          height: 50,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <IonIcon
                          // source={images.location}
                          name={icons.locationFill}
                          size={currentIndex === i ? 50 : 40}
                          color={currentIndex === i ? COLORS.red : COLORS.blue}
                        />
                        <View
                          style={{
                            position: 'absolute',
                            top: 10,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: COLORS.white,
                            borderRadius: 10,
                            height: 20,
                            width: 20,
                          }}>
                          <Text
                            style={{
                              color:
                                currentIndex === i ? COLORS.red : COLORS.blue,
                              ...FONTS.h4,
                              fontSize: 12,
                              lineHeight: 12,
                            }}>
                            {day}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </MapboxGL.PointAnnotation>
                );
              })}
            </MapboxGL.MapView>
          </>
        </View>
      </Animatable.View>
    );
  };

  const renderPlaceInfo = () => {
    return (
      <Animated.FlatList
        ref={flatListRef}
        style={{
          position: 'absolute',
          top: SIZES.height - CARD_HEIGHT - 10,
          height: CARD_HEIGHT + 15,
        }}
        contentContainerStyle={{
          paddingHorizontall: SIZES.padding,
        }}
        horizontal
        scrollEventThrottle={16}
        pagingEnabled
        snapToAlignment="center"
        snapToInterval={CARD_WIDTH + 10}
        showsHorizontalScrollIndicator={false}
        onViewableItemsChanged={onViewChangeRef.current}
        viewabilityConfig={viewConfigRef.current}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {x: scrollX}}}],
          {useNativeDriver: false},
        )}
        data={places}
        renderItem={({item, i}) => {
          let place = item.place;
          let day =
            new Date(item.visitedTime).getDate() -
            new Date(plan.start).getDate() +
            1;
          return (
            <View
              key={i}
              style={{
                height: CARD_HEIGHT,
                width: CARD_WIDTH,
                backgroundColor: COLORS.white2,
                borderRadius: SIZES.radius,
                elevation: 6,
                marginHorizontal: SIZES.base,
              }}>
              <Image
                source={{uri: place.images[0]}}
                resizeMode="cover"
                style={{
                  height: CARD_HEIGHT * 0.55,
                  width: CARD_WIDTH,
                  borderTopLeftRadius: SIZES.radius,
                  borderTopRightRadius: SIZES.radius,
                }}
              />
              <View
                style={{
                  position: 'absolute',
                  top: 0,
                  height: CARD_HEIGHT * 0.55,
                  width: CARD_WIDTH,
                  backgroundColor: COLORS.transparentBlack1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderTopLeftRadius: SIZES.radius,
                  borderTopRightRadius: SIZES.radius,
                }}>
                <Text
                  style={{
                    color: COLORS.transparentWhite7,
                    ...FONTS.h2,
                    fontSize: 22,
                  }}>
                  {Lang.t('day')} {day}
                </Text>
                <Text
                  style={{
                    color: COLORS.transparentWhite8,
                    ...FONTS.h4,
                    fontSize: 14,
                  }}>
                  {moment.utc(item.visitedTime).format('LT')}
                </Text>
                <Text
                  style={{
                    color: COLORS.transparentWhite8,
                    ...FONTS.h4,
                    fontSize: 14,
                  }}>
                  {moment.utc(item.visitedTime).format('LL')}
                </Text>
              </View>
              <View
                style={{paddingHorizontal: SIZES.radius, paddingVertical: 5}}>
                <Text
                  style={{
                    color: COLORS.darkGray1,
                    ...FONTS.h5,
                    lineHeight: 17,
                  }}
                  numberOfLines={1}>
                  {place.name}
                </Text>
                <Text
                  style={{
                    color: COLORS.darkGray1,
                    ...FONTS.body5,
                    lineHeight: 17,
                  }}
                  numberOfLines={1}>
                  {place.address}
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'flex-start',
                    marginTop: 3,
                  }}>
                  <Text
                    style={{
                      color: COLORS.primary,
                      ...FONTS.h5,
                      lineHeight: 18,
                      fontSize: 14,
                    }}
                    numberOfLines={1}>
                    {item.expense} VND
                  </Text>
                  <TextButton
                    onPress={() =>
                      navigation.navigate('Main', {
                        screen: 'DetailPlace',
                        params: {item: place},
                      })
                    }
                    label={Lang.t('view_more')}
                    labelStyle={{
                      color: COLORS.orange,
                      ...FONTS.h5,
                    }}
                    buttonContainerStyle={{
                      height: 35,
                      width: 120,
                      backgroundColor: null,
                      borderWidth: 1,
                      borderColor: COLORS.orange,
                    }}
                  />
                </View>
              </View>
            </View>
          );
        }}>
        {/* {sections?.map((section, i) => {
          let day =
            new Date(section.start).getDate() -
            new Date(plan.start).getDate() +
            1;

          return section.places.map((item, i_) => {
            let place = item.place;
            return (
              <View
                key={i_}
                style={{
                  height: CARD_HEIGHT,
                  width: CARD_WIDTH,
                  backgroundColor: COLORS.white2,
                  borderRadius: SIZES.radius,
                  elevation: 6,
                  marginHorizontal: SIZES.base,
                }}>
                <Image
                  source={{uri: place.images[0]}}
                  resizeMode="cover"
                  style={{
                    height: CARD_HEIGHT * 0.55,
                    width: CARD_WIDTH,
                    borderTopLeftRadius: SIZES.radius,
                    borderTopRightRadius: SIZES.radius,
                  }}
                />
                <View
                  style={{
                    position: 'absolute',
                    top: 0,
                    height: CARD_HEIGHT * 0.55,
                    width: CARD_WIDTH,
                    backgroundColor: COLORS.transparentBlack1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderTopLeftRadius: SIZES.radius,
                    borderTopRightRadius: SIZES.radius,
                  }}>
                  <Text
                    style={{
                      color: COLORS.transparentWhite7,
                      ...FONTS.h2,
                      fontSize: 22,
                    }}>
                    DAY {day}
                  </Text>
                  <Text
                    style={{
                      color: COLORS.transparentWhite8,
                      ...FONTS.h4,
                      fontSize: 14,
                    }}>
                    {moment.utc(item.visitedTime).format('LT')}
                  </Text>
                  <Text
                    style={{
                      color: COLORS.transparentWhite8,
                      ...FONTS.h4,
                      fontSize: 14,
                    }}>
                    {moment.utc(item.visitedTime).format('LL')}
                  </Text>
                </View>
                <View
                  style={{paddingHorizontal: SIZES.radius, paddingVertical: 5}}>
                  <Text
                    style={{
                      color: COLORS.darkGray1,
                      ...FONTS.h5,
                      lineHeight: 17,
                    }}
                    numberOfLines={1}>
                    {place.name}
                  </Text>
                  <Text
                    style={{
                      color: COLORS.darkGray1,
                      ...FONTS.body5,
                      lineHeight: 17,
                    }}
                    numberOfLines={1}>
                    {place.address}
                  </Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'flex-start',
                      marginTop: 3,
                    }}>
                    <Text
                      style={{
                        color: COLORS.primary,
                        ...FONTS.h5,
                        lineHeight: 18,
                        fontSize: 14,
                      }}
                      numberOfLines={1}>
                      {item.expense} VND
                    </Text>
                    <TextButton
                      label="View more"
                      labelStyle={{
                        color: COLORS.orange,
                        ...FONTS.h5,
                      }}
                      buttonContainerStyle={{
                        height: 35,
                        width: 120,
                        backgroundColor: null,
                        borderWidth: 1,
                        borderColor: COLORS.orange,
                      }}
                    />
                  </View>
                </View>
              </View>
            );
          });
        })} */}
      </Animated.FlatList>
    );
  };

  /* ------------------------------ Render HEADER ----------------------------- */
  const renderHeader = () => {
    return (
      <View
        style={{
          justifyContent: 'space-between',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <TouchableOpacity
          activeOpacity={0.7}
          style={{
            height: 50,
            width: 50,
          }}
          onPress={() => navigation.goBack()}>
          <AwesIcon name={icons.angleLeft} size={33} color={COLORS.white} />
        </TouchableOpacity>
        <Tooltip
          withOverlay={false}
          backgroundColor={COLORS.white}
          overlayColor={COLORS.transparentBlack1}
          withPointer={false}
          skipAndroidStatusBar={true}
          containerStyle={{
            paddingHorizontal: SIZES.radius,
            paddingVertical: SIZES.radius,
            height: 85,
            alignItems: 'stretch',
          }}
          popover={
            <View>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate('EditPlan', {plan: currentPlan})
                }
                style={{
                  flexDirection: 'row',
                  // paddingVertical: SIZES.radius,
                  // paddingHorizontal: SIZES.radius,
                  justifyContent: 'space-around',
                }}>
                <Text
                  style={{
                    color: COLORS.darkGray1,
                    ...FONTS.body4,
                  }}>
                  {Lang.t('edit')}
                </Text>
                <MateIcon name={icons.pen} color={COLORS.gray2} size={23} />
              </TouchableOpacity>
              <Divider
                width={1}
                color={COLORS.lightGray}
                style={{
                  width: '100%',
                  marginVertical: SIZES.base,
                }}
              />
              <TouchableOpacity
                onPress={() => setComfirmDelete(true)}
                style={{
                  flexDirection: 'row',
                  // paddingVertical: SIZES.radius,
                  // paddingHorizontal: SIZES.radius,
                  justifyContent: 'space-around',
                }}>
                <Text
                  style={{
                    color: COLORS.darkGray1,
                    ...FONTS.body4,
                  }}>
                  {Lang.t('delete')}
                </Text>
                <AntIcon name={icons.delete_} color={COLORS.gray2} size={23} />
              </TouchableOpacity>
            </View>
          }>
          <FeaIcon name={icons.more} size={28} color={COLORS.white} />
        </Tooltip>
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
      }}>
      {/* Head background */}
      <Animatable.View
        duration={500}
        direction="alternate"
        delay={100}
        easing="ease-in-cubic"
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
        }}>
        <ImageBackground
          source={
            currentPlan ? {uri: currentPlan.photoUrl} : images.background3
          }
          resizeMode="cover"
          style={{
            height: SIZES.height * 0.35,
            width: SIZES.width,
            paddingHorizontal: SIZES.radius,
            paddingVertical: SIZES.radius,
          }}>
          <View
            style={{
              backgroundColor: COLORS.black,
              position: 'absolute',
              height: SIZES.height * 0.35,
              width: SIZES.width,
              top: 0,
              left: 0,
              opacity: 0.3,
            }}
          />
        </ImageBackground>
      </Animatable.View>
      {/* CONTENT */}
      <View
        style={{
          flex: 1,
          height: SIZES.height,
          width: SIZES.width,
        }}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingTop: SIZES.radius,
          }}>
          {/* Head */}
          <View
            style={{
              height: SIZES.height * 0.25,
              width: SIZES.width,
              paddingHorizontal: SIZES.radius,
              flex: 1,
            }}>
            {renderHeader()}
            <View
              style={{
                height: SIZES.height * 0.17,
                justifyContent: 'flex-end',
                paddingBottom: SIZES.radius,
              }}>
              <Text
                style={{
                  color: COLORS.white,
                  ...FONTS.h2,
                  fontSize: 22,
                }}>
                {currentPlan.name}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 10,
                }}>
                <AntIcon
                  name={icons.calendar}
                  size={26}
                  color={COLORS.white2}
                />
                <Text
                  style={{
                    color: COLORS.white,
                    ...FONTS.body3,
                    fontSize: 15,
                    marginLeft: 3,
                  }}>
                  {moment(currentPlan.start).utc(true).format('ll')} -{' '}
                </Text>
                <Text
                  style={{
                    color: COLORS.white,
                    ...FONTS.body3,
                  }}>
                  {moment(currentPlan.end).utc(true).format('ll')}
                </Text>
              </View>
            </View>
          </View>
          {/* Content */}
          <View
            style={{
              backgroundColor: COLORS.white,
              width: SIZES.width,
              paddingTop: 10,
              borderTopRightRadius: SIZES.padding * 2,
              borderTopLeftRadius: SIZES.padding * 2,
            }}>
            {renderTab()}
            {index === 0 && renderOverview()}
            {index === 1 && renderRoute()}
          </View>
        </ScrollView>
        {index === 1 && renderPlaceInfo()}
      </View>
      <InformModal
        isVisible={comfirmDelete}
        setIsVisible={setComfirmDelete}
        description={`Make sure that you want to delete this plan?`}
        title=""
        titleStyle={{
          fontSize: 10,
        }}
        containerStyle={{backgroundColor: COLORS.white3}}
        descriptionStyle={{
          fontSize: 15,
          paddingHorizontal: 2,
          color: COLORS.darkGray1,
        }}
        actionComponent={
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              marginTop: 15,
            }}>
            <TextButton
              onPress={() => setComfirmDelete(false)}
              labelStyle={{
                fontSize: 15,
              }}
              label={Lang.t('cancel')}
              buttonContainerStyle={{
                height: 45,
                width: 120,
                backgroundColor: COLORS.orange,
                borderRadius: 7,
              }}
            />
            <TextButton
              onPress={() => {
                setComfirmDelete(false);
                deletePlan_();
              }}
              labelStyle={{
                fontSize: 15,
              }}
              label={Lang.t('delete')}
              buttonContainerStyle={{
                height: 45,
                width: 120,
                borderRadius: 7,
              }}
            />
          </View>
        }
      />
      {isDeleting && (
        <View
          style={{
            backgroundColor: COLORS.transparentBlack1,
            position: 'absolute',
            height: SIZES.height,
            width: SIZES.width,
            alignItems: 'center',
            top: 0,
            left: 0,
            zIndex: 10,
            justifyContent: 'center',
          }}>
          <Spinner type="9CubeGrid" color={COLORS.primary} size={35} />
        </View>
      )}
      {alert && (
        <Animatable.View
          onAnimationEnd={() => {
            setAlert(false);
            // setStartSave(false);
            if (deleteSuccess) {
              dispatch(setGoBack({goBack: true, fromScreen: 'PlanDetail'}));
              navigation.goBack();
            }
          }}
          delay={2000}
          animation="fadeOut"
          duration={100}
          style={{
            backgroundColor: COLORS.transparentBlack1,
            position: 'absolute',
            height: SIZES.height,
            width: SIZES.width,
            alignItems: 'center',
            top: 0,
            left: 0,
            zIndex: 10,
            justifyContent: 'center',
          }}>
          <Animatable.View
            animation="zoomIn"
            easing="ease-in-cubic"
            duration={300}
            style={{
              height: 100,
              width: 150,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: SIZES.radius,
              backgroundColor: COLORS.transparentBlack7,
            }}>
            <Text
              style={{
                color: COLORS.white,
                ...FONTS.h4,
                textAlign: 'center',
              }}>
              {alertMessage}
            </Text>
          </Animatable.View>
        </Animatable.View>
      )}
    </View>
  );
};

export default PlanDetail;
