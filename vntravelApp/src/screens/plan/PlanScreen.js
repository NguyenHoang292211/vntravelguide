import moment from 'moment';
import React, {useEffect, useState} from 'react';
import {
  RefreshControl,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import Spinner from 'react-native-spinkit';
import {SceneMap, TabView} from 'react-native-tab-view';
import MaterIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useDispatch, useSelector} from 'react-redux';
import {InformModal, PlanCard, TextButton} from '../../components';
import {COLORS, constants, FONTS, images, SIZES} from '../../constants';
import {PlanCtrl} from '../../controller';
import {setAllPlan} from '../../stores/planSlice';
import {clearGoBack} from '../../stores/screenSlice';
import Lang from '../../language';

const PlanScreen = ({navigation}) => {
  const [index, setIndex] = useState(0);
  const isAuthorized = useSelector(state => state.auth.isAuthorized);
  const dispatch = useDispatch();
  const token = useSelector(state => state.auth.token);
  const plans = useSelector(state => state.plan.allPlan);
  const [allPlans, setAllPlans] = useState(null);
  const [activePlans, setActivePlans] = useState(null);
  const [upcomingPlans, setUpcomingPlans] = useState(null);
  const [pastPlans, setPastPlans] = useState(null);
  const [loginModalVisible, setLoginModalVisible] = useState(false);
  const [isLoadingPlan, setIsLoadingPlan] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  const onRefresh = () => {
    setRefreshing(true);
    if (isAuthorized) loadAllPlan();
  };

  const classifyPlan = plans => {
    let upcoming = [];
    let active = [];
    let past = [];

    for (let i = 0; i < plans.length; i++) {
      let element = plans[i];

      if (
        moment.utc(element.start).isSameOrBefore(moment(), 'date') &&
        moment.utc(element.end).isSameOrAfter(moment(), 'date')
      )
        active.push(element);
      else if (moment.utc(element.end).isBefore(moment(), 'date'))
        past.push(element);
      else if (moment.utc(element.start).isAfter(moment(), 'date'))
        upcoming.push(element);
    }
    setActivePlans(active);
    setUpcomingPlans(upcoming);
    setPastPlans(past);
  };

  const loadAllPlan = () => {
    PlanCtrl.getAllPlanOfUser(token)
      .then(res => {
        if (res.data.success) {
          dispatch(setAllPlan(res.data.plans));
          classifyPlan(res.data.plans);
        } else {
          setAllPlans([]);
        }
      })
      .catch(err => {
        console.log(err);
      })
      .finally(() => {
        setIsLoadingPlan(false);
        setRefreshing(false);
      });
  };

  useEffect(() => {
    if (token) {
      setIsLoadingPlan(true);
      loadAllPlan();
    }
  }, [isAuthorized]);

  useEffect(() => {
    if (plans) {
      classifyPlan(plans);
      dispatch(clearGoBack());
    }

    return () => {};
  }, [plans]);
  /* -------------------------------------------------------------------------- */
  /*                                  Render UI                                 */
  /* -------------------------------------------------------------------------- */
  const [routes] = useState([
    {key: 'firstTab', title: 'active'},
    {key: 'secondTab', title: 'upcoming'},
    {key: 'thirdTab', title: 'past'},
  ]);

  const activeRoute = () => {
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={() => onRefresh()}
          />
        }
        contentContainerStyle={{
          alignItems: 'center',
          backgroundColor: COLORS.white2,
          paddingVertical: SIZES.radius,
          paddingBottom: 130,
        }}>
        {isLoadingPlan && (
          <View
            style={{
              height: SIZES.height * 0.45,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Spinner
              isVisible={true}
              size={40}
              type="9CubeGrid"
              color={COLORS.primary}
            />
          </View>
        )}
        {activePlans &&
          !isLoadingPlan &&
          activePlans.map((item, index) => {
            return <PlanCard key={index} item={item} navigation={navigation} />;
          })}

        {!isLoadingPlan && activePlans !== null && activePlans.length === 0 && (
          <Text
            style={{
              ...FONTS.body3,
              color: COLORS.gray2,
              textAlign: 'center',
              marginTop: 100,
              height: SIZES.height * 0.45,
            }}>
            {Lang.t('no_plan')}
          </Text>
        )}
      </ScrollView>
    );
  };

  const upcomingRoute = () => {
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={() => onRefresh()}
          />
        }
        contentContainerStyle={{
          alignItems: 'center',
          backgroundColor: COLORS.white2,
          paddingVertical: SIZES.radius,
          paddingBottom: 130,
        }}>
        {isLoadingPlan && (
          <View
            style={{
              height: SIZES.height * 0.45,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Spinner
              isVisible={true}
              size={40}
              type="9CubeGrid"
              color={COLORS.primary}
            />
          </View>
        )}
        {upcomingPlans &&
          !isLoadingPlan &&
          upcomingPlans.map((item, index) => {
            return <PlanCard key={index} item={item} navigation={navigation} />;
          })}

        {!isLoadingPlan &&
          upcomingPlans !== null &&
          upcomingPlans.length === 0 && (
            <Text
              style={{
                ...FONTS.body3,
                color: COLORS.gray2,
                textAlign: 'center',
                marginTop: 100,
              }}>
              {Lang.t('no_plan')}
            </Text>
          )}
      </ScrollView>
    );
  };

  const pastRoute = () => {
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={() => onRefresh()}
          />
        }
        contentContainerStyle={{
          alignItems: 'center',
          backgroundColor: COLORS.white2,
          paddingVertical: SIZES.radius,
          paddingBottom: 130,
        }}>
        {isLoadingPlan && (
          <View
            style={{
              height: SIZES.height * 0.45,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Spinner
              isVisible={true}
              size={40}
              type="9CubeGrid"
              color={COLORS.primary}
            />
          </View>
        )}
        {pastPlans &&
          !isLoadingPlan &&
          pastPlans.map((item, index) => {
            return <PlanCard key={index} item={item} navigation={navigation} />;
          })}
        {!isLoadingPlan && pastPlans && pastPlans.length === 0 && (
          <Text
            style={{
              ...FONTS.body3,
              color: COLORS.gray2,
              textAlign: 'center',
              marginTop: 100,
            }}>
            {Lang.t('no_plan')}
          </Text>
        )}
      </ScrollView>
    );
  };

  const renderScene = SceneMap({
    firstTab: activeRoute,
    secondTab: upcomingRoute,
    thirdTab: pastRoute,
  });

  const renderTabBar = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    return (
      <View style={{flexDirection: 'row', height: 50}}>
        {props.navigationState.routes.map((route, i) => {
          const opacity = props.position.interpolate({
            inputRange,
            outputRange: inputRange.map(inputIndex =>
              inputIndex === i ? 1 : 0.5,
            ),
          });

          const borderColor = index === i ? COLORS.primary : COLORS.white;
          const titleColor = index === i ? COLORS.primary : COLORS.gray2;

          return (
            <TouchableOpacity
              key={i}
              style={{
                backgroundColor: COLORS.white,
                opacity: opacity,
                // width: SIZES.width / 3,
                justifyContent: 'center',
                alignItems: 'center',
                paddingTop: 8,
                paddingHorizontal: SIZES.radius,
              }}
              onPress={() => {
                setIndex(i);
              }}>
              <Animatable.Text
                animation="zoomIn"
                easing="ease-in-out-quad"
                style={{
                  textAlign: 'left',
                  color: titleColor,
                  ...FONTS.h4,
                  fontSize: 15,
                }}>
                {Lang.t(route.title)}
              </Animatable.Text>
              <Animatable.View
                transition="backgroundColor"
                // animation={tab1Animation}
                duration={600}
                easing="ease-in-out-back"
                style={{
                  justifyContent: 'flex-end',
                  width: 30,
                  height: 5,
                  borderRadius: 5,
                  backgroundColor: borderColor,
                  marginTop: 3,
                  marginBottom: 1,
                }}
              />
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  return (
    <View
      style={{
        paddingVertical: SIZES.base,
        flex: 1,
        backgroundColor: COLORS.white,
      }}>
      {/* Head */}
      <View
        style={{
          paddingHorizontal: SIZES.radius,
        }}>
        <Text
          style={{
            color: COLORS.black,
            ...FONTS.h2,
            fontSize: 19,
            letterSpacing: 0,
          }}>
          {Lang.t('trips')}
        </Text>
        <Text
          style={{
            color: COLORS.darkGray1,
            ...FONTS.body4,
            fontSize: 14,
            marginTop: 8,
            lineHeight: 18,
            letterSpacing: 0,
            // fontStyle: 'italic',
          }}>
          {Lang.t('plan_intro')}
        </Text>
        <View
          style={{
            alignItems: 'flex-end',
          }}>
          <TextButton
            onPress={() => {
              if (isAuthorized) navigation.navigate('CreatePlan');
              else setLoginModalVisible(true);
            }}
            label={Lang.t('create_trip')}
            labelStyle={{
              color: COLORS.white,
              ...FONTS.h4,
            }}
            buttonContainerStyle={{
              backgroundColor: COLORS.black,
              height: 45,
              width: 150,
              elevation: 5,
              marginVertical: 5,
            }}
          />
        </View>
      </View>
      {/* Unauthentication */}
      {!isAuthorized ? (
        <View
          style={{
            flex: 1,
            marginTop: SIZES.padding,
            // backgroundColor: COLORS.white2,
          }}>
          {constants.tripDescription.map((item, i) => {
            return (
              <View
                key={i}
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingLeft: SIZES.radius,
                  paddingRight: SIZES.padding,
                  marginVertical: SIZES.radius,
                }}>
                <View
                  style={{
                    height: 50,
                    width: 50,
                    borderRadius: 25,
                    backgroundColor: COLORS.primary,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <MaterIcon name={item.icon} size={28} color={COLORS.white} />
                </View>
                <Text
                  style={{
                    color: COLORS.darkGray,
                    ...FONTS.body4,
                    fontSize: 15,
                    paddingHorizontal: SIZES.radius,
                  }}>
                  {item.textEng}
                </Text>
              </View>
            );
          })}
          <TouchableOpacity
            onPress={() => navigation.replace('Auth', {screen: 'Signin'})}
            style={{
              paddingHorizontal: SIZES.padding,
              marginVertical: SIZES.padding * 2,
            }}
            activeOpacity={0.7}>
            <Text
              style={{
                color: COLORS.black,
                ...FONTS.h4,
                textAlign: 'center',
                textDecorationLine: 'underline',
              }}>
              Sign in now to create your trip and view your plans.
            </Text>
          </TouchableOpacity>
        </View>
      ) : (
        <View
          style={{
            flex: 1,
          }}>
          {/* Tab */}

          <View style={{flex: 1}}>
            <TabView
              navigationState={{index, routes}}
              layoutDirection="rtl"
              renderScene={renderScene}
              renderTabBar={renderTabBar}
              onIndexChange={setIndex}
              initialLayout={{width: SIZES.width}}
              overScrollMode="always"
              swipeEnabled={false}
            />
          </View>
        </View>
      )}
      {/* Plan list */}
      <InformModal
        navigation={navigation}
        isVisible={loginModalVisible}
        setIsVisible={setLoginModalVisible}
        description="Maybe you forget to sign in. Join us now to start your trip!"
        title="OPP!"
        descriptionStyle={{fontSize: 15, color: COLORS.black}}
        titleStyle={{
          fontSize: 30,
        }}
        imageStyle={{
          height: 100,
          width: 100,
        }}
        source={images.signin}
        actionComponent={
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 30,
            }}>
            <TextButton
              label="Sign in"
              onPress={() => navigation.replace('Auth', {screen: 'Signin'})}
              buttonContainerStyle={{
                height: 50,
                width: 150,
                elevation: 3,
              }}
            />
          </View>
        }
      />
    </View>
  );
};

export default PlanScreen;
