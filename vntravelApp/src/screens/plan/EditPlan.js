import React, {useState} from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  TextInput,
  ImageBackground,
} from 'react-native';
import {COLORS, SIZES, FONTS, icons, images} from '../../constants';
import {ImagesPicker, TextButton, InformModal} from '../../components';
import DatePicker from 'react-native-neat-date-picker';
import Icon from 'react-native-vector-icons/AntDesign';
import * as Animatable from 'react-native-animatable';
import moment from 'moment';
import {PlanCtrl} from '../../controller';
import {useSelector, useDispatch} from 'react-redux';
import Spinner from 'react-native-spinkit';
import {setCurrentPlan, updatePlans} from '../../stores/planSlice';
import Lang from '../../language';

const EditPlan = ({navigation, route}) => {
  const [alertMessage, setAlertMessage] = useState('');
  const [alert, setAlert] = useState(false);
  const dispatch = useDispatch();
  const [photo, setPhoto] = useState({uri: route.params?.plan.photoUrl});
  const [data, setData] = useState({
    id: route.params.plan?.id,
    name: route.params.plan?.name,
    end: new Date(route.params.plan?.end),
    start: new Date(route.params.plan?.start),
    note: route.params.plan?.note,
    photoUrl: route.params.plan?.photoUrl,
    isLoading: false,
  });
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [tripNameError, setTripNameError] = useState('');
  const [imageError, setImageError] = useState('');
  const isAuthorized = useSelector(state => state.auth.isAuthorized);
  const token = useSelector(state => state.auth.token);
  const [fail, setFail] = useState(false);
  const [planCreated, setPlanCreated] = useState(null);
  const updateData = obj => setData({...data, ...obj});

  /* --------------------------- Handle date Picker --------------------------- */
  const openDatePicker = () => {
    setShowDatePicker(true);
  };

  const onCancel = () => {
    // You should close the modal in here
    setShowDatePicker(false);
  };

  const onConfirm = (start, end) => {
    // You should close the modal in here
    setShowDatePicker(false);
    updateData({start: start, end: end});
    // The parameter 'date' is a Date object so that you can use any Date prototype method.
  };

  /* ----------------------- Validate data before saving ---------------------- */
  const validate = () => {
    let validateState = true;
    // updateData({photoUrl: photo})
    if (data.name === '') {
      setTripNameError(Lang.t('error_empty_field'));
      validateState = false;
    }
    if (!photo) {
      setImageError('You have to choose cover photo.');
      validateState = false;
    }
    return validateState;
  };

  /* ----------------------------- Update PLAN ---------------------------- */
  const updatePlan_ = () => {
    if (validate()) {
      try {
        updateData({isLoading: true});
        console.log(data);
        if (photo.data) {
          const url =
            'https://api.cloudinary.com/v1_1/vntravel285366/image/upload';
          const key = 'h9egsmwb';
          let base64Img = `data:image/jpg;base64,${photo.data}`;
          let image = {
            file: base64Img,
            upload_preset: key,
          };
          fetch(url, {
            body: JSON.stringify(image),
            headers: {
              'content-type': 'application/json',
            },
            method: 'POST',
          })
            .then(async r => {
              let upload = await r.json();
              let data_ = {...data, ...{photoUrl: upload.secure_url}};

              PlanCtrl.updatePlan(token, data_)
                .then(res => {
                  if (res) {
                    if (res.data.success) {
                      setAlert(true);
                      setAlertMessage(Lang.t('update_success'));
                      setPlanCreated(res.data.plan);
                      dispatch(updatePlans(res.data.plan));
                      dispatch(setCurrentPlan(res.data.plan));
                    } else {
                      setAlert(true);
                      setAlertMessage(res.data.message);
                    }
                  } else {
                    setAlert(true);
                    setAlertMessage(Lang.t('some_error'));
                  }
                })
                .catch(err => {
                  console.log('Error post plan: ', err);
                  setFail(true);
                });
            })
            .catch(err => {
              console.log('Error upload image: ', err);
              setFail(true);
            })
            .finally(() => updateData({isLoading: false}));
        } else {
          PlanCtrl.updatePlan(token, data)
            .then(res => {
              if (res) {
                if (res.data.success) {
                  setAlert(true);
                  setAlertMessage(Lang.t('update_success'));
                  setPlanCreated(res.data.plan);
                  dispatch(updatePlans(res.data.plan));
                  dispatch(setCurrentPlan(res.data.plan));
                } else {
                  setAlert(true);
                  setAlertMessage(res.data.message);
                }
              } else {
                setAlert(true);
                setAlertMessage(Lang.t('some_error'));
              }
            })
            .catch(err => {
              console.log('Error post plan: ', err);
              setFail(true);
            })
            .finally(() => updateData({isLoading: false}));
        }
      } catch (error) {
        setFail(true);
      }
    }
  };

  return (
    <View
      style={{
        flex: 1,
        // alignItems: 'center',
        paddingVertical: SIZES.padding,
        paddingHorizontal: SIZES.radius,
        backgroundColor: COLORS.white,
      }}>
      <TouchableOpacity
        onPress={() => setImageError('')}
        activeOpacity={0.7}
        style={{
          width: SIZES.width - 20,
          height: SIZES.height * 0.3,
          borderRadius: SIZES.radius,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ImageBackground
          style={{
            width: SIZES.width - 20,
            height: SIZES.height * 0.3,
          }}
          source={photo}
          imageStyle={{
            borderRadius: SIZES.radius,
          }}>
          <View
            style={{
              backgroundColor: COLORS.transparentBlack1,
              width: SIZES.width - 20,
              height: SIZES.height * 0.3,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: SIZES.radius,
            }}>
            <ImagesPicker
              iconSize={50}
              iconColor={COLORS.white}
              multipleImage={false}
              photos={photo}
              setPhotos={setPhoto}
              text={Lang.t('choose_cover_photo')}
              textStyle={{
                color: COLORS.white,
                ...FONTS.body3,
              }}
            />
          </View>
        </ImageBackground>
      </TouchableOpacity>
      {imageError !== '' && (
        <Text
          style={{
            ...FONTS.body4,
            color: COLORS.red,
            marginTop: 3,
            paddingHorizontal: SIZES.radius,
          }}>
          {imageError}
        </Text>
      )}

      <View
        style={{
          marginHorizontal: SIZES.radius,
          marginVertical: SIZES.radius,
        }}>
        <Text
          style={{
            color: COLORS.darkGray1,
            ...FONTS.body4,
          }}>
          {Lang.t('trip_name')}
        </Text>
        <TextInput
          onChangeText={value => {
            if (value != '') setTripNameError('');
            updateData({name: value});
          }}
          placeholder="Honey moon"
          defaultValue={data.name}
          style={{
            color: COLORS.darkGray1,
            ...FONTS.body3,
            borderBottomWidth: 1,
            borderColor: COLORS.darkGray1,
          }}
        />
        {tripNameError !== '' && (
          <Text
            style={{
              ...FONTS.body4,
              color: COLORS.red,
              marginTop: 3,
            }}>
            {tripNameError}
          </Text>
        )}
      </View>
      <View
        style={{
          marginHorizontal: SIZES.radius,
          marginVertical: SIZES.radius,
        }}>
        <Text
          style={{
            color: COLORS.darkGray1,
            ...FONTS.body4,
          }}>
          {Lang.t('description')}
        </Text>
        <TextInput
          defaultValue={data.note}
          onChangeText={value => {
            // if (value != '') setTripNameError('');
            updateData({note: value});
          }}
          placeholder={Lang.t('des_trip')}
          style={{
            color: COLORS.darkGray1,
            ...FONTS.body3,
            borderBottomWidth: 1,
            borderColor: COLORS.darkGray1,
          }}
        />
      </View>
      <View
        style={{
          marginHorizontal: SIZES.radius,
        }}>
        <Text
          style={{
            ...FONTS.body4,
            color: COLORS.darkGray1,
            marginTop: 5,
          }}>
          {Lang.t('duration')}
        </Text>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            borderWidth: 1,
            borderColor: COLORS.lightBlue1,
            paddingVertical: 8,
            paddingLeft: 5,
            paddingRight: 2,
            width: SIZES.width * 0.85,
            marginTop: SIZES.base,
            borderRadius: SIZES.base,
          }}
          onPress={() => {
            openDatePicker();
          }}>
          <Icon name={icons.calendar} size={30} color={COLORS.lightBlue1} />

          <Text
            style={{
              color: COLORS.darkGray1,
              ...FONTS.body3,
              paddingLeft: SIZES.radius,
            }}>
            {moment(data.start).utc(true).format('L')} -{' '}
            {moment(data.end).utc(true).format('L')}
          </Text>
        </TouchableOpacity>
      </View>
      <View
        style={{
          position: 'absolute',
          bottom: 20,
          left: 10,
        }}>
        <TextButton
          onPress={() => updatePlan_()}
          label={Lang.t('save')}
          buttonContainerStyle={{
            width: SIZES.width - SIZES.radius * 2,
            height: 55,
            backgroundColor: COLORS.lightBlue1,
          }}
        />
      </View>
      <DatePicker
        isVisible={showDatePicker}
        startDate={data.start}
        endDate={data.end}
        mode="range"
        minDate={new Date('2000-01-01T17:00:00.000Z')}
        maxDate={new Date('2025-01-01T17:00:00.000Z')}
        onCancel={onCancel}
        onConfirm={onConfirm}
        colorOptions={{
          headerColor: COLORS.lightBlue1,
          headerTextColor: COLORS.white,
          confirmButtonColor: COLORS.white,
          backgroundColor: COLORS.lightGray,
          dateTextColor: COLORS.darkGray,
          selectedDateBackgroundColor: COLORS.lightBlue1,
          weekDaysColor: COLORS.blue,
          changeYearModalColor: COLORS.lightBlue1,
        }}
      />
      {data.isLoading && (
        <View
          style={{
            backgroundColor: COLORS.transparentBlack1,
            position: 'absolute',
            height: SIZES.height - 50,
            width: SIZES.width,
            justifyContent: 'center',
            alignItems: 'center',
            top: 0,
            left: 0,
            zIndex: 10,
          }}>
          <Spinner type="WanderingCubes" color={COLORS.lightBlue1} size={50} />
        </View>
      )}
      <InformModal
        isVisible={fail}
        setIsVisible={setFail}
        description={Lang.t('some_error')}
        title={Lang.t('fail')}
        source={images.failGif}
        imageStyle={{
          height: 90,
          width: 90,
          marginTop: 8,
          marginBottom: 15,
        }}
        titleStyle={{
          fontSize: 30,
          color: COLORS.red,
          letterSpacing: 1,
        }}
        descriptionStyle={{
          fontSize: 15,
          paddingHorizontal: 2,
          color: COLORS.black,
        }}
      />
      {alert && (
        <Animatable.View
          onAnimationEnd={() => {
            setAlert(false);
            // setStartSave(false);
            if (planCreated)
              navigation.navigate('DetailPlan', {item: planCreated});
          }}
          delay={2000}
          animation="zoomOut"
          duration={200}
          style={{
            backgroundColor: COLORS.transparentBlack1,
            position: 'absolute',
            height: SIZES.height,
            width: SIZES.width,
            alignItems: 'center',
            top: 0,
            left: 0,
            zIndex: 10,
            justifyContent: 'center',
          }}>
          <Animatable.View
            animation="fadeIn"
            easing="ease-in-cubic"
            duration={300}
            style={{
              height: 100,
              width: 150,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: SIZES.radius,
              backgroundColor: COLORS.transparentBlack7,
            }}>
            <Text
              style={{
                color: COLORS.white,
                ...FONTS.h4,
                textAlign: 'center',
              }}>
              {alertMessage}
            </Text>
          </Animatable.View>
        </Animatable.View>
      )}
    </View>
  );
};

export default EditPlan;
