import React, {useState, useEffect} from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  TextInput,
  ImageBackground,
} from 'react-native';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
  renderers,
} from 'react-native-popup-menu';
import {COLORS, SIZES, FONTS, icons, images} from '../../constants';
import {
  ImagesPicker,
  TextButton,
  InformModal,
  InputText,
} from '../../components';
import DatePicker from 'react-native-neat-date-picker';
import Icon from 'react-native-vector-icons/AntDesign';
import IonIcon from 'react-native-vector-icons/Ionicons';
import Awes5Icon from 'react-native-vector-icons/FontAwesome5';
import MateIcon from 'react-native-vector-icons/MaterialIcons';

import * as Animatable from 'react-native-animatable';
import moment from 'moment';
import {PlanCtrl} from '../../controller';
import {useSelector, useDispatch} from 'react-redux';
import Spinner from 'react-native-spinkit';
import {addNewPlan, setCurrentPlan} from '../../stores/planSlice';
import Lang from '../../language';
import {Avatar} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';

const CreatePlan = ({navigation, route}) => {
  const [alertMessage, setAlertMessage] = useState('');
  const [alert, setAlert] = useState(false);
  const [photo, setPhoto] = useState(images.rectangle);
  const [data, setData] = useState({
    name: '',
    end: new Date(),
    start: new Date(),
    note: '',
    photoUrl: 'https://wallpaperaccess.com/full/2628032.jpg',
    isLoading: false,
  });
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [tripNameError, setTripNameError] = useState('');
  const [imageError, setImageError] = useState('');
  const isAuthorized = useSelector(state => state.auth.isAuthorized);
  const token = useSelector(state => state.auth.token);
  const [fail, setFail] = useState(false);
  const [planCreated, setPlanCreated] = useState(null);
  const updateData = obj => setData({...data, ...obj});
  const dispatch = useDispatch();

  useEffect(() => {
    if (route) {
      if (route.params) {
        let plan = route.params.plan;
        if (plan) {
          console.log('Plan ====>>>>', plan);
          updateData(plan);
        }
      }
    }
    console.log(data);
    return () => {};
  }, [route]);
  /* --------------------------- Handle date Picker --------------------------- */
  const openDatePicker = () => {
    setShowDatePicker(true);
  };

  const onCancel = () => {
    // You should close the modal in here
    setShowDatePicker(false);
  };

  const onConfirm = (start, end) => {
    // You should close the modal in here
    setShowDatePicker(false);
    updateData({start: start, end: end});
    // The parameter 'date' is a Date object so that you can use any Date prototype method.
  };

  /* ----------------------- Validate data before saving ---------------------- */
  const validate = () => {
    let validateState = true;
    // updateData({photoUrl: photo})
    if (data.name === '') {
      setTripNameError(Lang.t('error_empty_field'));
      validateState = false;
    }
    if (!photo) {
      setImageError('You have to choose cover photo.');
      validateState = false;
    }
    return validateState;
  };

  /* ----------------------------- Create NEW PLAN ---------------------------- */
  const createNewPlan = () => {
    if (validate()) {
      try {
        updateData({isLoading: true});
        if (photo !== images.rectangle && photo.data) {
          const url =
            'https://api.cloudinary.com/v1_1/vntravel285366/image/upload';
          const key = 'h9egsmwb';
          let base64Img = `data:image/jpg;base64,${photo.data}`;
          let image = {
            file: base64Img,
            upload_preset: key,
          };
          fetch(url, {
            body: JSON.stringify(image),
            headers: {
              'content-type': 'application/json',
            },
            method: 'POST',
          })
            .then(async r => {
              let upload = await r.json();
              // let temp = uploadedPhoto;
              let data_ = {...data, ...{photoUrl: upload.secure_url}};

              PlanCtrl.createPlan(token, data_)
                .then(res => {
                  if (res) {
                    if (res.data.success) {
                      setAlert(true);
                      setAlertMessage('Successfully');
                      setPlanCreated(res.data.plan);
                      dispatch(addNewPlan(res.data.plan));
                      dispatch(setCurrentPlan(res.data.plan));
                    } else {
                      setAlert(true);
                      setAlertMessage(res.data.message);
                    }
                  } else {
                    setAlert(true);
                    setAlertMessage(Lang.t('some_error'));
                  }
                })
                .catch(err => {
                  console.log('Error post plan: ', err);
                  setFail(true);
                });
            })
            .catch(err => {
              console.log('Error upload image: ', err);
              setFail(true);
            })
            .finally(() => updateData({isLoading: false}));
        } else {
          PlanCtrl.createPlan(token, data)
            .then(res => {
              if (res) {
                if (res.data.success) {
                  setAlert(true);
                  setAlertMessage(Lang.t('create_success'));
                  setPlanCreated(res.data.plan);
                  dispatch(addNewPlan(res.data.plan));
                  setCurrentPlan(res.data.plan);
                } else {
                  setAlert(true);
                  setAlertMessage(res.data.message);
                }
              } else {
                setAlert(true);
                setAlertMessage(Lang.t('some_error'));
              }
            })
            .catch(err => {
              console.log('Error post plan: ', err);
              setFail(true);
            })
            .finally(() => updateData({isLoading: false}));
        }
      } catch (error) {
        setFail(true);
      }
    }
  };

  /* ------------------------------ Upload image ------------------------------ */
  const uploadPhotos = async () => {
    await new Promise(async () => {
      return fetch(url, {
        body: JSON.stringify(data),
        headers: {
          'content-type': 'application/json',
        },
        method: 'POST',
      })
        .then(async r => {
          let data = await r.json();
          // let temp = uploadedPhoto;
          updateData({photoUrl: data.secure_url});
          // setUploadedPhoto(temp);
        })
        .catch(err => {
          console.log(err);
          // setFailModalVisible(true);
        });
    });
  };

  const SuggestedFriend = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-start',
          marginVertical: 5,
        }}>
        <Avatar source={images.background2} size={45} rounded />
        <Text
          style={{
            ...FONTS.h4,
            fontSize: 15,
            marginLeft: 8,
          }}>
          Le Thi Phuong Thao
        </Text>
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        // alignItems: 'center',
        paddingVertical: SIZES.base,
        paddingHorizontal: SIZES.radius,
        backgroundColor: COLORS.white,
      }}>
      <TouchableOpacity
        onPress={() => setImageError('')}
        activeOpacity={0.7}
        style={{
          width: SIZES.width - 20,
          height: SIZES.height * 0.25,
          borderRadius: SIZES.radius,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ImageBackground
          style={{
            width: SIZES.width - 20,
            height: SIZES.height * 0.25,
          }}
          source={photo}
          imageStyle={{
            borderRadius: SIZES.radius,
          }}>
          <View
            style={{
              backgroundColor: COLORS.transparentBlack1,
              width: SIZES.width - 20,
              height: SIZES.height * 0.25,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: SIZES.radius,
            }}>
            <ImagesPicker
              iconSize={50}
              iconColor={COLORS.white}
              multipleImage={false}
              photos={photo}
              setPhotos={setPhoto}
              text={Lang.t('choose_cover_photo')}
              textStyle={{
                color: COLORS.white,
                ...FONTS.body3,
              }}
            />
          </View>
        </ImageBackground>
      </TouchableOpacity>
      {imageError !== '' && (
        <Text
          style={{
            ...FONTS.body4,
            color: COLORS.red,
            marginTop: 3,
            paddingHorizontal: SIZES.radius,
          }}>
          {imageError}
        </Text>
      )}

      <View
        style={{
          marginHorizontal: SIZES.radius,
          marginVertical: SIZES.radius,
        }}>
        <Text
          style={{
            color: COLORS.darkGray1,
            ...FONTS.body4,
          }}>
          {Lang.t('trip_name')}
        </Text>
        <TextInput
          defaultValue={data.name}
          onChangeText={value => {
            if (value != '') setTripNameError('');
            updateData({name: value});
          }}
          placeholder="Honey moon"
          style={{
            color: COLORS.darkGray1,
            ...FONTS.body3,
            borderBottomWidth: 1,
            borderColor: COLORS.darkGray1,
          }}
        />
        {tripNameError !== '' && (
          <Text
            style={{
              ...FONTS.body4,
              color: COLORS.red,
              marginTop: 3,
            }}>
            {tripNameError}
          </Text>
        )}
      </View>
      <View
        style={{
          marginHorizontal: SIZES.radius,
          marginVertical: SIZES.radius,
        }}>
        <Text
          style={{
            color: COLORS.darkGray1,
            ...FONTS.body4,
          }}>
          {Lang.t('description')}
        </Text>
        <TextInput
          defaultValue={data.note}
          onChangeText={value => {
            // if (value != '') setTripNameError('');
            updateData({note: value});
          }}
          placeholder={Lang.t('des_trip')}
          style={{
            color: COLORS.darkGray1,
            ...FONTS.body3,
            borderBottomWidth: 1,
            borderColor: COLORS.darkGray1,
          }}
        />
      </View>
      <View
        style={{
          marginHorizontal: SIZES.radius,
        }}>
        <Text
          style={{
            ...FONTS.body4,
            color: COLORS.darkGray1,
            marginTop: 5,
          }}>
          {Lang.t('duration')}
        </Text>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            borderWidth: 1,
            borderColor: COLORS.lightBlue1,
            paddingVertical: 8,
            paddingLeft: 5,
            paddingRight: 2,
            width: SIZES.width * 0.85,
            marginTop: SIZES.base,
            borderRadius: SIZES.base,
          }}
          onPress={() => {
            openDatePicker();
          }}>
          <Icon name={icons.calendar} size={30} color={COLORS.lightBlue1} />

          <Text
            style={{
              color: COLORS.darkGray1,
              ...FONTS.body3,
              paddingLeft: SIZES.radius,
            }}>
            {moment(data.start).utc(true).format('L')} -{' '}
            {moment(data.end).utc(true).format('L')}
          </Text>
        </TouchableOpacity>
      </View>
      {/* Invite friends and status */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginVertical: SIZES.padding,
          marginHorizontal: SIZES.radius,
        }}>
        {/* Invite friends */}
        <Menu renderer={renderers.SlideInMenu}>
          <MenuTrigger>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <IonIcon name={icons.add} size={20} color={COLORS.darkGray1} />
              <Text
                style={{
                  ...FONTS.h4,
                  fontSize: 14,
                  color: COLORS.darkGray,
                }}>
                Invite a tripmate
              </Text>
            </View>
          </MenuTrigger>
          <MenuOptions
            optionsContainerStyle={{
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
            }}>
            <View
              style={{
                minHeight: SIZES.height * 0.4,
                maxHeight: SIZES.height * 0.6,
              }}>
              <View
                style={{
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: SIZES.width * 0.3,
                    height: 4,
                    backgroundColor: COLORS.lightGray,
                    borderRadius: 5,
                    marginTop: 3,
                    marginBottom: 5,
                  }}
                />
                <Text
                  style={{
                    ...FONTS.body3,
                    color: COLORS.black,
                    marginVertical: SIZES.radius,
                    textAlign: 'center',
                  }}>
                  Invite a tripmate
                </Text>
                <TextInput
                  defaultValue=""
                  style={{
                    backgroundColor: COLORS.lightGray,
                    height: 50,
                    paddingLeft: 15,
                    paddingRight: 25,
                    borderRadius: SIZES.radius,
                    ...FONTS.body4,
                    fontSize: 14,
                    color: COLORS.darkGray1,
                    width: SIZES.width * 0.95,
                  }}
                  placeholder={Lang.t('search_friend')}
                  placeholderTextColor={COLORS.gray3}
                />
              </View>

              <ScrollView
                style={{
                  paddingHorizontal: SIZES.radius,
                  paddingVertical: SIZES.padding,
                }}>
                {/* Friend suggestions */}
                <MenuOption onSelect={() => alert(`Save`)}>
                  {SuggestedFriend()}
                </MenuOption>
                <MenuOption onSelect={() => alert(`Save`)}>
                  {SuggestedFriend()}
                </MenuOption>
                <MenuOption onSelect={() => alert(`Save`)}>
                  {SuggestedFriend()}
                </MenuOption>
                <MenuOption onSelect={() => alert(`Save`)}>
                  {SuggestedFriend()}
                </MenuOption>
                <MenuOption onSelect={() => alert(`Save`)}>
                  {SuggestedFriend()}
                </MenuOption>
                <MenuOption onSelect={() => alert(`Save`)}>
                  {SuggestedFriend()}
                </MenuOption>
              </ScrollView>
            </View>
          </MenuOptions>
        </Menu>

        {/* Change status */}

        <Menu renderer={renderers.SlideInMenu}>
          <MenuTrigger>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Awes5Icon
                name={icons.friend}
                size={18}
                color={COLORS.darkGray}
              />
              <Text
                style={{
                  ...FONTS.body4,
                  fontSize: 14,
                  color: COLORS.darkGray,
                  marginLeft: 8,
                }}>
                Public
              </Text>
              <MateIcon name="arrow-drop-down" size={18} color={COLORS.gray} />
            </View>
          </MenuTrigger>
          <MenuOptions
            optionsContainerStyle={{
              backgroundColor: COLORS.white,
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
              paddingVertical: SIZES.radius,
            }}>
            <MenuOption onSelect={() => alert(`Save`)}>
              <View
                style={{
                  flexDirection: 'row',
                  paddingVertical: SIZES.base,
                  paddingHorizontal: SIZES.radius,
                  alignItems: 'center',
                }}>
                <MateIcon
                  name={icons.public_}
                  size={23}
                  color={COLORS.lightBlue1}
                />
                <View style={{marginLeft: 20}}>
                  <Text
                    style={{
                      ...FONTS.h4,
                    }}>
                    Public
                  </Text>
                  <Text
                    style={{
                      ...FONTS.body4,
                    }}>
                    Anyone can see.
                  </Text>
                </View>
              </View>
            </MenuOption>
            <MenuOption onSelect={() => alert(`Delete`)}>
              <View
                style={{
                  flexDirection: 'row',
                  paddingVertical: SIZES.base,
                  paddingHorizontal: SIZES.radius,
                  alignItems: 'center',
                }}>
                <Awes5Icon
                  name={icons.friend}
                  size={20}
                  color={COLORS.lightBlue1}
                />
                <View style={{marginLeft: 20}}>
                  <Text
                    style={{
                      ...FONTS.h4,
                    }}>
                    Friends
                  </Text>
                  <Text
                    style={{
                      ...FONTS.body4,
                    }}>
                    Users are your friend allowed to see this plan.
                  </Text>
                </View>
              </View>
            </MenuOption>
            <MenuOption onSelect={() => alert(`Not called`)}>
              <View
                style={{
                  flexDirection: 'row',
                  paddingVertical: SIZES.base,
                  paddingHorizontal: SIZES.radius,
                  alignItems: 'center',
                  marginBottom: 20,
                }}>
                <Icon name="lock" size={25} color={COLORS.lightBlue1} />
                <View style={{marginLeft: 20}}>
                  <Text
                    style={{
                      ...FONTS.h4,
                    }}>
                    Private
                  </Text>
                  <Text
                    style={{
                      ...FONTS.body4,
                    }}>
                    Only you and tripmate see this plan.
                  </Text>
                </View>
              </View>
            </MenuOption>
          </MenuOptions>
        </Menu>
      </View>

      <View
        style={{
          flexDirection: 'row',
          flexWrap: 'wrap',
          paddingHorizontal: SIZES.radius,
        }}>
        {[0, 1, 2, 3].map(item => (
          <Avatar
            size={40}
            source={images.background4}
            rounded
            containerStyle={{marginHorizontal: 1, marginBottom: 2}}
          />
        ))}
      </View>
      <View
        style={{
          position: 'absolute',
          bottom: 20,
          left: 10,
        }}>
        <TextButton
          onPress={() => createNewPlan()}
          label={Lang.t('create')}
          buttonContainerStyle={{
            width: SIZES.width - SIZES.radius * 2,
            height: 55,
            backgroundColor: COLORS.lightBlue1,
          }}
        />
      </View>
      <DatePicker
        isVisible={showDatePicker}
        startDate={data.start}
        endDate={data.end}
        mode="range"
        minDate={new Date('2000-01-01T17:00:00.000Z')}
        maxDate={new Date('2025-01-01T17:00:00.000Z')}
        onCancel={onCancel}
        onConfirm={onConfirm}
        colorOptions={{
          headerColor: COLORS.lightBlue1,
          headerTextColor: COLORS.white,
          confirmButtonColor: COLORS.primary,
          backgroundColor: COLORS.lightGray,
          dateTextColor: COLORS.darkGray,
          selectedDateBackgroundColor: COLORS.lightBlue1,
          weekDaysColor: COLORS.blue,
          changeYearModalColor: COLORS.lightBlue1,
        }}
      />
      {data.isLoading && (
        <View
          style={{
            backgroundColor: COLORS.transparentBlack1,
            position: 'absolute',
            height: SIZES.height - 50,
            width: SIZES.width,
            justifyContent: 'center',
            alignItems: 'center',
            top: 0,
            left: 0,
            zIndex: 10,
          }}>
          <Spinner type="WanderingCubes" color={COLORS.lightBlue1} size={50} />
        </View>
      )}
      <InformModal
        isVisible={fail}
        setIsVisible={setFail}
        description={Lang.t('some_error')}
        title={Lang.t('fail')}
        source={images.failGif}
        imageStyle={{
          height: 90,
          width: 90,
          marginTop: 8,
          marginBottom: 15,
        }}
        titleStyle={{
          fontSize: 30,
          color: COLORS.red,
          letterSpacing: 1,
        }}
        descriptionStyle={{
          fontSize: 15,
          paddingHorizontal: 2,
          color: COLORS.black,
        }}
      />
      {alert && (
        <Animatable.View
          onAnimationEnd={() => {
            setAlert(false);
            // setStartSave(false);
            if (planCreated)
              navigation.navigate('DetailPlan', {item: planCreated});
          }}
          delay={2000}
          animation="fadeOut"
          duration={200}
          style={{
            backgroundColor: COLORS.transparentBlack1,
            position: 'absolute',
            height: SIZES.height,
            width: SIZES.width,
            alignItems: 'center',
            top: 0,
            left: 0,
            zIndex: 10,
            justifyContent: 'center',
          }}>
          <Animatable.View
            animation="zoomIn"
            easing="ease-in-cubic"
            duration={300}
            style={{
              height: 100,
              width: 150,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: SIZES.radius,
              backgroundColor: COLORS.transparentBlack7,
            }}>
            <Text
              style={{
                color: COLORS.white,
                ...FONTS.h4,
                textAlign: 'center',
              }}>
              {alertMessage}
            </Text>
          </Animatable.View>
        </Animatable.View>
      )}
    </View>
  );
};

export default CreatePlan;
