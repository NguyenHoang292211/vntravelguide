import moment from 'moment';
import React, {useEffect, useState} from 'react';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import * as Animatable from 'react-native-animatable';
import Spinner from 'react-native-spinkit';
import AntIcon from 'react-native-vector-icons/AntDesign';
import AwesIcon from 'react-native-vector-icons/FontAwesome';
import {useDispatch, useSelector} from 'react-redux';
import {BigSection} from '../../components';
import {COLORS, FONTS, icons, SIZES} from '../../constants';
import {PlanCtrl} from '../../controller';
import {setGoBack} from '../../stores/screenSlice';
import Lang from '../../language';

const TripPlan = ({navigation, route}) => {
  const token = useSelector(state => state.auth.token);
  const dispatch = useDispatch();
  const [plan, setPlan] = useState(route.params.item);
  const [listDay, setListDay] = useState([]);
  const [selectedDayIndex, setSelectedDayIndex] = useState(0);
  const [allSection, setAllSection] = useState([]);
  const [currentSection, setCurrentSection] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (plan) {
      let duration =
        new Date(plan.end).getUTCDate() - new Date(plan.start).getUTCDate();
      let days = [];
      for (let i = 0; i < duration + 1; i++) {
        days.push(
          moment(new Date(plan.start), 'DD-MM-YYYY').utc(true).add(i, 'd'),
        );
      }
      setListDay(days);

      // Get list section
      PlanCtrl.getSectionOfPlan(token, plan.id)
        .then(res => {
          if (res.data.success) {
            setAllSection(res.data.sections);
          } else {
            alert('Sorry, some error happend.');
          }
        })
        .then(err => console.log(err))
        .finally(() => setIsLoading(false));
    }
    return () => {};
  }, []);

  useEffect(() => {
    if (allSection) handleChangeDay(selectedDayIndex);
  }, [allSection]);

  const handleChangeDay = index => {
    if (allSection) {
      for (let i = 0; i < allSection.length; i++) {
        const day = allSection[i].start;
        if (moment(day).isSame(listDay[index], 'day')) {
          setCurrentSection(allSection[i]);
          return;
        }
      }
      setCurrentSection(null);
    }
  };

  const filterSection = index => {
    for (let i = 0; i < allSection.length; i++) {
      const day = allSection[i].start;
      if (moment(day).isSame(listDay[index], 'day')) {
        return allSection[i];
      }
    }
    return null;
  };
  /* -------------------------------------------------------------------------- */
  /*                                  RENDER UI                                 */
  /* -------------------------------------------------------------------------- */

  const renderHeader = () => {
    return (
      <View
        style={{
          justifyContent: 'space-between',
          flexDirection: 'row',
          alignItems: 'center',
          paddingHorizontal: SIZES.radius,
          height: 60,
        }}>
        <TouchableOpacity
          activeOpacity={0.7}
          style={{
            height: 50,
            width: 50,
            justifyContent: 'center',
          }}
          onPress={() => {
            dispatch(setGoBack({goBack: true, fromScreen: 'TripPlan'}));
            navigation.goBack();
          }}>
          <AwesIcon name={icons.angleLeft} size={35} color={COLORS.primary} />
        </TouchableOpacity>
        <Text
          numberOfLines={1}
          style={{
            color: COLORS.primary,
            ...FONTS.h4,
            fontSize: 16,
          }}>
          {plan.name}
        </Text>
      </View>
    );
  };

  const renderDayButton = (day, index) => {
    return (
      <View
        style={{
          alignItems: 'center',
          //   height: 75,
        }}>
        <TouchableOpacity
          onPress={() => {
            setSelectedDayIndex(index);
            handleChangeDay(index);
          }}
          key={index}
          style={{
            width: SIZES.width * 0.33,
            height: 65,
            backgroundColor:
              selectedDayIndex === index
                ? COLORS.supperLightBlue
                : COLORS.lightGray,
            justifyContent: 'center',
            alignItems: 'center',
            // elevation: selectedDayIndex === index ? 5 : 0,
          }}>
          <Text
            style={{
              color: COLORS.primary,
              ...FONTS.h3,
              fontSize: 16,
            }}>
            {Lang.t('day')} {index + 1}
          </Text>
          <Text
            style={{
              color: COLORS.gray,
              ...FONTS.body4,
            }}>
            {moment(day).utc(true).format('ll')}
          </Text>
        </TouchableOpacity>
        {selectedDayIndex === index && (
          <Animatable.View animation="zoomInDown" duration={400}>
            <AntIcon
              style={{marginTop: -8}}
              name={icons.caretdown}
              color={COLORS.supperLightBlue}
              size={23}
            />
          </Animatable.View>
        )}
      </View>
    );
  };

  const renderListDay = () => {
    return (
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={listDay}
        renderItem={({item, index}) => {
          return renderDayButton(item, index);
        }}
      />
    );
  };

  return (
    <View
      style={{
        backgroundColor: COLORS.white2,
        flex: 1,
      }}>
      {renderHeader()}
      <View>{renderListDay()}</View>
      {isLoading ? (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Spinner type="9CubeGrid" color={COLORS.primary} size={30} />
        </View>
      ) : (
        <View
          style={{
            flex: 1,
          }}>
          {listDay &&
            listDay.map((day, i) => {
              let currentSection_ = filterSection(i);
              let isVisible = selectedDayIndex === i;
              return (
                <BigSection
                  planId={plan.id}
                  day={day}
                  key={i}
                  isVisible={isVisible}
                  item={currentSection_}
                  navigation={navigation}
                />
              );
            })}
        </View>
      )}
    </View>
  );
};

export default TripPlan;
