import ObjectId from 'bson-objectid';
import React, {useEffect, useState} from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {ScrollView} from 'react-native-gesture-handler';
import Spinner from 'react-native-spinkit';
import AwesIcon from 'react-native-vector-icons/FontAwesome';
import Ionicon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import {CollectionItem} from '../../components';
import {COLORS, FONTS, icons, SIZES} from '../../constants';
import {UserCtrl} from '../../controller';
import {updateUserData} from '../../stores/authSlice';
import {setGoBack} from '../../stores/screenSlice';
import Lang from '../../language';

const Collection = ({navigation, route}) => {
  const dispatch = useDispatch();

  const user = useSelector(state => state.auth.userData);
  const goBack = useSelector(state => state.screen.goBack);
  const [alert, setAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  const fromScreen = useSelector(state => state.screen.fromScreen);
  const token = useSelector(state => state.auth.token);
  const [loading, setLoading] = useState(false);
  const [places, setPlaces] = useState([]);
  const [currentSection, setCurrentSection] = useState(
    route.params?.section ? route.params.section : null,
  );
  const [startDelete, setStartDelete] = useState(false);
  const [day, setDay] = useState(route.params?.day ? route.params.day : null);

  useEffect(() => {
    if (user) {
      if (fromScreen !== 'SearchPlace') setLoading(true);
      UserCtrl.getFavoritePlaces(token)
        .then(res => {
          if (res.data.success) {
            setPlaces(res.data.favorite);
          }
        })
        .catch(err => console.log(err))
        .finally(() => setLoading(false));
    }

    // return () => {};
  }, [goBack]);

  const deleteFavoritePlace = placeId => {
    setStartDelete(true);
    UserCtrl.updateFavorite(placeId)
      .then(res => {
        if (res) {
          if (res.data.success) {
            if (res.data.add === true) {
              setAlertMessage(Lang.t('added_success'));
            } else {
              setAlertMessage(Lang.t('delete_success'));
            }
            setAlert(true);
            setPlaces(places.filter(item => item.id !== placeId));
            dispatch(updateUserData({user: res.data.user}));
            // setIsReload(true);
          } else {
            setAlertMessage(Lang.t('some_error'));
            setAlert(true);
          }
        } else {
          setAlertMessage(Lang.t('some_error'));
          setAlert(true);
        }
      })
      .catch(err => {
        console.log(err);
        setAlertMessage(Lang.t('some_error'));
        setAlert(true);
      })
      .finally(() => setStartDelete(false));
  };

  const handleAddNewSection = place => {
    //This is new section created
    dispatch(setGoBack({goBack: true, fromScreen: 'Collection'}));

    if (!currentSection) {
      route.params.setSection({
        plan: route.params.planId,
        note: '',
        start: day,
        places: [
          {
            _id: new ObjectId(new Date().getTime()),
            visitedTime: new Date(route.params.day),
            isVisited: false,
            place: place,
            expense: 0,
          },
        ],
      });
    } else {
      let places_temp = route.params.section.places
        ? route.params.section.places
        : [];
      let newPlace = {
        _id: new ObjectId(new Date().getTime()),
        visitedTime: new Date(route.params.day),
        isVisited: false,
        place: place,
        expense: 0,
      };
      places_temp.push(newPlace);
      places_temp = places_temp.sort(
        (a, b) => new Date(b.visitedTime) - new Date(a.visitedTime),
      );
      let section_ = {...route.params.section, places: places_temp};
      route.params.setSection(section_);
    }

    navigation.goBack();
  };
  /* -------------------------------------------------------------------------- */
  /*                                  RENDER UI                                 */
  /* -------------------------------------------------------------------------- */
  const renderHeader = () => {
    return (
      <View
        style={{
          // justifyContent: 'space-between',
          flexDirection: 'row',
          alignItems: 'center',
          paddingHorizontal: SIZES.radius,
          height: 60,
        }}>
        <TouchableOpacity
          activeOpacity={0.7}
          style={{
            height: 50,
            width: 50,
            justifyContent: 'center',
          }}
          onPress={() => {
            // dispatch(setGoBack({goBack: true, fromScreen: 'TripPlan'}));
            navigation.goBack();
          }}>
          <AwesIcon name={icons.angleLeft} size={35} color={COLORS.white} />
        </TouchableOpacity>
        <Text
          style={{
            color: COLORS.white,
            ...FONTS.h3,
            textAlign: 'center',
            marginLeft: 80,
          }}>
          {Lang.t('collection')}
        </Text>
      </View>
    );
  };

  const renderSearchPlace = () => {
    return (
      <View style={{paddingTop: 5, paddingBottom: 10, paddingHorizontal: 15}}>
        <TextInput
          onPressIn={() => navigation.push('SearchPlace', {from: 'Collection'})}
          style={{
            marginTop: 5,
            borderColor: COLORS.lightGray1,
            borderWidth: 1,
            height: 55,
            paddingLeft: 15,
            paddingRight: 25,
            borderRadius: 5,
            ...FONTS.body4,
            fontSize: 14,
            color: COLORS.white2,
          }}
          placeholder={Lang.t('add_to_collection')}
          placeholderTextColor={COLORS.lightGray1}></TextInput>
        <Ionicon
          name="search"
          size={25}
          color={COLORS.lightGray1}
          style={{position: 'absolute', top: 25, right: 20}}
        />
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
      }}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            backgroundColor: COLORS.lightBlue1,
            paddingBottom: 20,
            borderBottomRightRadius: 20,
            borderBottomLeftRadius: 20,
          }}>
          {renderHeader()}
          {renderSearchPlace()}
        </View>

        <View
          style={{
            paddingVertical: SIZES.radius,
            paddingHorizontal: SIZES.base,
            flex: 1,
          }}>
          {loading ? (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 150,
              }}>
              <Spinner type="9CubeGrid" color={COLORS.primary} size={40} />
            </View>
          ) : (
            <View
              style={{
                justifyContent: 'space-between',
                flexWrap: 'wrap',
                flexDirection: 'row',
              }}>
              {!places ||
                (places.length === 0 && (
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      paddingHorizontal: SIZES.padding,
                      paddingVertical: 50,
                      width: '100%',
                    }}>
                    <Text
                      style={{
                        color: COLORS.darkGray,
                        ...FONTS.body3,
                        textAlign: 'center',
                      }}>
                      No place in your collection.
                    </Text>
                  </View>
                ))}
              {places?.map((item, i) => {
                return (
                  <CollectionItem
                    navigation={navigation}
                    addButtonVisible={route.params ? true : false}
                    item={item}
                    key={i}
                    deleteFavoritePlace={deleteFavoritePlace}
                    onPress={() => handleAddNewSection(item)}
                  />
                );
              })}
            </View>
          )}
        </View>
      </ScrollView>
      {startDelete && (
        <View
          style={{
            backgroundColor: COLORS.transparentBlack1,
            position: 'absolute',
            height: SIZES.height,
            width: SIZES.width,
            alignItems: 'center',
            top: 0,
            left: 0,
            zIndex: 10,
            justifyContent: 'center',
          }}>
          <Spinner type="ArcAlt" color={COLORS.lightBlue} size={30} />
        </View>
      )}
      {/* ALERT AND INFORM */}
      {alert && (
        <Animatable.View
          onAnimationEnd={() => {
            setAlert(false);
            // setStartSave(false);
          }}
          delay={1500}
          animation="fadeOut"
          duration={400}
          style={{
            backgroundColor: COLORS.transparentBlack1,
            position: 'absolute',
            height: SIZES.height,
            width: SIZES.width,
            alignItems: 'center',
            top: 0,
            left: 0,
            zIndex: 10,
            justifyContent: 'center',
          }}>
          <Animatable.View
            animation="fadeIn"
            easing="ease-in-cubic"
            duration={300}
            style={{
              height: 100,
              width: 150,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: SIZES.radius,
              backgroundColor: COLORS.transparentBlack7,
            }}>
            <Text
              style={{
                color: COLORS.white,
                ...FONTS.h4,
                textAlign: 'center',
              }}>
              {alertMessage}
            </Text>
          </Animatable.View>
        </Animatable.View>
      )}
    </View>
  );
};

export default Collection;
