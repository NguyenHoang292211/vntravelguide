import LottieView from 'lottie-react-native';
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  Image,
} from 'react-native';
import Spinner from 'react-native-spinkit';
import {FONTS, COLORS, SIZES, icons, images} from '../../constants';
import {PlaceCtrl} from '../../controller';
import {RatingCustom} from '../../components';
import Ionicon from 'react-native-vector-icons/Ionicons';
import Anticon from 'react-native-vector-icons/AntDesign';
import Lang from '../../language';

const defaultImage =
  'https://images.unsplash.com/photo-1503614472-8c93d56e92ce?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8YmFuZmZ8ZW58MHx8MHx8&w=1000&q=80';

const ExploreView = ({navigation, route}) => {
  const [places, setPlaces] = useState([]);
  const exploreInfo = route.params.item;
  const [loading, setLoading] = useState(false);

  const getPlacesByTags = () => {
    setLoading(true);
    setPlaces([]);
    PlaceCtrl.getPlaceByTags(exploreInfo.tags)
      .then(data => {
        setPlaces(data.places);
      })
      .catch(error => {
        console.error(error);
      })
      .then(() => setLoading(false));
  };

  useEffect(() => {
    getPlacesByTags();
    return () => {};
  }, []);

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      style={{
        backgroundColor: COLORS.white2,
        paddingBottom: SIZES.padding * 2,
      }}>
      <ImageBackground
        source={{
          uri: exploreInfo?.banner ? exploreInfo.banner : defaultImage,
        }}
        resizeMode="cover"
        style={{
          height: SIZES.height * 0.35,
          width: SIZES.width,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            position: 'absolute',
            backgroundColor: COLORS.transparentBlack1,
            top: 0,
            left: 0,
            height: SIZES.height * 0.35,
            width: SIZES.width,
          }}
        />
        <Text
          style={{
            color: COLORS.white,
            ...FONTS.h2,
            marginHorizontal: SIZES.padding,
          }}>
          {exploreInfo.title}
        </Text>
        <Text
          style={{
            color: COLORS.white,
            ...FONTS.body4,
            marginHorizontal: SIZES.padding,
            marginVertical: SIZES.radius,
          }}>
          {exploreInfo.description}
        </Text>
      </ImageBackground>

      <View
        style={{
          marginBottom: SIZES.padding,
        }}>
        {/* Place List */}
        {loading && (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: SIZES.height * 0.5,
            }}>
            <Spinner size={35} type="9CubeGrid" color={COLORS.primary} />
          </View>
        )}
        {places?.map((item, index) => {
          return (
            <PlaceCard
              key={index}
              navigation={navigation}
              index={index}
              item={item}
            />
          );
        })}
        {!loading && places.length === 0 && (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: SIZES.height * 0.5,
            }}>
            <LottieView
              source={images.empty}
              autoPlay
              loop
              style={{
                height: 150,
                width: 200,
              }}
            />
            <Text
              style={{
                color: COLORS.gray3,
                ...FONTS.h4,
              }}>
              No place found in here!
            </Text>
          </View>
        )}
      </View>
    </ScrollView>
  );
};

const PlaceCard = ({item, index, navigation}) => {
  return (
    <View
      style={{
        paddingVertical: SIZES.base,
        paddingHorizontal: SIZES.radius,
      }}>
      <Text
        style={{
          color: COLORS.black,
          ...FONTS.h3,
          marginVertical: SIZES.base,
        }}>
        {index + 1}. {item.name}
      </Text>
      <View
        style={{
          paddingVertical: SIZES.base,
          flexDirection: 'row',
        }}>
        <TouchableOpacity
          activeOpacity={0.6}
          onPress={() => {
            navigation.navigate('DetailPlace', {item: item});
          }}>
          <View
            style={{
              height: 160,
              width: SIZES.width * 0.4,
            }}>
            <Image
              style={{
                height: 160,
                width: SIZES.width * 0.4,
                resizeMode: 'cover',
              }}
              source={{uri: item?.images ? item.images[0] : defaultImage}}
            />
            <TouchableOpacity
              activeOpacity={0.7}
              style={{
                position: 'absolute',
                top: 7,
                right: 7,
                backgroundColor: COLORS.transparentBlack1,
                padding: 5,
                borderRadius: 20,
              }}>
              <Anticon
                name={icons.outlineHeart}
                size={18}
                color={COLORS.white}
              />
            </TouchableOpacity>
          </View>
        </TouchableOpacity>

        <View
          style={{
            paddingHorizontal: SIZES.radius,
            width: SIZES.width * 0.6,
            alignItems: 'flex-start',
            justifyContent: 'flex-end',
          }}>
          {item.tags.includes('61bcaced425e06001651abc2', 0) && (
            <Text
              style={{
                color: COLORS.red,
                ...FONTS.body4,
              }}>
              {Lang.t('attractive')}
            </Text>
          )}
          <View
            style={{
              flexDirection: 'row',
            }}>
            <RatingCustom
              tintColor={COLORS.white2}
              fractions={3}
              value={
                item.rateVoting ? Math.round(item.rateVoting * 10) / 10 : 1
              }
              isDisabled={false}
            />
            <Text
              style={{
                color: COLORS.darkGray,
                ...FONTS.body4,
                marginLeft: SIZES.base,
              }}>
              ({item.reviewCount})
            </Text>
          </View>
          <Text
            style={{
              color: COLORS.darkGray,
              ...FONTS.body4,
              fontSize: 12,
            }}>
            {Lang.t('opening')} {item.openTime}-{item.closeTime}
          </Text>
          <View
            style={{
              padding: 5,
              borderRadius: 20,
              backgroundColor: item.category.color,
              marginVertical: SIZES.base,
            }}>
            <Text
              style={{
                color: COLORS.white,
                ...FONTS.body4,
                fontSize: 12,
              }}>
              {item.category.name}
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'center',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Ionicon
              name={icons.locationFill}
              color={COLORS.primary}
              size={23}
            />
            <Text
              numberOfLines={2}
              style={{
                color: COLORS.darkGray1,
                ...FONTS.body5,
                fontSize: 12,
                lineHeight: 16,
                marginRight: 15,
              }}>
              {item.address}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default ExploreView;
