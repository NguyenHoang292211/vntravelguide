import React, {useEffect, useState} from 'react';
import {
  View,
  TextInput,
  TouchableOpacity,
  Text,
  Image,
  ScrollView,
  FlatList,
  RefreshControl,
} from 'react-native';
import {COLORS, FONTS, SIZES, images, icons} from '../../constants';
import Icon from 'react-native-vector-icons/Feather';
import {BigPlaceCard, FilterModal} from '../../components';
import {useDispatch, useSelector} from 'react-redux';
import {setCurrentCategory} from '../../stores/placeSlice';
import Spinner from 'react-native-spinkit';
import {PlaceCtrl} from '../../controller';
import {VIEW_SCREEN_TYPE} from '../../utils/constants';
import Lang from '../../language';

const PlacesView = ({navigation, route, updateFavoritePlace}) => {
  const [categories, setCategories] = useState([]);
  const currentCategory = useSelector(state => state.place.currentCategory);
  const [placesFilter, setPlaceFilter] = useState([]);
  const [originalPlaces, setOriginalPlaces] = useState([]);
  const screenType = route.params.type;
  const loadDataFunc = route.params.loadData;
  const [showFilterModal, setShowFilterModal] = useState(false);
  const [isSpinnerVisible, setIsSpinnerVisible] = useState(true);
  const [searchInput, setSearchInput] = useState('');
  const dispatch = useDispatch();
  const user = useSelector(state => state.auth.userData);

  //*Handel search input
  const searchFilter = (keyWord, items) => {
    items.forEach(place => {
      if (user?.favorite.includes(place.id)) {
        place.isFavorite = true;
      } else {
        place.isFavorite = false;
      }
    });

    if (keyWord === '') {
      //TODO: handel search for capital word
      return items;
    } else
      return [
        ...items.filter(x =>
          x.name
            .toLowerCase()
            .normalize('NFC')
            .replace(/([\u0300-\u036f]|[^0-9a-zA-Z])/g, '')
            .includes(
              keyWord
                .toLowerCase()
                .normalize('NFC')
                .replace(/([\u0300-\u036f]|[^0-9a-zA-Z])/g, ''),
            ),
        ),
      ];
  };

  const handleSearch = value => {
    setSearchInput(value);
    // let filteredPlaces = searchFilter(value, originalPlaces);
    // setPlaceFilter(filteredPlaces);
    loadPlaceWithCategory(currentCategory);
  };

  //*Load place when select category
  const loadPlaceWithCategory = categoryId => {
    /* if (screenType === VIEW_SCREEN_TYPE.CATEGORY) {
      PlaceCtrl.getPlaceOfCategory(categoryId)
        .then(data => {
          setPlaceFilter(searchFilter(searchInput, data.places));
          setOriginalPlaces(data.places);
        })
        .catch(error => {
          console.log(error);
        })
        .finally(() => setIsSpinnerVisible(false));
    } else if (screenType === VIEW_SCREEN_TYPE.PROVINCE) {
      let places_;
      PlaceCtrl.getPlaceOfProvince(route.params.id)
        .then(data => {
          setOriginalPlaces(data.places);
          places_ = data.places?.filter(
            place => place.category._id === categoryId,
          );

          setPlaceFilter(searchFilter(searchInput, places_));
        })
        .catch(err => {
          console.log(err);
        })
        .finally(() => setIsSpinnerVisible(false));
    } else if (screenType === VIEW_SCREEN_TYPE.POPULAR) {
      let places_;
      PlaceCtrl.getPopularPlaces()

        .then(data => {
          setOriginalPlaces(data.places);
          places_ = data.places?.filter(
            place => place.category._id === categoryId,
          );
          console.log('Places_ ====>>>', places_);

          setPlaceFilter(searchFilter(searchInput, places_));
        })
        .catch(err => {
          console.log(err);
        })
        .finally(() => setIsSpinnerVisible(false));
    }*/
    let places_;
    places_ = originalPlaces?.filter(
      place => place.category._id === categoryId,
    );

    setPlaceFilter(searchFilter(searchInput, places_));
    setIsSpinnerVisible(false);
  };

  //*Initialize data
  const loadAllData = () => {
    PlaceCtrl.getAllCategories().then(data => {
      setCategories(data.categories);
    });

    if (loadDataFunc) {
      if (screenType === VIEW_SCREEN_TYPE.CATEGORY) {
        PlaceCtrl.getAllPlaces()
          .then(data => {
            setOriginalPlaces(data.places);
            console.log(currentCategory);
            let places_;
            places_ = data.places?.filter(
              place => place.category._id === currentCategory,
            );
            setPlaceFilter(places_);
          })
          .catch(err => {
            console.log(err);
          })
          .finally(() => setIsSpinnerVisible(false));
      } else {
        loadDataFunc
          .then(data => {
            setPlaceFilter(data.places);
            setOriginalPlaces(data.places);
            setIsSpinnerVisible(false);
          })
          .catch(err => {
            console.log(err);
            setIsSpinnerVisible(false);
          });
      }
    }
  };

  //*Refresh all screen
  const onRefresh = () => {
    //TODO: check filter to load
    setIsSpinnerVisible(true);
    setPlaceFilter([]);

    if (screenType === VIEW_SCREEN_TYPE.CATEGORY) {
      PlaceCtrl.getPlaceOfCategory(currentCategory)
        .then(data => {
          setPlaceFilter(searchFilter(searchInput, data.places));
          // setOriginalPlaces(data.places);
        })
        .catch(error => {
          console.log(error);
        })
        .finally(() => {
          setIsSpinnerVisible(false);
        });
    } else if (screenType === VIEW_SCREEN_TYPE.PROVINCE) {
      PlaceCtrl.getPlaceOfProvince(route.params.id)
        .then(data => {
          setOriginalPlaces(data.places);
          let places_;
          if (currentCategory !== null) {
            places_ = data.places?.filter(
              place => place.category._id === currentCategory,
            );
          } else {
            places_ = data.places;
          }

          setPlaceFilter(searchFilter(searchInput, places_));
        })
        .catch()
        .finally(() => {
          setIsSpinnerVisible(false);
        });
    } else if (screenType === VIEW_SCREEN_TYPE.POPULAR) {
      PlaceCtrl.getPopularPlaces()
        .then(data => {
          setOriginalPlaces(data.places);
          let places_;
          if (currentCategory !== null) {
            places_ = data.places?.filter(
              place => place.category._id === currentCategory,
            );
          } else {
            places_ = data.places;
          }
          setPlaceFilter(searchFilter(searchInput, places_));
        })
        .catch()
        .finally(() => {
          setIsSpinnerVisible(false);
        });
    }
  };

  useEffect(() => {
    loadAllData();
  }, []);

  /* -------------------------------------------------------------------------- */
  /*                                  Render UI                                 */
  /* -------------------------------------------------------------------------- */

  /* ------------------------------ Render Header ----------------------------- */
  const renderHeader = () => {
    return (
      <View
        style={{
          paddingTop: 10,
          paddingBottom: 7,
          paddingHorizontal: SIZES.radius,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <View
          style={{
            width: SIZES.width * 0.95,
          }}>
          <TextInput
            value={searchInput}
            onChangeText={value => handleSearch(value)}
            style={{
              backgroundColor: COLORS.lightGray,
              height: 60,
              paddingLeft: 15,
              paddingRight: 25,
              borderRadius: SIZES.radius,
              ...FONTS.body4,
              fontSize: 14,
              color: COLORS.darkGray1,
            }}
            placeholder={Lang.t('explore_by_name')}
            placeholderTextColor={COLORS.gray3}></TextInput>
          <Icon
            name="search"
            size={25}
            color={COLORS.primary}
            style={{
              position: 'absolute',
              top: 20,
              right: 10,
            }}
          />
        </View>
        {/* <TouchableOpacity
          onPress={() => setShowFilterModal(true)}
          style={{
            elevation: 5,
            marginLeft: SIZES.base,
            width: SIZES.width * 0.15,
            alignItems: 'center',
          }}>
          <Image
            source={images.filter}
            resizeMode="cover"
            style={{
              height: 50,
              width: 50,
            }}
          />
        </TouchableOpacity> */}
      </View>
    );
  };

  /* -------------------------------- CATEGORY -------------------------------- */
  const renderCategories = () => (
    <View>
      <FlatList
        contentContainerStyle={{alignItems: 'center'}}
        ItemSeparatorComponent={
          Platform.OS !== 'android' &&
          (({highlighted}) => (
            <View style={[style.separator, highlighted && {marginLeft: 0}]} />
          ))
        }
        horizontal
        listKey="category"
        data={categories}
        keyExtractor={item => `category-${item.id}`}
        showsHorizontalScrollIndicator={false}
        renderItem={({item}) => {
          return (
            <TouchableOpacity
              style={{
                paddingHorizontal: SIZES.base,
                paddingTop: SIZES.padding,
                paddingBottom: SIZES.base,
              }}
              onPress={() => {
                setPlaceFilter([]);
                setIsSpinnerVisible(true);
                dispatch(setCurrentCategory(item.id));
                loadPlaceWithCategory(item.id);
              }}>
              <Text
                style={{
                  color:
                    item.id === currentCategory ? COLORS.white : COLORS.gray2,
                  ...FONTS.body4,
                  fontSize: 14,
                  backgroundColor:
                    item.id === currentCategory ? COLORS.lightBlue1 : null,
                  // borderWidth: item.id === selectedCategory ? 1 : 0,
                  borderRadius: SIZES.radius * 2,
                  paddingVertical: 10,
                  paddingHorizontal: 12,
                  elevation: item.id === currentCategory ? 5 : 0,
                }}>
                {item.name}
              </Text>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );

  /* ------------------------------- Place list ------------------------------- */
  const renderPlaceList = () => {
    return (
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={false} onRefresh={() => onRefresh()} />
        }
        style={{
          flex: 1,
          paddingVertical: SIZES.radius,
          backgroundColor: COLORS.white3,
        }}>
        <View
          style={{
            paddingBottom: 50,
          }}>
          <Spinner
            style={{
              marginTop: SIZES.height * 0.25,
              marginLeft: SIZES.width / 2 - 20,
            }}
            isVisible={isSpinnerVisible}
            size={40}
            type="9CubeGrid"
            color={COLORS.primary}
          />
          {placesFilter?.map((place, index) => {
            return (
              <BigPlaceCard
                place={place}
                key={index}
                navigation={navigation}
                updateFavoritePlace={route.params.updateFavoritePlace}
              />
            );
          })}
        </View>
      </ScrollView>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: COLORS.white,
      }}>
      {/* Header */}
      {renderHeader()}

      {/* Categories  */}
      {renderCategories()}

      {/* Place List */}

      {renderPlaceList()}

      {/* Filter modal */}
      {showFilterModal && (
        <FilterModal
          isVisible={showFilterModal}
          onClose={() => setShowFilterModal(false)}
          children={<View></View>}
        />
      )}
    </View>
  );
};

export default PlacesView;
