import MapboxGL from '@react-native-mapbox-gl/maps';
import LottieView from 'lottie-react-native';
import React, {useEffect, useRef, useState} from 'react';
import {
  ActivityIndicator,
  Animated,
  FlatList,
  Modal,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {Divider} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';
import HeaderImageScrollView, {
  TriggeringView,
} from 'react-native-image-header-scroll-view';
import {SceneMap, TabView} from 'react-native-tab-view';
import IconAnt from 'react-native-vector-icons/AntDesign';
import IconAwes from 'react-native-vector-icons/FontAwesome';
import IconIon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import {
  InformModal,
  PlaceCard,
  RatingCustom,
  ReviewList,
  Skeleton,
  TextButton,
} from '../../components';
import BottomModal from '../../components/BottomModal';
import {COLORS, FONTS, icons, images, SIZES, constants} from '../../constants';
import {
  ContributeCtrl,
  PlaceCtrl,
  ReviewCtrl,
  UserCtrl,
  Translate,
} from '../../controller';
import toast from '../../helpers/toast';
import Lang from '../../language';
import {updateFavorite, updateUserData} from '../../stores/authSlice';
import {clearGoBack, setGoBack} from '../../stores/screenSlice';
import {setDestinationPlace} from '../../stores/searchSlide';

MapboxGL.setAccessToken(
  'pk.eyJ1IjoidGhhb2xlMzY2IiwiYSI6ImNrcnpydnJwaDAxOG8ydXFtOXdwNWR0eGsifQ.FCbrlFuxwWzIghMVU_T0iA',
);

const MIN_HEIGHT = Platform.OS === 'ios' ? 70 : 70;
const MAX_HEIGHT = SIZES.height * 0.5;

const bgAnimation = {
  0: {
    opacity: 0,
    height: MAX_HEIGHT * 1.5,
  },
  1: {
    opacity: 1,
    height: MAX_HEIGHT,
  },
};
const InfoPlace = (place, onNavigation) => {
  const goDirect = () => {
    onNavigation(place);
  };
  return (
    <View>
      <View style={{flexDirection: 'row', flex: 1}}>
        <TouchableOpacity
          style={{
            padding: SIZES.base * 2,
            borderRadius: SIZES.base,
            width: 55,
            height: 55,
            alignContent: 'center',
            justifyContent: 'center',
            backgroundColor: COLORS.transparentPrimary3,
          }}>
          <IconIon name={icons.car} color={COLORS.primary} size={26} />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            padding: SIZES.base * 2,
            borderRadius: SIZES.base,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: COLORS.primary,
            marginLeft: SIZES.base,
            flex: 1,
          }}
          onPress={goDirect}>
          <Text style={{...FONTS.body3, color: COLORS.white}}>
            {Lang.t('more_direct')}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
const PlaceDetailScreen = ({route, navigation}) => {
  const isAuthorized = useSelector(state => state.auth.isAuthorized);
  const token = useSelector(state => state.auth.token);
  const [description, setDescription] = useState('');
  const language = useSelector(state => state.auth.language);
  const navTitleView = useRef(null);
  const bgImageRef = useRef(null);
  const [place, setPlace] = useState(route.params.item);
  const bacImageFlistRef = useRef(null);
  const [nearbyPlaces, setNearbyPlaces] = useState([]);
  const scrollX = new Animated.Value(0);
  let position = Animated.divide(scrollX, SIZES.width);
  const [dataList, setDataList] = useState(place.images);
  let intervalSiler;
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'firstTab', title: Lang.t('overview')},
    {key: 'secondTab', title: Lang.t('direction')},
    {key: 'thirdTab', title: Lang.t('reviews')},
  ]);
  const contributeText = useRef(null);
  const [isReadMore, setIsReadMore] = useState(false);
  const [showFilterModal, setShowFilterModal] = useState(false);
  const [reviews, setReviews] = useState(null);
  const [isLoadingReview, setIsLoadingReview] = useState(false);
  const [loginModalVisible, setLoginModalVisible] = useState(false);
  const dispatch = useDispatch();
  const goBack = useSelector(state => state.screen.goBack);
  const [reviewValues, setreviewValues] = useState();
  const [successModalVisible, setSuccessModalVisible] = useState(false);
  const [failModalVisible, setFailModalVisible] = useState(false);
  const [contributeLoading, setContributeLoading] = useState(false);
  const [loadingNearby, setLoadingNearby] = useState(false);
  const updateContribute = obj =>
    setContributeText({...contributeText, ...obj});

  const createContribute = () => {
    if (isAuthorized) {
      if (contributeText.current.value) {
        setContributeLoading(true);
        console.log('Hi: ', contributeText.current.value);
        let data = {
          place: place.id,
          content: contributeText.current.value,
        };

        ContributeCtrl.createContribute(token, data)

          .then(res => {
            if (res.data.success) {
              setSuccessModalVisible(true);
            } else setFailModalVisible(true);
          })
          .catch(err => {
            console.log(err);
            setFailModalVisible(true);
          })
          .finally(() => setContributeLoading(false));
      }
    } else setLoginModalVisible(true);
  };

  const loadReviews = () => {
    setIsLoadingReview(true);

    ReviewCtrl.getReviewsOfPlace(place.id)
      .then(data => {
        if (data.reviews) {
          setReviews(
            data.reviews.length > 5 ? data.reviews.slice(0, 5) : data.reviews,
          );
        }
      })
      .catch(err => {
        console.log(err);
        setReviews(null);
      })
      .finally(() => setIsLoadingReview(false));
  };
  /* -------------------------------------------------------------------------- */
  /*                           * * Tab screen contents                          */
  /* -------------------------------------------------------------------------- */

  const renderReadmore = () => {
    return (
      <Modal
        style={{flex: 1}}
        visible={showFilterModal}
        animationType="slide"
        presentationStyle="fullScreen">
        <View
          style={{
            flex: 1,
            backgroundColor: COLORS.transparentBlack7,
          }}>
          <View
            style={{
              flex: 1,
              flexGrow: 1,
              marginTop: 50,
              width: '100%',
              padding: SIZES.padding,
              backgroundColor: COLORS.white,
              borderTopRightRadius: SIZES.padding,
              borderTopLeftRadius: SIZES.padding,
            }}>
            {/* Header */}
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View
                style={{
                  width: '40%',
                }}>
                <Text
                  style={{
                    flex: 1,
                    ...FONTS.h3,
                    color: COLORS.primary,
                    fontSize: 15,
                    marginBottom: 5,
                  }}>
                  {Lang.t('description')}
                </Text>
                <Divider
                  width={2}
                  color={COLORS.lightBlue}
                  // style={{marginRight: SIZES.width * 0.4}}
                />
              </View>
              <TouchableOpacity
                style={{
                  backgroundColor: COLORS.lightGray,
                  padding: 6,
                  borderRadius: 20,
                }}
                onPress={() => setShowFilterModal(false)}>
                <IconAnt name={icons.close} size={23} color={COLORS.primary} />
              </TouchableOpacity>
            </View>
            <ScrollView
              // style={{flex: 1}}
              scrollEnabled={true}
              showsVerticalScrollIndicator>
              <Text
                style={{
                  ...FONTS.body3,
                  color: COLORS.darkGray1,
                  marginVertical: SIZES.padding,
                }}>
                {place.description}
              </Text>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  };
  //* Overview Tab
  const overviewRoute = () => {
    return (
      <ScrollView
        contentContainerStyle={{
          // flex: 1,
          paddingVertical: SIZES.padding,
          paddingHorizontal: SIZES.padding,
          marginBottom: 20,
          backgroundColor: COLORS.white2,
        }}>
        {/* Introduce */}
        <View>
          <Text
            style={{
              color: COLORS.black,
              ...FONTS.h2,
              fontSize: 20,
              paddingBottom: SIZES.radius,
            }}>
            {Lang.t('description')}
          </Text>
          {description === '' ? (
            <Text
              style={{
                color: COLORS.darkGray1,
                ...FONTS.body3,
                textAlign: 'center',
              }}>
              Loading...
            </Text>
          ) : (
            <Text
              // numberOfLines={10}
              style={{
                color: COLORS.darkGray1,
                ...FONTS.body3,
                textAlign: 'justify',
              }}>
              {!isReadMore
                ? description.length > 300
                  ? description.slice(0, 300) + '. . . '
                  : description
                : description}
            </Text>
          )}
          <TouchableOpacity onPress={() => setIsReadMore(!isReadMore)}>
            {place.description.length > 300 && (
              <Text
                style={{
                  ...FONTS.h4,
                  color: COLORS.primary,
                  textAlign: 'right',
                }}>
                {isReadMore == true ? Lang.t('hide') : Lang.t('read_more')}
              </Text>
            )}
          </TouchableOpacity>
        </View>
        {/* Additional information */}
        <Divider
          width={1}
          color={COLORS.lightGray1}
          style={{
            marginTop: SIZES.padding * 1.5,
          }}
        />
        <View>
          <View
            style={{
              flexDirection: 'row',
              paddingVertical: SIZES.radius,
              justifyContent: 'space-between',
            }}>
            {/* Open-close Time */}
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <IconAnt name={icons.calendar} size={30} color={COLORS.primary} />
              <View
                style={{
                  marginLeft: 5,
                }}>
                <Text
                  style={{
                    color: COLORS.darkGray1,
                    ...FONTS.body4,
                  }}>
                  {Lang.t('opening_day')}
                </Text>
                <Text
                  style={{
                    color: COLORS.gray,
                    ...FONTS.body4,
                  }}>
                  {Lang.t('everyday')}
                </Text>
              </View>
            </View>
            <Divider
              width={1}
              orientation="vertical"
              color={COLORS.lightGray1}
              style={{}}
            />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <IconAnt name={icons.clock} size={30} color={COLORS.primary} />
              <View
                style={{
                  marginLeft: 5,
                }}>
                <Text
                  style={{
                    color: COLORS.darkGray1,
                    ...FONTS.body4,
                  }}>
                  {Lang.t('opening_time')}
                </Text>
                <Text
                  style={{
                    color: COLORS.gray,
                    ...FONTS.body4,
                  }}>
                  {place.openTime} - {place.closeTime}
                </Text>
              </View>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: SIZES.radius,
            }}>
            <IconAwes name={icons.dollar} size={30} color={COLORS.primary} />
            <View
              style={{
                marginLeft: 10,
              }}>
              <Text
                style={{
                  color: COLORS.darkGray1,
                  ...FONTS.h3,
                }}>
                {Lang.t('price')}
              </Text>
              <Text
                style={{
                  color: COLORS.gray,
                  ...FONTS.body3,
                }}>
                {place ? place.price?.start : 0} -{' '}
                {place ? place.price?.end : 0}
                <Text
                  style={{
                    color: COLORS.darkGray1,
                    ...FONTS.body4,
                  }}>
                  VND
                </Text>
              </Text>
            </View>
          </View>
          {/* Tags */}
          <View
            style={{
              marginTop: SIZES.padding,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            {/* Icon */}
            <View>
              <IconIon name={icons.tag} color={COLORS.primary} size={26} />
            </View>
            {/* Tags list */}
            <View
              style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
              }}>
              {place?.tags.map((item, index) => (
                <Text
                  key={index}
                  style={{
                    color: COLORS.darkGray,
                    padding: 5,
                    backgroundColor: COLORS.supperLightBlue,
                    borderRadius: 5,
                    marginLeft: 3,
                    marginVertical: 2,
                    ...FONTS.body4,
                  }}>
                  {item.name}
                </Text>
              ))}
            </View>
          </View>
        </View>

        {/* Nearby */}
        <View
          style={{
            marginTop: SIZES.radius,
          }}>
          <Text
            style={{
              color: COLORS.primary,
              ...FONTS.h2,
              fontSize: 18,
              paddingVertical: SIZES.base,
            }}>
            {Lang.t('nearby')}
          </Text>
          {loadingNearby && (
            <View
              style={{
                flexDirection: 'row',
                height: 300,
              }}>
              <Skeleton
                layout={[
                  {
                    key: '1',
                    width: 220,
                    height: 300,
                    marginHorizontal: SIZES.radius,
                    alignItems: 'flex-end',
                    borderRadius: 0,
                  },
                ]}>
                <View
                  key={1}
                  style={{
                    height: 300,
                    width: 220,
                    alignItems: 'flex-end',
                  }}
                />
              </Skeleton>

              <Skeleton
                containerStyle={{width: 250, marginLeft: SIZES.radius}}
                layout={[
                  {
                    key: '1',
                    width: 220,
                    height: 300,
                    marginHorizontal: SIZES.radius,
                    alignItems: 'flex-end',
                    borderRadius: 0,
                  },
                ]}>
                <View
                  key={1}
                  style={{
                    height: 300,
                    width: 220,
                    alignItems: 'flex-end',
                  }}
                />
              </Skeleton>
            </View>
          )}
          {loadingNearby === false && nearbyPlaces.length === 0 && (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                height: 150,
              }}>
              <Text
                style={{
                  color: COLORS.lightGray1,
                  ...FONTS.h4,
                }}>
                No place founded
              </Text>
            </View>
          )}
          <View style={{paddingBottom: 10}}>
            <FlatList
              horizontal
              data={nearbyPlaces}
              key="nearby"
              listKey="nearby"
              showsHorizontalScrollIndicator={false}
              keyExtractor={item => `nearby-${item.id}`}
              renderItem={({item}) => {
                return (
                  <PlaceCard
                    containerStyle={{
                      height: 300,
                      width: 220,
                    }}
                    item={item}
                    navigation={navigation}
                  />
                );
              }}
            />
          </View>
        </View>

        {/* Contribute of users */}
        <View style={{marginBottom: SIZES.base}}>
          <Text
            style={{
              color: COLORS.blue,
              ...FONTS.h3,
              fontSize: 16,
              marginTop: SIZES.radius,
            }}>
            {Lang.t('missing')}
          </Text>
          <Text
            style={{
              color: COLORS.darkGray,
              ...FONTS.body4,
            }}>
            {Lang.t('give_suggestion')}
          </Text>
          <View
            style={{
              backgroundColor: COLORS.lightGray,
              alignContent: 'flex-start',
              justifyContent: 'flex-start',
              height: 200,
            }}>
            <TextInput
              ref={contributeText}
              multiline
              onChangeText={value => {
                contributeText.current.value = value;
              }}
              keyboardType="default"
              style={{
                padding: SIZES.base,
                color: COLORS.darkGray1,
                height: 200,
                textAlignVertical: 'top',
              }}
              // underlineColorAndroid="transparent"
              placeholder={Lang.t('write_suggestion')}
              placeholderTextColor={COLORS.gray3}
            />
          </View>
          <TextButton
            label={contributeLoading ? '' : Lang.t('send')}
            backgroundColor={
              contributeText.current?.value === '' ||
              contributeLoading === true ||
              !contributeText.current?.value
                ? COLORS.gray
                : COLORS.primary
            }
            onPress={e => {
              e.preventDefault();
              createContribute();
            }}
            buttonContainerStyle={{
              borderRadius: 10,
              height: 50,
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: COLORS.primary,
              marginTop: SIZES.radius,
            }}
            startIcon={
              contributeLoading === true && (
                <ActivityIndicator
                  size="large"
                  color={COLORS.white2}
                  animating={true}
                />
              )
            }
          />
        </View>
      </ScrollView>
    );
  };

  const directionRoute = () => {
    // const mapRef = useRef();
    const [isShowBottomInfo, setIsShowBottomInfo] = useState(false);
    const onNavigation = place => {
      dispatch(setDestinationPlace(place));
      navigation.navigate('Search1', {
        screen: 'FindPlace',
        params: {
          isSearchDirect: true,
          isFromOriginStack: false,
        },
      });
    };
    return (
      <View
        style={{flex: 1, backgroundColor: COLORS.lightGray, height: '100%'}}>
        {place?.longtitude && place?.lattitude ? (
          <>
            <MapboxGL.MapView
              style={{height: '100%', width: '100%'}}
              zoomEnabled={true}
              // ref={mapRef}
            >
              {/* /* Render place point */}
              <MapboxGL.Camera
                zoomLevel={10}
                centerCoordinate={[place.longtitude, place.lattitude]}
              />
              <MapboxGL.PointAnnotation
                id={place.id}
                coordinate={[place.longtitude, place.lattitude]}
                selected={true}
                onSelected={e => setIsShowBottomInfo(true)}>
                <IconIon
                  name={icons.locationFill}
                  size={45}
                  color={'red'}
                  style={{paddingBottom: 40}}
                />
              </MapboxGL.PointAnnotation>
            </MapboxGL.MapView>
            <BottomModal
              showHeader={true}
              title={Lang.t('more_info')}
              show={isShowBottomInfo}
              onTouchOutSide={() => setIsShowBottomInfo(false)}
              close={() => setIsShowBottomInfo(false)}
              renderContent={InfoPlace(place, onNavigation)}
            />
          </>
        ) : (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: '50%',
            }}>
            <LottieView
              source={images.empty}
              autoPlay
              loop
              style={{
                height: 150,
                width: 200,
              }}
            />
          </View>
        )}
      </View>
    );
  };
  const reviewsRoute = () => {
    const reviewTypes = [
      Lang.t('excellent'),
      Lang.t('very_good'),
      Lang.t('average'),
      Lang.t('poor'),
      Lang.t('terrible'),
    ];

    return (
      <View
        style={
          {
            // flex: 1,
          }
        }>
        <ScrollView
          overScrollMode="auto"
          style={{
            paddingVertical: SIZES.padding,
            marginBottom: 20,
            backgroundColor: COLORS.white2,
          }}>
          {/* Review overview */}
          <View
            style={{
              paddingHorizontal: SIZES.radius,
              paddingBottom: SIZES.radius,
              marginTop: SIZES.radius,
            }}>
            <Text
              style={{
                color: COLORS.black,
                ...FONTS.h2,
                fontSize: 20,
                marginBottom: SIZES.radius,
              }}>
              {Lang.t('reviews')}
              <Text
                style={{
                  color: COLORS.gray,
                  fontStyle: 'italic',
                  ...FONTS.body3,
                  fontSize: 15,
                }}>
                ({place.reviewCount})
              </Text>
            </Text>
            <View
              style={{
                height: 160,
                flexDirection: 'row',
              }}>
              <View
                style={{
                  flexDirection: 'column',
                  flex: 3,
                  justifyContent: 'space-around',
                }}>
                {reviewTypes?.map((item, index) => {
                  return (
                    <Text
                      key={`tab-${index}`}
                      style={{
                        color: COLORS.darkGray1,
                        ...FONTS.h4,
                        justifyContent: 'space-around',
                      }}>
                      {item}
                    </Text>
                  );
                })}
              </View>
              <View
                style={{
                  flexDirection: 'column',
                  flex: 5,
                  width: '100%',
                  paddingRight: 20,
                  justifyContent: 'space-around',
                }}>
                {reviewValues?.map((item, index) => (
                  <View
                    key={index}
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        width:
                          place.reviewCount !== 0
                            ? (item / place.reviewCount) * 100 + '%'
                            : '5%',
                        height: 8,
                        borderRadius: 10,
                        backgroundColor: COLORS.primary,
                      }}
                    />
                    <Text
                      style={{
                        color: COLORS.gray,
                        ...FONTS.body4,
                        marginLeft: 12,
                      }}>
                      {item}
                    </Text>
                  </View>
                ))}
              </View>
            </View>
            <TextButton
              label={Lang.t('write_review')}
              labelStyle={{color: COLORS.primary, ...FONTS.body3}}
              buttonContainerStyle={{
                backgroundColor: null,
                borderWidth: 2,
                borderColor: COLORS.primary,
                width: '100%',
                marginVertical: SIZES.padding,
                height: 50,
              }}
              onPress={() => {
                if (!isAuthorized) setLoginModalVisible(true);
                else {
                  clearInterval(intervalSiler);
                  dispatch(clearGoBack());
                  dispatch(setGoBack({previousScreen: 'DetailPlace'}));
                  navigation.navigate('CreateReview', {item: place});
                }
              }}
            />
          </View>
          {/* User's review */}

          {!reviews || reviews.length === 0 ? (
            isLoadingReview === false && (
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <LottieView
                  source={images.empty}
                  autoPlay
                  loop
                  style={{
                    height: 150,
                    width: 200,
                  }}
                />
                <Text
                  style={{
                    marginHorizontal: SIZES.radius,
                    ...FONTS.h4,
                    color: COLORS.gray2,
                    marginVertical: SIZES.padding,
                    textAlign: 'center',
                  }}>
                  {Lang.t('empty_review')}
                </Text>
              </View>
            )
          ) : (
            <View
              style={{
                paddingHorizontal: SIZES.padding,
                paddingBottom: SIZES.radius,
              }}>
              <ReviewList
                navigation={navigation}
                reviews={reviews}
                overview={true}
              />
              <TextButton
                label={Lang.t('view_more')}
                labelStyle={{
                  color: COLORS.darkGray1,
                  ...FONTS.body3,
                }}
                buttonContainerStyle={{
                  height: 45,
                  width: '100%',
                  borderRadius: 0,
                  borderColor: COLORS.darkGray,
                  borderWidth: 2,
                  elevator: 5,
                  backgroundColor: COLORS.transparent,
                }}
                onPress={() => {
                  navigation.navigate('CommentsView', {item: place});
                }}
              />
            </View>
          )}
        </ScrollView>
      </View>
    );
  };

  const renderScene = SceneMap({
    firstTab: overviewRoute,
    secondTab: directionRoute,
    thirdTab: reviewsRoute,
  });

  /* --------------------------------- Tab Bar -------------------------------- */

  const renderTabBar = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    return (
      <View style={{flexDirection: 'row', height: 70, width: '100%'}}>
        {props.navigationState.routes.map((route, i) => {
          const opacity = props.position.interpolate({
            inputRange,
            outputRange: inputRange.map(inputIndex =>
              inputIndex === i ? 1 : 0.5,
            ),
          });

          const borderColor = index === i ? COLORS.primary : COLORS.white;
          const titleColor = index === i ? COLORS.darkGray : COLORS.gray2;

          return (
            <TouchableOpacity
              key={i}
              style={{
                backgroundColor: COLORS.white,
                opacity: opacity,
                width: SIZES.width / 3,
                justifyContent: 'center',
                alignItems: 'center',
                paddingTop: 8,
              }}
              onPress={() => {
                setIndex(i);
                loadReviews();
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  color: titleColor,
                  ...FONTS.h4,
                  fontSize: 16,
                }}>
                {route.title}
              </Text>
              <View
                style={{
                  justifyContent: 'flex-end',
                  width: 50,
                  height: 5,
                  borderRadius: 4,
                  backgroundColor: borderColor,
                  marginTop: 15,
                  marginBottom: 1,
                }}
              />
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  /* ------------------------Render fixed foreground ----------------------- */
  const renderFixedForeground = () => {
    return (
      <Animatable.View style={styles.navTitleView} ref={navTitleView}>
        <View
          style={{
            position: 'absolute',
            top: 12,
            left: 10,
            height: 50,
            width: 50,
            zIndex: 1000,
          }}>
          {renderBackButton()}
        </View>
        <Text
          numberOfLines={1}
          style={{
            color: COLORS.white2,
            ...FONTS.h3,
            backgroundColor: 'transparent',
            alignItems: 'center',
            paddingHorizontal: SIZES.padding + 10,
          }}>
          {place.name}
        </Text>
      </Animatable.View>
    );
  };

  /* ---------------------------- Render Foreground --------------------------- */
  const renderForeground = () => {
    return (
      <View
        style={{
          flex: 1,
          alignSelf: 'stretch',
          justifyContent: 'flex-end',
          alignItems: 'flex-start',
          paddingLeft: SIZES.radius,
          paddingBottom: SIZES.radius,
        }}>
        <View
          style={{
            position: 'absolute',
            top: 10,
            left: 5,
          }}>
          {renderBackButton()}
        </View>

        <Text
          style={{
            color: COLORS.white,
            backgroundColor: 'transparent',
            ...FONTS.h2,
            fontSize: 24,
            marginBottom: SIZES.padding,
          }}>
          {place.name}
        </Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          {/* Stars */}
          {/* {renderStars(place.rating)} */}
          <RatingCustom
            value={
              place.rateVoting ? Math.round(place.rateVoting * 10) / 10 : 1
            }
            isDisabled={false}
          />
          <Text
            style={{
              color: COLORS.white3,
              ...FONTS.h4,
            }}>
            ({place.rateVoting ? Math.round(place.rateVoting * 10) / 10 : 1})
          </Text>
          {/* Rating */}
          {/* <Text
            style={{
              color: COLORS.white,
              ...FONTS.body4,
              paddingLeft: 10,
            }}>
            {place.reviewCount} reviews
          </Text> */}
        </View>
        {/* Address */}
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginBottom: 15,
            marginTop: SIZES.radius,
          }}>
          <IconIon name={icons.location} color={COLORS.white2} size={25} />
          <Text
            style={{
              color: COLORS.white2,
              paddingLeft: 5,
              paddingRight: SIZES.padding,
              ...FONTS.body3,
              fontSize: 14,
            }}>
            {place.address}
          </Text>
        </View>
        <View
          style={{
            // position: 'absolute',
            // bottom: 5,
            // left: 15,
            marginBottom: 5,
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
          }}>
          {place.images?.map((_, i) => {
            let opacity = position.interpolate({
              inputRange: [i - 1, i, i + 1],
              outputRange: [0.3, 1, 0.3],
              extrapolate: 'clamp',
            });
            return (
              <Animated.View
                key={i}
                style={{
                  opacity,
                  height: 10,
                  width: 10,
                  backgroundColor: COLORS.primary,
                  margin: 5,
                  borderRadius: 5,
                }}
              />
            );
          })}
        </View>
        <TouchableOpacity
          onPress={() => updateFavoritePlace(place.id)}
          style={{
            position: 'absolute',
            top: 20,
            right: 20,
          }}>
          <IconAnt
            name={place.isFavorite ? icons.solidHeart : icons.outlineHeart}
            color={COLORS.white}
            size={25}
          />
        </TouchableOpacity>
      </View>
    );
  };

  /* ------------------------------- Back button ------------------------------ */

  const renderBackButton = () => {
    return (
      <TouchableOpacity
        style={{
          // backgroundColor: COLORS.transparentWhite4,
          alignItems: 'center',
          justifyContent: 'center',
          width: 40,
          height: 40,
          borderRadius: 20,
          backgroundColor: COLORS.transparentWhite4,
        }}
        onPress={() => {
          dispatch(setGoBack({goBack: true}));
          navigation.goBack();
        }}
        activeOpacity={0.6}>
        <IconAwes name={icons.angleLeft} color={COLORS.white2} size={35} />
      </TouchableOpacity>
    );
  };

  /* ----------------------- Header of Background slider ---------------------- */
  const renderHeader = () => {
    return (
      <Animatable.View
        ref={bgImageRef}
        animation={bgAnimation}
        easing="ease-in-out-sine"
        duration={600}
        // source={place.image}
        style={{
          resizeMode: 'cover',
          height: MAX_HEIGHT,
        }}>
        <FlatList
          data={place.images}
          ref={bacImageFlistRef}
          keyExtractor={(item, index) => 'key' + index}
          horizontal
          pagingEnabled
          scrollEnabled
          snapToAlignment="center"
          scrollEventThrottle={16}
          onScrollToIndexFailed={() => {
            bacImageFlistRef?.current.scrollToIndex({
              index: 0,
              animated: false,
            });
          }}
          showsHorizontalScrollIndicator={false}
          renderItem={({item}) => {
            return (
              <Animatable.Image
                source={{uri: item}}
                animation={bgAnimation}
                easing="ease-in-out-quad"
                duration={300}
                resizeMode="cover"
                style={{
                  height: MAX_HEIGHT,
                  width: SIZES.width,
                }}
              />
            );
          }}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {x: scrollX}}}],
            {useNativeDriver: false},
          )}
        />
      </Animatable.View>
    );
  };

  useEffect(() => {
    if (place.description) {
      if (language !== constants.LANGUAGE[0]) {
        Translate.translate(language.name, place.description)
          .then(res => {
            if (res.data.success) setDescription(res.data.data);
          })
          .catch(err => {
            console.log(err);
          });
      } else setDescription(place.description);
    }
  }, [language]);

  useEffect(() => {
    if (place) {
      PlaceCtrl.getPlaceById(place.id)
        .then(res => {
          if (res && res.data.success) {
            setPlace(res.data.place);
          }
        })
        .catch(err => {
          console.log(err);
        });
      setreviewValues([
        place.reviewStatus.excellent,
        place.reviewStatus.good,
        place.reviewStatus.average,
        place.reviewStatus.poor,
        place.reviewStatus.terrible,
      ]);

      setDataList(place.images);
      // Auto carousel images
      const numberOfData = place.images.length;
      if (numberOfData && numberOfData > 0) {
        let scrollValue = -1;
        // let scrolled = 0;
        intervalSiler = setInterval(function () {
          if (scrollValue + 1 < numberOfData) {
            scrollValue = scrollValue + 1;
          } else {
            scrollValue = 0;
          }
          bacImageFlistRef?.current?.scrollToIndex({
            index: scrollValue,
            animated: true,
          });
        }, 3000);
      }
    }

    return () => {
      clearInterval(intervalSiler);
    };
  }, []);

  useEffect(() => {
    //Get nearby places
    //With recent place, province is an objectId

    const provinceId = place.province._id ? place.province._id : place.province;
    setLoadingNearby(true);
    PlaceCtrl.getNearbyPlaces(place.longtitude, place.lattitude).then(data => {
      setNearbyPlaces(data.places.filter(item => item.id != place.id));
      setLoadingNearby(false);
    });
  }, []);

  useEffect(() => {
    //Reload when go back from create review
    if (goBack === true) {
      dispatch(clearGoBack());
      PlaceCtrl.getPlaceById(place.id)
        .then(res => {
          if (res && res.data.success) {
            setPlace(res.data.place);
            let data = res.data.place;
            setreviewValues([
              data.reviewStatus.excellent,
              data.reviewStatus.good,
              data.reviewStatus.average,
              data.reviewStatus.poor,
              data.reviewStatus.terrible,
            ]);
          }
        })
        .catch(err => console.log('hi:', err));

      ReviewCtrl.getReviewsOfPlace(place.id)
        .then(data => {
          setReviews(data.reviews);
        })
        .catch(err => console.log(err));
    }
  }, [goBack]);

  const updateFavoritePlace = async placeId => {
    UserCtrl.updateFavorite(placeId)
      .then(res => {
        if (res) {
          if (res.data.success) {
            if (res.data.add === true) {
              toast.normal({message: Lang.t('added_success'), duration: 2000});
              //* Update to trigger change solid heart
              let tmpPlace = place;
              tmpPlace.isFavorite = true;
              setPlace(tmpPlace);
              //Update in userData
              dispatch(updateFavorite({placeId: placeId, favoriteValue: true}));
            } else {
              toast.normal({message: Lang.t('delete_success'), duration: 2000});
              //* Update to trigger change solid heart
              let tmpPlace = place;
              tmpPlace.isFavorite = false;
              setPlace(tmpPlace);

              //Update in userData
              dispatch(
                updateFavorite({placeId: placeId, favoriteValue: false}),
              );
            }
            dispatch(updateUserData({user: res.data.user}));
            // setIsReload(true);
          } else {
            toast.danger({message: Lang.t('not_sign_warning'), duration: 2000});
          }
        } else {
          toast.danger({message: Lang.t('not_sign_warning'), duration: 2000});
        }
      })
      .catch(err => {
        console.log(err);
        toast.danger({message: Lang.t('some_error'), duration: 2000});
      });
  };
  return (
    <View style={{flex: 1}}>
      <StatusBar barStyle="light-content" backgroundColor={COLORS.primary} />
      <HeaderImageScrollView
        maxHeight={MAX_HEIGHT}
        minHeight={MIN_HEIGHT}
        maxOverlayOpacity={0.6}
        minOverlayOpacity={0.42}
        renderHeader={renderHeader}
        renderFixedForeground={renderFixedForeground}
        renderForeground={renderForeground}>
        <TriggeringView
          style={styles.section}
          onHide={() => {
            navTitleView.current.fadeInUp(500);
          }}
          onDisplay={() => {
            navTitleView.current.fadeOut(500);
            // bgImageRef.current.bounceIn(200);
          }}></TriggeringView>
        <View
          style={{
            height: SIZES.height - MIN_HEIGHT + 45,
            flex: 1,
          }}>
          <TabView
            style={{flex: 1}}
            navigationState={{index, routes}}
            renderTabBar={renderTabBar}
            layoutDirection="rtl"
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={{width: SIZES.width}}
            overScrollMode="always"
            swipeEnabled={true}
          />
        </View>
        {showFilterModal === true && renderReadmore()}
      </HeaderImageScrollView>

      <InformModal
        navigation={navigation}
        isVisible={loginModalVisible}
        setIsVisible={setLoginModalVisible}
        description="Maybe you forget to sign in. Join us now to start to review!"
        title="OPP!"
        source={images.signin}
        actionComponent={
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 30,
            }}>
            <TextButton
              label="Sign in"
              onPress={() => navigation.replace('Auth', {screen: 'Signin'})}
              buttonContainerStyle={{
                height: 50,
                width: 200,
                elevation: 3,
              }}
            />
          </View>
        }
      />
      <InformModal
        navigation={navigation}
        isVisible={successModalVisible}
        setIsVisible={setSuccessModalVisible}
        description="We're so grateful for your contributation. Wish you have more great experiences."
        title="Thanks!"
        source={images.successGif}
        titleStyle={{
          fontSize: 33,
          color: COLORS.green,
        }}
        descriptionStyle={{
          fontSize: 15,
          paddingHorizontal: 2,
        }}
      />
      <InformModal
        navigation={navigation}
        isVisible={failModalVisible}
        setIsVisible={setFailModalVisible}
        description="Sorry for this inconvenience. Please try again!"
        title="Something wrong"
        source={images.failGif}
        imageStyle={{
          height: 150,
          width: 200,
          marginTop: 8,
          marginBottom: 15,
        }}
        titleStyle={{
          fontSize: 30,
          color: COLORS.red,
        }}
        descriptionStyle={{
          fontSize: 15,
          paddingHorizontal: 2,
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    alignSelf: 'stretch',
    resizeMode: 'cover',
  },
  title: {
    fontSize: 20,
  },
  name: {
    fontWeight: 'bold',
  },
  section: {
    borderBottomColor: COLORS.blue,
    backgroundColor: null,
    height: 2,
  },
  sectionTitle: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  sectionContent: {
    fontSize: 16,
    textAlign: 'justify',
  },
  keywords: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
  },
  keywordContainer: {
    backgroundColor: '#999999',
    borderRadius: 10,
    margin: 10,
    padding: 10,
  },
  keyword: {
    fontSize: 16,
    color: 'white',
  },
  titleContainer: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageTitle: {
    color: 'white',
    backgroundColor: 'transparent',
    fontSize: 24,
  },
  navTitleView: {
    height: MIN_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
    opacity: 0,
  },
  navTitle: {
    color: 'white',
    fontSize: 18,
    backgroundColor: 'transparent',
  },
  sectionLarge: {
    height: 600,
  },
});

export default PlaceDetailScreen;
