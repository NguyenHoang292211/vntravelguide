import React, {useEffect, useState} from 'react';
import {RefreshControl, ScrollView, TextInput, View} from 'react-native';
import Spinner from 'react-native-spinkit';
import Icon from 'react-native-vector-icons/AntDesign';
import {ProvinceCard} from '../../components';
import {COLORS, FONTS, icons, SIZES} from '../../constants';
import {PlaceCtrl} from '../../controller';
import {VIEW_SCREEN_TYPE} from '../../utils/constants';
import Lang from '../../language';
import { useSelector } from 'react-redux';

const ProvincesView = ({navigation, route}) => {
  const [provinces, setProvinces] = useState([]);
  const [filteredProvinces, setfilteredProvinces] = useState([]);
  const [isSpinnerVisible, setIsSpinnerVisible] = useState(true);
  const [refreshing, setRefreshing] = useState(false);
  const [searchInput, setSearchInput] = useState('');
  const user = useSelector(state => state.auth.userData);
  //*Handel search input
  const searchFilter = (keyWord, items) => {
    if (keyWord === '') {
      //TODO: handel search for capital word
      return items;
    } else
      return [
        ...items.filter(x =>
          x.name
            .toLowerCase()
            .normalize('NFC')
            .replace(/([\u0300-\u036f]|[^0-9a-zA-Z])/g, '')
            .includes(
              keyWord
                .toLowerCase()
                .normalize('NFC')
                .replace(/([\u0300-\u036f]|[^0-9a-zA-Z])/g, ''),
            ),
        ),
      ];
  };

  const searchProvince = value => {
    setfilteredProvinces(searchFilter(value, provinces));
    setSearchInput(value);
  };

  const onRefresh = () => {
    setProvinces([]);
    setfilteredProvinces([]);
    setRefreshing(true);
    setIsSpinnerVisible(true);
    try {
      PlaceCtrl.getAllProvinces().then(data => {
       

        setProvinces(data.provinces);
        setfilteredProvinces(data.provinces);
        setIsSpinnerVisible(false);
        setRefreshing(false);
      });
    } catch (error) {
      console.log(error);
      setRefreshing(false);
    }
  };
  const updateFavoritePlace = route.params.updateFavoritePlace;
  const viewPlaceNavigator = (title, loadData = null, type, id = null) => {
    navigation.navigate('PlacesView', {
      title,
      loadData,
      type,
      id,
      updateFavoritePlace,
    });
  };

  useEffect(() => {
    onRefresh();
  }, []);
  const renderHeader = () => {
    return (
      <View
        style={{
          paddingVertical: 10,
          paddingHorizontal: SIZES.radius,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <View
          style={{
            width: '100%',
          }}>
          <TextInput
            value={searchInput}
            onChangeText={value => searchProvince(value)}
            style={{
              backgroundColor: COLORS.lightGray,
              height: 60,
              paddingLeft: 15,
              paddingRight: 25,
              borderRadius: SIZES.radius,
              ...FONTS.body4,
              fontSize: 14,
              color: COLORS.darkGray1,
            }}
            placeholder={Lang.t('search_province')}
            placeholderTextColor={COLORS.gray3}></TextInput>
          <Icon
            name={icons.search}
            size={25}
            color={COLORS.primary}
            style={{
              position: 'absolute',
              top: 20,
              right: 10,
            }}
          />
        </View>
      </View>
    );
  };

  /* ------------------------------- Place list ------------------------------- */
  const renderPlaceList = () => {
    return (
      <View
        style={{
          flex: 1,
        }}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => onRefresh()}
            />
          }
          style={{
            flex: 1,
            paddingVertical: SIZES.radius,
            backgroundColor: COLORS.white3,
          }}>
          <View
            style={{
              paddingBottom: 50,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Spinner
              style={{
                marginTop: SIZES.height * 0.25,
              }}
              isVisible={isSpinnerVisible}
              size={40}
              type="9CubeGrid"
              color={COLORS.primary}
            />
            {filteredProvinces.map((province, index) => {
              return (
                <ProvinceCard
                  onPress={() => {
                    viewPlaceNavigator(
                      province.name,
                      PlaceCtrl.getPlaceOfProvince(province.id),
                      VIEW_SCREEN_TYPE.PROVINCE,
                      province.id,
                    );
                  }}
                  item={province}
                  key={index}
                  navigation={navigation}
                  cardStyle={{
                    width: SIZES.width - 20,
                    height: SIZES.height * 0.28,
                    borderRadius: 30,
                  }}
                />
              );
            })}
          </View>
        </ScrollView>
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: COLORS.white,
      }}>
      {/* Header */}
      {renderHeader()}

      {/* Place List */}
      {renderPlaceList()}
    </View>
  );
};

export default ProvincesView;
