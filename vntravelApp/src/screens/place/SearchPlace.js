import React, {useEffect, useState} from 'react';
import {View, TouchableOpacity, TextInput, Image, Text} from 'react-native';
import {Divider} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import IconIon from 'react-native-vector-icons/Ionicons';
import Spinner from 'react-native-spinkit';
import IconAwes from 'react-native-vector-icons/FontAwesome';
import {COLORS, SIZES, FONTS, icons, images} from '../../constants';
import {PlaceCtrl, UserCtrl} from '../../controller';
import {updateUserData} from '../../stores/authSlice';
import {setGoBack} from '../../stores/screenSlice';
import {useDispatch, useSelector} from 'react-redux';
import * as Animatable from 'react-native-animatable';
import Lang from '../../language';

const SearchPlace = ({navigation, route}) => {
  const [recentPlace, setRecentPlace] = useState([]);
  const [searchInput, setSearchInput] = useState('');
  const [suggestionPlaces, setSuggestionPlaces] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [noResult, setNoResult] = useState(false);
  const dispatch = useDispatch();
  const user = useSelector(state => state.auth.userData);
  const isAuthorized = useSelector(state => state.auth.isAuthorized);
  const token = useSelector(state => state.auth.token);
  const [alert, setAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  const [startSave, setStartSave] = useState(false);
  //FOR CHOOSE COLLECTION
  const [fromCollection, setFromCollection] = useState(
    route.params ? true : false,
  );

  const updateFavoritePlace = placeId => {
    setStartSave(true);
    UserCtrl.updateFavorite(placeId)
      .then(res => {
        if (res) {
          if (res.data.success) {
            if (res.data.add === true) {
              setAlertMessage(Lang.t('added_success'));
            } else {
              setAlertMessage(Lang.t('delete_success'));
            }
            setAlert(true);
            dispatch(updateUserData({user: res.data.user}));
            // setIsReload(true);
          } else {
            setAlertMessage(Lang.t('some_error'));
            setAlert(true);
          }
        } else {
          setAlertMessage(Lang.t('some_error'));
          setAlert(true);
        }
      })
      .catch(err => {
        console.log(err);
        setAlertMessage(Lang.t('some_error'));
        setAlert(true);
      });
  };

  const saveToRecentSearch = async item => {
    if (isAuthorized === true && user != null)
      await UserCtrl.saveRecentSearch(item.id ? item.id : item._id, token)
        .then(res => {
          if (res.data.success) {
            dispatch(updateUserData(res.data));
          }
        })
        .catch(err => {
          console.log(err);
        });
  };

  useEffect(() => {
    if (user) {
      if (user.recentSearch) setRecentPlace(user.recentSearch);
      setSuggestionPlaces(suggestionPlaces);
    }
  }, [user]);

  const searchPlace = value => {
    if (value.length >= 3) {
      setIsLoading(true);
      setSuggestionPlaces([]);
      PlaceCtrl.searchPlace(value)
        .then(data => {
          if (data.places.length == 0) setNoResult(true);
          setSuggestionPlaces(data.places);
        })
        .catch(error => {
          console.log(error);
          setSuggestionPlaces([]);
          setNoResult(true);
        })
        .finally(() => setIsLoading(false));
    } else setSuggestionPlaces([]);
  };

  const suggestionPlace = (item, key) => {
    let liked = user?.favorite.includes(item.id.toString());
    return (
      <View
        key={key}
        style={{
          marginVertical: SIZES.base,
          marginHorizontal: SIZES.base,
        }}>
        <Divider
          width={1}
          color={COLORS.lightGray}
          style={{marginHorizontal: SIZES.radius, marginBottom: 10}}
        />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View
            style={{
              height: 70,
              width: 70,
              borderRadius: 5,
            }}>
            <Image
              source={item ? {uri: item.images[0]} : images.background1}
              style={{
                height: 70,
                width: 70,
                resizeMode: 'cover',
                borderRadius: 5,
              }}
            />
            {fromCollection && (
              <TouchableOpacity
                onPress={() => {
                  updateFavoritePlace(item.id);
                }}
                style={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  height: 70,
                  width: 70,
                  borderRadius: 5,
                  backgroundColor: COLORS.transparentBlack1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name={liked ? icons.solidHeart : icons.outlineHeart}
                  size={20}
                  color={liked ? COLORS.lightBlue : COLORS.white}
                />
              </TouchableOpacity>
            )}
          </View>

          <TouchableOpacity
            onPress={() => {
              saveToRecentSearch(item);
              navigation.navigate('DetailPlace', {item: item});
            }}
            style={{
              paddingLeft: 5,
              flex: 1,
            }}>
            <Text
              numberOfLines={2}
              style={{
                color: COLORS.black,
                ...FONTS.h4,
                lineHeight: 15,
                fontSize: 13,
              }}>
              {item.name}
            </Text>
            <Text
              numberOfLines={2}
              style={{
                color: COLORS.darkGray,
                ...FONTS.body4,
                lineHeight: 14,
                fontSize: 12,
                marginTop: 5,
              }}>
              {item.address}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  const suggestionProvince = item => {
    return (
      <View>
        <Divider
          width={1}
          color={COLORS.lightGray}
          style={{
            marginHorizontal: SIZES.padding,
          }}
        />
        <TouchableOpacity
          activeOpacity={0.7}
          style={{
            flexDirection: 'row',
            padding: SIZES.radius,
            alignItems: 'center',
          }}>
          <View
            style={{
              padding: 5,
              borderRadius: 20,
              backgroundColor: COLORS.white2,
              elevation: 8,
            }}>
            <IconIon name={icons.location} size={25} color={COLORS.lightBlue} />
          </View>
          <Text
            style={{
              ...FONTS.h5,
              color: COLORS.darkGray1,
              paddingHorizontal: SIZES.radius,
            }}>
            TP Hồ Chí Minh
          </Text>
        </TouchableOpacity>
      </View>
    );
  };
  const renderBackButton = () => {
    return (
      <TouchableOpacity
        style={{
          // backgroundColor: COLORS.transparentWhite4,
          alignItems: 'center',
          justifyContent: 'center',
          paddingLeft: 2,
          paddingRight: 12,
        }}
        onPress={() => {
          if (route.params && route.params.from === 'Collection') {
            dispatch(setGoBack({goBack: true, fromScreen: 'SearchPlace'}));
          }
          navigation.goBack();
        }}
        activeOpacity={0.6}>
        <IconAwes name={icons.angleLeft} color={COLORS.primary} size={35} />
      </TouchableOpacity>
    );
  };
  const renderHeader = () => {
    return (
      <View>
        <View
          style={{
            paddingTop: 10,
            paddingBottom: 10,
            paddingHorizontal: 10,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View>{renderBackButton()}</View>
          <View
            style={{
              width: '90%',
            }}>
            <TextInput
              onChangeText={value => {
                setSearchInput(value);
                setNoResult(false);
                searchPlace(value);
              }}
              value={searchInput}
              style={{
                backgroundColor: COLORS.lightGray,
                height: 60,
                paddingLeft: 15,
                paddingRight: 25,
                borderRadius: SIZES.radius,
                ...FONTS.body4,
                fontSize: 14,
                color: COLORS.darkGray1,
              }}
              placeholder={Lang.t('where_to')}
              placeholderTextColor={COLORS.gray3}></TextInput>
            <TouchableOpacity
              activeOpacity={0.6}
              style={{
                padding: 8,
                borderRadius: 25,
                backgroundColor: COLORS.transparentWhite7,
                position: 'absolute',
                top: 15,
                right: 5,
              }}>
              <Icon
                name={icons.search}
                size={22}
                color={COLORS.primary}
                // style={{position: 'absolute', top: 20, right: 10}}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  const renderRecentSearch = () => {
    return (
      <View>
        <Text
          style={{
            ...FONTS.h3,
            color: COLORS.black,
          }}>
          {Lang.t('recently_viewed')}
        </Text>
        {recentPlace?.map((item, index) => suggestionPlace(item.place, index))}
      </View>
    );
  };

  return (
    <View
      style={{
        backgroundColor: COLORS.white,
        flex: 1,
      }}>
      {/* HEADER */}
      {renderHeader()}
      <ScrollView
        style={{
          paddingHorizontal: SIZES.radius,
          marginTop: SIZES.radius,
        }}>
        {searchInput === '' && isAuthorized ? (
          renderRecentSearch()
        ) : (
          <View>
            <Spinner
              style={{
                marginTop: SIZES.height * 0.25,
                marginLeft: SIZES.width / 2 - 20,
              }}
              isVisible={isLoading}
              size={40}
              type="9CubeGrid"
              color={COLORS.lightBlue1}
            />
            {noResult ? (
              <View>
                <Text
                  style={{
                    color: COLORS.darkGray1,
                    ...FONTS.body3,
                    paddingHorizontal: SIZES.padding,
                    paddingVertical: SIZES.radius,
                  }}>
                  {Lang.t('search_no_result')} "{searchInput}"
                </Text>
              </View>
            ) : (
              suggestionPlaces?.map((item, index) =>
                suggestionPlace(item, index),
              )
            )}
          </View>
        )}
      </ScrollView>
      {startSave && (
        <Animatable.View
          animation="fadeIn"
          duration={500}
          style={{
            backgroundColor: COLORS.transparentBlack1,
            position: 'absolute',
            height: SIZES.height,
            width: SIZES.width,
            alignItems: 'center',
            top: 0,
            left: 0,
            zIndex: 10,
            justifyContent: 'center',
          }}>
          {!alert && (
            <Spinner type="9CubeGrid" color={COLORS.white3} size={40} />
          )}
          {alert && (
            <Animatable.View
              onAnimationEnd={() => {
                setAlert(false);
                setStartSave(false);
              }}
              delay={1000}
              animation="fadeOut"
              easing="ease-in-cubic"
              duration={300}
              style={{
                height: 100,
                width: 150,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: SIZES.radius,
                backgroundColor: COLORS.transparentBlack7,
              }}>
              <Text
                style={{
                  color: COLORS.white,
                  ...FONTS.h4,
                  textAlign: 'center',
                }}>
                {alertMessage}
              </Text>
            </Animatable.View>
          )}
        </Animatable.View>
      )}
    </View>
  );
};
export default SearchPlace;
