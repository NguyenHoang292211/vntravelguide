import {GoogleSignin} from '@react-native-google-signin/google-signin';
import React, {useEffect, useState} from 'react';
import {
  Alert,
  FlatList,
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Tooltip} from 'react-native-elements';
import {Divider} from 'react-native-elements/dist/divider/Divider';
import Icon from 'react-native-vector-icons/AntDesign';
import AwesIcon from 'react-native-vector-icons/FontAwesome';
import IonIcon from 'react-native-vector-icons/Ionicons';
import MateIcon from 'react-native-vector-icons/MaterialIcons';
import {useDispatch, useSelector} from 'react-redux';
import {
  InformModal,
  PlaceCard,
  Skeleton,
  TextButton,
  TripItem,
} from '../../components';
import {COLORS, constants, FONTS, icons, images, SIZES} from '../../constants';
import {PlaceCtrl, PlanCtrl, UserCtrl} from '../../controller';
import Lang from '../../language';
import {removeUserData} from '../../stores/authSlice';
import {setSelectedTab} from '../../stores/tabSlide';
import setAuthToken from '../../utils/setAuthToken';
import {clearStorage, getItem} from '../../utils/Utils';
import IconFeather from 'react-native-vector-icons/Feather';
const ProfileItem = ({label, icon, onPress}) => {
  return (
    <TouchableOpacity
      style={{
        flexDirection: 'row',
        margin: SIZES.base / 2,
        marginBottom: SIZES.base,
        alignItems: 'center',
        paddingLeft: SIZES.base * 2,
        borderRadius: SIZES.base,
        width: '100%',
      }}
      onPress={onPress}>
      <Icon
        name={icon}
        style={{
          color: COLORS.white,
          backgroundColor: COLORS.primary,
          borderRadius: 100,
          justifyContent: 'center',
          alignContent: 'center',
          padding: SIZES.base * 2,
        }}
        size={22}
      />
      <Text style={{marginLeft: 15, color: COLORS.gray, ...FONTS.body3}}>
        {label}
      </Text>
    </TouchableOpacity>
  );
};

const headerHeight = 0.17;

const ProfileScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const isAuthorized = useSelector(state => state.auth.isAuthorized);
  const userProfile = useSelector(state => state.auth.userData);
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingFriends, setIsLoadingFriends] = useState(true);

  const [myFavorite, setMyFavorite] = useState([]);
  const [myFriends, setMyFriends] = useState([]);
  const [myTrips, setMyTrips] = useState([]);

  //* User effect call api
  useEffect(async () => {
    await PlaceCtrl.getAllFavorite().then(data => {
      setMyFavorite(data.favorite);
      setIsLoading(false);
    });
    await UserCtrl.getAllFriend().then(data => {
      setMyFriends(data.friends);
      setIsLoadingFriends(false);
    });
    await UserCtrl.getAllPlanOfUser().then(data => {
      setMyTrips(data.plans);
    });
    return () => {};
  }, []);

  const signOut = async () => {
    getItem(constants.METHOD_SIGN_IN).then(value => async () => {
      if (value == true) {
        console.log('Google ', true);
        GoogleSignin.signOut();
      }
    });

    if (isAuthorized) {
      clearStorage()
        .then(() => {
          setAuthToken(null);

          dispatch(removeUserData());
          alert(Lang.t('mess_success_sign_out'));
        })
        .catch(error => {
          alert(error);
        });
    } else alert(Lang.t('mess_not_sign_in'));
  };
  const handleSignOut = () => {
    Alert.alert(
      Lang.t('inform'),
      Lang.t('confirm_login'),
      [
        {
          text: Lang.t('action_yes'),
          onPress: signOut,
        },
        {
          text: Lang.t('action_cancel'),
          onPress: () => {},
          style: 'Cancel',
        },
      ],
      {cancelable: false},
      //clicking out side of alert will not cancel
    );
  };
  const changePassword = async () => {
    setIsLoading(true);
    const canChangePassword = await UserCtrl.checkMethodLogin(
      userProfile.email,
    );

    if (canChangePassword) {
      setIsLoading(false);
      navigation.navigate('MainProfile', {screen: 'ChangePassword'});
    } else {
      setIsLoading(false);
      alert(Lang.t('mess_wrong_method_sign_in'));
    }
  };

  const changeLanguage = () => {
    navigation.navigate('MainProfile', {
      screen: 'ChangeLanguage',
    });
  };

  /* ----------------------------- RENDER TOOLTIP ----------------------------- */

  const renderTooltip = () => {
    return (
      <Tooltip
        withOverlay={false}
        backgroundColor={COLORS.white}
        overlayColor={COLORS.transparentBlack1}
        withPointer={false}
        skipAndroidStatusBar={true}
        containerStyle={{
          paddingHorizontal: SIZES.radius,
          paddingVertical: SIZES.radius,
          alignItems: 'stretch',
          left: SIZES.width - 260,
          top: 35,
          height: 240,
          width: 250,
        }}
        popover={
          <View>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('MainProfile', {
                  screen: 'EditProfile',
                  params: {
                    profile: userProfile,
                  },
                });
              }}
              style={{
                flexDirection: 'row',
                // paddingVertical: SIZES.radius,
                // paddingHorizontal: SIZES.radius,
                justifyContent: 'space-between',
              }}>
              <Text style={styles.itemMenu}>{Lang.t('edit')}</Text>
              <MateIcon name={icons.pen} color={COLORS.gray2} size={22} />
            </TouchableOpacity>
            <Divider
              width={1}
              color={COLORS.lightGray}
              style={{
                width: '100%',
                marginVertical: SIZES.base,
              }}
            />
            <TouchableOpacity
              onPress={() => changePassword()}
              style={{
                flexDirection: 'row',
                // paddingVertical: SIZES.radius,
                // paddingHorizontal: SIZES.radius,
                justifyContent: 'space-between',
              }}>
              <Text style={styles.itemMenu}>
                {Lang.t('mess_password_changed')}
              </Text>
              <Icon
                name={icons.changePassword}
                color={COLORS.gray2}
                size={22}
              />
            </TouchableOpacity>
            <Divider
              width={1}
              color={COLORS.lightGray}
              style={{
                width: '100%',
                marginVertical: SIZES.base,
              }}
            />
            <TouchableOpacity
              onPress={() => changeLanguage()}
              style={{
                flexDirection: 'row',
                // paddingVertical: SIZES.radius,
                // paddingHorizontal: SIZES.radius,
                justifyContent: 'space-between',
              }}>
              <Text style={styles.itemMenu}>{Lang.t('change_language')}</Text>
              <Icon name={icons.language} color={COLORS.gray2} size={22} />
            </TouchableOpacity>
            <Divider
              width={1}
              color={COLORS.lightGray}
              style={{
                width: '100%',
                marginVertical: SIZES.base,
              }}
            />
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('MainProfile', {
                  screen: 'MyFavorites',
                });
              }}
              style={{
                flexDirection: 'row',
                // paddingVertical: SIZES.radius,
                // paddingHorizontal: SIZES.radius,
                justifyContent: 'space-between',
              }}>
              <Text style={styles.itemMenu}>{Lang.t('screen_favorite')}</Text>
              <Icon name={icons.solidStar} color={COLORS.gray2} size={22} />
            </TouchableOpacity>
            <Divider
              width={1}
              color={COLORS.lightGray}
              style={{
                width: '100%',
                marginVertical: SIZES.base,
              }}
            />
            <TouchableOpacity
              onPress={() => handleSignOut()}
              style={{
                flexDirection: 'row',
                // paddingVertical: SIZES.radius,
                // paddingHorizontal: SIZES.radius,
                justifyContent: 'space-between',
              }}>
              <Text style={styles.itemMenu}>{Lang.t('logout')}</Text>
              <Icon name={icons.logout} color={COLORS.gray2} size={22} />
            </TouchableOpacity>
          </View>
        }>
        <Icon name={icons.more_app} size={24} color={COLORS.lightBlue1} />
      </Tooltip>
    );
  };

  /* ------------------------------ RENDER HEADER ----------------------------- */
  const renderHeader = () => {
    return (
      <View
        style={{
          height: SIZES.height * headerHeight,
          width: SIZES.width,
          padding: SIZES.padding * 0.5,
        }}>
        {/* Back */}

        <Image
          style={styles.bgImage}
          resizeMode="cover"
          source={images.background5}
        />
        <View
          style={[
            styles.bgImage,
            {opacity: 0.4, backgroundColor: COLORS.black},
          ]}
        />

        {/* More   */}

        <View style={{position: 'absolute', right: 8, top: 8}}>
          {renderTooltip()}
        </View>

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'flex-end',
            marginTop: SIZES.height * 0.1,
          }}>
          <View>
            <Image
              resizeMode="cover"
              style={styles.profile}
              source={{
                uri: `${userProfile?.image}`,
              }}
            />
            {/* Status */}
            <View
              style={{
                height: 14,
                width: 14,
                borderRadius: 7,
                backgroundColor: COLORS.orange,
                position: 'absolute',
                bottom: 5,
                right: 10,
              }}
            />
          </View>
          <View
            style={{
              justifyContent: 'center',
              marginLeft: 8,
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: COLORS.white,
                  ...FONTS.h2,
                  fontSize: 18,
                }}>
                {userProfile?.fullName}
              </Text>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('MainProfile', {
                    screen: 'EditProfile',
                    params: {
                      profile: userProfile,
                    },
                  });
                }}
                activeOpacity={0.6}
                style={{
                  height: 30,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginLeft: 5,
                }}>
                <MateIcon name={icons.pen} color={COLORS.lightBlue} size={22} />
              </TouchableOpacity>
            </View>
            <Text
              style={{
                color: COLORS.white2,
                ...FONTS.body3,
                fontSize: 16,
                marginLeft: 5,
              }}>
              {userProfile?.email}
            </Text>
            <View style={{height: 30, opacity: 0}}></View>
            {/* <TouchableOpacity
              activeOpacity={0.6}
              style={{
                backgroundColor: COLORS.lightBlue1,
                paddingHorizontal: 5,
                paddingVertical: 8,
                width: 120,
                borderRadius: 5,
                justifyContent: 'center',
                alignItems: 'center',
                marginLeft: 10,
                marginTop: 10,
                flexDirection: 'row',
              }}
              onPress={() => {
                navigation.navigate('MainProfile', {
                  screen: 'Friends',
                  params: {
                    profile: userProfile,
                  },
                });
              }}>
              <IonIcon name={icons.add} color={COLORS.white2} size={19} />
              <Text
                style={{
                  color: COLORS.white2,
                  ...FONTS.body3,
                  fontSize: 15,
                }}>
                {Lang.t('add_friend')}
              </Text>
            </TouchableOpacity> */}
          </View>
        </View>
      </View>
    );
  };

  /* ------------------------------ RENDER ABOUT ------------------------------ */
  const renderAbout = () => {
    return (
      <View
        style={{
          paddingHorizontal: SIZES.radius,
          paddingVertical: SIZES.base,
          marginTop: 60,
        }}>
        <View
          style={{
            backgroundColor: COLORS.lightBlue1,
            opacity: 0.2,
            // borderRadius: 25,
            position: 'absolute',
            justifyContent: 'center',
            alignItems: 'center',
            width: SIZES.width,
            bottom: 0,
            top: 0,
          }}>
          <Text
            style={{
              ...FONTS.h1,
              color: COLORS.gray2,
            }}>
            ABOUT ME
          </Text>
        </View>
        <AwesIcon name={icons.quote_left} size={24} color={COLORS.gray3} />
        <Text
          style={{
            ...FONTS.body3,
            color: COLORS.gray,
            zIndex: 50,
            textAlign: 'center',
            fontSize: 15,
            paddingHorizontal: SIZES.radius,
          }}>
          Today, we are going to show you our exploration of Travel App.
          Travelling is exciting but also stressful.
        </Text>
        <View
          style={{
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
          }}>
          <AwesIcon name={icons.quote_right} size={24} color={COLORS.gray3} />
        </View>
      </View>
    );
  };

  /* ----------------------------- RENDER FRIENDS ----------------------------- */

  const friendAvatar = (item, key) => {
    return (
      <TouchableOpacity
        key={key}
        style={{
          height: 60,
          width: 60,
          elevation: 5,
          backgroundColor: COLORS.white3,
          borderRadius: 30,
          marginHorizontal: 3,
        }}>
        <Image
          resizeMode="cover"
          style={[styles.profile, {height: 60, width: 60}]}
          source={{
            uri: `${item?.image}`,
          }}
        />
      </TouchableOpacity>
    );
  };
  const renderFriends = () => {
    return (
      <View
        style={{
          // padding: SIZES.radius,
          paddingVertical: SIZES.radius,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            // paddingHorizontal: SIZES.radius,
            // paddingVertical: SIZES.radius,
          }}>
          <Text
            style={{
              ...FONTS.h3,
              color: COLORS.black,
              fontSize: 20,
              marginBottom: 10,
              paddingVertical: SIZES.base,
              paddingHorizontal: SIZES.radius,
            }}>
            {Lang.t('txt_friends')}
          </Text>
          <TouchableOpacity
            activeOpacity="0.6"
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginRight: SIZES.base,
            }}
            onPress={() => {
              navigation.navigate('MainProfile', {
                screen: 'Friends',
                params: {
                  profile: userProfile,
                },
              });
            }}>
            <Text
              style={{
                color: COLORS.primary,
                fontSize: 13,
                ...FONTS.body4,
                fontWeight: '500',
              }}>
              {Lang.t('more')}
            </Text>

            <IconFeather
              style={{paddingTop: 4}}
              name="chevrons-right"
              size={18}
              color={COLORS.primary}
            />
          </TouchableOpacity>
        </View>
        <View>
          {isLoadingFriends && (
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
              }}>
              {Array.from(new Array(5)).map(item => (
                <Skeleton
                  containerStyle={{
                    width: 73,
                    backgroundColor: COLORS.white,
                    paddingHorizontal: SIZES.radius,
                    paddingBottom: 5,
                  }}
                  layout={[
                    {
                      key: '1',
                      width: 60,
                      height: 60,

                      borderRadius: 50,
                    },
                  ]}>
                  <View></View>
                </Skeleton>
              ))}
            </View>
          )}
          {!isLoadingFriends && myFriends && (
            <FlatList
              contentContainerStyle={{
                backgroundColor: COLORS.white,
                paddingHorizontal: SIZES.radius,
                paddingBottom: 5,
              }}
              data={myFriends}
              key="avatars"
              listKey="friends"
              ItemSeparatorComponent={({highlighted}) => (
                <View style={{marginLeft: 3}} />
              )}
              horizontal
              keyExtractor={(item, index) =>
                `${(Math.random() + 1).toString(36).substring(7)}_${item.id}`
              }
              showsHorizontalScrollIndicator={false}
              renderItem={({item, index}) => {
                return friendAvatar(item, index);
              }}
            />
          )}
          {!isLoadingFriends && myFriends.length <= 0 && (
            <View
              style={{
                // flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                // paddingHorizontal: SIZES.radius,
                // paddingVertical: SIZES.radius,
              }}>
              <ImageBackground
                resizeMode="contain"
                style={{height: 70, width: 280}}
                source={images.empty_friend}
              />
            </View>
          )}
        </View>
      </View>
    );
  };

  const renderTrips = () => {
    return (
      <View>
        <Text
          style={{
            ...FONTS.h3,
            color: COLORS.black,
            fontSize: 20,
            paddingHorizontal: SIZES.radius,
            paddingVertical: SIZES.base,
          }}>
          {Lang.t('trips')}
        </Text>
        <FlatList
          data={myTrips}
          contentContainerStyle={{
            padding: SIZES.radius,
          }}
          ItemSeparatorComponent={({highlighted}) => (
            <View style={{marginLeft: 2}} />
          )}
          listKey="trips"
          horizontal={true}
          pagingEnabled
          scrollEnabled={true}
          keyExtractor={(item, index) =>
            `${(Math.random() + 1).toString(36).substring(7)}_${item}`
          }
          showsHorizontalScrollIndicator={false}
          renderItem={(item, index) => (
            <TripItem item={item} navigation={navigation} key={index} />
          )}
        />
        {myTrips.length <= 0 && (
          <Text
            style={{
              ...FONTS.body3,
              color: COLORS.gray2,
              textAlign: 'center',
              marginTop: 10,
              marginBottom: 20,
            }}>
            {Lang.t('no_plan')}
          </Text>
        )}
      </View>
    );
  };

  const renderFavoritePlaces = () => {
    return (
      <View>
        <Text
          style={{
            ...FONTS.h3,
            color: COLORS.black,
            fontSize: 20,
            marginBottom: 10,
            paddingVertical: SIZES.base,
            paddingHorizontal: SIZES.radius,
          }}>
          {Lang.t('txt_favorites')}
        </Text>
        <FlatList
          contentContainerStyle={{
            paddingVertical: SIZES.base,
            paddingHorizontal: SIZES.base,
          }}
          data={myFavorite}
          ItemSeparatorComponent={({highlighted}) => (
            <View style={{marginLeft: 3}} />
          )}
          listKey="trips"
          horizontal
          scrollEnabled={true}
          keyExtractor={(item, index) =>
            `${(Math.random() + 1).toString(36).substring(7)}_${item.id}`
          }
          showsHorizontalScrollIndicator={false}
          renderItem={({item}) => {
            return (
              <PlaceCard
                keyExtractor={`${item.id}-index-extractor`}
                item={item}
                navigation={navigation}
                key={`${item.id}-index`}
                id={`${item.id}-index`}
                onPress={() => {}}
              />
            );
          }}
        />
        {myFavorite.length <= 0 && (
          <View
            style={{
              // flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              // paddingHorizontal: SIZES.radius,
              // paddingVertical: SIZES.radius,
            }}>
            <ImageBackground
              resizeMode="contain"
              style={{height: 70, width: 280}}
              source={images.empty_favorite}
            />
            <Text
              style={{
                ...FONTS.body3,
                color: COLORS.gray2,
                textAlign: 'center',
                marginTop: 10,
              }}>
              {Lang.t('no_item')}
            </Text>
          </View>
        )}
      </View>
    );
  };

  const logedProfile = () => {
    return (
      <ScrollView
        style={{
          paddingBottom: 200,
        }}>
        {/* User Info */}
        {renderHeader()}
        {renderAbout()}

        <FlatList
          ListHeaderComponent={
            <View>
              {renderFriends()}
              {renderTrips()}
              {renderFavoritePlaces()}
            </View>
          }
          data={[]}
          ListFooterComponent={
            <View
              style={{
                height: 60,
              }}
            />
          }
          renderItem={() => <View />}
          keyExtractor={() => (Math.random() + 1).toString(36).substring(7)}
        />
      </ScrollView>
    );
  };

  const ss = () => (
    <>
      <Image
        style={styles.bgImage}
        resizeMode="cover"
        source={images.background5}
      />
      <View style={styles.bottomContainer}>
        <Image
          style={styles.profile}
          source={{
            uri: `${userProfile?.image}`,
          }}
        />
        <Text style={styles.profileName}> {userProfile?.fullName} </Text>
        <Text style={styles.profileEmail}>{userProfile?.email}</Text>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('MainProfile', {
              screen: 'EditProfile',
              params: {
                profile: userProfile,
              },
            });
          }}>
          <Text style={styles.profileEdit}>{Lang.t('user_info')}</Text>
        </TouchableOpacity>
        {/* /* Setting */}
        <View
          style={{
            width: '100%',
            height: 'auto',
            flexDirection: 'column',
            justifyContent: 'center',
            // alignItems: 'flex-start',
          }}>
          <ProfileItem
            label={Lang.t('screen_favorite')}
            icon={icons.solidHeart}
            onPress={() => {
              navigation.navigate('MainProfile', {screen: 'MyFavorites'});
            }}
          />
          <ProfileItem
            label={Lang.t('mess_password_changed')}
            icon={icons.changePassword}
            onPress={changePassword}
          />
          <ProfileItem
            label={Lang.t('change_language')}
            icon={icons.language}
            onPress={() => {
              navigation.navigate('MainProfile', {
                screen: 'ChangeLanguage',
              });
            }}
          />
          <ProfileItem
            label={Lang.t('logout')}
            icon={icons.logout}
            onPress={handleSignOut}
          />
        </View>
        <InformModal
          isVisible={isLoading}
          // setIsVisible={setIsLoading}
          description={Lang.t('txt_loading')}
          source={images.loading}
          imageStyle={{
            height: 220,
            width: 220,
            marginTop: 8,
            marginBottom: 15,
          }}
        />
      </View>
    </>
  );

  return (
    <View style={styles.containerImage}>
      {isAuthorized ? (
        logedProfile()
      ) : (
        <View style={styles.profileContainer}>
          <Image
            style={styles.bgImage}
            resizeMode="cover"
            source={images.background5}
          />
          <View style={styles.bottomContainer}>
            <Image style={styles.profile} source={images.avatar} />
            <Text style={styles.profileName}>{Lang.t('guest_name')} </Text>

            <Text style={[styles.profileEdit, styles.profileGuest]}>
              {Lang.t('mess_create_profile')}
            </Text>

            {/* /* Setting */}
            <View
              style={{
                width: '100%',
                height: 'auto',
                flexDirection: 'column',
                justifyContent: 'center',
                // alignItems: 'flex-start',
              }}>
              <ProfileItem
                label={Lang.t('change_language')}
                icon={icons.language}
                onPress={() => {
                  navigation.navigate('MainProfile', {
                    screen: 'ChangeLanguage',
                  });
                }}
              />
            </View>
            <TextButton
              label={Lang.t('sign_in_now')}
              buttonContainerStyle={{
                paddingVertical: 12,

                width: '94%',
                marginBottom: SIZES.padding + 10,
                marginTop: 30,
              }}
              onPress={() => {
                dispatch(setSelectedTab(constants.screens.home));
                navigation.push('Auth', {screen: 'Signin'});
              }}
            />
          </View>
        </View>
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  profileContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: 150,
    height: 40,
    marginVertical: 10,
    backgroundColor: COLORS.mainBlue,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    elevation: 5,
  },
  containerImage: {
    alignContent: 'center',
  },
  bgImage: {
    flex: 1,
    top: 0,
    position: 'absolute',
    width: SIZES.width,
    height: SIZES.height * headerHeight,
    justifyContent: 'center',
    borderBottomLeftRadius: 40,
    borderBottomRightRadius: 40,
  },

  bottomContainer: {
    marginTop: '52%',
    height: '90%',
    width: '100%',
    backgroundColor: COLORS.white3,
    borderTopStartRadius: 50,
    borderBottomEndRadius: 50,
    alignItems: 'center',
  },
  profile: {
    height: 80,
    width: 80,
    borderRadius: 50,
    borderColor: COLORS.white3,
    borderWidth: 4,
  },
  profileName: {
    ...FONTS.h3,
    bottom: '8%',
  },
  profileEmail: {
    ...FONTS.body4,
    color: COLORS.darkGray,
    bottom: '7%',
  },
  profileEdit: {
    ...FONTS.h4,
    // fontStyle: 'italic',
    color: COLORS.primary,
  },
  profileGuest: {
    paddingLeft: SIZES.padding,
    paddingRight: SIZES.padding,
    bottom: '8%',
    textAlign: 'center',
  },
  itemMenu: {
    ...FONTS.body3,
    fontSize: 16,
    color: COLORS.darkGray,
    paddingVertical: 3,
  },
});
export default ProfileScreen;
