import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {COLORS, constants, FONTS, icons, SIZES} from '../../constants';
import {Divider} from 'react-native-elements';
import {updateLanguage} from '../../stores/authSlice';
import I18n from 'react-native-i18n';
import {removeItem, setItem} from '../../utils/Utils';
import Spinner from 'react-native-spinkit';
import Lang from '../../language';
const ChangeLanguage = () => {
  const language = useSelector(state => state.auth.language);
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);

  const changeLanguage = lang => {
    if (lang.t !== language.t) {
      setIsLoading(true);
      removeItem('language')
        .then(val => {
          setItem('language', JSON.stringify(lang)).then(val => {
            I18n.locale = lang.t;
            dispatch(updateLanguage(lang));
          });
        })
        .catch(err => {
          console.error(err);
        })
        .finally(() => setIsLoading(false));
    }
  };
  const Item = lang => {
    return (
      <View
        key={lang.t}
        style={{
          justifyContent: 'center',
          paddingHorizontal: SIZES.base,
        }}>
        <TouchableOpacity
          onPress={() => changeLanguage(lang)}
          activeOpacity={0.7}
          style={{
            paddingHorizontal: SIZES.padding,
            paddingVertical: SIZES.padding,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: language.t === lang.t ? COLORS.lightGray1 : null,
          }}>
          <Text
            style={{
              color: COLORS.black,
              ...FONTS.body3,
            }}>
            {Lang.t(lang.name)}
          </Text>
          {language.t === lang.t && (
            <Icon name={icons.done} color={COLORS.primary} size={28} />
          )}
        </TouchableOpacity>
        <Divider
          width={1}
          color={COLORS.lightGray}
          style={{
            width: SIZES.width * 0.95,
            marginLeft: SIZES.width * 0.025,
          }}
        />
      </View>
    );
  };
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: COLORS.white2,
        paddingVertical: SIZES.padding,
      }}>
      {constants.LANGUAGE.map(item => {
        return Item(item);
      })}
      {isLoading && (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Spinner type="9CubeGrid" color={COLORS.primary} size={30} />
        </View>
      )}
    </View>
  );
};

export default ChangeLanguage;
