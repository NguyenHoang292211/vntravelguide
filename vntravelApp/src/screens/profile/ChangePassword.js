import React, {useRef, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import Spacer from '../../components/Spacer';
import {Button, Input} from 'react-native-elements';
import {useSelector} from 'react-redux';
import {UserCtrl} from '../../controller';
import {COLORS, FONTS, icons, images, SIZES} from '../../constants';
import {FormInput, TextButton} from '../../components';
import Icon1 from 'react-native-vector-icons/Ionicons';
import Lang from '../../language';
/**
 * Include 1 uppercase, 1 lowercase, and 1 letter, least 6 character
 * @param {*} password
 */
export const checkStrongPassword = password => {
  const rex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})', 'g');
  return rex.test(password);
};

const ChangePassword = () => {
  //get email
  const userProfile = useSelector(state => state.auth.userData);
  const [password, setPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmNewPassword, setConfirmNewPassword] = useState('');

  const [errPassword, setErrPassword] = useState('');
  const [errNewPassword, setErrNewPassword] = useState('');
  const [errConfirmNewPassword, setErrConfirmNewPassword] = useState('');

  const [securePassword, setSecurePassword] = useState(false);
  const [secureNewPassword, setSecureNewPassword] = useState(false);
  const [secureConfirmPassword, setSecureConfirmPassword] = useState(false);

  const [isLoading, setIsLoading] = useState(false);

  const changePassword = () => {
    //Reset error message
    setErrPassword('');
    setErrNewPassword('');
    setErrConfirmNewPassword('');

    if (newPassword !== confirmNewPassword) {
      setErrConfirmNewPassword(Lang.t('mess_password_not_match'));
      return;
    } else {
      if (!checkStrongPassword(newPassword)) {
        setErrNewPassword(Lang.t('password_rule'));
        return;
      } else {
        UserCtrl.changePassword({
          email: userProfile.email,
          password: password,
          newPassword: newPassword,
        }).then(
          value => {
            //Successfully change password
            if (value.data.success) {
              alert(Lang.t('change_password'));
            } else {
              //Password was incorrect
              setErrPassword(Lang.t('mess_password_incorrect'));
            }
          },
          error => {
            console.log('Error change password ');
            alert(Lang.t('mess_error_happen'));
          },
        );
      }
    }
  };

  return (
    <View style={styles.mainContainer}>
      <View style={{alignItems: 'flex-start', marginTop: SIZES.base * 2}}>
        <Text style={{...FONTS.h2, color: COLORS.white}}>
          {Lang.t('create_new_password')}
        </Text>
        <Text style={{...FONTS.body4, color: COLORS.gray}}>
          {Lang.t('password_rule')}{' '}
        </Text>
      </View>
      <View style={styles.profileContainer}>
        <View
          style={{
            width: '100%',
            flexDirection: 'column',
            marginTop: 10,
          }}>
          <ScrollView>
            {/* /* Old password */}
            <FormInput
              labelStyle={{
                color: COLORS.gray,
                ...FONTS.body4,
              }}
              label={Lang.t('old_password')}
              inputStyle={{
                color: COLORS.darkGray,
                ...FONTS.body3,
              }}
              containerStyle={{
                paddingHorizontal: 2,
                marginTop: 5,
              }}
              placeHolder="123455@avc"
              autoCompleteType="password"
              secureTextEntry={securePassword}
              onChange={value => setPassword(value)}
              errorMsg={errPassword}
              appendCommponent={
                <TouchableOpacity
                  activeOpacity={0.6}
                  onPress={() => setSecurePassword(!securePassword)}
                  style={{
                    justifyContent: 'center',
                  }}>
                  <Icon1
                    name={securePassword ? icons.eyeOff : icons.eye}
                    color={COLORS.gray}
                    size={22}
                  />
                </TouchableOpacity>
              }
            />

            {/* /* New password */}
            <FormInput
              labelStyle={{
                color: COLORS.gray,
                ...FONTS.body4,
              }}
              label={Lang.t('new_password')}
              inputStyle={{
                color: COLORS.darkGray,
                ...FONTS.body3,
              }}
              containerStyle={{
                paddingHorizontal: 2,
                marginTop: 5,
              }}
              placeHolder="123455@avc"
              autoCompleteType="password"
              secureTextEntry={secureNewPassword}
              onChange={value => setNewPassword(value)}
              errorMsg={errNewPassword}
              appendCommponent={
                <TouchableOpacity
                  activeOpacity={0.6}
                  onPress={() => setSecureNewPassword(!secureNewPassword)}
                  style={{
                    justifyContent: 'center',
                  }}>
                  <Icon1
                    name={secureNewPassword ? icons.eyeOff : icons.eye}
                    color={COLORS.gray}
                    size={22}
                  />
                </TouchableOpacity>
              }
            />

            {/* /* Confirm password */}
            <FormInput
              labelStyle={{
                color: COLORS.gray,
                ...FONTS.body4,
              }}
              label={Lang.t('confirm_password')}
              inputStyle={{
                color: COLORS.darkGray,
                ...FONTS.body3,
              }}
              containerStyle={{
                paddingHorizontal: 2,
                marginTop: 5,
              }}
              placeHolder="123455@avc"
              autoCompleteType="password"
              secureTextEntry={secureConfirmPassword}
              onChange={value => setConfirmNewPassword(value)}
              errorMsg={errConfirmNewPassword}
              appendCommponent={
                <TouchableOpacity
                  activeOpacity={0.6}
                  onPress={() =>
                    setSecureConfirmPassword(!secureConfirmPassword)
                  }
                  style={{
                    justifyContent: 'center',
                  }}>
                  <Icon1
                    name={secureConfirmPassword ? icons.eyeOff : icons.eye}
                    color={COLORS.gray}
                    size={22}
                  />
                </TouchableOpacity>
              }
            />

            <TextButton
              label={Lang.t('change_password')}
              labelStyle={{
                ...FONTS.h3,
              }}
              buttonContainerStyle={{
                paddingVertical: 12,

                width: '100%',
                backgroundColor: COLORS.primary,
                marginTop: 50,
                marginBottom: 50,
              }}
              onPress={changePassword}
            />
          </ScrollView>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: COLORS.primary,
  },
  profileContainer: {
    marginTop: '10%',
    height: '90%',
    width: '100%',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    borderTopLeftRadius: SIZES.base * 4,
    borderTopRightRadius: SIZES.base * 4,
    marginRight: SIZES.base,
    marginLeft: SIZES.base,
    padding: SIZES.base,
  },
});
export default ChangePassword;
