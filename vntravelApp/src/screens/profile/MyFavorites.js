import LottieView from 'lottie-react-native';
import React, {useEffect, useState} from 'react';
import {FlatList, View} from 'react-native';
import {BigPlaceCard} from '../../components';
import {COLORS, images} from '../../constants';
import {PlaceCtrl} from '../../controller';
import {BigPlaceCardSkeleton} from '../../screens/home/HomeScreen';
const MyFavorites = ({navigation}) => {
  const [isLoading, setIsLoading] = useState(true);
  const [myFavorite, setMyFavorite] = useState([]);

  //* User effect call api
  useEffect(async () => {
    await PlaceCtrl.getAllFavorite().then(data => {
      setMyFavorite(data.favorite);
      setIsLoading(false);
    });
    return () => {};
  }, []);
  return (
    <View style={{backgroundColor: COLORS.white}}>
      {isLoading ? (
        Array.from(new Array(6)).map(item => <BigPlaceCardSkeleton />)
      ) : myFavorite?.length <= 0 ? (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            height: '100%',
          }}>
          <LottieView
            source={images.empty}
            autoPlay
            loop
            style={{
              height: 150,
              width: 200,
            }}
          />
        </View>
      ) : (
        <FlatList
          listKey="mFavorite"
          data={myFavorite}
          style={{backgroundColor: COLORS.white2}}
          showsHorizontalScrollIndicator={false}
          keyExtractor={item => `bestRating-${item.id}`}
          renderItem={({item}) => {
            return (
              <BigPlaceCard
                place={item}
                navigation={navigation}
                key={`${item.id}-index`}
                id={`${item.id}-index`}
              />
            );
          }}
        />
      )}
    </View>
  );
};

export default MyFavorites;
