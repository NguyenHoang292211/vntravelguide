import React, {useState} from 'react';
import {Image, ImageBackground, StyleSheet, View} from 'react-native';
import {Button} from 'react-native-elements';
import {useDispatch} from 'react-redux';
import {FormInput, ImagesPicker} from '../../components';
import Spacer from '../../components/Spacer';
import {COLORS, FONTS, images, SIZES} from '../../constants';
import {UserCtrl} from '../../controller';
import Lang from '../../language';
import {updateUserData} from '../../stores/authSlice';
import {setItems} from '../../utils/Utils';

const uploadPhotos = async avatar => {
  const url = 'https://api.cloudinary.com/v1_1/vntravel285366/image/upload';
  let base64Img = `data:image/jpg;base64,${avatar.data}`;
  let data = {
    file: base64Img,
    upload_preset: 'h9egsmwb',
  };
  return fetch(url, {
    body: JSON.stringify(data),
    headers: {
      'content-type': 'application/json',
    },
    method: 'POST',
  })
    .then(async r => {
      let data = await r.json();
      // let temp = uploadedPhoto;
      return data.secure_url;
      // setUploadedPhoto(temp);
    })
    .catch(err => {
      console.log(err);
      return '';
    });
};
const EditProfile = props => {
  let userData = props.route.params.profile;
  let [profile, setProfile] = useState(userData);
  //Is photo file
  const [avatar, setAvatar] = useState(null);

  const [fullName, setFullName] = useState(profile.fullName);
  const [about, setAbout] = useState('');

  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  const onSaveProfile = async () => {
    setIsLoading(true);
    setProfile({...profile, fullName: fullName});
    if (avatar != null) {
      uploadPhotos(avatar).then(uriResult => {
        setProfile({...profile, image: uriResult});
        updateProfile({fullName: fullName, image: uriResult});
      });
    } else {
      updateProfile({fullName: fullName, image: profile.image});
    }
  };
  const updateProfile = async profileData => {
    UserCtrl.updateProfile(profileData).then(res => {
      //Save in store
      dispatch(updateUserData(res.data));
      //Save in async storage
      let set = [['user', JSON.stringify(res.data.user)]];
      setItems(set).then(
        value => {
          setIsLoading(false);
          setAvatar(null);
        },
        error => {
          console.log('Error ', error);
          setIsLoading(false);
          setAvatar(null);
        },
      );
    });
  };
  return (
    <View style={styles.mainContainer}>
      <Image
        style={{
          height: SIZES.height * 0.35,
          width: SIZES.width,
          position: 'absolute',
          top: 0,
          left: 0,
        }}
        source={images.background2}
      />
      <View
        style={{
          height: SIZES.height * 0.35,
          width: SIZES.width,
          position: 'absolute',
          top: 0,
          left: 0,
          backgroundColor: COLORS.black,
          opacity: 0.3,
        }}
      />
      <View
        style={{
          position: 'absolute',
          top: 100,
          right: -60,
        }}>
        <ImagesPicker
          iconSize={20}
          textStyle={{
            color: COLORS.transparent,
          }}
          iconColor={COLORS.white}
          multipleImage={false}
          photos={avatar}
          setPhotos={setAvatar}
          text={Lang.t('select_image')}
        />
      </View>

      <View style={styles.profileContainer}>
        <Image
          style={styles.profile}
          source={{
            uri: avatar ? avatar.uri : profile.image,
          }}
        />
        {/* <TouchableOpacity style={{bottom:'8%'}} >
          <Text style={styles.profileEdit}>Change avatar</Text>
        </TouchableOpacity> */}
        <View style={{bottom: '12%'}}>
          <ImagesPicker
            iconSize={25}
            multipleImage={false}
            photos={avatar}
            setPhotos={setAvatar}
            text={Lang.t('select_image')}
          />
        </View>
        <View
          style={{
            width: '100%',
            flexDirection: 'column',
          }}>
          <Spacer>
            <FormInput
              value={fullName}
              labelStyle={{
                color: COLORS.darkGray,
                fontSize: 16,
              }}
              placeHolder={Lang.t('full_name')}
              label={Lang.t('full_name')}
              onChangeText={value => setFullName(value)}
            />
          </Spacer>
          <Spacer>
            <FormInput
              value={about}
              labelStyle={{
                color: COLORS.darkGray,
                fontSize: 16,
              }}
              placeHolder={Lang.t('about_you')}
              label={Lang.t('about_you')}
              onChangeText={value => setAbout(value)}
              multiline={true}
              numberOfLines={5}
            />
          </Spacer>
          <Spacer>
            <Button
              titleStyle={{
                fontSize: 19,
              }}
              buttonStyle={{
                backgroundColor: COLORS.primary,
                height: 45,
              }}
              title={Lang.t('save_change')}
              onPress={() => onSaveProfile()}
              loadingProps={{
                size: 'small',
                color: '#FFF',
              }}
              loading={isLoading}
            />
          </Spacer>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: COLORS.primary,
  },
  profile: {
    height: 100,
    width: 100,
    borderRadius: 50,
    bottom: '10%',
  },
  profileEdit: {
    ...FONTS.body4,
    fontStyle: 'italic',
    color: COLORS.primary,
  },
  profileContainer: {
    flex: 1,
    marginTop: '40%',
    width: '100%',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    borderTopLeftRadius: SIZES.base * 4,
  },
});
export default EditProfile;
