import React, {useEffect, useState} from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  Text,
  TextInput,
  View,
  ActivityIndicator,
} from 'react-native';
import {COLORS, FONTS, SIZES, icons, images} from '../../constants';
import DatePicker from 'react-native-date-picker';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  ImagesPicker,
  RatingCustom,
  TextButton,
  LoginModal,
  InformModal,
} from '../../components';
import ImagePicker from 'react-native-image-crop-picker';
import {ReviewCtrl} from '../../controller';
import Spinner from 'react-native-spinkit';
import {useSelector, useDispatch} from 'react-redux';
import {setGoBack} from '../../stores/screenSlice';
import Lang from '../../language';

const CreateReview = ({navigation, route}) => {
  let isSubscribed = true;
  const [open, setOpen] = useState(false);
  const [photos, setPhotos] = useState([]);
  const [data, setData] = useState({
    title: '',
    content: '',
    rate: 1,
    place: currentPlace ? currentPlace.id : null,
    visitedTime: new Date(),
    isLoading: false,
    images: [],
  });
  const [successModalVisible, setSuccessModalVisible] = useState(false);
  const [failModalVisible, setFailModalVisible] = useState(false);
  const [contentError, setContentError] = useState('');
  const [titleError, setTitleError] = useState('');
  const updateData = obj => setData({...data, ...obj});
  const currentPlace = route.params?.item;
  const token = useSelector(state => state.auth.token);
  const dispatch = useDispatch();

  const cleanupSingleImage = index => {
    let image = photos && photos.length ? photos[index] : null;

    ImagePicker.cleanSingle(image ? image.uri : null)
      .then(() => {
        setPhotos(photos.filter((_, i) => i !== index));
      })
      .catch(e => {
        setFailModalVisible(true);
      });
  };

  const validate = () => {
    if (data.content.trim() === '' || data.title.trim() === '') {
      setContentError(Lang.t('error_empty_field'));
      setTitleError(Lang.t('error_empty_field'));
      return false;
    } else if (data.content.length < 5) {
      setContentError(Lang.t('error_mininum_content'));
      return false;
    }
    if (currentPlace.id) {
      updateData({place: currentPlace.id});
    } else {
      updateData({place: currentPlace._id});
    }
    return true;
  };

  let uploads = [];
  const uploadPhotos = async () => {
    const url = 'https://api.cloudinary.com/v1_1/vntravel285366/image/upload';
    const key = 'h9egsmwb';

    await Promise.all(
      photos.map(async (i, index) => {
        let base64Img = `data:image/jpg;base64,${i.data}`;
        let data = {
          file: base64Img,
          upload_preset: 'h9egsmwb',
        };
        return fetch(url, {
          body: JSON.stringify(data),
          headers: {
            'content-type': 'application/json',
          },
          method: 'POST',
        })
          .then(async r => {
            let data = await r.json();
            // let temp = uploadedPhoto;
            uploads.push(data.secure_url);
            // setUploadedPhoto(temp);
          })
          .catch(err => {
            console.log(err);
            setFailModalVisible(true);
          });
      }),
    );
  };

  useEffect(() => {
    // let abort = new AbortController();
    return () => {
      isSubscribed = false;
    };
  }, []);

  const handleCreateReview = async () => {
    if (!validate()) return;
    if (token) {
      updateData({isLoading: true});

      uploadPhotos().then(() => {
        let data_ = {
          ...data,
          images: uploads,
          place: currentPlace.id,
        };
        ReviewCtrl.createReview(token, data_)
          .then(async res => {
            if (res) {
              if (res.data.success && isSubscribed) {
                //Inform create successfully
                setSuccessModalVisible(true);
                dispatch(setGoBack({goBack: true, fromScreen: 'CreateReview'}));
                updateData({isLoading: false});
                await sleep(4000);
                setSuccessModalVisible(false);
                // await sleep(500);
                navigation.goBack();
              } else {
                setFailModalVisible(true);
              }
            }
          })
          .catch(err => {
            console.log(err);
            j9;
            updateData({isLoading: false});
            setFailModalVisible(true);
          });
      });
    }
  };

  const sleep = milliseconds => {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
  };

  /* -------------------------------------------------------------------------- */
  /*                                  RENDER UI                                 */
  /* -------------------------------------------------------------------------- */

  /* ---------------------------- Render Place info --------------------------- */
  const renderPlaceInfo = () => {
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={{
          marginHorizontal: SIZES.base,
          elevation: 5,
          flexDirection: 'row',
          backgroundColor: COLORS.white2,
          marginVertical: SIZES.radius,
        }}>
        <Image
          source={{uri: currentPlace.images[0]}}
          style={{
            height: 100,
            width: 120,
            resizeMode: 'cover',
          }}
        />
        <View
          style={{
            flex: 1,
            paddingHorizontal: SIZES.base,
            justifyContent: 'center',
          }}>
          <Text
            numberOfLines={2}
            style={{
              color: COLORS.primary,
              ...FONTS.h3,
              fontSize: 14,
            }}>
            {currentPlace.name}
          </Text>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              paddingTop: 3,
              alignItems: 'center',
              marginRight: 12,
            }}>
            <Icon name={icons.location} color={COLORS.primary} size={22} />
            <Text
              numberOfLines={2}
              style={{
                flexWrap: 'wrap',
                width: '100%',
                color: COLORS.gray,
                ...FONTS.body4,
                fontSize: 12,
                paddingLeft: 3,
                lineHeight: 16,
              }}>
              {currentPlace.address}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  /* ------------------------------ Render Header ----------------------------- */
  const renderHeader = () => {
    return (
      <View
        style={{
          height: 45,
          backgroundColor: COLORS.white,
          justifyContent: 'center',
          width: '100%',
          paddingLeft: SIZES.base,
        }}>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <Icon name={icons.close} size={30} color={COLORS.darkGray1} />
        </TouchableOpacity>
      </View>
    );
  };

  /* --------------------------- render Write Review -------------------------- */

  const renderWriteReview = () => {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: COLORS.white,
          borderTopLeftRadius: SIZES.radius * 2,
          borderTopRightRadius: SIZES.radius * 2,
          marginTop: SIZES.radius,
          paddingHorizontal: SIZES.radius,
          paddingTop: SIZES.radius,
          paddingBottom: SIZES.padding,
        }}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{
            flex: 1,
          }}>
          {/* Rating */}
          <View
            style={{
              alignItems: 'flex-start',
              marginVertical: SIZES.base,
            }}>
            <Text
              style={{
                color: COLORS.black,
                ...FONTS.h4,
                marginBottom: SIZES.base,
              }}>
              {Lang.t('review_q1')}
            </Text>
            <RatingCustom
              updateData={updateData}
              value={1}
              imageSize={35}
              readonly={false}
              tintColor={COLORS.white}
              fractions={0}
            />
          </View>
          {/* PICK DATE */}
          <View
            style={{
              marginVertical: SIZES.radius,
            }}>
            <Text
              style={{
                color: COLORS.black,
                ...FONTS.h4,
                marginBottom: SIZES.base,
              }}>
              {Lang.t('review_q2')}
            </Text>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                borderWidth: 1,
                borderColor: COLORS.primary,
                paddingVertical: 5,
                paddingLeft: 5,
                paddingRight: 2,
                justifyContent: 'space-between',
                width: SIZES.width * 0.6,
                marginTop: SIZES.base,
              }}
              onPress={() => {
                setOpen(true);
              }}>
              <Text
                style={{
                  color: COLORS.primary,
                  ...FONTS.body3,
                  paddingLeft: SIZES.radius,
                }}>
                {data.visitedTime?.getDate()}/{data.visitedTime?.getMonth() + 1}
                /{data.visitedTime.getFullYear()}
              </Text>
              <Icon name={icons.calendar} size={25} color={COLORS.primary} />
            </TouchableOpacity>

            <DatePicker
              modal
              open={open}
              androidVariant="nativeAndroid"
              date={data.visitedTime}
              mode="date"
              title="Pick date"
              fadeToColor={COLORS.primary}
              textColor={COLORS.secondary}
              maximumDate={new Date()}
              minimumDate={new Date('2000-01-01')}
              onConfirm={date => {
                setOpen(false);
                updateData({visitedTime: date});
              }}
              onCancel={() => {
                setOpen(false);
              }}
            />
          </View>
          {/* Title */}
          <View
            style={{
              marginVertical: SIZES.base,
            }}>
            <Text
              style={{
                color: COLORS.black,
                ...FONTS.h4,
              }}>
              {Lang.t('review_q3')}
            </Text>
            <TextInput
              onChangeText={value => {
                updateData({title: value});
                setTitleError('');
              }}
              clearButtonMode="always"
              placeholder={Lang.t('review_q3_des')}
              placeholderTextColor={COLORS.gray2}
              numberOfLines={2}
              maxLength={50}
              style={{
                color: COLORS.darkGray,
                ...FONTS.h4,
                fontSize: 13,
                paddingHorizontal: SIZES.radius,
                marginTop: SIZES.base,
                paddingVertical: SIZES.base,
                paddingBottom: 10,
                borderWidth: 1,
                borderColor: COLORS.lightGray1,
                textAlignVertical: 'top',
              }}
            />

            <Text
              style={{
                position: 'absolute',
                bottom: 25,
                right: 10,
                color: COLORS.gray,
                ...FONTS.body4,
                textAlign: 'right',
              }}>
              {data.title.length}/50
            </Text>
            <Text
              style={{
                color: COLORS.red,
                ...FONTS.body4,
              }}>
              {titleError}
            </Text>
          </View>
          {/* Review content */}
          <View
            style={{
              marginVertical: SIZES.base,
            }}>
            <Text
              style={{
                color: COLORS.black,
                ...FONTS.h4,
              }}>
              {Lang.t('review_q4')}
            </Text>
            <TextInput
              onChangeText={value => {
                updateData({content: value});
                setContentError('');
              }}
              clearButtonMode="always"
              placeholder={Lang.t('review_q4_des')}
              placeholderTextColor={COLORS.gray2}
              numberOfLines={6}
              maxLength={300}
              multiline
              style={{
                color: COLORS.darkGray1,
                ...FONTS.body4,
                fontSize: 14,
                paddingHorizontal: SIZES.radius,
                marginTop: SIZES.base,
                paddingVertical: SIZES.base,
                borderWidth: 1,
                borderColor: COLORS.lightGray1,
                textAlignVertical: 'top',
                flexWrap: 'wrap',
              }}
            />
            <Text
              style={{
                color: COLORS.red,
                ...FONTS.body4,
              }}>
              {contentError}
            </Text>
            <Text
              style={{
                position: 'absolute',
                bottom: 25,
                right: 10,
                color: COLORS.gray,
                ...FONTS.body4,
                textAlign: 'right',
              }}>
              {data.content.length}/300
            </Text>
          </View>

          {/* UPLOAD PHOTO */}
          {renderSelectPhoto()}

          {/* SAVE  */}
          <TextButton
            onPress={() => {
              handleCreateReview();
            }}
            label={Lang.t('save')}
            disabled={data.isLoading}
            buttonContainerStyle={{
              height: 55,
              marginHorizontal: SIZES.radius,
              borderRadius: 5,
              marginVertical: SIZES.padding,
            }}
          />
        </ScrollView>
      </View>
    );
  };

  /* ---------------------------------- PHOTO --------------------------------- */
  const renderSelectPhoto = () => {
    return (
      <View
        style={{
          marginVertical: SIZES.radius,
        }}>
        <Text
          style={{
            color: COLORS.black,
            ...FONTS.h4,
          }}>
          {Lang.t('review_q5')}
        </Text>
        {/* SELECT IMAGES */}
        <ImagesPicker
          multipleImage={true}
          photos={photos}
          setPhotos={setPhotos}
          iconSize={60}
        />
        {photos && photos.length > 0 && (
          <Text
            style={{
              color: COLORS.gray,
              ...FONTS.body4,
              textAlign: 'right',
            }}>
            {photos.length}/5
          </Text>
        )}

        <View
          style={{
            flexWrap: 'wrap',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            marginTop: SIZES.radius,
          }}>
          {photos?.map((photo, i) => {
            return renderImage(photo, i);
          })}
        </View>
      </View>
    );
  };

  const renderImage = (photo, i) => {
    return (
      <View
        key={`image-${i}`}
        style={{
          height: 120,
          width: SIZES.width / 3 - 10,
          marginRight: 1,
          marginBottom: 2,
        }}>
        <ImageBackground
          source={{uri: photo.uri}}
          style={{
            resizeMode: 'cover',
            height: 120,
            width: SIZES.width / 3 - 10,
          }}>
          <TouchableOpacity
            onPress={() => cleanupSingleImage(i)}
            style={{
              top: 5,
              left: 5,
              padding: 2,
              borderRadius: 20,
              backgroundColor: COLORS.transparentWhite4,
              width: 30,
            }}>
            <Icon name={icons.close} size={26} color={COLORS.gray} />
          </TouchableOpacity>
        </ImageBackground>
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: COLORS.lightGray,
      }}>
      {data.isLoading && (
        <View
          style={{
            backgroundColor: COLORS.transparentBlack1,
            position: 'absolute',
            height: SIZES.height,
            width: SIZES.width,
            justifyContent: 'center',
            alignItems: 'center',
            top: 0,
            left: 0,
            zIndex: 10,
          }}>
          <Spinner type="9CubeGrid" color={COLORS.primary} size={50} />
        </View>
      )}
      {/* Header */}
      {renderHeader()}
      {/* Place info */}
      {renderPlaceInfo()}

      {/* Main content: Write review */}
      {renderWriteReview()}
      <InformModal
        navigation={navigation}
        isVisible={successModalVisible}
        setIsVisible={setSuccessModalVisible}
        description="We're so grateful for your review. Wish you have more great experiences."
        title="Thank you!"
        source={images.successGif}
        titleStyle={{
          fontSize: 30,
          color: COLORS.green,
        }}
        descriptionStyle={{
          fontSize: 15,
          paddingHorizontal: 2,
        }}
      />
      <InformModal
        navigation={navigation}
        isVisible={failModalVisible}
        setIsVisible={setFailModalVisible}
        description="Sorry for this inconvenience. Please try again!"
        title="Something wrong"
        source={images.failGif}
        titleStyle={{
          fontSize: 30,
          color: COLORS.green,
        }}
        descriptionStyle={{
          fontSize: 15,
          paddingHorizontal: 2,
        }}
      />
    </View>
  );
};

export default CreateReview;
