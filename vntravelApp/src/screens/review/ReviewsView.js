import React, {useEffect, useState} from 'react';
import {View, ScrollView, Text, TouchableOpacity} from 'react-native';
import {Divider} from 'react-native-elements';
import IconAnt from 'react-native-vector-icons/AntDesign';
import IconMater from 'react-native-vector-icons/MaterialIcons';
import {
  RatingCustom,
  ReviewList,
  InformModal,
  TextButton,
} from '../../components';
import {COLORS, FONTS, images, icons, SIZES} from '../../constants';
import {ReviewCtrl} from '../../controller';
import {clearGoBack, setGoBack} from '../../stores/screenSlice';
import {useDispatch, useSelector} from 'react-redux';
import Lang from '../../language';
const ReviewsView = ({navigation, route}) => {
  const place = route.params?.item;
  const [reviews, setReviews] = useState([]);
  const goBack = useSelector(state => state.screen.goBack);
  const [loginModalVisible, setLoginModalVisible] = useState(false);
  const isAuthorized = useSelector(state => state.auth.isAuthorized);

  const dispatch = useDispatch();
  const loadReviews = async () => {
    await ReviewCtrl.getReviewsOfPlace(place.id)
      .then(data => {
        if (data.reviews) {
          setReviews(data.reviews);
        }
      })
      .catch(err => {
        console.log(err);
        setReviews([]);
      });
  };

  useEffect(() => {
    loadReviews();
  }, [goBack]);
  /* -------------------------- Create review button -------------------------- */
  const createReviewButton = () => {
    return (
      <TouchableOpacity
        activeOpacity={0.6}
        style={{
          position: 'absolute',
          bottom: 20,
          right: 20,
          padding: 7,
          borderRadius: 30,
          backgroundColor: COLORS.white,
          elevation: 4,
          zIndex: 1000,
        }}
        onPress={() => {
          if (!isAuthorized) setLoginModalVisible(true);
          else {
            dispatch(clearGoBack());
            dispatch(setGoBack({previousScreen: 'ReviewView'}));
            navigation.push('CreateReview', {item: place});
          }
        }}>
        <IconMater name={icons.pen} color={COLORS.primary} size={40} />
      </TouchableOpacity>
    );
  };

  /* ----------------------------- Review overview ---------------------------- */
  const renderOverview = () => {
    const reviewTypes = [
      Lang.t('excellent'),
      Lang.t('very_good'),
      Lang.t('average'),
      Lang.t('poor'),
      Lang.t('terrible'),
    ];
    const reviewValues = [
      place.reviewStatus.excellent,
      place.reviewStatus.good,
      place.reviewStatus.average,
      place.reviewStatus.poor,
      place.reviewStatus.terrible,
    ];

    return (
      <View
        style={{
          paddingHorizontal: SIZES.base,
          paddingBottom: SIZES.radius,
          marginTop: SIZES.radius,
          width: '100%',
        }}>
        <View
          style={{
            height: 160,
            flexDirection: 'row',
          }}>
          <View
            style={{
              flexDirection: 'column',
              flex: 3,
              justifyContent: 'space-around',
            }}>
            {reviewTypes.map((item, index) => {
              return (
                <Text
                  key={index}
                  style={{
                    color: COLORS.darkGray1,
                    ...FONTS.h4,
                    justifyContent: 'space-around',
                  }}>
                  {item}
                </Text>
              );
            })}
          </View>
          <View
            style={{
              flexDirection: 'column',
              flex: 5,
              width: '100%',
              paddingRight: 20,
              justifyContent: 'space-around',
            }}>
            {reviewValues.map((item, index) => (
              <View
                key={index}
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width:
                      place.reviewCount !== 0
                        ? (item / place.reviewCount) * 100 + '%'
                        : '5%',
                    height: 8,
                    borderRadius: 10,
                    backgroundColor: COLORS.primary,
                  }}
                />
                <Text
                  style={{
                    color: COLORS.gray,
                    ...FONTS.body4,
                    marginLeft: 12,
                  }}>
                  {item}
                </Text>
              </View>
            ))}
          </View>
        </View>
      </View>
    );
  };

  return (
    <View style={{flex: 1}}>
      {createReviewButton()}

      <ScrollView
        scrollEventThrottl={16}
        style={{
          flex: 1,
          backgroundColor: COLORS.white2,
          paddingHorizontal: SIZES.radius + 2,
        }}>
        {/* Overview reviews */}

        <View>
          <View
            style={{
              marginTop: SIZES.radius,
              marginBottom: SIZES.radius,
            }}>
            <Text
              style={{
                color: COLORS.black,
                ...FONTS.h2,
                fontSize: 24,
              }}>
              {Lang.t('reviews')}
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: COLORS.darkGray1,
                ...FONTS.h2,
                paddingVertical: SIZES.base,
              }}>
              {place.rateVoting ? Math.round(place.rateVoting * 10) / 10 : 1}
              /5
            </Text>
            <RatingCustom
              readonly={true}
              imageSize={30}
              value={
                place.rateVoting ? Math.round(place.rateVoting * 10) / 10 : 1
              }
              tintColor={COLORS.white2}
            />
            <Text
              style={{
                color: COLORS.darkGray,
                ...FONTS.body3,
              }}>
              {place.reviewCount} {Lang.t('reviews')}
            </Text>
          </View>
          <View>{renderOverview()}</View>
        </View>
        <Divider
          width={1}
          style={{
            marginHorizontal: SIZES.padding,
            marginVertical: 20,
          }}
          color={COLORS.lightGray}
        />
        {/* List all reviews */}
        <View>
          <ReviewList reviews={reviews} overview={false} />
        </View>
      </ScrollView>
      <InformModal
        navigation={navigation}
        isVisible={loginModalVisible}
        setIsVisible={setLoginModalVisible}
        description={Lang.t('remind_signin_review')}
        title="OPP!"
        source={images.signin}
        actionComponent={
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 30,
            }}>
            <TextButton
              label={Lang.t('signin')}
              onPress={() => navigation.replace('Auth', {screen: 'Signin'})}
              buttonContainerStyle={{
                height: 50,
                width: 200,
                elevation: 3,
              }}
            />
          </View>
        }
      />
    </View>
  );
};

export default ReviewsView;
