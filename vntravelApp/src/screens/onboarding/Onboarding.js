import React, {useRef, useState} from 'react';
import {
  ImageBackground,
  Image,
  View,
  Text,
  Animated,
  StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import LinearGradient from 'react-native-linear-gradient';
import {COLORS, SIZES, FONTS, constants, images, icons} from '../../constants';
import {TextButton} from '../../components';

const Onboarding = ({navigation}) => {
  const scrollX = useRef(new Animated.Value(0)).current;
  const flatListRef = useRef();
  const [currentIndex, setCurrentIndex] = useState(0);
  const onViewChangeRef = useRef(({viewableItems, changed}) => {
    setCurrentIndex(viewableItems[0].index);
  });

  const Dots = () => {
    const dotPosition = Animated.divide(scrollX, SIZES.width);
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        {constants.onboarding_screens.map((item, index) => {
          const dotColor = dotPosition.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: [COLORS.lightBlue1, COLORS.white, COLORS.lightBlue1],
            extrapolate: 'clamp',
          });

          const dotWidth = dotPosition.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: [10, 40, 10],
            extrapolate: 'clamp',
          });
          return (
            <Animated.View
              key={`dot-${index}`}
              style={{
                borderRadius: 5,
                marginHorizontal: 6,
                width: dotWidth,
                height: 10,
                backgroundColor: dotColor,
              }}></Animated.View>
          );
        })}
      </View>
    );
  };

  const renderLogo = () => {
    return (
      <View
        style={{
          position: 'absolute',
          top: SIZES.height > 800 ? 120 : 45,
          left: 0,
          right: 0,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Image
          source={images.logo}
          resizeMode="contain"
          style={{
            width: SIZES.width * 0.3,
            height: 150,
          }}
        />
      </View>
    );
  };

  const renderFooter = () => {
    return (
      <View
        style={{
          position: 'absolute',
          bottom: 20,
          right: 0,
          left: 0,
        }}>
        {/* Pagination */}

        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            marginBottom: SIZES.padding,
          }}>
          <Dots />
        </View>
        {/* Button */}
        {currentIndex < constants.onboarding_screens.length - 1 && (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingHorizontal: SIZES.radius,
            }}>
            <TextButton
              label="Skip"
              buttonContainerStyle={{
                backgroundColor: null,
                borderWidth: 2,
                borderColor: COLORS.lightBlue,
                width: (SIZES.width / 2) * 0.85,
                height: 55,
              }}
              onPress={() => navigation.replace('Signin')}
            />
            <TextButton
              label="Continue"
              labelStyle={{
                color: COLORS.primary,
              }}
              buttonContainerStyle={{
                backgroundColor: COLORS.lightBlue,
                height: 55,
                width: (SIZES.width / 2) * 0.85,
              }}
              endIcon={
                <Icon
                  name={icons.arrowright}
                  color={COLORS.primary}
                  size={25}
                />
              }
              onPress={() => {
                flatListRef?.current?.scrollToIndex({
                  index: currentIndex + 1,
                  animated: true,
                });
              }}
            />
          </View>
        )}
        {currentIndex === constants.onboarding_screens.length - 1 && (
          <View
            style={{
              paddingHorizontal: SIZES.radius,
            }}>
            <TextButton
              label="Let's Join Now"
              buttonContainerStyle={{
                backgroundColor: COLORS.primary,
                height: 55,
                width: '100%',
                marginBottom: SIZES.radius,
                opacity: 0.9,
              }}
              onPress={() => navigation.replace('Signin')}
            />
            <TextButton
              labelStyle={{
                color: COLORS.black,
              }}
              label="Not Now!"
              buttonContainerStyle={{
                backgroundColor: COLORS.transparentWhite7,
                height: 55,
                width: '100%',
              }}
              onPress={() => navigation.replace('Main')}
            />
          </View>
        )}
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: COLORS.darkGray,
      }}>
      <StatusBar backgroundColor={COLORS.primary} />
      <Animated.FlatList
        ref={flatListRef}
        horizontal
        pagingEnabled
        data={constants.onboarding_screens}
        scrollEventThrottle={16}
        snapToAlignment="center"
        showsHorizontalScrollIndicator={false}
        keyExtractor={item => `item-${item.id}`}
        onViewableItemsChanged={onViewChangeRef.current}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {x: scrollX}}}],
          {useNativeDriver: false},
        )}
        renderItem={({item, index}) => {
          return (
            <View
              style={{
                flex: 1,
                
                height: SIZES.height,
                width: SIZES.width,
              }}>
              <ImageBackground
                source={item.backgroundImage}
                resizeMode="cover"
                style={{
                  flex: 2,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {/* Detail */}
                <LinearGradient
                  colors={['#656565', '#717171', '#2C2C2C']}
                  style={{
                    position: 'absolute',
                    height: '100%',
                    width: '100%',
                    opacity: 0.4,
                  }}></LinearGradient>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    alignItems: 'center',
                    marginTop: SIZES.height * 0.3,
                  }}>
                  <Text
                    style={{
                      color: COLORS.white,
                      textAlign: 'center',
                      ...FONTS.h1,
                      paddingHorizontal: SIZES.padding,
                      marginTop: SIZES.base,
                    }}>
                    {item.title}
                  </Text>
                  <Text
                    style={{
                      color: COLORS.lightGray1,
                      ...FONTS.body3,
                      paddingHorizontal: SIZES.padding,
                      marginTop: SIZES.padding,
                    }}>
                    {item.subtitle}
                  </Text>
                </View>
              </ImageBackground>
            </View>
          );
        }}
      />

      {/* Render Logo */}
      {/* {renderLogo()} */}
      {/* Render Footer */}

      {renderFooter()}
      <View />
    </View>
  );
};

export default Onboarding;
