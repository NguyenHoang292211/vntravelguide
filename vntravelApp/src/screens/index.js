import Home from './home/HomeScreen';
import Search from './search/SearchScreen';
import Plan from './plan/PlanScreen';
import Profile from './profile/ProfileScreen';
import PlaceDetail from './place/PlaceDetailScreen';
import ReviewsView from './review/ReviewsView';
import CreateReview from './review/CreateReview';
import PlacesView from './place/PlacesView';
import ProvincesView from './place/ProvincesView';
import SearchPlace from './place/SearchPlace';
import CheckMail from './auth/CheckMail';
import MainLayout from './MainLayout';
import Onboarding from './onboarding/Onboarding';
import ForgotPassword from './auth/ForgotPassword';
import FindPlace from './search/FindPlaceScreen';
import ChooseLocation from './search/ChooseLocationScreen';
import Signin from './auth/SigninScreen';
import Signup from './auth/SignupScreen';
import CreatePlan from './plan/CreatePlan';
import PlanDetail from './plan/PlanDetail';
import TripPlan from './plan/TripPlan';
import EditPlan from './plan/EditPlan';
import Collection from './plan/Collection';
import VoiceDetect from './voice/VoiceDetect';
import ChangeLanguage from './profile/ChangeLanguage';
import ExploreView from './place/ExploreView';
import Map from './voice/Map';
import FriendScreen from './friend/FriendScreen';

export {
  Home,
  Search,
  Plan,
  Profile,
  PlaceDetail,
  ReviewsView,
  CreateReview,
  PlacesView,
  ProvincesView,
  SearchPlace,
  CheckMail,
  MainLayout,
  Onboarding,
  ForgotPassword,
  FindPlace,
  ChooseLocation,
  Signin,
  Signup,
  CreatePlan,
  PlanDetail,
  TripPlan,
  Collection,
  EditPlan,
  VoiceDetect,
  ChangeLanguage,
  ExploreView,
  Map,
  FriendScreen,
};
