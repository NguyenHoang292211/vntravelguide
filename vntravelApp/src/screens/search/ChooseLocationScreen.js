import MapboxGL from '@react-native-mapbox-gl/maps';
import React, {useRef, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon1 from 'react-native-vector-icons/FontAwesome';
import {COLORS, FONTS, icons, SIZES} from '../../constants';
import {
  getCurrentLocation,
  locationPermission,
} from '../../helpers/helperFunction';
import {renderAnnotations} from './SearchScreen';
import Lang from "../../language"
const styleAnnotation = 2;
const centerCoordinateDefault = [106.695897129, 10.782633132001];
const ChooseLocationScreen = ({navigation, route}) => {
  const getLocation = route.params.getLocation;
  const cameraRef = useRef();
  const mapRef = useRef();

  const [currentLocation, setCurrentLocation] = useState();

  const handleGetCurrentDirection = async () => {
    const locPermissionDenied = await locationPermission();
    if (locPermissionDenied) {
      const {lattitude, longtitude} = await getCurrentLocation();
      setCurrentLocation([longtitude, lattitude]);
      cameraRef.current.camera.moveTo([longtitude, lattitude], 25, 200);
    }
  };

  const handlePickLocation = async () => {
    const center = await mapRef.current.getCenter();
    if (center) {
      getLocation(center);
      navigation.goBack();
    } else {
      // TODO: display fail notify
    }
  };

  return (
    <View>
      <MapboxGL.MapView
        ref={mapRef}
        style={StyleSheet.absoluteFillObject}
        zoomEnabled={true}
        zoomEnabled={true}>
        <MapboxGL.Camera
          zoomLevel={15}
          ref={cameraRef}
          centerCoordinate={[
            centerCoordinateDefault[0],
            centerCoordinateDefault[1],
          ]}
        />
        {currentLocation?.length > 0 &&
          renderAnnotations(
            (COLORSPlace = COLORS.primary),
            {},
            (lattitude = currentLocation[1]),
            (longtitude = currentLocation[0]),
            (styleDisplay = styleAnnotation),
          )}
      </MapboxGL.MapView>

      <View style={styles.container}>
        <Icon
          name={icons.locationFill}
          size={50}
          color={COLORS.red}
          style={styles.icon}
        />
        {/* /* Button action */}
        <TouchableOpacity
          style={styles.btnCurrentLocation}
          onPress={() => handleGetCurrentDirection()}>
          <Text>
            <Icon1 name="crosshairs" size={22} color={COLORS.primary} />
          </Text>
        </TouchableOpacity>
      </View>

      {/* /* Button action */}
      <TouchableOpacity
        style={styles.btnPickLocation}
        onPress={() => handlePickLocation()}>
        <Text style={styles.txtOk}>{Lang.t('mess_ok')}</Text>
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    // zIndex: 99,
  },
  icon: {
    fontWeight: 'bold',
  },
  btnCurrentLocation: {
    borderRadius: 50,
    width: 55,
    height: 55,
    backgroundColor: COLORS.white,
    zIndex: 88,
    bottom: 155,
    right: 15,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    elevation: 7,
    borderColor: COLORS.lightBlue1,
  },
  btnPickLocation: {
    width: '90%',
    height: 50,
    backgroundColor: COLORS.primary,
    borderRadius: SIZES.base,
    position: 'absolute',
    bottom: 10,
    right: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtOk: {
    ...FONTS.h3,
    color: COLORS.white,
  },
});

export default ChooseLocationScreen;
