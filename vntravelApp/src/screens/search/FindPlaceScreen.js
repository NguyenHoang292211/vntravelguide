import {useNavigation} from '@react-navigation/core';
import React, {useEffect, useMemo, useRef, useState} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon1 from 'react-native-vector-icons/FontAwesome';
import AutoComplete from '../../components/AutoComplete';
import {resentSearch} from '../../assets/mockupData';
import {COLORS, FONTS, icons, SIZES, constants} from '../../constants';
import toast from '../../helpers/toast';
import {FlatListPlaceSuggestion} from '../../components';
import {getItem, setItems} from '../../utils/Utils';
import {useDispatch, useSelector} from 'react-redux';
import {setDestinationPlace, setSelectedPlace} from '../../stores/searchSlide';
import {setSelectedTab} from '../../stores/tabSlide';
import Lang from '../../language';

const checkValid = (destinationLocation, pickupLocation, isSearchDirect) => {
  if (isSearchDirect) {
    if (Object.keys(destinationLocation).length === 0) {
      toast.danger({
        message: constants.NOTIFY_EMPTY_DESTINATION_LOCATION,
        duration: 3000,
      });
      return false;
    }
  }
  if (Object.keys(pickupLocation).length === 0) {
    toast.danger({
      message: constants.NOTIFY_EMPTY_PICKUP_LOCATION,
      duration: 3000,
    });
    return false;
  }
  return true;
};
const formatPlaceInfo = dataJson => {
  return {
    name: dataJson.name,
    address: dataJson.formatted_address,
    placeId: dataJson.place_id,
    longtitude: dataJson.geometry.location.lng,
    lattitude: dataJson.geometry.location.lat,
  };
};
const FindPlaceScreen = props => {
  const navigation = useNavigation();
  const [visiblePrediction, setVisiblePrediction] = useState(false);
  const [pinLocation, setPinLocation] = useState({
    pickup: false,
    destination: false,
  });
  let isFocusPickupLocation = true;

  const pickupRef = useRef();
  const destinationRef = useRef();

  const isSearchDirect = props.route.params.isSearchDirect;
  const dispatch = useDispatch();

  /*  Save result search*/
  const pickupLocation = useSelector(state => state.search.selectedPlace);
  const destinationLocation = useSelector(
    state => state.search.destinationPlace,
  );

  const [recentSearch, setRecentSearch] = useState([]);
  // const [predictedPlaces, setPredictedPlaces] = useState([]);
  const fetchPickupValue = async data => {
    if (data !== null) {
      dispatch(
        setSelectedPlace({
          name: data.result.name,
          placeId: data.result.place_id,
          address: data.result.formatted_address,
          longtitude: data.result.geometry.location.lng,
          lattitude: data.result.geometry.location.lat,
        }),
      );
    } else {
      dispatch(setSelectedPlace({}));
    }
    //TODO:Check is effect when select recent
    let tmpRecentSearch = [...recentSearch];
    tmpRecentSearch.push(formatPlaceInfo(data.result));
    setRecentSearch(tmpRecentSearch);
    await setItems([['recentSearch', JSON.stringify(tmpRecentSearch)]]);
  };

  const fetchDestinationValue = async data => {
    // destinationLocation = data;
    if (data !== null) {
      dispatch(
        setDestinationPlace({
          name: data.result.name,
          placeId: data.result.place_id,
          address: data.result.formatted_address,
          longtitude: data.result.geometry.location.lng,
          lattitude: data.result.geometry.location.lat,
        }),
      );
    } else {
      dispatch(setDestinationPlace({}));
    }
    //TODO:Check is effect when select recent
    let tmpRecentSearch = [...recentSearch];
    tmpRecentSearch.push(formatPlaceInfo(data.result));
    setRecentSearch(tmpRecentSearch);
    await setItems([
      ['recentSearch', JSON.stringify(formatPlaceInfo(tmpRecentSearch))],
    ]);
  };
  const onDone = () => {
    const isValid = checkValid(
      destinationLocation,
      pickupLocation,
      isSearchDirect,
    );
    if (isValid) {
      dispatch(setSelectedPlace(pickupLocation));
      if (isSearchDirect) {
        dispatch(setDestinationPlace(destinationLocation));
      }
      if (props.route.params.isFromOriginStack) {
        navigation.goBack();
      } else {
        dispatch(setSelectedTab(constants.screens.search));
        navigation.navigate('Main', {
          screen: 'Home',
        });
      }
    }
  };
  const focusPickupLocation = () => {
    isFocusPickupLocation = true;
  };
  const focusDestinationLocation = () => {
    isFocusPickupLocation = false;
  };

  const mapDataToLocation = data => {
    return {
      name: Lang.t('pin_name'),
      placeId: 'selected_place',
      address: Lang.t('pin_name'),

      longtitude: data[0],
      lattitude: data[1],
    };
  };
  /**
   *
   * * Get data from Find Screen
   * @param {*} data
   */
  const selectedLocation = data => {
    if (!isSearchDirect) {
      dispatch(setSelectedPlace(mapDataToLocation(data)));
      setPinLocation({...pinLocation, pickup: true});
      pickupRef.current.pinLocation(Lang.t('pin_name'));
    } else {
      if (isFocusPickupLocation) {
        dispatch(setSelectedPlace(mapDataToLocation(data)));
        setPinLocation({...pinLocation, pickup: true});
        pickupRef.current.pinLocation(Lang.t('pin_name'));
      } else {
        dispatch(setDestinationPlace(mapDataToLocation(data)));
        setPinLocation({...pinLocation, destination: true});
        destinationRef.current.pinLocation(Lang.t('pin_name'));
      }
    }
  };
  const selectRecentSearch = async data => {
    // let tmpResult = mapDataToLocation(data);

    if (!isSearchDirect) {
      dispatch(setSelectedPlace(data));
      setPinLocation({...pinLocation, pickup: true});
      pickupRef.current.pinLocation(data.name);
    } else {
      if (isFocusPickupLocation) {
        dispatch(setSelectedPlace(data));
        setPinLocation({...pinLocation, pickup: true});
        pickupRef.current.pinLocation(data.name);
      } else {
        dispatch(setDestinationPlace(data));
        setPinLocation({...pinLocation, destination: true});
        destinationRef.current.pinLocation(data.name);
      }
    }
  };
  const goSelectedLocation = () => {
    props.navigation.navigate('Search1', {
      screen: 'PickupLocation',
      params: {
        getLocation: selectedLocation,
      },
    });
  };
  const clearRecentSearch = async () => {
    await setItems([['recentSearch', JSON.stringify([])]]);
    setRecentSearch([]);
  };

  useEffect(async () => {
    const dataStorage = await getItem('recentSearch');
    setRecentSearch(dataStorage);
    if (pickupLocation?.name) {
      pickupRef.current.pinLocation(pickupLocation.name);
    }
    if (destinationLocation?.name) {
      destinationRef.current.pinLocation(destinationLocation.name);
    }
    return () => {};
  }, []);
  return (
    <View style={styles.container}>
      {/* /* Result search */}

      <View style={styles.contentContainer}>
        <AutoComplete
          placeHolder={Lang.t('select_destination')}
          getLocation={fetchPickupValue}
          notifyFocus={focusPickupLocation}
          isPinLocation={pinLocation.pickup}
          ref={pickupRef}
        />
        {isSearchDirect && (
          <AutoComplete
            placeHolder={Lang.t('select_destination')}
            getLocation={fetchDestinationValue}
            notifyFocus={focusDestinationLocation}
            isPinLocation={pinLocation.destination}
            ref={destinationRef}
          />
        )}

        {/* /* Select in map */}
        <View
          style={{
            flexDirection: 'row',
            backgroundColor: COLORS.white,
            padding: 15,
            margin: 2,
          }}>
          <TouchableOpacity
            style={{flexDirection: 'row'}}
            onPress={() => goSelectedLocation()}>
            <Icon1 name="map-o" size={22} color={COLORS.darkGray} />
            <Text style={styles.btnChooseOnMap}>
              {' '}
              {Lang.t('choose_on_map')}
            </Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={styles.btnDirect} onPress={onDone}>
          <Text style={{color: COLORS.white, ...FONTS.h3}}>
            {isSearchDirect ? Lang.t('direct') : Lang.t('find')}
          </Text>
        </TouchableOpacity>

        {/* /* Recent search */}
        {!visiblePrediction && (
          <View>
            <Text
              style={{
                backgroundColor: COLORS.primary,
                opacity: 0.5,
                padding: 10,
                ...FONTS.h4,
                borderTopLeftRadius: 10,
                borderTopRightRadius: 10,
              }}>
              {Lang.t('recent_search')}
            </Text>
            <TouchableOpacity
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                padding: SIZES.base,
              }}
              onPress={clearRecentSearch}>
              <Text
                style={{
                  fontStyle: 'italic',
                  color: COLORS.blueLight,
                  ...FONTS.body4,
                }}>
                {Lang.t('clear_search')}
              </Text>
            </TouchableOpacity>
            <View>
              <FlatList
                data={recentSearch}
                renderItem={({item}) => {
                  return (
                    <FlatListPlaceSuggestion
                      icon="search"
                      name={item.name}
                      address={item.address}
                      onPress={selectRecentSearch}
                      data={item}
                    />
                  );
                }}></FlatList>
            </View>
          </View>
        )}
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  btnDirect: {
    backgroundColor: COLORS.primary,
    borderRadius: SIZES.base,
    color: COLORS.white,
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 25,
    borderRadius: SIZES.base,
    height: 55,
    alignItems: 'center',
    marginBottom: SIZES.base,
  },
  container: {},
  btnChooseOnMap: {
    ...FONTS.h4,
    marginLeft: 5,
    color: COLORS.darkGray,
  },

  contentContainer: {
    paddingLeft: 10,
    paddingRight: 10,
  },
});

export default FindPlaceScreen;
