import MapboxGL from '@react-native-mapbox-gl/maps';
import React, {useEffect, useRef, useState} from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  LogBox,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon1 from 'react-native-vector-icons/FontAwesome';
import Icon3 from 'react-native-vector-icons/FontAwesome5';
import Icon2 from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import {mockupRecentSearch} from '../../assets/mockupData';
import BottomModal from '../../components/BottomModal';
import ButtonGroup from '../../components/ButtonGroup';
import {COLORS, constants, FONTS, SIZES} from '../../constants';
import icons from '../../constants/icons';
import {PlaceCtrl} from '../../controller';
import {
  getCurrentLocation,
  locationPermission,
} from '../../helpers/helperFunction';
import toast from '../../helpers/toast';
import {getAllCategories} from '../../stores/placeSlice';
import {setDestinationPlace, setSelectedPlace} from '../../stores/searchSlide';
import Lang from '../../language';

LogBox.ignoreLogs([
  'Warning:Non-serializable values were found in the navigation state',
]);

MapboxGL.setAccessToken(
  'pk.eyJ1IjoidGhhb2xlMzY2IiwiYSI6ImNrcnpydnJwaDAxOG8ydXFtOXdwNWR0eGsifQ.FCbrlFuxwWzIghMVU_T0iA',
);

const {width, height} = Dimensions.get('window');
const centerCoordinateDefault = [106.695897129, 10.782633132001];
const styleIcon = 1;
const styleAnnotation = 2;
const zoomSize = 15;

const PlaceInfo = (placeInfo, handleGoMoreInfo, handleSetDirect) => {
  const mapRating = rate => {
    const roundNumber = Math.round(rate);
    return (
      <>
        <View style={{flexDirection: 'row'}}>
          {Array.from(new Array(roundNumber)).map((item, index) => {
            return (
              <Icon
                key={index}
                style={styles.starIcon}
                name="star"
                size={17}
                color={COLORS.orange}
              />
            );
          })}
          <Text>({rate.toFixed(2)})</Text>
        </View>
      </>
    );
  };
  return (
    <>
      <View style={{flexDirection: 'row', marginTop: 10}}>
        <Image
          source={{
            uri:
              placeInfo?.images?.length > 0
                ? placeInfo.images[0]
                : 'https://res.cloudinary.com/vntravel285366/image/upload/v1638780474/Place_Not_Support_iy9bun.jpg',
          }}
          style={{width: 100, height: 100}}
        />
        <View style={{flex: 1, paddingLeft: 10}}>
          <Text style={styles.namePlace}>{placeInfo?.name}</Text>
          <Text style={styles.address}>{placeInfo?.address}</Text>

          {/* /* Rating */}
          {placeInfo?.rateVoting && mapRating(placeInfo.rateVoting)}
          <View></View>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          // justifyContent: 'flex-end',
          flex: 1,
          paddingTop: 10,
        }}>
        {/* /* Button direct */}
        <TouchableOpacity
          style={[styles.button, styles.btnGoDirect]}
          onPress={() => handleSetDirect(placeInfo)}>
          <View style={styles.btnContent}>
            <Icon3 name="directions" color={COLORS.white} size={26} />
            <Text style={{color: COLORS.white, ...FONTS.body4}}>
              {' '}
              {Lang.t('direct')}
            </Text>
          </View>
        </TouchableOpacity>
        {/*  /* Button  Get More Info */}
        <TouchableOpacity
          style={[styles.button, styles.btnGoInfo]}
          onPress={() => handleGoMoreInfo(placeInfo)}>
          <View style={styles.btnContent}>
            <Icon name="info" color={COLORS.primary} size={26} />
            <Text style={{color: COLORS.primary, ...FONTS.body4}}>
              {' '}
              {Lang.t('more_info')}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </>
  );
};
const InfoDirect = (place, onClear, onStartDirect) => {
  // const calculateDuration=(second)=>{
  let totalSeconds = place.duration;
  let hours = Math.floor(totalSeconds / 3600);
  totalSeconds %= 3600;
  let minutes = Math.floor(totalSeconds / 60);
  let seconds = totalSeconds % 60;

  return (
    <View>
      <View style={{flexDirection: 'row'}}>
        <Text style={{...FONTS.body5, color: COLORS.gray2}}>
          {Lang.t('from')}{' '}
        </Text>
        <Text style={{...FONTS.body4, color: COLORS.black}}>
          {place.selectedPlace}
        </Text>
      </View>
      <View style={{flexDirection: 'row'}}>
        <Text style={{...FONTS.body5, color: COLORS.gray2}}>
          {Lang.t('to')}{' '}
        </Text>
        <Text style={{...FONTS.body4, color: COLORS.black}}>
          {place.destinationPlace}
        </Text>
      </View>
      {/* /* Approximate time */}
      <View style={{flexDirection: 'row'}}>
        <Text style={{...FONTS.body5, color: COLORS.gray2}}>
          {Lang.t('estimate')}{' '}
        </Text>
        <Text
          style={{
            ...FONTS.body3,
            color: COLORS.transparentBlack7,
          }}>
          ({hours > 0 && hours.toString() + Lang.t('time_hours')}
          {minutes > 0 && minutes.toString() + Lang.t('time_minutes')})
        </Text>
        {/* /* Approximate Distance */}
        <Text style={{...FONTS.body3, color: COLORS.transparentBlack1}}>
          {' '}
          {Math.round((place.distance / 1000) * 100) / 100} km
        </Text>
      </View>
      <Text style={{...FONTS.body4}}>
        {Lang.t('mess_traffic')}
      </Text>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-end',
          paddingTop: 10,
        }}>
        <TouchableOpacity
          style={[styles.button, styles.btnGoDirect]}
          onPress={onStartDirect}>
          <View style={styles.btnContent}>
            <Icon3 name="directions" color={COLORS.white} size={26} />
            <Text style={{color: COLORS.white, ...FONTS.body4}}> {Lang.t('start')}</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, styles.btnGoInfo]}
          onPress={onClear}>
          <View style={styles.btnContent}>
            <Icon name="close" color={COLORS.primary} size={26} />
            <Text style={{color: COLORS.primary, ...FONTS.body4}}> {Lang.t('clear')}</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const SearchScreen = ({navigation}) => {
  const selectedPlace = useSelector(state => state.search.selectedPlace);
  const destinationPlace = useSelector(state => state.search.destinationPlace);
  const dispatch = useDispatch();
  const [centerCoordinate, setCenterCoordinate] = useState(
    centerCoordinateDefault,
  );
  const [placeInfo, setPlaceInfo] = useState({});
  const [directInfo, setDirectInfo] = useState({});
  const [recentSearch, setRecentSearch] = useState(mockupRecentSearch);
  const categories = useSelector(state => state.place.categories);
  const [places, setPlaces] = useSelector(state => state.search.places);
  const [placesByCategory, setPlacesByCategory] = useState([]);
  const [currentLocation, setCurrentLocation] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState(categories[0] ?? {});
  // const modalRef = useRef();
  // const modalDirectRed = useRef();

  const mapRef = useRef();
  const cameraRef = useRef();

  const [showBottomInfo, setShowBottomInfo] = useState(false);
  const [showDirectInfo, setShowDirectInfo] = useState(false);

  const [isLoading, setIsLoading] = useState(false);

  //TODO: Route item for direct
  const [route, setRoute] = useState({
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        properties: {},
        geometry: {
          type: 'LineString',
          coordinates: [
            [centerCoordinateDefault[0], centerCoordinateDefault[1]],
          ],
        },
      },
    ],
  });
  const selectGroup = async category => {
    setSelectedCategory(category);
    setIsLoading(true);
    //*? new handle
    //  setPlacesByCategory(places.filter(place => place.category === category.id));
    await PlaceCtrl.getPlaceOfCategory(category.id)
      .then(data => {
        if (data) {
          setPlacesByCategory(data.places.slice());
        }
        setIsLoading(false);
      })
      .catch(err => {
        console.log('Erro ', err);
      });
  };

  const getLiveLocation = async () => {
    const locPermissionDenied = await locationPermission();
    if (locPermissionDenied) {
      const {lattitude, longtitude} = await getCurrentLocation();
      setCenterCoordinate([longtitude, lattitude]);
      setCurrentLocation([longtitude, lattitude]);
      cameraRef.current.camera.moveTo([longtitude, lattitude], 25, 200);
    }
  };

  const getDirection = async (selectedPlace, destinationPlace) => {
    const urlDirect = `${constants.baseSuggestUrl}/${constants.defaultVehicle}/${selectedPlace.longtitude},${selectedPlace.lattitude};${destinationPlace.longtitude},${destinationPlace.lattitude}?annotations=maxspeed&overview=full&geometries=geojson&access_token=${constants.GoongApiKey}`;
    await fetch(urlDirect)
      .then(response => {
        if (response.ok) {
          return response.json();
        }
      })
      .then(data => {
        setDirectInfo({
          duration: data.routes[0].duration,
          distance: data.routes[0].distance,
          selectedPlace: selectedPlace.name,
          destinationPlace: destinationPlace.name,
        });
        setRoute({
          ...route,
          features: [
            {
              type: 'Feature',
              properties: {},
              geometry: {
                type: 'LineString',
                coordinates: data.routes[0].geometry.coordinates,
              },
            },
          ],
        });
        cameraRef.current.fitBounds(
          [selectedPlace.longtitude, selectedPlace.lattitude],
          [destinationPlace.longtitude, destinationPlace.lattitude],
          150,
          1000,
        );
      })
      .catch(error => {
        console.log(error);
      });
  };

  const onPressLocation = () => {
    navigation.navigate('Search1', {
      screen: 'FindPlace',
      params: {
        isSearchDirect: false,
        isFromOriginStack: true,
      },
    });
  };

  //* Function in float button
  const handleGetCurrentDirection = async () => {
    await getLiveLocation();
  };
  const handleFindDirection = () => {
    navigation.navigate('Search1', {
      screen: 'FindPlace',
      params: {
        isSearchDirect: true,
      },
    });
  };

  /**
   * * Fly to place was found
   */
  useEffect(() => {
    if (
      Object.keys(selectedPlace).length > 0 &&
      typeof cameraRef.current !== 'undefined'
    ) {
      cameraRef.current.flyTo(
        [selectedPlace.longtitude, selectedPlace.lattitude],
        15,
        4000,
      );
    }
    return () => {};
  }, [selectedPlace]);

  /**
   * * Update line direction
   */
  useEffect(async () => {
    if (
      Object.keys(destinationPlace).length > 0 &&
      Object.keys(selectedPlace).length > 0 &&
      typeof selectedPlace !== 'undefined' &&
      typeof destinationPlace !== 'undefined' &&
      destinationPlace !== null &&
      selectedPlace !== null
    ) {
      await getDirection(selectedPlace, destinationPlace);
    } else {
      setRoute({
        ...route,
        features: [
          {
            type: 'Feature',
            properties: {},
            geometry: {
              type: 'LineString',
              coordinates: [],
            },
          },
        ],
      });
    }
  }, [destinationPlace, selectedPlace]);

  const getDirectInfo = () => {
    // setShowDirectInfo(true);
    if (
      Object.keys(destinationPlace).length > 0 &&
      Object.keys(selectedPlace).length > 0
    ) {
      setShowDirectInfo(true);
    } else {
      toast.info({message: constants.NOTIFY_EMPTY_DIRECT, duration: 2000});
    }
  };
  const mapPlaceSelected = place => {
    setPlaceInfo(place);
    setShowBottomInfo(true);
  };

  useEffect(() => {
    dispatch(getAllCategories());
  }, []);
  const handleSetDirect = place => {
    dispatch(setDestinationPlace(place));
    navigation.navigate('Search1', {
      screen: 'FindPlace',
      params: {
        isSearchDirect: true,
        isFromOriginStack: true,
      },
    });
  };
  const handleGoMoreInfo = place => {
    if (Object.keys(place).length > 5) {
      setShowBottomInfo(false);
      navigation.navigate('DetailPlace', {item: place});
    } else {
      setShowBottomInfo(false);
      toast.info({
        message: Lang.t('mess_place_not_support'),
        duration: 2000,
      });
    }
  };
  const handleClearDirectInfo = () => {
    dispatch(setSelectedPlace({}));
    dispatch(setDestinationPlace({}));
    setShowDirectInfo(false);
  };
  const handleStartDirect = () => {
    setShowDirectInfo(false);
    toast.info({message:Lang.t('mess_being_develope'), duration: 3000});
  };
  return (
    <View style={{flex: 1}}>
      <View style={{zIndex: 99}}>
        <View style={styles.searchContainer}>
          <TextInput
            style={styles.searchBox}
            placeholder={Lang.t('search_place')}
            placeholderTextColor="#A5A5A5"
            onPressIn={() => {
              onPressLocation();
            }}></TextInput>

          <Icon
            name={icons.search}
            size={22}
            COLORS={COLORS.darkGray}
            style={styles.searchIcon}
          />
        </View>

        <View>
          <ButtonGroup
            style={styles.btnGroup}
            selectGroup={selectGroup}
            buttons={categories}
            selectedId={selectedCategory.id}
            placesOfCategories={placesByCategory.length}
          />
          {isLoading && (
            <ActivityIndicator
              style={{
                marginTop: SIZES.padding * 2,
              }}
              color={COLORS.blue}
              size="large"
            />
          )}
        </View>

        {/* /* Map */}
      </View>
      {/* /* Button action */}
      <TouchableOpacity
        style={styles.btnCurrentPosition}
        onPress={() => handleGetCurrentDirection()}>
        <Text>
          <Icon1 name="crosshairs" size={22} color={COLORS.blueLight} />
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.btnDirection}
        onPress={() => handleFindDirection()}>
        <Text>
          <Icon3 name="directions" size={22} color={COLORS.white} />
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.btnDirectInfo}
        onPress={() => getDirectInfo()}>
        <Text>
          <Icon3 name="taxi" size={22} color={COLORS.primary} />
        </Text>
      </TouchableOpacity>
      {/* <View style={styles.mapContainer}> */}
      <MapboxGL.MapView
        style={StyleSheet.absoluteFillObject}
        zoomEnabled={true}
        zoomEnabled={true}
        ref={mapRef}>
        <MapboxGL.Camera
          zoomLevel={zoomSize}
          ref={cameraRef}
          centerCoordinate={centerCoordinate}
        />
        {/* /* Template redirect */}

        {route.features[0].geometry.coordinates.length > 0 && (
          <MapboxGL.ShapeSource id="line1" shape={route}>
            <MapboxGL.LineLayer
              id="linelayer1"
              style={{
                lineColor: COLORS.secondary,
                lineWidth: 3,
              }}
            />
          </MapboxGL.ShapeSource>
        )}

        {Object.keys(destinationPlace).length > 0 &&
          renderAnnotations(
            (COLORSPlace = COLORS.red),
            (info = destinationPlace),
            (lattitude = destinationPlace.lattitude),
            (longtitude = destinationPlace.longtitude),
            (styleDisplay = styleIcon),
            (onShowBottomInfo = mapPlaceSelected),
          )}
        {Object.keys(selectedPlace).length > 0 &&
          renderAnnotations(
            (COLORSPlace = COLORS.red),
            (info = selectedPlace),
            (lattitude = selectedPlace.lattitude),
            (longtitude = selectedPlace.longtitude),
            (styleDisplay = styleIcon),
            (onShowBottomInfo = mapPlaceSelected),
          )}

        {currentLocation.length > 0 &&
          renderAnnotations(
            (COLORSPlace = COLORS.primary),
            {},
            (lattitude = currentLocation[1]),
            (longtitude = currentLocation[0]),
            (styleDisplay = styleAnnotation),
            (onShowBottomInfo = {}),
          )}
        {/* /* Places by Category */}
        {/* {placesByCategory?.length > 0 && */}
        {placesByCategory.map((item, index) =>
          renderAnnotations(
            (COLORSPlace = selectedCategory.color),
            (info = item),
            (lattitude = item.lattitude),
            (longtitude = item.longtitude),
            (styleDisplay = styleIcon),
            (onShowBottomInfo = mapPlaceSelected),
            // (onShowBottomInfo = setShowBottomInfo),
          ),
        )}
      </MapboxGL.MapView>

      {/* /* Info for place selected */}
      <BottomModal
        // ref={modalRef}
        showHeader={true}
        show={showBottomInfo}
        onTouchOutSide={setShowBottomInfo}
        close={setShowBottomInfo}
        title={Lang.t('info_place')}
        renderContent={PlaceInfo(placeInfo, handleGoMoreInfo, handleSetDirect)}
      />
      {/* /* Info direct  */}
      <BottomModal
        // ref={modalDirectRed}
        showHeader={true}
        show={showDirectInfo}
        onTouchOutSide={() => setShowDirectInfo(false)}
        close={() => setShowDirectInfo(false)}
        title={Lang.t('info_direct')}
        renderContent={InfoDirect(
          directInfo,
          handleClearDirectInfo,
          handleStartDirect,
        )}
      />
      {/* <Toast /> */}
    </View>
    // </View>
  );
};
const styles = StyleSheet.create({
  menuIcon: {
    alignItems: 'flex-start',
    marginLeft: 15,
    marginTop: 16,
  },
  searchContainer: {
    paddingTop: 5,
    paddingBottom: 10,
    paddingHorizontal: 15,
  },
  searchBox: {
    marginTop: 5,
    backgroundColor: COLORS.lightGray,
    height: 55,
    paddingLeft: 15,
    paddingRight: 25,
    borderRadius: SIZES.radius,
    ...FONTS.body4,
    fontSize: 14,
    color: COLORS.darkGray1,
    // flex:1
  },
  searchIcon: {
    position: 'absolute',
    top: 27,
    right: 25,
  },
  txtRecent: {
    fontSize: 14,
    fontWeight: 'bold',
    marginTop: 16,
    marginLeft: 16,
    marginBottom: 10,
  },
  recentContainer: {
    borderColor: '#DADADA',
    borderRadius: 16,
    padding: 3,
    borderWidth: 1,
    padding: 4,
    marginLeft: 5,
  },
  recentName: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  recentProvince: {fontSize: 12},
  btnGroup: {
    flex: 1,
    flexDirection: 'row',
  },
  mapContainer: {
    width: '100%',
  },
  // Style Content Info
  namePlace: {
    ...FONTS.h4,
  },
  button: {
    borderRadius: 10,
    flex: 1,
    height: 55,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    margin: SIZES.base,
    elevation: 4,
  },
  icon: {marginRight: 10},
  address: {...FONTS.body4},
  btnContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btnCurrentPosition: {
    borderRadius: 50,
    width: 55,
    height: 55,
    backgroundColor: COLORS.white,
    zIndex: 88,
    bottom: 210,
    right: 15,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 1,
    borderColor: COLORS.lightBlue1,
  },
  btnDirection: {
    borderRadius: SIZES.base,
    width: 55,
    height: 55,
    backgroundColor: COLORS.blue,
    zIndex: 88,
    position: 'absolute',
    bottom: 140,
    right: 15,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 3,
  },
  btnGoDirect: {
    backgroundColor: COLORS.primary,
  },
  btnGoInfo: {
    backgroundColor: COLORS.white,
  },
  btnDirectInfo: {
    borderRadius: 100,
    width: 55,
    height: 55,
    backgroundColor: COLORS.white,
    zIndex: 88,
    position: 'absolute',
    bottom: 140,
    left: 20,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 3,
    borderColor: COLORS.lightBlue1,
  },
});

/**
 * Render single marked
 */
export const renderAnnotations = (
  COLORSPlace = '#4c4c',
  info = {},
  lattitude = 10.8599583650001,
  longtitude = 106.773641966,
  styleDisplay = 1,
  onShowBottomInfo = data => {},
) => {
  let keyRandom = (Math.random() + 1).toString(36).substring(2);

  return (
    <MapboxGL.PointAnnotation
      key={keyRandom}
      id={keyRandom}
      // id="pointAnnotation"
      coordinate={[longtitude, lattitude]}
      selected={true}
      onSelected={e => onShowBottomInfo(info)}>
      {styleDisplay === styleIcon && (
        <Icon2
          name={icons.locationFill}
          size={45}
          color={COLORSPlace ? COLORSPlace : 'red'}
          style={{paddingBottom: 50}}
        />
      )}
      {styleDisplay === styleAnnotation && (
        <View
          style={{
            height: 20,
            width: 20,
            backgroundColor: COLORSPlace ? COLORSPlace : 'red',
            borderRadius: 50,
            borderColor: '#fff',
            borderWidth: 3,
          }}
        />
      )}

      {/* <MapboxGL.Callout title="This is a sample" /> */}
    </MapboxGL.PointAnnotation>
  );
};

export default SearchScreen;
