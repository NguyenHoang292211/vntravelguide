import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon1 from 'react-native-vector-icons/Ionicons';
import IconAwes from 'react-native-vector-icons/FontAwesome';
import {COLORS, SIZES, FONTS, icons, constants} from '../../constants';
import {FormInput, TextButton} from '../../components';
import setAuthToken from '../../utils/setAuthToken';

import {
  clearStorage,
  removeItem,
  setItems,
  validateEmail,
  validatePassword,
} from '../../utils/Utils';
import {useDispatch} from 'react-redux';
import {saveUserData} from '../../stores/authSlice';
import Spinner from 'react-native-spinkit';
import {AuthCtrl} from '../../controller';
import * as Animatable from 'react-native-animatable';

const saveCurrentUser = (navigation, res, methodSignIn) => {
  //Save token to default axios authenticated
  setAuthToken(res.data.token);
  clearStorage().then(() => {
    let set = [
      ['user', JSON.stringify(res.data.user)],
      ['token', JSON.stringify(res.data.token)],
      [constants.METHOD_SIGN_IN, JSON.stringify(methodSignIn)],
    ];
    setItems(set).then(
      value => {
        setAlert(true);
        setAlertMessage('Create account successfully.');
        setSignUpSuccess(true);
      },
      error => {
        setAlert(true);
        setAlertMessage('Some error happened. Please try again.');
        setSignUpSuccess(true);
      },
    );
  });
};
const SignupScreen = ({navigation}) => {
  const [signUpSuccess, setSignUpSuccess] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  const [alert, setAlert] = useState(false);
  const [emailError, setEmailError] = useState('');
  const [fullnameError, setFullnameError] = useState('');
  const [passError, setPassError] = useState('');
  const [confirmPassError, setConfirmPassError] = useState('');
  const [showPass, setShowPass] = useState(true);
  const [showConfirmPass, setShowConfirmPass] = useState(true);
  const [data, setData] = useState({
    isLoading: false,
    email: '',
    password: '',
    fullname: '',
    confirmPassword: '',
    isLoading: false,
  });
  const dispatch = useDispatch();
  const updateData = obj => setData({...data, ...obj});

  const validateData = (email, password) => {
    let result = true;
    if (!validateEmail(email, setEmailError)) result = false;
    if (!validatePassword(password, setPassError)) result = false;
    if (data.confirmPassword !== data.password) {
      setConfirmPassError('Your confirm password  is not like password.');
      result = false;
    }
    return result;
  };

  const signUp = async () => {
    if (validateData(data.email, data.password)) {
      updateData({isLoading: true});

      await AuthCtrl.signUp(data)
        .then(res => {
          if (res) {
            if (res.data.success) {
              dispatch(saveUserData(res.data));
              saveCurrentUser(navigation, res, constants.methodSignIn.user);
            } else if (
              res.status === 401 ||
              res.status === 403 ||
              res.status === 400
            ) {
              setPassError(res.data.message);
            } else {
              setAlertMessage('Some error happened. Please try again.');
              setAlert(true);
            }
          }
        })
        .catch(error => {
          console.log('Error', error);
          setAlert('Some error happened. Please try again.');
          setAlert(true);
        })
        .finally(() => {
          updateData({isLoading: false});
        });
    }
  };

  const comeBack = () => {
    navigation.navigate('Signin');
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={COLORS.primary} />

      <View style={styles.header}>
        <TouchableOpacity
          activeOpacity={0.6}
          onPress={comeBack}
          style={{
            position: 'absolute',
            height: 40,
            width: 40,
            left: 5,
            top: 5,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <IconAwes name="angle-left" color={COLORS.darkGray} size={30} />
        </TouchableOpacity>

        <Text
          style={{
            color: COLORS.darkGray1,
            ...FONTS.body3,
          }}>
          Register
        </Text>
      </View>
      <View style={styles.form}>
        {/* Header */}
        <Text style={styles.title}>Let's join us</Text>
        <Text style={styles.missionText}>
          Welcome, join our community and discover interesting things
        </Text>
        {/* Form  */}
        <View
          style={{
            flex: 1,
            paddingHorizontal: SIZES.radius,
            width: '100%',
          }}>
          {/* Email */}
          <FormInput
            label="Email"
            labelStyle={{
              color: COLORS.gray,
              ...FONTS.body4,
            }}
            inputStyle={{
              color: COLORS.black,
              ...FONTS.body3,
            }}
            containerStyle={{
              marginTop: 15,
              paddingHorizontal: 2,
            }}
            placeHolder="nguyenvana@gmail.com"
            keyboardType="email-address"
            autoCompleteType="email"
            onChange={value => {
              updateData({email: value});
              validateEmail(value, setEmailError);
            }}
            errorMsg={emailError}
            appendCommponent={
              <View
                style={{
                  justifyContent: 'center',
                }}>
                <Icon
                  name={
                    data.email === '' ||
                    (data.email !== '' && emailError === '')
                      ? icons.rightCheck
                      : icons.wrongCheck
                  }
                  color={
                    data.email === ''
                      ? COLORS.gray
                      : data.email !== '' && emailError === ''
                      ? COLORS.green
                      : COLORS.red
                  }
                  size={18}
                />
              </View>
            }
          />
          {/* Full name */}
          <FormInput
            labelStyle={{
              color: COLORS.gray,
              ...FONTS.body4,
            }}
            label="Fullname"
            inputStyle={{
              color: COLORS.black,
              ...FONTS.body3,
            }}
            containerStyle={{
              marginTop: 15,
              paddingHorizontal: 2,
            }}
            placeHolder="Nguyen Van A"
            onChange={value => {
              updateData({fullname: value});
            }}
            errorMsg={fullnameError}
          />
          {/* Passwrod */}
          <FormInput
            labelStyle={{
              color: COLORS.gray,
              ...FONTS.body4,
            }}
            inputStyle={{
              color: COLORS.black,
              ...FONTS.body3,
            }}
            label="Password"
            // inputStyle={{
            //   color: COLORS.darkGray,
            //   ...FONTS.body3,
            // }}
            containerStyle={{
              paddingHorizontal: 2,
              marginTop: 10,
            }}
            placeHolder="123455@avc"
            autoCompleteType="password"
            secureTextEntry={showPass}
            onChange={value => {
              updateData({password: value});
              setPassError('');
            }}
            errorMsg={passError}
            appendCommponent={
              <TouchableOpacity
                activeOpacity={0.6}
                onPress={() => setShowPass(!showPass)}
                style={{
                  justifyContent: 'center',
                }}>
                <Icon1
                  name={showPass ? icons.eyeOff : icons.eye}
                  color={COLORS.gray}
                  size={22}
                />
              </TouchableOpacity>
            }
          />
          {/*Confirm Passwrod */}
          <FormInput
            label="Confirm password"
            labelStyle={{
              color: COLORS.gray,
              ...FONTS.body4,
            }}
            inputStyle={{
              color: COLORS.black,
              ...FONTS.body3,
            }}
            containerStyle={{
              paddingHorizontal: 2,
              marginTop: 10,
            }}
            placeHolder="123455@avc"
            autoCompleteType="password"
            secureTextEntry={showConfirmPass}
            onChange={value => {
              updateData({confirmPassword: value});
              setConfirmPassError('');
            }}
            errorMsg={confirmPassError}
            appendCommponent={
              <TouchableOpacity
                activeOpacity={0.6}
                onPress={() => setShowConfirmPass(!showConfirmPass)}
                style={{
                  justifyContent: 'center',
                }}>
                <Icon1
                  name={showConfirmPass ? icons.eyeOff : icons.eye}
                  color={COLORS.gray}
                  size={22}
                />
              </TouchableOpacity>
            }
          />
          <TextButton
            label="Sign up"
            labelStyle={{
              ...FONTS.h3,
            }}
            buttonContainerStyle={{
              paddingVertical: 12,
              marginTop: 20,
              width: '100%',
              backgroundColor: COLORS.secondary,
              marginTop: 50,
            }}
            onPress={() => signUp()}
          />
        </View>
      </View>
      {data.isLoading && (
        <View
          style={{
            backgroundColor: COLORS.transparentBlack7,
            position: 'absolute',
            height: SIZES.height,
            width: SIZES.width,
            justifyContent: 'center',
            alignItems: 'center',
            top: 0,
            left: 0,
            zIndex: 10,
          }}>
          <Spinner type="9CubeGrid" color={COLORS.lightBlue1} size={50} />
        </View>
      )}
      {alert && (
        <Animatable.View
          onAnimationEnd={() => {
            setAlert(false);
            // setStartSave(false);
            if (signUpSuccess) navigation.replace('Main');
          }}
          delay={2000}
          animation="fadeOut"
          duration={200}
          style={{
            backgroundColor: COLORS.transparentBlack1,
            position: 'absolute',
            height: SIZES.height,
            width: SIZES.width,
            alignItems: 'center',
            top: 0,
            left: 0,
            zIndex: 10,
            justifyContent: 'center',
          }}>
          <Animatable.View
            animation="zoomIn"
            easing="ease-in-cubic"
            duration={300}
            style={{
              height: 100,
              width: 150,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: SIZES.radius,
              backgroundColor: COLORS.transparentBlack7,
            }}>
            <Text
              style={{
                color: COLORS.white,
                ...FONTS.h4,
                textAlign: 'center',
              }}>
              {alertMessage}
            </Text>
          </Animatable.View>
        </Animatable.View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    top: 0,
    backgroundColor: COLORS.white,
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 12,

    backgroundColor: COLORS.lightGray,
  },
  headerTitle: {
    color: '#FFF',
    fontSize: 19,
    textAlign: 'center',
  },
  form: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
  },
  title: {
    color: COLORS.black,
    ...FONTS.h2,
    fontSize: 22,
    marginTop: 15,
  },

  missionText: {
    ...FONTS.body4,
    fontSize: 14,
    color: COLORS.gray,
    textAlign: 'center',
    paddingHorizontal: SIZES.padding,
  },
});

export default SignupScreen;
