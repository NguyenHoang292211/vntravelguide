import React, {useState, useEffect} from 'react';
import {
  KeyboardAvoidingView,
  Text,
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
  StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon1 from 'react-native-vector-icons/Ionicons';
import {AuthCtrl} from '../../controller';
import {FormInput, TextButton} from '../../components';
import {images, icons, SIZES, FONTS, COLORS, constants} from '../../constants';
import {
  clearStorage,
  removeItem,
  setItems,
  validateEmail,
  validatePassword,
} from '../../utils/Utils';
import {useDispatch} from 'react-redux';
import {saveUserData} from '../../stores/authSlice';
import Spinner from 'react-native-spinkit';
import setAuthToken from '../../utils/setAuthToken';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';

// GoogleSignin.configure({
//   webClientId:
//     '927857795085-e8n5bvu3tggje95rbt6tsn38kf7cr3fr.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
//   offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
//   forceCodeForRefreshToken: true,
// });
GoogleSignin.configure({
  webClientId:
    '927857795085-3evtvbh5sv3r6mn4tvsg9r7unqolpat4.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
  offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
  forceCodeForRefreshToken: true,
});
const saveCurrentUser = (navigation, res, methodSignIn) => {
  //Save token to default axios authenticated
  setAuthToken(res.data.token);
  clearStorage().then(() => {
    let set = [
      ['user', JSON.stringify(res.data.user)],
      ['token', JSON.stringify(res.data.token)],
      [constants.METHOD_SIGN_IN, JSON.stringify(methodSignIn)],
    ];
    setItems(set).then(
      value => {
        navigation.replace('Main');
      },
      error => {
        console.log(error);
      },
    );
  });
};

const SigninScreen = ({navigation}) => {
  const [emailError, setEmailError] = useState('');
  const [passError, setPassError] = useState('');
  const [showPass, setShowPass] = useState(false);
  const [data, setData] = useState({
    isLoading: false,
    email: '',
    password: '',
  });

  const updateData = obj => setData({...data, ...obj});
  const validateData = (email, password) => {
    validateEmail(email, setEmailError);
    validatePassword(password, setPassError);
  };

  const dispatch = useDispatch();
  const onSignin = async () => {
    validateData(data.email, data.password);
    if (emailError === '' && passError === '') {
      updateData({isLoading: true});

      await AuthCtrl.signin(data)
        .then(res => {
          if (res) {
            if (res.data.success) {
              dispatch(saveUserData(res.data));
              saveCurrentUser(navigation, res, constants.methodSignIn.user);
            } else if (res.status === 401 || res.status === 403) {
              setPassError(res.data.message);
            } else {
              alert('Problem having! try again');
            }
          }
        })
        .catch(error => {
          console.log('Error', error);
        })
        .finally(() => {
          updateData({isLoading: false});
        });
    }
  };

  useEffect(() => {
    return () => {};
  });
  const signInWithGoogle = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      updateData({isLoading: true});
      await AuthCtrl.signInWithGoogle(userInfo.idToken)
        .then(res => {
          if (res) {
            if (res.data.success) {
              dispatch(saveUserData(res.data));
              saveCurrentUser(navigation, res, constants.methodSignIn.google);
            } else if (res.status === 401 || res.status === 403) {
              alert(res.data.message);
              GoogleSignin.revokeAccess();
            } else {
              alert('Problem having! try again');
            }
          }
        })
        .catch(error => {
          console.log('Error', error);
          alert('Problem having! try again');
        })
        .finally(() => {
          updateData({isLoading: false});
        });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        console.log('User cancel login');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
        console.log('Progress loading ');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        // console.log('Not available ');
        alert(Lang.t('mess_error_google_service'));
      } else {
        // some other error happened
        alert(Lang.t('mess_error_interval'));
      }
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={{
        flex: 1,
        justifyContent: 'center',
      }}>
      <StatusBar backgroundColor={COLORS.primary} />

      {data.isLoading && (
        <View
          style={{
            backgroundColor: COLORS.transparentBlack7,
            position: 'absolute',
            height: SIZES.height,
            width: SIZES.width,
            justifyContent: 'center',
            alignItems: 'center',
            top: 0,
            left: 0,
            zIndex: 10,
          }}>
          <Spinner type="9CubeGrid" color={COLORS.lightBlue1} size={50} />
        </View>
      )}
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
        }}>
        <ImageBackground
          source={images.background5}
          resizeMode="cover"
          style={{
            height: SIZES.height,
            width: SIZES.width,
          }}>
          {/* Head */}
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {/* Logo */}
            <Image
              source={images.logo}
              resizeMode="cover"
              style={{
                height: 80,
                width: 80,
              }}
            />
            <Text
              style={{
                ...FONTS.special,
                fontSize: 21,
                color: COLORS.white2,
                lineHeight: 25,
              }}>
              VN Travel
            </Text>
            {/* Introduce */}
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: COLORS.white,
                  ...FONTS.h2,
                }}>
                Let's join us
              </Text>
              <Text
                style={{
                  color: COLORS.white,
                  ...FONTS.body3,
                  paddingVertical: 10,
                }}>
                Discovery our beautiful country now
              </Text>
            </View>
          </View>

          {/* Form */}
          <View
            style={{
              flex: 2,
              backgroundColor: COLORS.transparentBlack1,
              borderTopLeftRadius: SIZES.padding * 2.5,
              paddingVertical: 15,
              paddingHorizontal: 15,
            }}>
            <FormInput
              label="Email"
              inputStyle={{
                color: COLORS.darkGray,
                ...FONTS.body3,
              }}
              containerStyle={{
                marginTop: 10,
                paddingHorizontal: 2,
              }}
              placeHolder="nguyenvana@gmail.com"
              keyboardType="email-address"
              autoCompleteType="email"
              onChange={value => {
                updateData({email: value});
                validateEmail(value, setEmailError);
              }}
              errorMsg={emailError}
              appendCommponent={
                <View
                  style={{
                    justifyContent: 'center',
                  }}>
                  <Icon
                    name={
                      data.email === '' ||
                      (data.email !== '' && emailError === '')
                        ? icons.rightCheck
                        : icons.wrongCheck
                    }
                    color={
                      data.email === ''
                        ? COLORS.gray
                        : data.email !== '' && emailError === ''
                        ? COLORS.green
                        : COLORS.red
                    }
                    size={18}
                  />
                </View>
              }
            />

            <FormInput
              labelStyle={{}}
              label="Password"
              inputStyle={{
                color: COLORS.darkGray,
                ...FONTS.body3,
              }}
              containerStyle={{
                paddingHorizontal: 2,
                marginTop: 5,
              }}
              placeHolder="123455@avc"
              autoCompleteType="password"
              secureTextEntry={showPass}
              onChange={value => {
                updateData({password: value});
                setPassError('');
              }}
              errorMsg={passError}
              appendCommponent={
                <TouchableOpacity
                  activeOpacity={0.6}
                  onPress={() => setShowPass(!showPass)}
                  style={{
                    justifyContent: 'center',
                  }}>
                  <Icon1
                    name={showPass ? icons.eyeOff : icons.eye}
                    color={COLORS.gray}
                    size={22}
                  />
                </TouchableOpacity>
              }
            />
            <TouchableOpacity
              style={{
                justifyContent: 'center',
                alignItems: 'flex-end',
                marginTop: 10,
              }}
              onPress={() => navigation.navigate('Forgotpassword')}>
              <Text
                style={{
                  color: COLORS.lightGray2,
                  ...FONTS.h4,
                }}>
                Forgot password?
              </Text>
            </TouchableOpacity>
            <TextButton
              label="Sign in"
              disabled={
                !(
                  data.email !== '' &&
                  data.password !== '' &&
                  passError == '' &&
                  emailError == ''
                )
              }
              labelStyle={{
                ...FONTS.h3,
              }}
              buttonContainerStyle={{
                paddingVertical: 12,
                marginTop: 20,
              }}
              onPress={() => onSignin()}
            />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 10,
              }}>
              <Text
                style={{
                  color: COLORS.white,
                  ...FONTS.body4,
                  fontSize: 14,
                }}>
                You have an account ?{' '}
              </Text>
              <TouchableOpacity
                style={{
                  paddingLeft: SIZES.base,
                }}
                onPress={() => navigation.navigate('Signup')}>
                <Text
                  style={{
                    color: COLORS.lightBlue,
                    ...FONTS.body3,
                    fontFamily: 'LexendDeca-Regular',
                  }}>
                  Sign up
                </Text>
              </TouchableOpacity>
            </View>
            <Text
              style={{
                color: COLORS.lightGray1,
                ...FONTS.body3,
                fontSize: 15,
                textAlign: 'center',
                paddingVertical: SIZES.base,
              }}>
              or
            </Text>
            <TextButton
              startIcon={
                <Image
                  source={images.google}
                  style={{
                    height: 30,
                    width: 30,
                  }}
                />
              }
              label="Sign in with Google"
              buttonContainerStyle={{
                paddingVertical: 12,
                marginTop: 5,
                backgroundColor: '#C9320C',
                justifyContent: 'space-evenly',
                paddingRight: 40,
              }}
              onPress={signInWithGoogle}
            />
            <TextButton
              onPress={() => {
                navigation.replace('Main');
              }}
              label="Skip"
              labelStyle={{
                color: COLORS.white,
                ...FONTS.h3,
                fontSize: 16,
                textDecorationLine: 'underline',
              }}
              buttonContainerStyle={{
                height: 40,
                backgroundColor: null,
              }}
            />
          </View>
        </ImageBackground>
      </View>
    </KeyboardAvoidingView>
  );
};

export default SigninScreen;
