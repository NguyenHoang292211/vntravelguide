import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {COLORS, images, SIZES, FONTS} from '../../constants';
import {TextButton} from '../../components';
import {Divider} from 'react-native-elements';

const CheckMail = ({navigation}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: COLORS.white,
      }}>
      {/* Image */}
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'flex-start',
          paddingHorizontal: SIZES.radius,
          paddingTop: SIZES.padding * 2,
        }}>
        <Image
          source={images.sendEmail}
          style={{
            width: 300,
            height: 250,
          }}
          resizeMode="contain"
        />
        <Text
          style={{
            color: COLORS.black,
            ...FONTS.h2,
            textAlign: 'center',
            marginVertical: SIZES.radius,
            fontSize: 24,
          }}>
          Check your mail
        </Text>
        <Text
          style={{
            color: COLORS.darkGray,
            ...FONTS.body3,
            textAlign: 'center',
          }}>
          We have sent a password recovery instructions to your email.
        </Text>

        {/* Button  */}
        <TextButton
          label="Open email app"
          labelStyle={{
            ...FONTS.h3,
          }}
          buttonContainerStyle={{
            paddingVertical: 12,
            marginTop: SIZES.padding * 3,
            width: '100%',
          }}
          onPress={() => {
            console.log('Open mail');
          }}
        />
        <TextButton
          label="Skip, I'll check later"
          labelStyle={{
            ...FONTS.h3,
            color: COLORS.darkGray,
            fontSize: 15,
          }}
          buttonContainerStyle={{
            paddingVertical: 12,
            marginTop: SIZES.padding,
            width: '100%',
            backgroundColor: null,
          }}
          onPress={() => {
            navigation.replace('Signin');
          }}
        />
        {/* Note */}
        <View
          style={{
            flex: 1,
            justifyContent: 'flex-end',
            paddingHorizontal: SIZES.base,
            marginBottom: SIZES.radius,
          }}>
          <Text
            style={{
              color: COLORS.gray,
              ...FONTS.body4,
              textAlign: 'center',
            }}>
            Did not recieve email? Check your spam filter.
          </Text>
          <Divider
            color={COLORS.gray3}
            width={1}
            style={{marginHorizontal: 100, marginVertical: SIZES.base}}
          />
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => navigation.navigate('Forgotpassword')}>
            <Text
              style={{
                color: COLORS.blue,
                ...FONTS.h4,
                textAlign: 'center',
              }}>
              Try another email address
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default CheckMail;
