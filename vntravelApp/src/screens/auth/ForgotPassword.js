import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  Animated,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import {COLORS, SIZES, FONTS, images, icons, constants} from '../../constants';
import IconAwes from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/AntDesign';
import IconIoni from 'react-native-vector-icons/Ionicons';
import {FormInput, TextButton} from '../../components';
import LottieView from 'lottie-react-native';
import * as Animatable from 'react-native-animatable';
import Spinner from 'react-native-spinkit';
import Auth from '../../controller/Auth';
import {AuthCtrl} from '../../controller';
import {checkStrongPassword} from '../profile/ChangePassword';
import Lang from '../../language';
const EnterEmail = forwardRef((props, ref) => {
  const [email, setEmail] = useState('');
  const [emailError, setEmailError] = useState('');
  useImperativeHandle(ref, () => ({
    getInput() {
      return {
        email: email,
      };
    },
    setError(res) {
      setEmailError(res.err);
    },
  }));
  return (
    <>
      <>
        <Image
          source={images.forgotPassword}
          style={{
            width: 300,
            height: 250,
          }}
          resizeMode="contain"
        />
        <Text
          style={{
            color: COLORS.darkGray,
            ...FONTS.body3,
            textAlign: 'justify',
          }}>
          {'  '}
          {Lang.t('mess_info_forgot')}
        </Text>
        <KeyboardAvoidingView>
          <FormInput
            label=""
            inputStyle={{
              color: COLORS.darkGray,
              ...FONTS.body4,
            }}
            containerStyle={{
              marginTop: SIZES.padding,
              width: '100%',
            }}
            placeHolder="nguyenvana@gmail.com"
            keyboardType="email-address"
            autoCompleteType="email"
            onChange={value => {
              setEmail(value);
            }}
            errorMsg={emailError}
          />
        </KeyboardAvoidingView>
      </>
    </>
  );
});

const EnterVerifyCode = forwardRef((props, ref) => {
  const [verifyCode, setVerifyCode] = useState('');
  const [verifyCodeError, setVerifyCodeError] = useState('');
  useImperativeHandle(ref, () => ({
    getInput() {
      if (verifyCode.length < constants.verifyCodeLength) {
        setVerifyCodeError(Lang.t('rule_verify'));
        return null;
      } else {
        return {
          verifyCode: verifyCode,
        };
      }
    },
    setError(res) {
      setVerifyCodeError(res.err);
    },
  }));
  return (
    <>
      <Image
        source={images.sendEmail}
        style={{
          width: 300,
          height: 250,
        }}
        resizeMode="contain"
      />
      <Text
        style={{
          color: COLORS.black,
          ...FONTS.h2,
          textAlign: 'center',
          marginVertical: SIZES.radius,
          fontSize: 24,
        }}>
        {Lang.t('mess_check_mail')}
      </Text>
      <Text
        style={{
          color: COLORS.darkGray,
          ...FONTS.body3,
          textAlign: 'center',
        }}>
        {Lang.t('mess_have_seen')}
      </Text>
      <FormInput
        label=""
        inputStyle={{
          color: COLORS.darkGray,
          ...FONTS.body4,
        }}
        containerStyle={{
          marginTop: SIZES.padding,
          width: '100%',
        }}
        placeHolder="------"
        keyboardType="numeric"
        onChange={value => {
          setVerifyCode(value);
        }}
        errorMsg={verifyCodeError}
        appendCommponent={
          <View
            style={{
              justifyContent: 'center',
            }}>
            <Icon name={icons.rightCheck} color={COLORS.gray} size={18} />
          </View>
        }
      />
    </>
  );
});

const EnterNewPassword = forwardRef((props, ref) => {
  const [newPassword, setNewPassword] = useState('');
  const [confirmNewPassword, setConfirmNewPassword] = useState('');
  const [errNewPassword, setErrNewPassword] = useState('');
  const [errConfirmNewPassword, setErrConfirmNewPassword] = useState('');

  const [secureNewPassword, setSecureNewPassword] = useState(false);
  const [secureConfirmPassword, setSecureConfirmPassword] = useState(false);

  useImperativeHandle(ref, () => ({
    getInput() {
      if (confirmNewPassword != newPassword) {
        setErrConfirmNewPassword(Lang.t('mess_password_not_match'));
        return null;
      } else if (checkStrongPassword(newPassword)) {
        return {
          password: newPassword,
        };
      } else {
        setErrNewPassword(Lang.t('password_rule'));
        return null;
      }
    },
    setError(res) {},
  }));
  return (
    <>
      <Image
        source={images.fillChangePassword}
        style={{
          width: 300,
          height: 220,
        }}
        resizeMode="contain"
      />
      <Text
        style={{
          color: COLORS.black,
          ...FONTS.h2,
          textAlign: 'center',
          marginVertical: SIZES.radius,
          fontSize: 24,
        }}>
        {Lang.t('mess_change_password')}
      </Text>
      <Text
        style={{
          color: COLORS.darkGray,
          ...FONTS.body3,
          textAlign: 'center',
        }}>
        {Lang.t('password_rule')}
      </Text>
      {/* /* New password */}
      <FormInput
        labelStyle={{
          color: COLORS.gray,
          ...FONTS.body4,
        }}
        inputStyle={{
          color: COLORS.black,
          ...FONTS.body3,
        }}
        label={Lang.t('new_password')}
        inputStyle={{
          color: COLORS.darkGray,
          ...FONTS.body3,
        }}
        containerStyle={{
          paddingHorizontal: 2,
          marginTop: 5,
        }}
        placeHolder="123455@avc"
        autoCompleteType="password"
        secureTextEntry={secureNewPassword}
        onChange={value => setNewPassword(value)}
        errorMsg={errNewPassword}
        appendCommponent={
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => setSecureNewPassword(!secureNewPassword)}
            style={{
              justifyContent: 'center',
            }}>
            <IconIoni
              name={secureNewPassword ? icons.eyeOff : icons.eye}
              color={COLORS.gray}
              size={22}
            />
          </TouchableOpacity>
        }
      />

      {/* /* Confirm password */}
      <FormInput
        labelStyle={{
          color: COLORS.gray,
          ...FONTS.body4,
        }}
        inputStyle={{
          color: COLORS.black,
          ...FONTS.body3,
        }}
        label={Lang.t('confirm_password')}
        inputStyle={{
          color: COLORS.darkGray,
          ...FONTS.body3,
        }}
        containerStyle={{
          paddingHorizontal: 2,
          marginTop: 5,
        }}
        placeHolder="123455@avc"
        autoCompleteType="password"
        secureTextEntry={secureConfirmPassword}
        onChange={value => setConfirmNewPassword(value)}
        errorMsg={errConfirmNewPassword}
        appendCommponent={
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => setSecureConfirmPassword(!secureConfirmPassword)}
            style={{
              justifyContent: 'center',
            }}>
            <IconIoni
              name={secureConfirmPassword ? icons.eyeOff : icons.eye}
              color={COLORS.gray}
              size={22}
            />
          </TouchableOpacity>
        }
      />
    </>
  );
});

const SuccessChangePassword = forwardRef((props, ref) => {
  useImperativeHandle(ref, () => ({
    getInput() {
      return;
      null;
    },
    setError(res) {},
  }));

  return (
    <View style={{alignItems: 'center', justifyContent: 'center'}}>
      <Animatable.View
        animation="zoomIn"
        easing="ease-in-circ"
        duration={700}
        style={{
          width: SIZES.width * 0.9,
          borderRadius: 20,

          justifyContent: 'center',
          alignItems: 'center',
          paddingVertical: 5,
          paddingBottom: 20,
        }}>
        <LottieView
          speed={0.7}
          source={images.successUpdate}
          resizeMode="contain"
          autoPlay
          loop
          style={{
            height: 300,
            width: 250,
          }}
        />
      </Animatable.View>
      <Text
        style={{
          color: COLORS.black,
          ...FONTS.h3,
          textAlign: 'center',
          marginVertical: SIZES.radius,
          fontSize: 24,
        }}>
        {Lang.t('mess_password_changed')}
      </Text>
      <Text
        style={{
          color: COLORS.darkGray,
          ...FONTS.body3,
          textAlign: 'center',
        }}>
        {Lang.t('mess_password_change_successful')}
      </Text>
      <TextButton
        label={Lang.t('sign_in_now')}
        labelStyle={{
          ...FONTS.h3,
        }}
        buttonContainerStyle={{
          paddingVertical: 12,
          marginTop: SIZES.padding + 10,
          width: '100%',
        }}
        onPress={() => {
          props.comeBack();
        }}
      />
    </View>
  );
});

const StepRequestPassword = forwardRef((props, ref) => {
  const {step, currentIndex, comeBack} = props;
  const inputRef = useRef();
  useImperativeHandle(ref, () => ({
    getInput() {
      return inputRef?.current.getInput();
    },
    setError(res) {
      inputRef?.current.setError(res);
    },
  }));

  return (
    <View
      style={{
        width: SIZES.width - SIZES.base * 3,
        height: SIZES.height - SIZES.base * 5,
        flex: 1,
      }}>
      {/* /*  Step 1: Enter Email */}
      {step.id === constants.forgotPasswordMode.email && (
        <EnterEmail ref={inputRef} />
      )}

      {/* /*  Step 2: Entere verify Code */}
      {step.id === constants.forgotPasswordMode.code && (
        <EnterVerifyCode ref={inputRef} />
      )}

      {/* /*  Step 3: Enter new password */}
      {step.id === constants.forgotPasswordMode.resetPassword && (
        <EnterNewPassword ref={inputRef} />
      )}
      {/* /* Step 4: Change password successfully */}
      {step.id === constants.forgotPasswordMode.finishChangePassword && (
        <SuccessChangePassword ref={inputRef} comeBack={comeBack} />
      )}
    </View>
  );
});
const generateCode = () => {
  return Math.floor(100000 + Math.random() * 900000);
};
const ForgotPassword = ({navigation}) => {
  const scrollX = useRef(new Animated.Value(0)).current;
  const flatListRef = useRef();
  const [currentIndex, setCurrentIndex] = useState(0);
  const onViewChangeRef = useRef(({viewableItems, changed}) => {
    setCurrentIndex(viewableItems[0].index);
  });
  const [isLoading, setIsLoading] = useState(false);
  const viewConfigRef = React.useRef({viewAreaCoveragePercentThreshold: 20});

  const [code, setCode] = useState(0);
  const [email, setEmail] = useState('');
  const [verifyCode, setVerifyCode] = useState('');
  const Dots = () => {
    const dotPosition = Animated.divide(scrollX, SIZES.width);
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          marginBottom: SIZES.padding + 10,
          marginTop: SIZES.base,
        }}>
        {constants.onForgotPassword.map((item, index) => {
          const dotColor = dotPosition.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: [COLORS.lightBlue1, COLORS.primary, COLORS.lightBlue1],
            extrapolate: 'clamp',
          });

          const dotWidth = dotPosition.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: [10, 40, 10],
            extrapolate: 'clamp',
          });
          return (
            <Animated.View
              key={`dot-${index}`}
              style={{
                borderRadius: 5,
                marginHorizontal: 6,
                width: dotWidth,
                height: 10,
                backgroundColor: dotColor,
              }}></Animated.View>
          );
        })}
      </View>
    );
  };

  const getEmailRef = useRef();
  const getVerifyCodeRef = useRef();
  const getPWRef = useRef();
  const successChangeRef = useRef();

  //Store list ref
  const refList = [];

  refList.push(getEmailRef);
  refList.push(getVerifyCodeRef);
  refList.push(getPWRef);
  refList.push(successChangeRef);

  const comeBack = () => {
    navigation.navigate('Signin');
  };

  const confirmInput = async () => {
    setIsLoading(true);
    if (currentIndex === constants.forgotPasswordMode.email) {
      const data = getEmailRef?.current.getInput();
      if (data) {
        await AuthCtrl.verifyEmailExist(data.email).then(res => {
          if (res.data.isExist) {
            //Set when exist
            let tmpCode = generateCode();
            setCode(tmpCode);
            setEmail(data.email);

            AuthCtrl.requestVerifyCode(data.email, tmpCode);

            flatListRef?.current?.scrollToIndex({
              index: currentIndex + 1,
              animated: true,
            });
            setIsLoading(false);
          } else {
            setIsLoading(false);
            getEmailRef?.current.setError({err: res.data.message});
          }
        });
      } else {
        setIsLoading(false);
      }
    } else if (currentIndex === constants.forgotPasswordMode.code) {
      const data = getVerifyCodeRef?.current.getInput();
      if (data) {
        await AuthCtrl.verifyCode(code, data.verifyCode).then(res => {
          if (res.data.success) {
            setVerifyCode(data.verifyCode);

            flatListRef?.current?.scrollToIndex({
              index: currentIndex + 1,
              animated: true,
            });
            setIsLoading(false);
          } else {
            setIsLoading(false);
            getVerifyCodeRef?.current.setError({err: res.data.message});
          }
        });
      } else {
        setIsLoading(false);
      }
    } else if (currentIndex === constants.forgotPasswordMode.resetPassword) {
      const data = getPWRef?.current.getInput();
      if (data) {
        await AuthCtrl.changeForgotPassword({
          code: code,
          verifyCode: verifyCode,
          password: data.password,
          mail: email,
        }).then(res => {
          if (res.data.success) {
            flatListRef?.current?.scrollToIndex({
              index: currentIndex + 1,
              animated: true,
            });
            setIsLoading(false);
          } else {
            alert(Lang.t('mess_error_interval'));
            setIsLoading(false);
          }
        });
      } else {
        setIsLoading(false);
      }
    }
  };
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: COLORS.white,
      }}>
      {/* Header */}
      <View
        style={{
          justifyContent: 'flex-start',
          alignItems: 'center',
          paddingVertical: 12,
        }}>
        <TouchableOpacity
          activeOpacity={0.6}
          onPress={comeBack}
          style={{position: 'absolute', left: 20, top: 10}}>
          <IconAwes name="angle-left" color={COLORS.darkGray} size={30} />
        </TouchableOpacity>
        <Text
          style={{
            color: COLORS.darkGray1,
            ...FONTS.body3,
          }}>
          {Lang.t('forgot_password')}
        </Text>
      </View>
      {isLoading && (
        <View
          style={{
            backgroundColor: COLORS.transparentBlack7,
            position: 'absolute',
            height: SIZES.height,
            width: SIZES.width,
            justifyContent: 'center',
            alignItems: 'center',
            top: 0,
            left: 0,
            zIndex: 10,
          }}>
          <Spinner type="9CubeGrid" color={COLORS.primary} size={50} />
        </View>
      )}
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          paddingHorizontal: SIZES.radius,
        }}>
        <StatusBar backgroundColor={COLORS.primary} />
        <ScrollView>
          <Animated.FlatList
            ref={flatListRef}
            horizontal
            pagingEnabled
            data={constants.onForgotPassword}
            // scrollEventThrottle={12}
            snapToAlignment="center"
            showsHorizontalScrollIndicator={false}
            keyExtractor={item => `item-${item.id}`}
            onViewableItemsChanged={onViewChangeRef.current}
            viewabilityConfig={viewConfigRef.current}
            onScroll={Animated.event(
              [{nativeEvent: {contentOffset: {x: scrollX}}}],
              {useNativeDriver: false},
            )}
            scrollEnabled={false}
            renderItem={({item, index}) => {
              return (
                <StepRequestPassword
                  step={item}
                  currentIndex={currentIndex}
                  ref={refList[index]}
                  comeBack={comeBack}
                />
              );
            }}
          />
        </ScrollView>

        <Dots />
        {/* Button confirm */}
        {currentIndex != constants.onForgotPassword.length - 1 && (
          <TextButton
            label={Lang.t('submit')}
            labelStyle={{
              ...FONTS.h3,
            }}
            buttonContainerStyle={{
              paddingVertical: 12,

              width: '100%',
              marginBottom: SIZES.padding + 10,
            }}
            onPress={confirmInput}
          />
        )}
      </View>
    </View>
  );
};

export default ForgotPassword;
