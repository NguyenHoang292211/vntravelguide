import React, {useEffect, useState} from 'react';
import {
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  RefreshControl,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import color from '../../constants/Color';
import CategoryItems from '../../components/home/CategoryItems';
import Carousel from '../../components/home/Carousel/Carousel';
import {COLORS, FONTS, SIZES} from '../../constants';
import {
  BigPlaceCard,
  ProvinceCard,
  Skeleton,
  PlaceCard,
} from '../../components';
import {useDispatch, useSelector} from 'react-redux';
import {setCurrentCategory} from '../../stores/placeSlice';
import {PlaceCtrl, UserCtrl} from '../../controller';
import {VIEW_SCREEN_TYPE} from '../../utils/constants';
import Lang from '../../language';
import * as Animatable from 'react-native-animatable';
import {
  updateUserData,
  updateFavorite,
  updateInRecentPlace,
} from '../../stores/authSlice';
import toast from '../../helpers/toast';
/* ---------------------------- Recommended Place --------------------------- */
export const BigPlaceCardSkeleton = () => {
  return (
    <View
      style={{
        flexDirection: 'row',
      }}>
      <Skeleton
        containerStyle={{width: 130}}
        layout={[
          {
            width: 120,
            height: 120,
            marginHorizontal: SIZES.radius,
            marginVertical: SIZES.base,
          },
        ]}>
        <View />
      </Skeleton>
      <View
        style={{
          paddingHorizontal: SIZES.base,
        }}>
        <Skeleton
          containerStyle={{width: SIZES.width - 160}}
          layout={[
            {
              width: 150,
              height: 25,
              marginVertical: SIZES.base,
            },
            {
              width: 230,
              height: 30,
              marginVertical: SIZES.base,
              marginTop: 18,
            },
            {
              width: 100,
              height: 30,
              borderRadius: 30,
            },
          ]}></Skeleton>
      </View>
    </View>
  );
};
const wrappingIsFavorite = (user, resData) => {
  //resData=.places
  resData.places.forEach(element => {
    if (user?.favorite.includes(element.id)) {
      element.isFavorite = true;
    } else {
      element.isFavorite = false;
    }
  });

  return resData;
};
const HomeScreen = ({navigation}) => {
  const [places, setPlaces] = useState([]);
  const [popularPlaces, setPopularPlaces] = useState([]);
  const [categories, setCategories] = useState([]);
  const [provinces, setProvinces] = useState([]);
  const [refreshing, setRefreshing] = useState(true);
  const [exploreBanners, setExploreBanners] = useState([]);
  const [bestRating, setBestRating] = useState([]);
  const recentSearch = useSelector(state =>
    state.auth.userData ? state.auth.userData.recentSearch : [],
  );

  const user = useSelector(state => state.auth.userData);

  const isAuthorized = useSelector(state => state.auth.isAuthorized);

  const [alert, setAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');

  const dispatch = useDispatch();

 
  const updateFavoritePlace = (placeId) => {
    UserCtrl.updateFavorite(placeId)
      .then(res => {
        if (res) {
          if (res.data.success) {
            if (res.data.add === true) {
              toast.normal({message: Lang.t('added_success'), duration: 2000});
              dispatch(updateFavorite({placeId: placeId, favoriteValue: true}));
            } else {
              toast.normal({message: Lang.t('delete_success'), duration: 2000});
              dispatch(
                updateFavorite({placeId: placeId, favoriteValue: false}),
              );
            }
            // UPDATE isFavorite in Places
            let tmpPlaces = places;
            tmpPlaces.forEach(place => {
              if (place.id === placeId) {
                place.isFavorite = !place.isFavorite;
              }
            });
            setPlaces(tmpPlaces);
            //Update isFavorite in popular
            tmpPlaces = popularPlaces;
            tmpPlaces.forEach(place => {
              if (place.id === placeId) {
                place.isFavorite = !place.isFavorite;
              }
            });
            setPopularPlaces(tmpPlaces);
            //Update bestRating in popular
            tmpPlaces = bestRating;
            tmpPlaces.forEach(place => {
              if (place.id === placeId) {
                place.isFavorite = !place.isFavorite;
              }
            });
            setBestRating(tmpPlaces);

            //Update in reducer
            dispatch(updateUserData({user: res.data.user}));
            // setIsReload(true);
          } else {
            toast.danger({message: Lang.t('some_error'), duration: 2000});
          }
        } else {
          toast.danger({message: Lang.t('some_error'), duration: 2000});
        }
      })
      .catch(err => {
        console.log(err);
        toast.danger({message: Lang.t('some_error'), duration: 2000});
      });
  };
  const viewPlaceNavigator = (title, loadData = null, type, id = null) => {
    navigation.navigate('PlacesView', {title, loadData, type, id, updateFavoritePlace});
  };
  const loadAllData = () => {
    try {
      PlaceCtrl.getAllPlaces().then(data => {
        let tmpData = data;

        tmpData = wrappingIsFavorite(user, tmpData);
        setPlaces(tmpData.places);
      });
      PlaceCtrl.getAllCategories().then(data => {
        setCategories(data.categories);
      });
      PlaceCtrl.getAllProvinces().then(data => {
        setProvinces(data.provinces);
      });

      PlaceCtrl.getExploreBanner().then(data => {
        setExploreBanners(data.explorers);
      });

      PlaceCtrl.getPopularPlaces()
        .then(data => {
          let tmpData = data;
          tmpData = wrappingIsFavorite(user, tmpData);

          setPopularPlaces(tmpData.places.slice(0, 8));
        })
        .catch(error => {
          setPopularPlaces([]);
        });

      PlaceCtrl.getBestRating()
        .then(data => {
          let tmpData = data;
          tmpData = wrappingIsFavorite(user, tmpData);

          setBestRating(tmpData.places.slice(0, 8));
        })
        .catch(error => {
          setBestRating([]);
        });
    } catch (error) {
      console.log(error);
    } finally {
      setRefreshing(false);
    }
  };
  const onRefresh = () => {
    setRefreshing(true);
    setCategories([]);
    setProvinces([]);
    setPlaces([]);
    setPopularPlaces([]);
    setBestRating([]);
    loadAllData();
  };

  useEffect(() => {
    loadAllData();
  }, []);

  /* -------------------------------------------------------------------------- */
  /*                                  Render UI                                 */
  /* -------------------------------------------------------------------------- */

  const renderHeader = () => {
    return (
      <View>
        <View style={{paddingTop: 5, paddingBottom: 10, paddingHorizontal: 15}}>
          <View>
            <TextInput
              onPressIn={() => navigation.navigate('SearchPlace')}
              style={{
                marginTop: 10,
                backgroundColor: COLORS.lightGray,
                height: 60,
                paddingLeft: 15,
                paddingRight: 25,
                borderRadius: SIZES.radius,
                ...FONTS.body4,
                fontSize: 14,
                color: COLORS.darkGray1,
              }}
              placeholder={Lang.t('where_do_you_want_to_go')}
              placeholderTextColor={COLORS.gray3}></TextInput>
            <Icon
              name="search"
              size={22}
              color={COLORS.primary}
              style={{position: 'absolute', top: 30, right: 10}}
            />
          </View>
        </View>
      </View>
    );
  };
  /* ---------------------------- Render categories --------------------------- */
  const renderCategories = () => (
    <View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginBottom: SIZES.base,
        }}>
        {!categories || categories.length === 0 ? (
          <Skeleton
            layout={[
              {
                key: '2',
                width: 180,
                height: 35,
                marginHorizontal: SIZES.radius,
                marginVertical: SIZES.radius,
              },
            ]}>
            <Text
              key={2}
              style={{
                ...FONTS.h3,
              }}></Text>
          </Skeleton>
        ) : (
          <Text
            style={{
              color: COLORS.black,
              ...FONTS.h2,
              fontSize: 17,
              paddingHorizontal: SIZES.radius,
            }}>
            {Lang.t('categories')}
          </Text>
        )}
      </View>
      <View style={styles.categoryContainer}>
        {!categories || categories.length === 0 ? (
          <View
            style={{
              flexDirection: 'row',
            }}>
            <Skeleton
              containerStyle={{
                width: 150,
              }}
              layout={[
                {
                  key: '1',
                  width: 150,
                  height: 50,
                  marginHorizontal: SIZES.radius,
                  borderRadius: 30,
                },
              ]}>
              <View></View>
            </Skeleton>
            <Skeleton
              containerStyle={{
                width: 150,
                marginHorizontal: 10,
              }}
              layout={[
                {
                  key: '1',
                  width: 150,
                  height: 50,
                  marginHorizontal: SIZES.radius,
                  borderRadius: 30,
                },
              ]}>
              <View></View>
            </Skeleton>
            <Skeleton
              containerStyle={{
                width: 150,
                marginHorizontal: 10,
              }}
              layout={[
                {
                  key: '1',
                  width: 150,
                  height: 50,
                  marginHorizontal: SIZES.radius,
                  borderRadius: 30,
                },
              ]}>
              <View></View>
            </Skeleton>
          </View>
        ) : (
          <FlatList
            contentContainerStyle={{height: 80, alignItems: 'center'}}
            ItemSeparatorComponent={
              Platform.OS !== 'android' &&
              (({highlighted}) => (
                <View
                  style={[style.separator, highlighted && {marginLeft: 0}]}
                />
              ))
            }
            horizontal
            listKey="category"
            data={categories}
            keyExtractor={item => `category-${item.id}`}
            showsHorizontalScrollIndicator={false}
            renderItem={({item}) => {
              return (
                <CategoryItems
                  item={item}
                  onPress={() => {
                    dispatch(setCurrentCategory(item.id));
                    viewPlaceNavigator(
                      'Categories',
                      PlaceCtrl.getPlaceOfCategory(item.id),
                      VIEW_SCREEN_TYPE.CATEGORY,
                    );
                  }}
                />
              );
            }}
          />
        )}
      </View>
    </View>
  );

  /* ------------------------------ Popular place ----------------------------- */
  const renderPopularPlace = () => (
    <View
      style={{
        marginTop: 10,
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          paddingRight: 10,
          alignItems: 'center',
          marginBottom: SIZES.radius,
        }}>
        {!popularPlaces || popularPlaces.length === 0 ? (
          <View>
            <Skeleton
              layout={[
                {
                  key: '2',
                  width: 160,
                  height: 35,
                  marginHorizontal: SIZES.radius,
                  marginVertical: SIZES.radius,
                },
              ]}>
              <Text
                key={2}
                style={{
                  ...FONTS.h3,
                }}></Text>
            </Skeleton>

            <View
              style={{
                flexDirection: 'row',
                flex: 1,
              }}>
              <Skeleton
                layout={[
                  {
                    key: '1',
                    width: 250,
                    height: 300,
                    marginHorizontal: SIZES.radius,
                    alignItems: 'flex-end',
                    borderRadius: 0,
                  },
                ]}>
                <View
                  key={1}
                  style={{
                    height: 300,
                    width: 250,
                    alignItems: 'flex-end',
                  }}
                />
              </Skeleton>

              <Skeleton
                containerStyle={{width: 250, marginLeft: SIZES.radius}}
                layout={[
                  {
                    key: '1',
                    width: 250,
                    height: 300,
                    marginHorizontal: SIZES.radius,
                    alignItems: 'flex-end',
                    borderRadius: 0,
                  },
                ]}>
                <View
                  key={1}
                  style={{
                    height: 300,
                    width: 250,
                    alignItems: 'flex-end',
                  }}
                />
              </Skeleton>
            </View>
          </View>
        ) : (
          <View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text
                style={{
                  color: COLORS.black,
                  ...FONTS.h2,
                  fontSize: 17,
                  paddingHorizontal: SIZES.radius,
                }}>
                {Lang.t('popular')}
              </Text>
              <TouchableOpacity
                activeOpacity="0.6"
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
                onPress={() => {
                  dispatch(setCurrentCategory(null));
                  viewPlaceNavigator(
                    Lang.t('popular'),
                    PlaceCtrl.getPopularPlaces(),
                    VIEW_SCREEN_TYPE.POPULAR,
                  );
                }}>
                <Text
                  style={{
                    color: COLORS.primary,
                    fontSize: 13,
                    ...FONTS.body4,
                    fontWeight: '500',
                  }}>
                  {Lang.t('more')}
                </Text>

                <Icon
                  style={{paddingTop: 4}}
                  name="chevrons-right"
                  size={18}
                  color={COLORS.primary}
                />
              </TouchableOpacity>
            </View>

            <View style={{paddingBottom: 10, paddingHorizontal: 5}}>
              <FlatList
                horizontal
                data={popularPlaces}
                key="popular"
                listKey="popular"
                showsHorizontalScrollIndicator={false}
                keyExtractor={item => `popular-${item.id}`}
                renderItem={({item}) => {
                  return (
                    <PlaceCard
                      item={item}
                      navigation={navigation}
                      updateFavoritePlace={updateFavoritePlace}
                    />
                  );
                }}
              />
            </View>
          </View>
        )}
      </View>
    </View>
  );

  /* ------------------------------ Best rating place ----------------------------- */
  const renderBestRatingPlace = () => (
    <View
      style={{
        marginTop: 10,
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          paddingRight: 10,
          alignItems: 'center',
          marginBottom: SIZES.radius,
        }}>
        {!bestRating || bestRating.length === 0 ? (
          <View>
            <Skeleton
              layout={[
                {
                  key: '2',
                  width: 160,
                  height: 35,
                  marginHorizontal: SIZES.radius,
                  marginVertical: SIZES.radius,
                },
              ]}>
              <Text
                key={2}
                style={{
                  ...FONTS.h3,
                }}></Text>
            </Skeleton>

            <View
              style={{
                flexDirection: 'row',
                flex: 1,
              }}>
              <Skeleton
                layout={[
                  {
                    key: '1',
                    width: 250,
                    height: 300,
                    marginHorizontal: SIZES.radius,
                    alignItems: 'flex-end',
                    borderRadius: 0,
                  },
                ]}>
                <View
                  key={1}
                  style={{
                    height: 300,
                    width: 250,
                    alignItems: 'flex-end',
                  }}
                />
              </Skeleton>

              <Skeleton
                containerStyle={{width: 250, marginLeft: SIZES.radius}}
                layout={[
                  {
                    key: '1',
                    width: 250,
                    height: 300,
                    marginHorizontal: SIZES.radius,
                    alignItems: 'flex-end',
                    borderRadius: 0,
                  },
                ]}>
                <View
                  key={1}
                  style={{
                    height: 300,
                    width: 250,
                    alignItems: 'flex-end',
                  }}
                />
              </Skeleton>
            </View>
          </View>
        ) : (
          <View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text
                style={{
                  color: COLORS.black,
                  ...FONTS.h2,
                  fontSize: 17,
                  paddingHorizontal: SIZES.radius,
                  paddingVertical: SIZES.base,
                }}>
                {Lang.t('best_rating')}
              </Text>
              <TouchableOpacity
                activeOpacity="0.6"
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
                onPress={() => {
                  dispatch(setCurrentCategory(null));
                  viewPlaceNavigator(
                    'Popular',
                    PlaceCtrl.getPopularPlaces(),
                    VIEW_SCREEN_TYPE.POPULAR,
                  );
                }}>
                <Text
                  style={{
                    color: COLORS.primary,
                    fontSize: 13,
                    ...FONTS.body4,
                    fontWeight: '500',
                  }}>
                  {Lang.t('more')}
                </Text>

                <Icon
                  style={{paddingTop: 4}}
                  name="chevrons-right"
                  size={18}
                  color={COLORS.primary}
                />
              </TouchableOpacity>
            </View>

            <View style={{paddingBottom: 10, paddingHorizontal: 5}}>
              <FlatList
                horizontal
                data={bestRating}
                key="bestRating"
                listKey="bestRating"
                showsHorizontalScrollIndicator={false}
                keyExtractor={item => `bestRating-${item.id}`}
                renderItem={({item}) => {
                  return (
                    <PlaceCard
                      item={item}
                      navigation={navigation}
                      updateFavoritePlace={updateFavoritePlace}
                    />
                  );
                }}
              />
            </View>
          </View>
        )}
      </View>
    </View>
  );

  /* ---------------------------- Render all places --------------------------- */
  const recentPlace = () => {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
        }}>
        <FlatList
          ListHeaderComponent={
            <View>
              {renderPopularPlace()}
              {renderCategories()}
              {renderExplore()}
              {renderProvinceList()}
              {renderBestRatingPlace()}
              <View
                style={{
                  paddingRight: 10,
                  alignItems: 'flex-start',
                  marginBottom: SIZES.padding,
                  paddingHorizontal: SIZES.radius,
                }}>
                {!bestRating || bestRating.length === 0 ? (
                  <View>
                    <Skeleton
                      containerStyle={{width: 160}}
                      layout={[
                        {
                          width: 160,
                          height: 35,
                          marginHorizontal: SIZES.radius,
                          marginVertical: SIZES.padding,
                        },
                      ]}>
                      <Text />
                    </Skeleton>
                    <View>
                      <BigPlaceCardSkeleton />
                      <BigPlaceCardSkeleton />
                      <BigPlaceCardSkeleton />
                    </View>
                  </View>
                ) : (
                  <Text
                    style={{
                      color: COLORS.black,
                      ...FONTS.h2,
                      fontSize: 17,
                    }}>
                    {isAuthorized
                      ? Lang.t('recently_viewed')
                      : Lang.t('best_rating')}
                  </Text>
                )}
              </View>
            </View>
          }
          data={
            bestRating.length === 0
              ? []
              : isAuthorized === true
              ? recentSearch
              : bestRating
          }
          style={{
            backgroundColor: COLORS.white2,
          }}
          keyExtractor={item => `province-${isAuthorized ? item._id : item.id}`}
          showsHorizontalScrollIndicator={false}
          ListFooterComponent={<View style={{height: 160}} />}
          renderItem={({item, i}) => {
            return (
              <BigPlaceCard
                place={isAuthorized ? item.place : item}
                navigation={navigation}
                key={i}
                updateFavoritePlace={updateFavoritePlace}
              />
            );
          }}
        />
      </View>
    );
  };

  /* ---------------------------- Explore Carousel ---------------------------- */

  const renderExplore = () => {
    return (
      <View style={styles.exploreSection}>
        <Text style={styles.headerSection}>
          {Lang.t('explore_our_country')}
        </Text>
        <Carousel navigation={navigation} listItem={exploreBanners} />
      </View>
    );
  };

  /* ---------------------------- Render Provinces List --------------------------- */
  const renderProvinceList = () => {
    return (
      <View style={{paddingBottom: 10, paddingHorizontal: 5}}>
        <View>
          {!provinces || provinces.length === 0 ? (
            <Skeleton
              layout={[
                {
                  width: 220,
                  height: 35,
                  marginHorizontal: SIZES.radius,
                  marginBottom: 10,
                },
              ]}>
              <Text></Text>
            </Skeleton>
          ) : (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingRight: 10,
                alignItems: 'center',
                marginBottom: SIZES.base,
              }}>
              <Text
                style={{
                  color: COLORS.black,
                  ...FONTS.h2,
                  fontSize: 17,
                  padding: 10,
                }}>
                {Lang.t('recommended')}
              </Text>
              <TouchableOpacity
                activeOpacity="0.6"
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
                onPress={() => {
                  dispatch(setCurrentCategory(null));
                  navigation.navigate('ProvincesView', {
                    title: Lang.t('recommended'),
                    loadData: PlaceCtrl.getAllProvinces(),
                    updateFavoritePlace:updateFavoritePlace
                  });
                }}>
                <Text
                  style={{
                    color: COLORS.primary,
                    fontSize: 13,
                    ...FONTS.body4,
                    fontWeight: '500',
                  }}>
                  {Lang.t('more')}
                </Text>

                <Icon
                  style={{paddingTop: 4}}
                  name="chevrons-right"
                  size={18}
                  color={COLORS.primary}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
        <View>
          {!provinces || provinces.length === 0 ? (
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Skeleton
                containerStyle={{width: 210}}
                layout={[
                  {
                    width: 200,
                    height: 150,
                    marginHorizontal: SIZES.base,
                  },
                ]}>
                <View />
              </Skeleton>
              <Skeleton
                containerStyle={{width: 210}}
                layout={[
                  {
                    width: 200,
                    height: 150,
                    marginHorizontal: SIZES.base,
                  },
                ]}>
                <View />
              </Skeleton>
              <Skeleton
                containerStyle={{width: 210}}
                layout={[
                  {
                    width: 200,
                    height: 150,
                    marginHorizontal: SIZES.base,
                  },
                ]}>
                <View />
              </Skeleton>
            </View>
          ) : (
            <FlatList
              horizontal
              data={provinces.slice(0, 5)}
              key="provinces"
              listKey="province"
              showsHorizontalScrollIndicator={false}
              keyExtractor={item => `province-${item.id}`}
              // refreshing={}
              onRefresh={() => console.log('Refressh')}
              alwaysBounceVertical={false}
              refreshing={false}
              bouncesZoom={true}
              bounces={true}
              alwaysBounceHorizontal={true}
              renderItem={({item}) => {
                return (
                  <ProvinceCard
                    item={item}
                    onPress={() => {
                      dispatch(setCurrentCategory(null));

                      viewPlaceNavigator(
                        item.name,
                        PlaceCtrl.getPlaceOfProvince(item.id),
                        VIEW_SCREEN_TYPE.PROVINCE,
                        item.id,
                      );
                    }}
                  />
                );
              }}
            />
          )}
        </View>

        {alert && (
          <Animatable.View
            animation="fadeIn"
            duration={500}
            style={{
              backgroundColor: COLORS.transparentBlack1,
              position: 'absolute',
              height: SIZES.height,
              width: SIZES.width,
              alignItems: 'center',
              top: 0,
              left: 0,
              zIndex: 10,
              justifyContent: 'center',
            }}>
            <Animatable.View
              onAnimationEnd={() => {
                setAlert(false);
              }}
              animation="fadeOut"
              easing="ease-in-cubic"
              duration={300}
              style={{
                height: 100,
                width: 150,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: SIZES.radius,
                backgroundColor: COLORS.transparentBlack7,
              }}>
              <Text
                style={{
                  color: COLORS.white,
                  ...FONTS.h4,
                  textAlign: 'center',
                }}>
                {alertMessage}
              </Text>
            </Animatable.View>
          </Animatable.View>
        )}
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <ScrollView
        horizontal={false}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={() => onRefresh()}
          />
        }>
        {renderHeader()}
        {recentPlace()}
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
  headerMenu: {
    flex: 1,
    display: 'flex',
    paddingHorizontal: 15,
    paddingVertical: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  headerImage: {
    width: '100%',
    height: 270,
  },
  DarkeOverlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    height: 270,
    backgroundColor: '#000',
    opacity: 0.4,
    borderBottomRightRadius: 70,
  },
  searhContainer: {
    paddingTop: 50,
    paddingHorizontal: 16,
  },
  titleText: {
    fontSize: 24,
    color: color.headerTitle,
    fontFamily: 'LexendDeca-Medium',
  },
  userText: {
    fontSize: 15,
    color: color.descText,
    fontFamily: 'LexendDeca-Medium',
  },
  searchBox: {
    marginTop: 10,
    backgroundColor: COLORS.lightGray,
    height: 60,
    paddingLeft: 15,
    paddingRight: 25,
    borderRadius: SIZES.radius,
    ...FONTS.body4,
    fontSize: 14,
    color: COLORS.darkGray1,
  },
  searchContainer: {
    paddingTop: 5,
    paddingBottom: 10,
    paddingHorizontal: 15,
  },
  searchIcon: {
    position: 'absolute',
    top: 30,
    right: 10,
  },
  categoryContainer: {
    paddingHorizontal: 5,
  },
  provinceName: {
    color: color.descText,
    fontSize: 16,
  },
  placeCount: {
    color: '#A8A8A9',
    fontSize: 12,
    marginTop: 10,
  },
  headerSection: {
    color: COLORS.black,
    ...FONTS.h2,
    fontSize: 17,

    paddingHorizontal: SIZES.radius,
    paddingVertical: 10,
  },
  recommendSection: {
    marginBottom: 20,
  },

  provinceItem: {
    flexDirection: 'row',
    paddingRight: SIZES.base,
    backgroundColor: COLORS.lightGray,
    marginHorizontal: SIZES.radius,
    borderRadius: SIZES.radius,

    marginBottom: SIZES.base,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  exploreSection: {
    marginBottom: 35,
  },
});

export default HomeScreen;
