import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import AwesIcon from 'react-native-vector-icons/FontAwesome';
import IonIcon from 'react-native-vector-icons/Ionicons';
import {useDispatch} from 'react-redux';
import {Skeleton, TripItem} from '../../components';
import {COLORS, FONTS, icons, images, SIZES} from '../../constants';
import {UserCtrl} from '../../controller';
import Lang from '../../language';

const headerHeight = 0.17;

const FriendProfileScreen = ({route, navigation}) => {
  const dispatch = useDispatch();

  const userProfile = route.params.user;

  const [isLoadingTrips, setIsLoadingTrips] = useState(true);
  const [isLoadingFriends, setIsLoadingFriends] = useState(true);
  const [friends, setFriends] = useState([]);
  const [trips, setTrips] = useState([]);

  //TODO: handle send request
  const handleAddFriend = async () => {};

  useEffect(async () => {
    await UserCtrl.getInfoUser(userProfile.id).then(data => {
      setFriends(data.user?.friends);
      setIsLoadingFriends(false);
    });

    await UserCtrl.getTripPublicOfUser(userProfile.id).then(data => {
      setTrips(data.plans);
      setIsLoadingTrips(false);
    });

    return () => {};
  }, []);

  /* ------------------------------ RENDER HEADER ----------------------------- */
  const renderHeader = () => {
    return (
      <View
        style={{
          height: SIZES.height * headerHeight,
          width: SIZES.width,
          padding: SIZES.padding * 0.5,
        }}>
        {/* Back */}

        <ImageBackground
          style={styles.bgImage}
          resizeMode="cover"
          source={images.background5}
        />
        <View
          style={[
            styles.bgImage,
            {opacity: 0.4, backgroundColor: COLORS.black},
          ]}
        />

        {/* More   */}

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'flex-end',
            marginTop: SIZES.height * 0.1,
          }}>
          <View>
            <Image
              resizeMode="cover"
              style={styles.profile}
              source={{
                uri: `${userProfile?.image}`,
              }}
            />
            {/* Status */}
            <View
              style={{
                height: 14,
                width: 14,
                borderRadius: 7,
                backgroundColor: COLORS.orange,
                position: 'absolute',
                bottom: 5,
                right: 10,
              }}
            />
          </View>
          <View
            style={{
              justifyContent: 'center',
              marginLeft: 8,
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: COLORS.white,
                  ...FONTS.h2,
                  fontSize: 18,
                }}>
                {userProfile?.fullName}
              </Text>
            </View>
            <Text
              style={{
                color: COLORS.white2,
                ...FONTS.body3,
                fontSize: 16,
                marginLeft: 5,
              }}>
              {userProfile?.email}
            </Text>
            <TouchableOpacity
              activeOpacity={0.6}
              style={{
                backgroundColor: COLORS.lightBlue1,
                paddingHorizontal: 5,
                paddingVertical: 8,
                width: 120,
                borderRadius: 5,
                justifyContent: 'center',
                alignItems: 'center',
                marginLeft: 10,
                marginTop: 10,
                flexDirection: 'row',
              }}
              onPress={() => {}}>
              <IonIcon name={icons.add} color={COLORS.white2} size={19} />
              <Text
                style={{
                  color: COLORS.white2,
                  ...FONTS.body3,
                  fontSize: 15,
                }}>
              {Lang.t('add_friend')}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  /* ------------------------------ RENDER ABOUT ------------------------------ */
  const renderAbout = () => {
    return (
      <View
        style={{
          paddingHorizontal: SIZES.radius,
          paddingVertical: SIZES.base,
          marginTop: 60,
        }}>
        <View
          style={{
            backgroundColor: COLORS.lightBlue1,
            opacity: 0.2,
            // borderRadius: 25,
            position: 'absolute',
            justifyContent: 'center',
            alignItems: 'center',
            width: SIZES.width,
            bottom: 0,
            top: 0,
          }}>
          <Text
            style={{
              ...FONTS.h1,
              color: COLORS.gray2,
            }}>
            CONNECT WITH ME
          </Text>
        </View>
        <AwesIcon name={icons.quote_left} size={24} color={COLORS.gray3} />
        <Text
          style={{
            ...FONTS.body3,
            color: COLORS.gray,
            zIndex: 50,
            textAlign: 'center',
            fontSize: 15,
            paddingHorizontal: SIZES.radius,
          }}>
          Fake it unstill I Make it. What's not yours even if you force is
          useless
        </Text>
        <View
          style={{
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
          }}>
          <AwesIcon name={icons.quote_right} size={24} color={COLORS.gray3} />
        </View>
      </View>
    );
  };

  /* ----------------------------- RENDER FRIENDS ----------------------------- */

  const renderFriends = () => {
    return (
      <View
        style={{
          // padding: SIZES.radius,
          paddingVertical: SIZES.radius,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            // paddingHorizontal: SIZES.radius,
            // paddingVertical: SIZES.radius,
          }}>
          <Text
            style={{
              ...FONTS.h3,
              color: COLORS.black,
              fontSize: 20,
              marginBottom: 10,
              paddingVertical: SIZES.base,
              paddingHorizontal: SIZES.radius,
            }}>
            Friends
          </Text>
        </View>
        <View>
          {isLoadingFriends && (
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
              }}>
              {Array.from(new Array(5)).map(item => (
                <Skeleton
                  containerStyle={{
                    width: 73,
                    paddingHorizontal: SIZES.radius,
                    paddingBottom: 5,
                  }}
                  layout={[
                    {
                      key: '1',
                      width: 60,
                      height: 60,
                      borderRadius: 50,
                    },
                  ]}>
                  <View></View>
                </Skeleton>
              ))}
            </View>
          )}
          {!isLoadingFriends && friends && (
            <FlatList
              contentContainerStyle={{
                paddingHorizontal: SIZES.radius,
                paddingBottom: 5,
              }}
              data={friends}
              // key="avatars"
              listKey="friends"
              ItemSeparatorComponent={({highlighted}) => (
                <View style={{marginLeft: 3}} />
              )}
              horizontal
              keyExtractor={(item, index) =>
                (Math.random() + 1).toString(36).substring(7)
              }
              showsHorizontalScrollIndicator={false}
              renderItem={({item, index}) => {
                return friendAvatar(
                  item,
                  (Math.random() + 1).toString(36).substring(7),
                  navigation,
                );
              }}
            />
          )}
        </View>
      </View>
    );
  };
  const renderTrips = () => {
    return (
      <View>
        <Text
          style={{
            ...FONTS.h3,
            color: COLORS.black,
            fontSize: 20,
            paddingHorizontal: SIZES.radius,
            paddingVertical: SIZES.base,
          }}>
          {Lang.t('trips')}
        </Text>
        {isLoadingTrips && (
          <Skeleton
            containerStyle={{
              width: SIZES.width * 0.93,
              // backgroundColor: COLORS.white,
              paddingHorizontal: SIZES.radius,
              paddingBottom: 5,
            }}
            layout={[
              {
                key: '1',
                width: SIZES.width * 0.93,
                height: SIZES.height * 0.35,

                borderRadius: 50,
              },
            ]}>
            <View></View>
          </Skeleton>
        )}

        {!isLoadingTrips && trips && (
          <FlatList
            data={trips}
            contentContainerStyle={{
              padding: SIZES.radius,
            }}
            ItemSeparatorComponent={({highlighted}) => (
              <View style={{marginLeft: 2}} />
            )}
            listKey="trips"
            horizontal={true}
            pagingEnabled
            scrollEnabled={true}
            keyExtractor={item => `${item.id}_ ${(Math.random() + 1).toString(36).substring(7)}`}
            showsHorizontalScrollIndicator={false}
            renderItem={(item, index) => (
              <TripItem item={item} navigation={navigation} />
            )}
          />
        )}

        {trips.length <= 0 && (
          <Text
            style={{
              ...FONTS.body3,
              color: COLORS.gray2,
              textAlign: 'center',
              marginTop: 100,
            }}>
            {Lang.t('no_plan')}
          </Text>
        )}
      </View>
    );
  };

  const logedProfile = () => {
    return (
      <ScrollView
        style={{
          paddingBottom: 200,
        }}>
        {/* User Info */}
        {renderHeader()}
        {renderAbout()}

        {/* <FlatList
          ListHeaderComponent={
            <View>
              {renderFriends()}
              {renderTrips()}
            </View>
          }
          data={[]}
          ListFooterComponent={
            <View
              style={{
                height: 60,
              }}
            />
          }
          renderItem={index => <View key={index} />}
        /> */}
        {renderFriends()}
        {renderTrips()}
      </ScrollView>
    );
  };

  return <View style={styles.containerImage}>{logedProfile()}</View>;
};
const styles = StyleSheet.create({
  profileContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: 150,
    height: 40,
    marginVertical: 10,
    backgroundColor: COLORS.mainBlue,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    elevation: 5,
  },
  containerImage: {
    alignContent: 'center',
  },
  bgImage: {
    flex: 1,
    top: 0,
    position: 'absolute',
    width: SIZES.width,
    height: SIZES.height * headerHeight,
    justifyContent: 'center',
    borderBottomLeftRadius: 40,
    borderBottomRightRadius: 40,
  },

  bottomContainer: {
    marginTop: '52%',
    height: '90%',
    width: '100%',
    backgroundColor: COLORS.white3,
    borderTopStartRadius: 50,
    borderBottomEndRadius: 50,
    alignItems: 'center',
  },
  profile: {
    height: 80,
    width: 80,
    borderRadius: 50,
    borderColor: COLORS.white3,
    borderWidth: 4,
  },
  profileName: {
    ...FONTS.h3,
    bottom: '8%',
  },
  profileEmail: {
    ...FONTS.body4,
    color: COLORS.darkGray,
    bottom: '7%',
  },
  profileEdit: {
    ...FONTS.h4,
    // fontStyle: 'italic',
    color: COLORS.primary,
  },
  profileGuest: {
    paddingLeft: SIZES.padding,
    paddingRight: SIZES.padding,
    bottom: '8%',
    textAlign: 'center',
  },
  itemMenu: {
    ...FONTS.body3,
    fontSize: 16,
    color: COLORS.darkGray,
    paddingVertical: 3,
  },
});
export default FriendProfileScreen;

const friendAvatar = (item, key, navigation) => {
  return (
    <TouchableOpacity
      key={key}
      style={{
        height: 60,
        width: 60,
        elevation: 5,
        backgroundColor: COLORS.white3,
        borderRadius: 30,
        marginHorizontal: 3,
      }}
      onPress={() => {
        navigation.push('MainProfile', {
          screen: 'ViewUserProfile',
          params: {user: item},
        });
      }}>
      <Image
        resizeMode="cover"
        style={[styles.profile, {borderRadius: 30, height: 60, width: 60}]}
        source={{
          uri: `${item?.image}`,
        }}
      />
    </TouchableOpacity>
  );
};
