import React, {useEffect, useState} from 'react';
import {View, Text, TextInput, TouchableOpacity, Button} from 'react-native';
import {COLORS, icons, SIZES, FONTS, images} from '../../constants';
import Lang from '../../language';
import Icon from 'react-native-vector-icons/AntDesign';
import Awes5Icon from 'react-native-vector-icons/FontAwesome5';

import {Avatar} from 'react-native-elements';
import {UserCtrl} from '../../controller';

const FriendScreen = ({navigation}) => {
  //Default variable
  const [suggestionFriend, setSuggestionFriend] = useState([]);
  const [searchFriend, setSearchFriend] = useState('nhung');

  /* --------------------------------- HEADER --------------------------------- */
  const renderHeader = () => {
    return (
      <View
        style={{
          paddingTop: 10,
          paddingBottom: 10,
          paddingHorizontal: 10,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 40,
        }}>
        <TextInput
          // defaultValue=""
          onChangeText={value => {
            setSearchFriend(value);
          }}
          value={searchFriend}
          style={{
            backgroundColor: COLORS.lightGray,
            height: 50,
            paddingLeft: 15,
            paddingRight: 25,
            borderRadius: SIZES.radius,
            ...FONTS.body4,
            fontSize: 14,
            color: COLORS.darkGray1,
            width: SIZES.width * 0.9,
          }}
          placeholder={Lang.t('search_friend')}
          placeholderTextColor={COLORS.gray3}></TextInput>
        <TouchableOpacity
          activeOpacity={0.6}
          style={{
            padding: 8,
            borderRadius: 25,
            backgroundColor: COLORS.transparentWhite7,
            position: 'absolute',
            right: 25,
          }}>
          <Icon
            name={icons.search}
            size={22}
            color={COLORS.primary}
            // style={{position: 'absolute', top: 20, right: 10}}
          />
        </TouchableOpacity>
      </View>
    );
  };

  const friendItem = (item, navigation) => {
    return (
      <TouchableOpacity
        key={item}
        style={{
          flexDirection: 'row',
          marginVertical: 5,
          // backgroundColor: COLORS.white2,
          paddingVertical: 10,
        }}
        onPress={() => {
          navigation.navigate('MainProfile', {
            screen: 'ViewUserProfile',
            params: {user: item},
          });
        }}>
        <Avatar source={{uri: item.image}} rounded size={45}>
          <Avatar.Accessory solid size={11} source={images.circle} />
        </Avatar>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingHorizontal: SIZES.base,
            alignItems: 'center',
            width: SIZES.width * 0.82,
          }}>
          <Text
            style={{
              ...FONTS.h3,
              fontSize: 15,
            }}>
            {item.email}
          </Text>
          <TouchableOpacity
            style={{
              backgroundColor: '#CDF2FA',
              paddingVertical: 8,
              paddingHorizontal: 10,
              borderRadius: 4,
              elevation: 4,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Awes5Icon name={icons.friend} color="#0CC4ED" size={15} />
            <Text
              style={{
                ...FONTS.h5,
                // fontSize: 14,
                color: '#0CC4ED',
                marginLeft: 8,
              }}>
              {Lang.t('txt_add')}
            </Text>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  };

  //
  useEffect(async () => {
    await UserCtrl.getSuggestionEmail(searchFriend).then(data => {
      setSuggestionFriend(data.users);
    });
  }, [searchFriend]);

  const friendList = () => {
    return (
      <View style={{}}>
        {suggestionFriend?.map(item => {
          return friendItem(item, navigation);
        })}
      </View>
    );
  };
  return (
    <View style={{flex: 1, backgroundColor: COLORS.white}}>
      {/* Search  */}
      {renderHeader()}
      <View
        style={{
          padding: SIZES.radius,
        }}>
        {friendList()}
      </View>
    </View>
  );
};

export default FriendScreen;
