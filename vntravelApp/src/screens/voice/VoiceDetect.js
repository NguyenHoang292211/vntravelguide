import React, {useEffect, useState, useRef} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import {COLORS, constants, FONTS, images, SIZES} from '../../constants';
import LottieView from 'lottie-react-native';
import {AlanView} from '@alan-ai/alan-sdk-react-native';
import {NativeEventEmitter, NativeModules} from 'react-native';
import {Map} from '..';
import {PlaceCtrl, PlanCtrl} from '../../controller';
import {useDispatch, useSelector} from 'react-redux';
import {addNewPlan, setCurrentPlan} from '../../stores/planSlice';
import Spinner from 'react-native-spinkit';
import command from './command';
import * as Animatable from 'react-native-animatable';
import {Translate} from '../../controller';
import {setSelectedTab} from '../../stores/tabSlide';
import {clearGoBack, setGoBack} from '../../stores/screenSlice';
import {useIsFocused} from '@react-navigation/native';

const VoiceDetect = ({navigation}) => {
  const isFocus = useIsFocused();
  const token = useSelector(state => state.auth.token);
  const dispatch = useDispatch();
  const {AlanManager, AlanEventEmitter} = NativeModules;
  const alanEventEmitter = new NativeEventEmitter(AlanEventEmitter);
  const [isGetLocation, setIsGetLocation] = useState(false);
  const [showMap, setShowMap] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [nearbyPlaces, setNearbyPlaces] = useState([]);
  const [showInstruction, setShowInstruction] = useState(false);
  const [location, setLocation] = useState([]);
  const goBack = useSelector(state => state.screen.goBack);
  const [showAlan, setShowAlan] = useState(false);
  const [placeGuide, setPlaceGuide] = useState({
    t: false,
    place: null,
    isLoading: false,
  });

  const alan = () => {
    return (
      <View>
        <AlanView
          authData={{token: token}}
          projectid={
            'd46f100e317501549b6ece4c678751d42e956eca572e1d8b807a3e2338fdd0dc/stage'
          }
        />
      </View>
    );
  };

  useEffect(() => {
    if (isFocus) setShowAlan(true);
    else setShowAlan(false);
    return () => {
      setShowAlan(false);
    };
  }, [isFocus]);

  useEffect(() => {
    if (placeGuide.t) {
      setPlaceGuide({...placeGuide, ...{isLoading: true}});
      Translate.translate('en', placeGuide.place.description)
        .then(res => {
          if (res.data.success) {
            console.log(res.data.data);
            setPlaceGuide({...placeGuide, ...{isLoading: false}});

            AlanManager.activate();
            AlanManager.callProjectApi(
              'readPlace',
              {place: res.data.data},
              (err, result) => {
                setPlaceGuide({t: false, place: null});
              },
            );
          }
        })
        .catch(err => {
          console.log(err);
        })
        .finally(() => setPlaceGuide({...placeGuide, ...{isLoading: true}}));
    }
  }, [placeGuide.t]);

  useEffect(() => {
    let isSigned = false;
    if (token) isSigned = true;

    alanEventEmitter.addListener('onCommand', data => {
      if (data.command === command.get_location) {
        setShowMap(true);
        setIsGetLocation(true);
      }
      if (data.command === command.back) {
        setShowMap(false);
        setIsGetLocation(false);
      }

      if (data.command === command.create_plan) {
        if (data.plan) {
          if (data.plan.photo === true)
            navigation.navigate('CreatePlan', {plan: data.plan});
          else {
            if (!isLoading) createPlan(data.plan);
          }
        }
      }

      if (data.command === command.load_plan) {
        if (token) {
          dispatch(setSelectedTab(constants.screens.my_plan));
          navigation.goBack();
        }
      }

      if (data.command === 'signin') {
        if (!token) navigation.replace('Auth', {screen: 'Signin'});
      }

      if (data.command === command.get_nearby) {
        if (!isLoading) {
          // if (isGetLocation && location) {
          setIsLoading(true);
          if (showMap === false) {
            setShowMap(true);
            setIsGetLocation(true);
          }
          getNearbyPlaces(107.0845881, 10.32660032);
          // }
        }
      }
    });

    return () => {
      alanEventEmitter.removeAllListeners('onCommand');
    };
  }, [token]);

  const getNearbyPlaces = (long, lat, category = '', distance = 10000) => {
    PlaceCtrl.getNearbyPlaces(long, lat, category, distance)
      .then(res => {
        setNearbyPlaces(res.places);
        AlanManager.activate();
        AlanManager.callProjectApi(
          'informSuccess',
          {type: 'load_nearby'},
          (err, result) => {},
        );
      })
      .catch(err => {
        console.log(err);
        AlanManager.activate();
        AlanManager.callProjectApi(
          'informFail',
          {type: 'load_nearby'},
          () => {},
        );
      })
      .finally(() => setIsLoading(false));
  };

  const createPlan = data_ => {
    setIsLoading(true);
    PlanCtrl.createPlan(token, data_)
      .then(res => {
        if (res) {
          if (res.data.success) {
            dispatch(addNewPlan(res.data.plan));
            dispatch(setCurrentPlan(res.data.plan));
            navigation.navigate('DetailPlan', {item: res.data.plan});
          } else {
            AlanManager.activate();
            AlanManager.callProjectApi(
              'informFail',
              {type: 'plan'},
              (err, result) => {
                // AlanManager.deactivate();
              },
            );
          }
        }
      })
      .catch(err => {
        console.log('Error post plan: ', err);
        setFail(true);
      })
      .finally(() => setIsLoading(false));
  };

  const Instruction = () => {
    return (
      <View
        style={{
          height: SIZES.height * 0.65,
          width: SIZES.width * 0.93,
          borderRadius: SIZES.radius,
          backgroundColor: COLORS.transparentWhite4,
          elevation: 4,
        }}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{
            paddingHorizontal: SIZES.radius,
            zIndex: 2,
          }}>
          <Text
            style={{
              color: COLORS.blueLight,
              ...FONTS.h2,
              fontSize: 17,
              marginVertical: SIZES.radius,
              textAlign: 'center',
            }}>
            My command
          </Text>
          <View style={styles.bacWord}>
            <Text style={styles.primaryText}>Call me say</Text>
            <Text style={styles.secondaryText}> "Hey Alan"</Text>
          </View>
          <View style={styles.bacWord}>
            <Text style={styles.primaryText}>Create plan</Text>
            <Text style={styles.secondaryText}>"Let/... create plan/trip"</Text>
            <Text style={styles.primaryText}>Cancel plan</Text>
            <Text style={styles.secondaryText}> "Cancel/no"</Text>
            <Text style={styles.primaryText}>Time start/end</Text>
            <Text style={styles.secondaryText}>
              "... today/tomorow-(next) Sunday/Monday/.. - 22/11/2021..."
            </Text>
            <Text style={styles.primaryText}>Skip description/picture </Text>
            <Text style={styles.secondaryText}>"Skip/no/not now..."</Text>
          </View>

          <View style={styles.bacWord}>
            <Text style={styles.primaryText}>Location</Text>
            <Text style={styles.secondaryText}>
              "Get/find my location/position/..."
            </Text>
            <Text style={styles.secondaryText}>
              "Get/find... places/attraction nearby (me/my location) ..."
            </Text>
          </View>
          <View style={styles.bacWord}>
            <Text style={styles.primaryText}>Stop me</Text>
            <Text style={styles.secondaryText}>"Stop Alan"</Text>
          </View>
          <View
            style={{
              height: 30,
            }}
          />
        </ScrollView>
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: COLORS.white2,
      }}>
      {!showMap && (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <LottieView
            resizeMode="cover"
            source={images.smartBot}
            autoPlay
            autoSize
            style={{
              height: SIZES.height * 0.25,
              // marginRight: 50,
            }}
          />
          {/* INSTRUCTION */}

          <Text
            style={{
              color: COLORS.secondary,
              ...FONTS.special,
              fontSize: 27,
              marginBottom: 10,
              marginTop: -40,
            }}>
            Guideline with Alan
          </Text>
          {Instruction()}
        </View>
      )}

      {showMap && (
        <View
          style={{
            width: SIZES.width,
            height: SIZES.height,
          }}>
          <Map
            navigation={navigation}
            isGetLocation={isGetLocation}
            setLocation={setLocation}
            nearbyPlaces={nearbyPlaces}
            setPlaceGuide={setPlaceGuide}
            placeGuide={placeGuide}
            showAlan={setShowAlan}
          />
          <TouchableOpacity
            onPress={() => {
              setShowInstruction(!showInstruction);
              // setShowAlan(!showAlan);
            }}
            activeOpacity={0.7}
            style={{
              position: 'absolute',
              top: 20,
              right: 10,
              height: 60,
              width: 60,
              zIndex: 80000,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={images.instruction}
              style={{
                height: 45,
                width: 45,
                resizeMode: 'cover',
              }}
            />
          </TouchableOpacity>
          {showInstruction && (
            <Animatable.View
              animation="zoomIn"
              easing="ease-in-circ"
              duration={300}
              style={{
                position: 'absolute',
                justifyContent: 'center',
                alignItems: 'center',
                top: 0,
                bottom: 0,
                right: 0,
                left: 0,
              }}>
              <Instruction />
            </Animatable.View>
          )}
        </View>
      )}

      {showAlan === true && alan()}
      {isLoading && (
        <View
          style={{
            backgroundColor: COLORS.transparentWhite4,
            position: 'absolute',
            height: SIZES.height,
            width: SIZES.width,
            justifyContent: 'flex-end',
            alignItems: 'center',
            top: 0,
            left: 0,
            zIndex: 10,
            paddingBottom: 200,
            zIndex: 99999,
          }}>
          <Spinner type="ThreeBounce" color={COLORS.lightBlue1} size={40} />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  primaryText: {
    color: COLORS.blueLight,
    ...FONTS.body4,
    fontSize: 13,
  },
  secondaryText: {
    color: COLORS.black,
    ...FONTS.h4,
    fontSize: 14,
  },
  bacWord: {
    justifyContent: 'space-between',
    // alignItems: 'center',
    backgroundColor: COLORS.supperLightBlue,
    paddingVertical: SIZES.base,
    borderRadius: 5,
    paddingHorizontal: SIZES.base,
    marginVertical: 3,
  },
});

export default VoiceDetect;
