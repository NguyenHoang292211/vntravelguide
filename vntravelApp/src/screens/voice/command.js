export default COMMAND_VOICE = {
  create_plan: 'create_plan',
  back: 'go_back',
  get_location: 'get_location',
  get_nearby: 'get_nearby',
  load_plan: 'load_plan',
};
