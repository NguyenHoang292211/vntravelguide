import React, {useState, useRef, useEffect} from 'react';
import {
  View,
  LogBox,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  FlatList,
} from 'react-native';
import MapboxGL from '@react-native-mapbox-gl/maps';
import {
  getCurrentLocation,
  locationPermission,
} from '../../helpers/helperFunction';
import IconAnt from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/Ionicons';
import IconAwes from 'react-native-vector-icons/FontAwesome5';
import {COLORS, FONTS, icons, images, SIZES} from '../../constants';
import {RatingCustom, FilterModal, TextButton} from '../../components';
import Lang from '../../language';
import {Translate} from '../../controller';
LogBox.ignoreLogs([
  'Warning:Non-serializable values were found in the navigation state',
]);

MapboxGL.setAccessToken(
  'pk.eyJ1IjoidGhhb2xlMzY2IiwiYSI6ImNrcnpydnJwaDAxOG8ydXFtOXdwNWR0eGsifQ.FCbrlFuxwWzIghMVU_T0iA',
);
const centerCoordinateDefault = [106.695897129, 10.782633132001];
const styleIcon = 1;
const styleAnnotation = 2;
const zoomSize = 15;

const Map = ({
  navigation,
  isGetLocation,
  setLocation,
  nearbyPlaces = [],
  setPlaceGuide,
  placeGuide,
  showAlan,
}) => {
  const [currentLocation, setCurrentLocation] = useState([]);
  const [centerCoordinate, setCenterCoordinate] = useState(
    centerCoordinateDefault,
  );
  const mapRef = useRef();
  const cameraRef = useRef();
  const [displayedPlace, setDisplayedPlace] = useState(null);
  const [showPlaceModal, setShowPlaceModal] = useState(false);
  //* Function in float button
  const handleGetCurrentDirection = async () => {
    await getLiveLocation();
  };

  const getLiveLocation = async () => {
    const locPermissionDenied = await locationPermission();
    if (locPermissionDenied) {
      const {lattitude, longtitude} = await getCurrentLocation();
      setCenterCoordinate([longtitude, lattitude]);
      setCurrentLocation([longtitude, lattitude]);
      setLocation([longtitude, lattitude]);
      //   cameraRef.current.camera.moveTo([longtitude, lattitude], 25, 200);
    }
  };

  useEffect(() => {
    if (isGetLocation) handleGetCurrentDirection();
    return () => {};
  }, [isGetLocation]);

  useEffect(() => {
    return () => {
      showAlan(false);
    };
  }, []);

  return (
    <View
      style={{
        flex: 1,
      }}>
      <MapboxGL.MapView
        style={StyleSheet.absoluteFillObject}
        zoomEnabled={true}
        zoomEnabled={true}
        ref={mapRef}>
        <MapboxGL.Camera
          zoomLevel={zoomSize}
          ref={cameraRef}
          centerCoordinate={centerCoordinate}
        />
        {/* /* Template redirect */}

        {currentLocation.length > 0 &&
          renderAnnotations(
            (lattitude = currentLocation[1]),
            (longtitude = currentLocation[0]),
            (styleDisplay = styleIcon),
            (place = {}),
          )}
        {/* /* Places by Category */}
        {nearbyPlaces?.map((item, index) => {
          return renderAnnotations(
            item.lattitude,
            item.longtitude,
            styleAnnotation,
            item,
            setDisplayedPlace,
            setShowPlaceModal,
          );
        })}
      </MapboxGL.MapView>

      {/* Filter modal */}
      {showPlaceModal && (
        <FilterModal
          isVisible={showPlaceModal}
          onClose={() => setShowPlaceModal(false)}
          children={
            <View
              style={
                {
                  // alignItems: 'center',
                  // alignItems: 'center',
                }
              }>
              <View
                style={{
                  width: '100%',
                  alignItems: 'flex-end',
                  marginBottom: 5,
                }}>
                <TouchableOpacity
                  style={{
                    backgroundColor: COLORS.lightGray,
                    padding: 6,
                    borderRadius: 20,
                  }}
                  onPress={() => setShowPlaceModal(false)}>
                  <IconAnt
                    name={icons.close}
                    size={23}
                    color={COLORS.primary}
                  />
                </TouchableOpacity>
              </View>

              <FlatList
                data={displayedPlace.images}
                keyExtractor={(item, index) => item.id}
                ItemSeparatorComponent={({highlighted}) => (
                  <View style={[{width: 4}, highlighted && {marginLeft: 0}]} />
                )}
                horizontal
                pagingEnabled
                scrollEnabled
                snapToAlignment="center"
                scrollEventThrottle={16}
                showsHorizontalScrollIndicator={false}
                renderItem={({item}) => {
                  return (
                    <View
                      key={item}
                      style={{
                        height: SIZES.height * 0.3,
                        width: SIZES.width * 0.93,
                        borderRadius: SIZES.base,
                      }}>
                      <Image
                        source={{uri: item}}
                        resizeMode="cover"
                        style={{
                          height: SIZES.height * 0.3,
                          width: SIZES.width * 0.93,
                          borderRadius: SIZES.base,
                        }}
                      />
                      <View
                        style={{
                          height: SIZES.height * 0.3,
                          width: SIZES.width * 0.93,
                          borderRadius: SIZES.base,
                          position: 'absolute',
                          top: 0,
                          left: 0,
                          backgroundColor: COLORS.transparentBlack2,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                      />
                    </View>
                  );
                }}
              />

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  paddingHorizontal: SIZES.radius,
                  marginVertical: SIZES.radius,
                }}>
                <RatingCustom
                  imageSize={35}
                  value={place.rateVoting}
                  tintColor={COLORS.white}
                />
                <Text
                  style={{
                    color: COLORS.black,
                    ...FONTS.h3,
                  }}>
                  (
                  {displayedPlace.rateVoting
                    ? Math.round(displayedPlace.rateVoting * 10) / 10
                    : 1}
                  )
                </Text>
              </View>
              <Text
                style={{
                  color: COLORS.black,
                  ...FONTS.h2,
                  fontSize: 18,
                  textAlign: 'center',
                }}>
                {displayedPlace.name}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  paddingHorizontal: SIZES.base,
                }}>
                <Icon2
                  name={icons.locationFill}
                  color={COLORS.primary}
                  size={28}
                />

                <Text
                  style={{
                    color: COLORS.gray,
                    ...FONTS.body4,

                    textAlign: 'center',
                  }}>
                  {displayedPlace.address}
                </Text>
              </View>
              {/* <View
                style={{
                  width: '100%',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <TextButton
                  label=""
                  buttonContainerStyle={{
                    borderRadius: 8,
                    width: 40,
                    alignItems: 'center',
                    height: 40,
                    justifyContent: 'center',
                  }}
                  startIcon={
                    <IconAwes
                      name="directions"
                      size={30}
                      color={COLORS.white}
                    />
                  }
                />
              </View> */}

              <View
                style={{
                  paddingVertical: SIZES.base,
                  justifyContent: 'flex-start',
                  width: '100%',
                  paddingHorizontal: SIZES.padding,
                }}>
                <Text
                  style={{
                    color: COLORS.darkGray1,
                    ...FONTS.body4,
                    fontSize: 14,
                    marginBottom: 5,
                  }}>
                  {Lang.t('opening_time')}:{'  '} {displayedPlace.openTime} -{' '}
                  {displayedPlace.closeTime}
                </Text>
                <Text
                  style={{
                    color: COLORS.darkGray1,
                    ...FONTS.body4,
                    fontSize: 14,
                    marginBottom: 5,
                  }}>
                  {Lang.t('price')}:{'  '}
                  {displayedPlace.price?.start >= 1000
                    ? displayedPlace.price.start + 'VND'
                    : 'Free'}{' '}
                  -{' '}
                  {displayedPlace.price?.end >= 1000
                    ? displayedPlace.price.end + 'VND'
                    : ''}{' '}
                </Text>
                <Text
                  style={{
                    color: COLORS.darkGray1,
                    ...FONTS.body4,
                    fontSize: 14,
                  }}>
                  {Lang.t('opening_day')}:{'  '} {Lang.t('everyday')}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  marginVertical: SIZES.radius,
                }}>
                <TextButton
                  onPress={() => {
                    setShowPlaceModal(false);
                    // showAlan(false);
                    navigation.navigate('DetailPlace', {item: displayedPlace});
                  }}
                  labelStyle={{
                    ...FONTS.h4,
                    fontSize: 14,
                  }}
                  label={Lang.t('view_more')}
                  buttonContainerStyle={{
                    borderRadius: 8,
                    width: 150,
                  }}
                />
                <TextButton
                  onPress={() =>
                    setPlaceGuide({t: true, place: displayedPlace})
                  }
                  labelStyle={{
                    ...FONTS.h4,
                    fontSize: 14,
                  }}
                  label={placeGuide.isLoading ? 'Loading...' : `Alan's guide`}
                  buttonContainerStyle={{
                    borderRadius: 8,
                    width: 150,
                    height: 50,
                  }}
                />
              </View>
            </View>
          }
        />
      )}
    </View>
  );
};

export const renderAnnotations = (
  lattitude = 10.8599583650001,
  longtitude = 106.773641966,
  styleDisplay = 1,
  place,
  setDisplayedPlace,
  setShowPlaceModal,
) => {
  let keyRandom = (Math.random() + 1).toString(36).substring(2);

  return (
    <MapboxGL.PointAnnotation
      key={keyRandom}
      id={keyRandom}
      // id="pointAnnotation"
      coordinate={[longtitude, lattitude]}
      selected={true}
      anchor={{x: 0.5, y: 0.5}}
      onSelected={e => {
        console.log('Hello');
        setDisplayedPlace(place);
        setShowPlaceModal(true);
      }}>
      {styleDisplay === styleIcon && (
        <Icon2
          name={icons.locationFill}
          size={45}
          color={COLORS.red}
          style={{paddingBottom: 50}}
        />
      )}
      {styleDisplay === styleAnnotation && (
        <PlaceCard1
          place={place}
          setDisplayedPlace={setDisplayedPlace}
          setShowPlaceModal={setShowPlaceModal}
        />
      )}

      {/* <MapboxGL.Callout title="This is a sample" /> */}
    </MapboxGL.PointAnnotation>
  );
};

const PlaceCard1 = ({place}) => {
  return (
    <TouchableOpacity
      style={{
        height: 95,
        width: 80,
        borderRadius: 5,
        alignItems: 'center',
        backgroundColor: COLORS.white,
        elevation: 5,
      }}>
      <Image
        style={{
          height: 50,
          width: 76,
          borderRadius: 5,
          resizeMode: 'cover',
          marginTop: 2,
        }}
        source={{uri: place.images[0]}}
      />
      <Text
        numberOfLines={2}
        style={{
          paddingHorizontal: 2,
          color: COLORS.primary,
          ...FONTS.body6,
          lineHeight: 9,
          fontSize: 7,
          textAlign: 'center',
        }}>
        {place.name}
      </Text>
      <RatingCustom
        imageSize={10}
        value={place.rateVoting}
        tintColor={COLORS.white}
      />
      <Text
        style={{
          paddingHorizontal: 2,
          color: COLORS.blue,
          ...FONTS.body6,
          lineHeight: 7,
          fontSize: 6,
          textAlign: 'center',
        }}>
        ({place.rateVoting ? Math.round(place.rateVoting * 10) / 10 : 1})
      </Text>
      {place.popular && (
        <Image
          style={{
            opacity: 0.7,
            position: 'absolute',
            top: 5,
            left: 5,
            height: 20,
            width: 20,
            resizeMode: 'cover',
          }}
          source={images.logo}
        />
      )}
    </TouchableOpacity>
  );
};

export default Map;
