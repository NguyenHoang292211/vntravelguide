export const mockupPlaceInfo = {
  image: {
    uri: 'https://i1.wp.com/vietnaminsider.vn/wp-content/uploads/2019/07/9D0B3F0C-1BAE-4C66-80F3-5E8E481254B0-e1563020575506.jpeg',
  },
  name: 'An Nhon tay',
  address: '1424/3 ap Cho Cu Xa An Nhon tay',
  id: 'system2321',
  rating: 4.7,
};
export const mockupDirectInfo = {
  duration: 1532.758,
  distance: 16565.463,
};
export const mockupRecentSearch = [
  {
    id: 1,
    name: 'Ha Noi Town',
    province: 'Honoi City',
  },
  {
    id: 2,
    name: 'Chua mot cot',
    province: 'Hanoi City',
  },
  {
    id: 3,
    name: 'Kinh thanh Hue',
    province: 'Hue City',
  },
];

export const mockupCategories = [
  {
    id: 1,
    name: 'Food',
    color:"#4c4c"
  },
  {
    id: 2,
    name: 'Drinking',
    color:"#ffff"
  },
  {
    id: 3,
    name: 'Street Food',
    color:"#5555"
  },
  {
    id: 4,
    name: 'Coffee',
    color:"#3333"
  },
  {
    id: 5,
    name: 'Bubble milk tea',
    color:"#2222"
  },
];



export const resentSearch = [
  {
    id: 'aaa',
    name: 'Dai hoc Su pham',
    address: 'So 1 Vo Van Ngan',
  },
  {
    id: 'bbb',
    name: 'KTX KHTN',
    address: '12A, Cu Long B, Binh Duong',
  },
];

export const mockupPlacesByCategory = [
  {
    category: 2,
    longitude: 106.69577129,
    latitude: 10.782633133,
  },
  {
    category: 1,
    longitude: 106.695897129,
    latitude: 10.79,
  },
  {
    category: 1,
    longitude: 106.695897929,
    latitude: 10.799,
  },
  {
    category: 3,
    longitude: 106.695899329,
    latitude: 10.78263313920,
  },
  {
    category: 1,
    longitude: 106.67,
    latitude: 10.7826331330441,
  },
];
