import React from 'react';
import {View, Text} from 'react-native';
import {COLORS, SIZES, FONTS} from '../../constants';

const Header = ({
  title,
  containerStyle,
  titleStyle,
  rightComponent,
  leftComponent,
}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        ...containerStyle,
      }}>
      {leftComponent}
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            textAlign: 'center',
            ...FONTS.body3,
            textTransform: 'capitalize',
            ...titleStyle,
          }}>
          {title}
        </Text>
      </View>
      {rightComponent}
    </View>
  );
};

export default Header;
