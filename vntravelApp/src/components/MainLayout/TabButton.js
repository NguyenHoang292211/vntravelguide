import React from 'react';
import {TouchableWithoutFeedback, Image, Text} from 'react-native';
import Animated from 'react-native-reanimated';
import {SIZES, FONTS, COLORS} from '../../constants';
import Icon from 'react-native-vector-icons/AntDesign';
import Lang from '../../language';
const TabButton = ({
  label,
  icon,
  isFocused,
  onPress,
  innerContainerStyle,
  outerContainerStyle,
}) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <Animated.View
        style={[
          {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          },
          outerContainerStyle,
        ]}>
        <Animated.View
          style={[
            {
              flexDirection: 'row',
              width: '100%',
              height: 62,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 35,
            },
            innerContainerStyle,
          ]}>
          <Icon
            name={icon}
            size={25}
            color={isFocused ? COLORS.white : COLORS.primary}
          />
          {isFocused && (
            <Text
              numberOfLines={1}
              style={{
                marginLeft: SIZES.base,
                color: COLORS.white,
                ...FONTS.h4,
              }}>
              {Lang.t(label)}

              {/* {label} */}
            </Text>
          )}
        </Animated.View>
      </Animated.View>
    </TouchableWithoutFeedback>
  );
};

export default TabButton;
