import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import {Rating, AirbnbRating} from 'react-native-elements';
import {COLORS, FONTS, SIZES} from '../constants';

const RatingCustom = ({
  value = 3.5,
  imageSize = 25,
  readonly = true,
  showRating = false,
  tintColor = COLORS.darkGray1,
  ratingTextColor = COLORS.white2,
  style,
  fractions = 2,
  updateData = null,
}) => {
  return (
    <View>
      {/* <AirbnbRating
        defaultRating={value}
        size={22}
        selectedColor={COLORS.orange}
        isDisabled={isDisabled}
      /> */}
      <Rating
        onFinishRating={rate => {
          updateData({rate: rate});
        }}
        type="custom"
        readonly={readonly}
        ratingColor={COLORS.orange}
        ratingCount={5}
        imageSize={imageSize}
        ratingBackgroundColor={COLORS.gray3}
        ratingTextColor={ratingTextColor}
        tintColor={tintColor}
        showRating={showRating}
        fractions={fractions}
        startingValue={value}
        style={{
          backgroundColor: COLORS.transparent,
          ...FONTS.body3,
          fontSize: 14,
          flexDirection: 'row-reverse',
          ...style,
        }}
      />
    </View>
  );
};

export default RatingCustom;
