import React, {useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Animated,
  Easing,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient'; // import LinearGradient
import HomeScreen from '../screens/HomeScreen';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon1 from 'react-native-vector-icons/AntDesign';

//Navigation
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import PlanScreen from '../screens/PlanScreen';
import AccountNavigator from '../navigations/AccountNavigator';

import CreatePopup from './CreatePopup';

import SearchNavigator from '../navigations/SearchNavigator';
import SearchScreen from '../screens/SearchScreen';

const Tab = createBottomTabNavigator();

const BottomNavigation = () => {
  const widthAnimation = useRef(new Animated.Value(0)).current;
  const totalWidth = Dimensions.get('window').width;
  const animatedTab = () => {
    return (
      <Animated.View
        style={[
          {
            height: 3,
            width: 5,
            backgroundColor: '#56BDC6',
            position: 'absolute',
            top: -3,
            justifyContent: 'center',
          },
          {width: widthAnimation},
        ]}></Animated.View>
    );
  };

  const appearingAnimation = () => {
    Animated.timing(widthAnimation, {
      toValue: 40,
      duration: 1000,
      useNativeDriver: false,
      easing: Easing.ease,
    }).start();
  };

  const disappearAnimation = () => {
    Animated.timing(widthAnimation, {
      toValue: 1,
      duration: 1000,
      easing: Easing.ease,
      useNativeDriver: false,
    }).start();
  };

  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName="home"
        screenOptions={({route}) => ({
          headerShown: false,
          tabBarShowLabel: false,

          style: {
            backgroundColor: '#fff',
            position: 'absolute',
            shadowColor: '#000',
            shadowOpacity: 0.06,
            shadowOffset: {
              width: 10,
              height: 10,
            },
          },
          tabBarIcon: ({focused, color, size}) => {
            let iconName;

            switch (route.name) {
              case 'home':
                iconName = 'home';

                break;
              case 'search':
                iconName = 'search-outline';

                break;

              case 'actionButton':
                return (
                  <LinearGradient
                    colors={['#14557b', '#7fcec5']}
                    start={{
                      x: 0.1,
                      y: 0,
                    }}
                    end={{
                      x: 1,
                      y: 1,
                    }}
                    style={styles.actionBtn}>
                    <Icon1 name="plus" size={32} color="#fff" />
                  </LinearGradient>
                );
              case 'plan':
                iconName = 'calendar-sharp';

                break;
              case 'profile':
                iconName = 'person-circle-outline';

                break;
            }
            return (
              <View style={styles.tabIcon}>
                {focused && (
                  <View
                    style={{
                      height: 4,
                      width: 4,
                      borderRadius: 2,
                      backgroundColor: '#56BDC6',
                    }}
                  />
                )}
                <Icon
                  name={iconName}
                  size={focused ? 25 : 22}
                  color={focused ? '#2D5F73' : '#B4CAC7'}
                />
                {focused && <Text style={styles.tabText}>{route.name}</Text>}
              </View>
            );
          },
        })}>
        <Tab.Screen name="home" component={HomeScreen} />
        <Tab.Screen name="search" component={SearchNavigator} />
        <Tab.Screen name="actionButton" component={CreatePopup} />
        <Tab.Screen name="plan" component={PlanScreen} />
        <Tab.Screen name="profile" component={AccountNavigator} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  tabIcon: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabText: {
    fontSize: 10,
    fontWeight: '500',
    letterSpacing: 1,
    color: '#2D5F73',
    textTransform: 'capitalize',
  },
  actionBtn: {
    height: 70,
    width: 70,
    borderRadius: 35,
    backgroundColor: '#2D5F73',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 40,
    elevation: 7,
  },
  activeTab: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  activeTabInner: {
    width: 48,
    height: 48,
    backgroundColor: '#52a7c1',
    opacity: 0.3,
    borderRadius: 24,
  },
});

export default BottomNavigation;
