import React from 'react';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import {COLORS, SIZES} from '../constants';
const Skeleton = ({containerStyle, children, layout}) => {
  return (
    <SkeletonContent
      containerStyle={{
        width: 250,
        ...containerStyle,
      }}
      animationDirection="diagonalDownRight"
      boneColor="#ededed"
      animationType="shiver"
      highlightColor={COLORS.lightGray}
      isLoading={true}
      layout={layout}>
      {children}
    </SkeletonContent>
  );
};

export default Skeleton;
