import MultiSlider from '@ptomasroos/react-native-multi-slider';
import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {COLORS, SIZES, FONTS} from '../constants';

const TwoPointSlider = ({
  values,
  min,
  max,
  prefix,
  postfix,
  onValuesChange,
}) => {
  return (
    <MultiSlider
      allowOverlap
      customLabel="Distance"
      values={values}
      sliderLength={SIZES.width - SIZES.padding * 2 - 35}
      min={min}
      max={max}
      step={1}
      markerOffsetY={20}
      selectedStyle={{
        backgroundColor: COLORS.primary,
      }}
      trackStyle={{
        height: 10,
        borderRadius: 10,
        backgroundColor: COLORS.lightGray1,
      }}
      minMarkerOverlapDistance={100}
      customMarker={e => {
        return (
          <View
            style={{
              height: 80,
              width: 80,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                height: 25,
                width: 25,
                borderRadius: 17,
                borderWidth: 4,
                borderColor: COLORS.white,
                backgroundColor: COLORS.primary,
                ...styles.shadow,
              }}
            />
            <Text
              style={{
                marginTop: 5,
                color: COLORS.darkGray,
                ...FONTS.body4,
                paddingHorizontal: 15,
              }}>
              {prefix}
              {e.currentValue}
              {postfix}
            </Text>
          </View>
        );
      }}
      onValuesChange={values => onValuesChange(values)}
    />
  );
};

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowRadius: 1,
    shadowOpacity: 0.1,
  },
});

export default TwoPointSlider;
