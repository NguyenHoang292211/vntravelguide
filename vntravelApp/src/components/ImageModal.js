import React from 'react';
import {Modal, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {COLORS, icons} from '../constants';
import Carousel from './home/Carousel/Carousel';

const ImageModal = ({
  visible,
  images,
  setShowImageModal,
  selectedIndex = 0,
}) => {
  const renderCloseButton = () => {
    return (
      <TouchableOpacity
        style={{
          position: 'absolute',
          top: 10,
          left: 10,
          height: 30,
          width: 30,
          zIndex: 200,
        }}
        onPress={() => setShowImageModal(false)}>
        <Icon name={icons.close} size={30} color={COLORS.white} />
      </TouchableOpacity>
    );
  };
  return (
    <Modal
      animationType="slide"
      presentationStyle="fullScreen"
      visible={visible}>
      {renderCloseButton()}

      <Carousel
        selectedIndex={selectedIndex}
        images={images}
        showImage={true}
      />
    </Modal>
  );
};

export default ImageModal;
