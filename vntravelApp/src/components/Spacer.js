import React from 'react';
import {View, Text} from 'react-native';
import {SIZES} from '../constants';

const Spacer = ({children}) => {
  return <View style={{margin: SIZES.base * 2}}>{children}</View>;
};

export default Spacer;                  
