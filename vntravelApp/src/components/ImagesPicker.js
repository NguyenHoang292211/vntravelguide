import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {icons, images, COLORS, FONTS, SIZES} from '../constants';
import ImagePicker from 'react-native-image-crop-picker';
import IconAwes from 'react-native-vector-icons/FontAwesome';
import Lang from '../language';

const ImagesPicker = ({
  iconSize = 40,
  icon = icons.camera,
  textStyle,
  text = Lang.t('add_photo'),
  multipleImage = true,
  setPhotos,
  photos,
  iconColor = COLORS.black,
}) => {
  const pickMultiple = () => {
    ImagePicker.openPicker({
      width: 500,
      height: 600,
      multiple: multipleImage,
      sortOrder: 'desc',
      forceJpg: true,
      mediaType: 'photo',
      includeBase64: true,
    })
      .then(images => {
        if (multipleImage) {
          let photoLength = photos ? photos.length : 0;
          let selectedImage = images;
          if (images.length + photoLength > 5)
            selectedImage = images.slice(0, 5 - photoLength);
          let filterPhoto = selectedImage.map(i => {
            return {
              uri: i.path,
              width: i.width,
              height: i.height,
              mime: i.mime,
              data: i.data,
            };
          });
          if (photos) setPhotos([...photos, ...filterPhoto]);
          else setPhotos(filterPhoto);
        } else {
          setPhotos({
            uri: images.path,
            width: images.width,
            height: images.height,
            mime: images.mime,
            data: images.data,
          });
        }
      })
      .catch(e => {
        alert(e);
        console.log(e);
      });
  };

  return (
    <View>
      <TouchableOpacity
        onPress={pickMultiple}
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: SIZES.padding,
        }}>
        <IconAwes name={icon} size={iconSize} color={iconColor} />
        <Text
          style={{
            color: COLORS.gray3,
            ...FONTS.body3,
            ...textStyle,
          }}>
          {text}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default ImagesPicker;
