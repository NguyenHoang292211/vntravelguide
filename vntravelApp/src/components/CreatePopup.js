import React from 'react';
import {Text, View, StyleSheet} from 'react-native';

const CreatePopup = () => {
  return (
    <View style={styles}>
      <View
        style={{
          backgroundColor: '#DDD',
          position: 'absolute',
          width: '100%',
        }}>
        <Text>Add popup</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  popupContainer: {
    backgroundColor: '#fff',
    opacity: 0.5,
    position: 'absolute',
    height: '100%',
    width: '100%',
    flex: 1,
  },
});

export default CreatePopup;
