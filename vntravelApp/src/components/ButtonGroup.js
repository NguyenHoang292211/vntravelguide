import React, {useState} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {Text} from 'react-native-elements';
import {Button} from 'react-native-elements/dist/buttons/Button';
import {FlatList, ScrollView} from 'react-native-gesture-handler';
import {COLORS, FONTS, SIZES} from '../constants';

const ButtonGroup = ({
  buttons,
  selectGroup,
  selectedId,
  placesOfCategories,
}) => {
  const handleClick = item => {
    selectGroup(item);
  };

  return (
    // <View style={styles.groupContainer}>
    <ScrollView
      style={styles.groupContainer}
      horizontal={true}
      showsHorizontalScrollIndicator={false}>
      {buttons.map((item, index) => {
        return (
          <TouchableOpacity
            activeOpacity={0.7}
            key={index}
            onPress={() => handleClick(item)}
            style={[
              item.id === selectedId ? styles.btnActive : styles.btn,
              item.id === selectedId ? {backgroundColor: item.color} : '',
            ]}>
            <Text
              style={[
                item.id === selectedId ? styles.textActive : styles.text,
              ]}>
              {item.name}
              {item.id === selectedId ? `(${placesOfCategories})` : ''}
            </Text>
          </TouchableOpacity>
        );
      })}
    </ScrollView>
    // </View>
  );
};
const styles = StyleSheet.create({
  groupContainer: {
    paddingLeft: 10,
    height: 45,
    paddingRight: 25,
  },
  btn: {
    marginHorizontal: 3,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 7,
    backgroundColor: COLORS.white2,
    paddingHorizontal: 10,

    borderRadius: SIZES.base,
    elevation: 3,
  },
  btnActive: {
    marginLeft: 5,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: SIZES.base,
    padding: 7,
    paddingHorizontal: 10,
    marginRight: 5,
    backgroundColor: COLORS.lightBlue1,
    elevation: 5,
  },
  text: {
    color: COLORS.darkGray,
    ...FONTS.h4,
    fontSize: 14,
  },
  textActive: {
    color: COLORS.white2,
    ...FONTS.h4,
    fontSize: 14,
  },
});
export default ButtonGroup;
