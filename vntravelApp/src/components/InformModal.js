import React from 'react';
import {COLORS, FONTS, images, SIZES} from '../constants';
import {View, Text, TouchableWithoutFeedback, Modal, Image} from 'react-native';
import LottieView from 'lottie-react-native';
import * as Animatable from 'react-native-animatable';

const InformModal = ({
  isVisible,
  setIsVisible,
  source,
  title,
  description,
  actionComponent,
  titleStyle,
  descriptionStyle,
  imageStyle,
  containerStyle,
}) => {
  return (
    <Modal
      transparent={true}
      animationType="none"
      visible={isVisible}
      presentationStyle="overFullScreen">
      <TouchableWithoutFeedback
        onPress={() => {
          if (setIsVisible) setIsVisible(false);
        }}>
        <Animatable.View
          animation="fadeIn"
          easing="ease-in-back"
          // duration={300}
          direction="normal"
          style={{
            backgroundColor: COLORS.transparentBlack7,
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
          }}>
          <Animatable.View
            animation={isVisible ? 'zoomIn' : 'zoomOut'}
            easing="ease-in-circ"
            duration={700}
            style={{
              width: SIZES.width * 0.9,
              borderRadius: 20,
              backgroundColor: COLORS.white3,
              justifyContent: 'flex-start',
              alignItems: 'center',
              paddingVertical: 5,
              paddingBottom: 20,
              ...containerStyle,
            }}>
            {source && (
              <LottieView
                speed={0.7}
                source={source}
                resizeMode="contain"
                autoPlay
                loop
                style={{
                  height: 170,
                  width: 200,
                  ...imageStyle,
                }}
              />
            )}

            <View
              style={{
                justifyContent: 'center',
                padding: 5,
              }}>
              <Text
                style={{
                  ...FONTS.special,
                  color: COLORS.black,
                  fontSize: 42,
                  textAlign: 'center',
                  ...titleStyle,
                }}>
                {title}
              </Text>
              <Text
                style={{
                  ...FONTS.body3,
                  color: COLORS.darkGray,
                  textAlign: 'center',
                  marginTop: 8,
                  ...descriptionStyle,
                }}>
                {description}
              </Text>

              {actionComponent}
            </View>
          </Animatable.View>
        </Animatable.View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

export default InformModal;
