import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import {COLORS, FONTS} from '../constants';
const TextButton = ({
  disabled,
  label,
  labelStyle,
  onPress,
  buttonContainerStyle,
  endIcon,
  startIcon,
  backgroundColor = COLORS.primary,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      disabled={disabled}
      style={{
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: backgroundColor,
        borderRadius: 50,
        ...buttonContainerStyle,
      }}
      onPress={onPress}>
      {startIcon}
      <Text
        style={{
          color: COLORS.white,
          ...FONTS.h3,
          ...labelStyle,
          marginRight: 2,
        }}>
        {label}
      </Text>
      {endIcon}
    </TouchableOpacity>
  );
};

export default TextButton;
