import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  Image,
} from 'react-native';
import {SIZES, COLORS, FONTS, icons, images} from '../../constants';
import moment from 'moment';
import AntIcon from 'react-native-vector-icons/AntDesign';
import MaterIcon from 'react-native-vector-icons/MaterialCommunityIcons';

const TripItem = ({item, navigation}) => {
  const infoTrip = item.item;
  const friendAvatar = (item, key, marginL = -10) => {
    return (
      <View
        key={key}
        style={{
          height: 30,
          width: 30,
          elevation: 5,
          backgroundColor: COLORS.white3,
          borderRadius: 30,
          marginLeft: marginL,
        }}>
        <Image
          resizeMode="cover"
          style={{
            height: 30,
            width: 30,
            borderRadius: 30,
            borderColor: COLORS.white,
            borderWidth: 1,
          }}
          source={{uri: item.image}}
        />
      </View>
    );
  };

  return (
    <View
      style={{
        backgroundColor: '#fff',
        borderRadius: SIZES.radius + 5,
        elevation: 4,
        margin: 2,
        marginRight: 6,
      }}>
      <TouchableOpacity
        style={{
          elevation: 5,
          backgroundColor: '#fff',
          borderRadius: SIZES.radius + 5,
          marginVertical: 2,
          marginHorizontal: 2,
        }}
        activeOpacity={0.7}
        // onPress={() => {
        //   navigation.navigate('DetailPlan', {item: item});
        // }}
        >
        <ImageBackground
          imageStyle={{borderRadius: SIZES.radius + 5}}
          source={{uri: infoTrip.photoUrl}}
          fadeDuration={400}
          resizeMode="cover"
          style={{
            height: SIZES.height * 0.35,
            width: SIZES.width * 0.93,
            justifyContent: 'flex-end',
            padding: 10,
            paddingBottom: 20,
          }}>
          <View
            style={{
              backgroundColor: COLORS.black,
              position: 'absolute',
              height: SIZES.height * 0.35,
              width: SIZES.width * 0.93,
              borderRadius: SIZES.radius + 5,
              top: 0,
              left: 0,
              opacity: 0.3,
            }}
          />
          <View>
            <Text
              style={{
                color: COLORS.white,
                ...FONTS.h2,
                fontSize: 21,
              }}>
              {infoTrip?.name}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 10,
              }}>
              <AntIcon name={icons.calendar} size={20} color={COLORS.white2} />
              <Text
                style={{
                  color: COLORS.white,
                  ...FONTS.body3,
                  fontSize: 15,
                  marginLeft: 3,
                }}>
                {moment(infoTrip.start, 'DD-MM-YYYY').utc(true).format('ll')} -{' '}
              </Text>
              <Text
                style={{
                  color: COLORS.white,
                  ...FONTS.body3,
                  fontSize: 15,
                }}>
                {moment(infoTrip.end, 'DD-MM-YYYY').utc(true).format('ll')}{' '}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 10,
              }}>
              <MaterIcon name={icons.route} size={20} color={COLORS.white} />
              <Text
                style={{
                  color: COLORS.white,
                  ...FONTS.body3,
                  fontSize: 14,
                }}>
                {item.item?.sections?.length}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'flex-end',
              }}>
              {/* {friendAvatar(null, 8, 0)} */}
              {item.item?.members?.map((e, i) =>
                friendAvatar((item = e), (key = i)),
              )}
            </View>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    </View>
  );
};

export default TripItem;
