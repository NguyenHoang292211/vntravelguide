import React, {useCallback, useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  Platform,
  ToastAndroid,
  DeviceEventEmitter,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/AntDesign';
import {COLORS, SIZES} from '../constants/themes';
import {SHOW_TOAST_MESSAGE} from '../constants/constants';
const colors = {
  info: COLORS.orange,
  success: '#28DF99',
  danger: COLORS.red,
  normal:COLORS.transparentBlack1
};
const icons = {
  info: 'infocirlce',
  success: 'heart',
  danger: 'closecircle',
  normal:'heart'
};

const Toast = () => {
  const [messageType, setMessageType] = useState(null);
  const timeOutRef = useRef(null);
  const animatedOpacity = useSharedValue(0);
  const animatedStyle = useAnimatedStyle(() => {
    return {
      opacity: animatedOpacity.value,
    };
  }, []);
  const [timeOutDuration, setTimeOutDuration] = useState(5000);

  const [message, setMessage] = useState(null);

  const onNewToast = data => {
    if (Platform.OS === 'android' && data.useNativeToast) {
      return ToastAndroid.show(data.message, ToastAndroid.LONG);
    }
    if (data.duration) {
      setTimeOutDuration(data.duration);
    }
    setMessage(data.message);
    setMessageType(data.type);
  };
  const closeToast = useCallback(() => {
    setMessage(null);
    setTimeOutDuration(5000);
    animatedOpacity.value = withTiming(0);
    clearInterval(timeOutRef.current);
  }, [animatedOpacity]);
  useEffect(() => {
    if (message) {
      timeOutRef.current = setInterval(() => {
        if (timeOutDuration === 0) {
          closeToast();
        } else {
          setTimeOutDuration(prev => prev - 1000);
        }
      }, 1000);
    }
    return () => {
      clearInterval(timeOutRef.current);
    };
  }, [closeToast, message, timeOutDuration]);

  useEffect(() => {
    if (message) {
      animatedOpacity.value = withTiming(1, {duration: 1000});
    }
  }, [message, animatedOpacity]);

  useEffect(() => {
    DeviceEventEmitter.addListener(SHOW_TOAST_MESSAGE, onNewToast);
    return () => {
      DeviceEventEmitter.removeAllListeners();
    };
  }, []);
  if (!message) {
    return null;
  }

  return (
    <Animated.View
      style={[
        {
          position: 'absolute',
          bottom: '9%',
          left: '4%',
          right: '4%',
          backgroundColor: colors[messageType],
          zIndex: 1,
          elevation: 1,
          borderRadius: 20,
          borderColor: colors[messageType],
          elevation: 2,
          elevationShadowColor: colors[messageType],
        },
        animatedStyle,
      ]}>
      <TouchableOpacity onPress={closeToast}>
        <Text
          style={{
            padding: 8,
            color: COLORS.white,
            fontSize: 16,
            textAlign: 'center',
          }}>
          <Icon
            style={20}
            name={icons[messageType]}
            style={{
              borderRadius: 50,
              width: 30,
              height: 30,
              justifyContent: 'center',
              textAlign: 'center',
              alignContent: 'center',
              marginRight: 12,
              color: COLORS.white,
              paddingRight: 20,
            }}
          />
          &#32; {message}
        </Text>
      </TouchableOpacity>
    </Animated.View>
  );
};

export default Toast;
