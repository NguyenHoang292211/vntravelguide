import React from 'react';
import {Image, Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {COLORS, FONTS, icons, SIZES} from '../../constants';

const CategoryItems = ({item, onPress}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={styles.container}
      onPress={onPress}>
      <View
        style={{
          paddingHorizontal: 7,
          paddingVertical: 10,
          borderRadius: 35,
          backgroundColor: COLORS.white,
          borderWidth: 1,
          borderColor: item.color,
          flexDirection: 'row',
          justifyContent: 'space-around',
          alignItems: 'center',
        }}>
        {/* <Icon
          name={icons.calendar}
          style={{
            padding: 7,
            backgroundColor: item.color,
            borderRadius: 20,
            elevation: 3,
          }}
          color={COLORS.white}
          size={25}
        /> */}
        <Text
          style={{
            color: item.color,
            ...FONTS.body3,
            fontSize: 15,
            paddingHorizontal: 5,
          }}>
          {item.name}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 5,
  },

  item: {
    paddingHorizontal: 5,
    paddingVertical: 6,
    borderRadius: 35,
    backgroundColor: COLORS.white,
    borderWidth: 1,
    borderColor: COLORS.lightBlue,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
});

export default CategoryItems;
