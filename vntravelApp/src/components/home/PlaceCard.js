import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ImageBackground,
} from 'react-native';
import Icon1 from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/Ionicons';
import {COLORS, FONTS, icons, SIZES} from '../../constants';
import {UserCtrl} from '../../controller';
import {updateUserData} from '../../stores/authSlice';
import {useDispatch, useSelector} from 'react-redux';

const PlaceCard = ({
  item,
  onPress,
  containerStyle,
  navigation,
  updateFavoritePlace,
}) => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.auth.userData);
  const isAuthorized = useSelector(state => state.auth.isAuthorized);
  const token = useSelector(state => state.auth.token);

  const saveToRecentSearch = async () => {
    if (isAuthorized === true && user != null)
      await UserCtrl.saveRecentSearch(item.id ? item.id : item._id, token)
        .then(res => {
          if (res.data.success) {
            dispatch(updateUserData(res.data));
          }
        })
        .catch(err => {
          console.log(err);
        });
  };
  return (
    <View style={{...styles.cardContainer}}>
      <TouchableOpacity
        activeOpacity={0.6}
        onPress={() => {
          navigation.push('Main', {
            screen: 'DetailPlace',
            params: {item: item},
          });
          saveToRecentSearch();
        }}>
        <ImageBackground
          source={{uri: item.images[0]}}
          style={{
            width: 220,
            height: 300,
            ...containerStyle,
          }}
          imageStyle={{borderRadius: SIZES.radius + 10}}>
          <View
            style={{
              width: 220,
              height: 300,
              position: 'absolute',
              top: 0,
              left: 0,
              backgroundColor: COLORS.black,
              opacity: 0.3,
              borderRadius: SIZES.radius + 10,
            }}></View>

          <View
            style={{
              position: 'absolute',
              top: 10,
              left: 10,
              backgroundColor: COLORS.white,
              padding: 1,
              justifyContent: 'center',
              alignItems: 'center',
              paddingHorizontal: 5,
              borderRadius: SIZES.radius * 0.6,
              flexDirection: 'row',
            }}>
            <Icon1
              style={styles.starIcon}
              name="star"
              size={15}
              color={COLORS.orange}
            />
            <Text style={{color: COLORS.orange, ...FONTS.body4}}>
              {item.rateVoting ? Math.round(item.rateVoting * 10) / 10 : 1}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => updateFavoritePlace(item.id)}
            activeOpacity={0.7}
            style={{
              position: 'absolute',
              top: 10,
              right: 10,
              backgroundColor: 'rgba(214,214,214,0.7)',

              padding: 5,
              borderRadius: 20,
            }}>
            <Icon1
              name={item.isFavorite ? icons.solidHeart : icons.outlineHeart}
              size={20}
              color={COLORS.white}
            />
          </TouchableOpacity>
          <View style={styles.placeInfo}>
            <Text numberOfLines={3} style={styles.placeName}>
              {item.name}
            </Text>

            <View style={styles.placeAddress}>
              <Icon name="location-sharp" size={22} color={COLORS.lightGray1} />
              <Text numberOfLines={2} style={styles.addressText}>
                {item.address}
              </Text>
            </View>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    marginLeft: 10,
    marginBottom: 15,
    backgroundColor: '#fff',
    borderRadius: SIZES.radius + 10,
    elevation: 4,
  },

  imageOverlay: {
    width: 220,
    height: 300,
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: COLORS.black,
    opacity: 0.6,
    borderRadius: SIZES.radius + 10,
  },
  boxWithShadow: {
    shadowColor: '#171717',
    elevation: 7,
  },

  placeInfo: {
    flex: 1,
    padding: 5,
    justifyContent: 'flex-end',
    flexDirection: 'column',
    display: 'flex',
  },
  placeName: {
    color: COLORS.white,
    paddingHorizontal: 3,
    marginBottom: 5,
    ...FONTS.h3,
  },
  placeRate: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  placeAddress: {
    flexDirection: 'row',
    paddingVertical: 5,
    paddingRight: 15,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  addressText: {
    color: COLORS.white,
    ...FONTS.body4,
    marginLeft: 5,
  },
  starIcon: {
    marginRight: 3,
  },
});

export default PlaceCard;
