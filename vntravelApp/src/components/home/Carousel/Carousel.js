import React, {useRef, useEffect, useState} from 'react';
import {
  Dimensions,
  Animated,
  View,
  Image,
  StyleSheet,
  FlatList,
} from 'react-native';
// import useInterval from './useInterval';
import CarouselItem from './CarouselItem';
import ImageCarouselItem from './ImageCarouselItem';
import Paginator from './Paginator';
const MAX_WIDTH = Dimensions.get('screen').width;

const Carousel = ({
  listItem,
  showImage = false,
  images,
  selectedIndex = 0,
  navigation,
}) => {
  const [currentIndex, setCurrentIndex] = useState(0);

  const scrollX = useRef(new Animated.Value(0)).current;
  const slideRef = useRef(null);

  const viewableItemsChanged = useRef(({viewableItems}) => {
    setCurrentIndex(viewableItems[0].index);
  }).current;

  const viewConfig = useRef({viewAreaCoveragePercentThreshold: 50});

  const viewPlaceNavigator = (title, loadData = null, type, id = null) => {
    navigation.navigate('PlacesView', {title, loadData, type, id});
  };
  useEffect(() => {
    if (selectedIndex > 0) {
      setCurrentIndex(selectedIndex);
      // slideRef?.current.scrollToIndex({
      //   index: selectedIndex,
      //   animated: true,
      // });
    }
  });

  return (
    <View style={styles.container}>
      <View style={{flex: 1}}>
        <FlatList
          horizontal
          data={showImage ? images : listItem}
          renderItem={({item, index}) => {
            if (showImage)
              return <ImageCarouselItem key={index} image={item} />;
            else
              return (
                <CarouselItem navigation={navigation} key={index} item={item} />
              );
          }}
          showsHorizontalScrollIndicator={false}
          initialScrollIndex={selectedIndex}
          onScrollToIndexFailed={() => {
            slideRef?.current.scrollToIndex({
              index: 0,
              animated: true,
            });
          }}
          pagingEnabled
          key="carousel"
          bounces={false}
          keyExtractor={(item, index) => `carousel-${index}`}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {x: scrollX}}}],
            {
              useNativeDriver: false,
            },
          )}
          onViewableItemsChanged={viewableItemsChanged}
          viewabilityConfig={viewConfig.current}
          scrollEventThrottle={16}
          ref={slideRef}
        />
      </View>
      <View style={{position: 'absolute', bottom: 8, zIndex: 9999999999}}>
        <Paginator
          isImage={showImage}
          data={showImage ? images : listItem}
          scrollX={scrollX}
          setCurrentIndex={setCurrentIndex}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  image: {
    resizeMode: 'cover',
    height: 500,
    width: MAX_WIDTH,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  indicatorContainer: {
    position: 'absolute',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: MAX_WIDTH,
    bottom: 10,
    zIndex: 2,
  },
  indicator: {
    width: 15,
    height: 15,
    borderRadius: 7.5,
    borderColor: 'white',
    borderWidth: 1,
    marginHorizontal: 10,
    marginBottom: 10,
  },
  activeIndicator: {
    backgroundColor: 'white',
  },
});
export default Carousel;
