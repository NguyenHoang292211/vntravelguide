import React from 'react';
import {Image, View} from 'react-native';
import {COLORS, SIZES} from '../../../constants';

const ImageCarouselItem = ({image}) => {
  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLORS.black,
      }}>
      <Image
        source={{uri: image}}
        resizeMode="contain"
        style={{
          height: SIZES.height,
          width: SIZES.width,
          marginTop: 20,
        }}
      />
    </View>
  );
};

export default ImageCarouselItem;
