import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  useWindowDimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {color} from 'react-native-elements/dist/helpers';
import Color from '../../../constants/Color';
import {COLORS, FONTS, SIZES} from '../../../constants';
import {TextButton} from '../..';
import {PlaceCtrl} from '../../../controller';
import {VIEW_SCREEN_TYPE} from '../../../utils/constants';
import Lang from '../../../language';

const CarouselItem = ({item, navigation}) => {
  const {width, height} = useWindowDimensions();
  return (
    <View style={[styles.container, {width}]}>
      <Image
        source={{uri: item.banner}}
        style={[
          styles.image,
          {width, height: height * 0.5, resizeMode: 'cover'},
        ]}
      />
      {/* Overlay */}
      <View
        style={[
          styles.overlay,
          {
            width,
            height: height * 0.5,
            backgroundColor: Color.shadow,
            opacity: 0.5,
          },
        ]}
      />
      <View
        style={[
          styles.overlay,
          {
            width,
            height: height * 0.5,
            paddingHorizontal: 10,
            alignItems: 'center',
          },
        ]}>
        <Text
          style={{
            color: COLORS.white,
            ...FONTS.h1,
            fontSize: 25,
            marginBottom: 15,
          }}>
          {item.title}
        </Text>
        <Text
          style={{
            color: COLORS.lightGray,
            ...FONTS.body4,
            fontSize: 14,
            lineHeight: 18,
            textAlign: 'center',
          }}>
          {item.description}
        </Text>
        <TextButton
          labelStyle={{
            ...FONTS.h4,
            paddingLeft: 4,
          }}
          onPress={() => {
            navigation.navigate('ExploreView', {item: item});
          }}
          label={Lang.t('explore_now')}
          endIcon={
            <Icon
              name="chevron-right"
              size={22}
              color={COLORS.white}
              style={{paddingTop: 5}}
            />
          }
          buttonContainerStyle={{
            height: 50,
            paddingHorizontal: SIZES.base,
            marginTop: SIZES.padding * 2,
          }}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    justifyContent: 'center',
  },
  overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  title: {
    fontSize: 22,
    padding: 5,
    color: Color.white,
  },
  description: {
    fontSize: 13,
    marginTop: 10,
    color: Color.white,
  },
  button: {
    backgroundColor: COLORS.primary,
    paddingHorizontal: 25,
    paddingVertical: 10,
    borderRadius: 30,
    marginTop: 30,
    elevation: 9,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
});

export default CarouselItem;
