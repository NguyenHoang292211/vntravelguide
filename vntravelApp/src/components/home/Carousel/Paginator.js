import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  useWindowDimensions,
  Animated,
  Image,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {COLORS, SIZES} from '../../../constants';
import Color from '../../../constants/Color';
const Paginator = ({isImage = false, data, scrollX, setCurrentIndex}) => {
  const {width} = useWindowDimensions();
  return (
    <View style={styles.container}>
      {data.map((_, i) => {
        const inputRange = [(i - 1) * width, i * width, (i + 1) * width];
        const dotWidth = scrollX.interpolate({
          inputRange,
          outputRange: [10, 25, 10],
          extrapolate: 'clamp',
        });

        const opacity = scrollX.interpolate({
          inputRange,
          outputRange: [0.5, 1, 0.5],
          extrapolate: 'clamp',
        });
        return (
          <Animated.View
            style={
              !isImage
                ? [styles.dot, {width: dotWidth, opacity}]
                : [styles.image, {opacity}]
            }
            key={i.toString()}>
            {isImage && (
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => {
                  setCurrentIndex(i);
                }}>
                <Image
                  source={{uri: data[i]}}
                  style={{
                    height: 50,
                    width: 50,
                    resizeMode: 'cover',
                    borderRadius: SIZES.radius,
                  }}
                />
              </TouchableOpacity>
            )}
          </Animated.View>
        );
      })}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  dot: {
    height: 10,
    borderRadius: 5,
    marginHorizontal: 5,
    backgroundColor: COLORS.lightBlue,
  },
  image: {
    borderWidth: 1,
    borderColor: COLORS.white,
    borderRadius: SIZES.radius,
    marginRight: 5,
  },
});

export default Paginator;
