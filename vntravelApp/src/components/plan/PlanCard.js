import React from 'react';
import {View, Text, TouchableOpacity, ImageBackground} from 'react-native';
import {SIZES, COLORS, FONTS, icons} from '../../constants';
import moment from 'moment';
import AntIcon from 'react-native-vector-icons/AntDesign';
import * as Animatable from 'react-native-animatable';
import {useSelector, useDispatch} from 'react-redux';
import {setCurrentPlan} from '../../stores/planSlice';

const PlanCard = ({item, navigation}) => {
  const dispatch = useDispatch();
  return (
    <Animatable.View
      animation="fadeIn"
      easing="ease"
      direction="alternate"
      duration={900}
      iterationDelay={300}
      style={{
        height: SIZES.height * 0.3,
        width: SIZES.width * 0.98,
        borderRadius: SIZES.radius + 10,
        elevation: 5,
        marginBottom: 8,
      }}>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => {
          navigation.navigate('DetailPlan', {item: item});
          dispatch(setCurrentPlan(item));
        }}>
        <ImageBackground
          imageStyle={{borderRadius: SIZES.radius + 10}}
          source={{uri: item.photoUrl}}
          fadeDuration={400}
          resizeMode="cover"
          style={{
            height: SIZES.height * 0.3,
            width: SIZES.width * 0.98,
            justifyContent: 'flex-end',
            padding: 10,
            paddingBottom: 20,
          }}>
          <View
            style={{
              backgroundColor: COLORS.black,
              position: 'absolute',
              height: SIZES.height * 0.3,
              width: SIZES.width * 0.98,
              borderRadius: SIZES.radius + 10,
              top: 0,
              left: 0,
              opacity: 0.3,
            }}
          />
          <View>
            <Text
              numberOfLines={2}
              style={{
                color: COLORS.white,
                ...FONTS.h2,
                fontSize: 20,
              }}>
              {item.name}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 10,
              }}>
              <AntIcon name={icons.calendar} size={26} color={COLORS.white2} />
              <Text
                style={{
                  color: COLORS.white,
                  ...FONTS.body3,
                  fontSize: 15,
                  marginLeft: 3,
                }}>
                {moment(item.start).utc(true).format('ll')} -{' '}
              </Text>
              <Text
                style={{
                  color: COLORS.white,
                  ...FONTS.body3,
                }}>
                {moment(item.end).utc(true).format('ll')}{' '}
              </Text>
            </View>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    </Animatable.View>
  );
};

export default PlanCard;
