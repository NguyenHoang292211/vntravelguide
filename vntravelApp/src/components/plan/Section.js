import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
} from 'react-native';
import {SIZES, COLORS, FONTS, icons, images} from '../../constants';
import moment from 'moment';
import AntIcon from 'react-native-vector-icons/AntDesign';
import IonIcon from 'react-native-vector-icons/Ionicons';
import AwesIcon from 'react-native-vector-icons/FontAwesome';
import Lang from '../../language';

const Section = ({item, navigation, index, planStartDate}) => {
  const [showNote, setShowNote] = useState(false);
  const [heigthLayout, setHeigthLayout] = useState(0);

  const onLayoutChange = event => {
    // event.preventDefault();
    setHeigthLayout(event.nativeEvent.layout.height);
  };
  return (
    <View
      style={{
        marginVertical: 3,
        flexDirection: 'row',
      }}>
      {/* Timeline */}
      <View
        style={{
          width: SIZES.width * 0.15,
          alignItems: 'center',
        }}>
        <View
          style={{
            height: 40,
            width: 40,
            borderRadius: 20,
            backgroundColor: COLORS.supperLightBlue,
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 10,
          }}>
          <Text
            style={{
              ...FONTS.h4,
              color: COLORS.primary,
            }}>
            {index + 1}
          </Text>
        </View>
        <View
          style={{
            height: 110 * item.places?.length + heigthLayout + 70,
            width: 1,
            backgroundColor: COLORS.supperLightBlue,
          }}
        />
      </View>
      {/* Place list */}
      <View
        style={{
          width: SIZES.width * 0.85,
        }}>
        <Text
          style={{
            color: COLORS.primary,
            ...FONTS.h4,
            lineHeight: 16,
            marginLeft: 3,
          }}>
          {Lang.t('day')}{' '}
          {new Date(item.start).getDate() -
            new Date(planStartDate).getDate() +
            1}
        </Text>
        <Text
          style={{
            color: COLORS.darkGray,
            ...FONTS.body4,
            marginLeft: 3,
          }}>
          {moment(item?.start).utc(true).format('LL')}
        </Text>
        <View
          onLayout={event => onLayoutChange(event)}
          style={{
            paddingHorizontal: SIZES.base,
            backgroundColor: COLORS.lightGray,
            borderRadius: SIZES.base,
            marginRight: 8,
          }}>
          <TouchableOpacity
            onPress={() => setShowNote(!showNote)}
            activeOpacity={0.7}
            style={{
              paddingVertical: SIZES.base,
              marginHorizontal: 5,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <IonIcon name={icons.pen} size={27} color={COLORS.lightBlue1} />
            <Text
              style={{
                color: COLORS.gray,
                ...FONTS.body4,
                fontSize: 15,
                marginLeft: 10,
              }}>
              {Lang.t('note')}
            </Text>
          </TouchableOpacity>
          {showNote && (
            <Text
              style={{
                color: item.note === '' ? COLORS.gray2 : COLORS.darkGray1,
                ...FONTS.body3,
                fontSize: 14,
                marginLeft: 10,
                lineHeight: 18,
                paddingVertical: 5,
                paddingBottom: 10,
              }}>
              {item.note === '' ? Lang.t('no_note') : item.note}
            </Text>
          )}
        </View>
        <View
          style={{
            paddingVertical: SIZES.radius,
          }}>
          {item.places?.map((item, i) => {
            return <PlaceItem navigation={navigation} item={item} key={i} />;
          })}
        </View>
      </View>
    </View>
  );
};

export const PlaceItem = ({item, navigation}) => {
  return (
    <View
      style={{
        backgroundColor: COLORS.white,
        // borderWidth: 1,
        marginVertical: 7,
        marginHorizontal: SIZES.base,
        borderRadius: SIZES.radius,
        elevation: 3,
      }}>
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          // alignItems: 'center',
        }}>
        <View
          style={{
            width: '30%',
            elevation: 5,
          }}>
          <Image
            // source={place ? {uri: place.images[0]} : images.background1}
            source={
              item.place ? {uri: item.place.images[0]} : images.background5
            }
            resizeMode="cover"
            style={{
              height: 125,
              width: 100,
              borderTopLeftRadius: SIZES.radius,
              borderBottomLeftRadius: SIZES.radius,
            }}
          />
        </View>

        <TouchableOpacity
          onPress={() =>
            navigation.navigate('Main', {
              screen: 'DetailPlace',
              params: {item: item.place},
            })
          }
          activeOpacity={0.5}
          style={{
            width: '70%',
            paddingLeft: SIZES.radius,
            paddingRight: 4,
            justifyContent: 'space-between',
            // alignItems: 'flex-start',
            paddingVertical: SIZES.base,
          }}>
          <Text
            numberOfLines={1}
            style={{
              color: COLORS.darkGray1,
              ...FONTS.h3,
              fontSize: 16,
              lineHeight: 20,
              // paddingBottom: SIZES.radius,
            }}>
            {/* {place.name} */}
            {item.place?.name}
          </Text>

          <View
            style={{
              flexDirection: 'row',
              paddingRight: 15,
              justifyContent: 'flex-start',
              alignItems: 'center',
              // marginBottom: SIZES.base,
            }}>
            <Text
              numberOfLines={2}
              style={{
                color: COLORS.gray,
                ...FONTS.body5,
                marginLeft: 2,
                lineHeight: 15,
              }}>
              {item.place?.address}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  paddingRight: 5,
                  alignItems: 'center',
                  marginBottom: 5,
                }}>
                <AntIcon name={icons.clock} size={16} color={COLORS.primary} />
                <Text
                  style={{
                    color: COLORS.darkGray,
                    ...FONTS.body5,
                    fontSize: 11,
                    marginLeft: 2,
                    lineHeight: 15,
                  }}>
                  {moment.utc(item.visitedTime).format('LT')}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  paddingRight: 5,
                  alignItems: 'center',
                }}>
                <AwesIcon
                  name={icons.dollar}
                  size={16}
                  color={COLORS.primary}
                />
                <Text
                  style={{
                    color: COLORS.darkGray,
                    ...FONTS.body5,
                    fontSize: 11,
                    marginLeft: 2,
                    lineHeight: 15,
                  }}>
                  {item.expense}VND
                </Text>
              </View>
            </View>

            <TouchableOpacity
              style={{
                paddingHorizontal: 3,
                backgroundColor: COLORS.lightGray,
                borderRadius: 30,
                paddingVertical: 2,
                marginRight: 3,
                rotation: 25,
              }}>
              <IonIcon name={icons.plane} color={COLORS.primary} size={25} />
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Section;
