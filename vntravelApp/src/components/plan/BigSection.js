import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {SpeedDial} from 'react-native-elements';
import Spinner from 'react-native-spinkit';
import InionIcon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import {InformModal, PlaceInSec, TextButton} from '../';
import {COLORS, FONTS, icons, SIZES} from '../../constants';
import {PlanCtrl} from '../../controller';
import Lang from '../../language';
import {clearGoBack} from '../../stores/screenSlice';
const BigSection = ({item, navigation, isVisible = false, day, planId}) => {
  const token = useSelector(state => state.auth.token);
  const goBack = useSelector(state => state.screen.goBack);
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const [showNote, setShowNote] = useState(false);
  const [section, setSection] = useState(item);
  const [loading, setLoading] = useState(false);
  const [informAlert, setInformAlert] = useState(false);
  const [alertMess, setAlertMess] = useState('');
  const [comfirmDelete, setComfirmDelete] = useState(false);
  const updateSection = obj => setSection({...section, ...obj});
  const [places, setPlaces] = useState(
    item?.places.sort(
      (a, b) => new Date(a.visitedTime) - new Date(b.visitedTime),
    ),
  );

  const handelDeleteSection = () => {
    if ((item === null || item === undefined) && section) {
      setSection(null);
      setPlaces([]);
      return;
    }
    if (section !== null) {
      setLoading(true);
      PlanCtrl.deleteSection(token, section.id)
        .then(res => {
          if (res && res.data.success) {
            setSection(null);
            setInformAlert(true);
            setAlertMess(Lang.t('delete_success'));
          } else {
            setInformAlert(true);
            setAlertMess(Lang.t('some_error'));
          }
        })
        .catch(err => {
          setComfirmDelete(true);
          setAlertMess(Lang.t('some_error'));
          console.log(err);
        })
        .finally(() => setLoading(false));
    } else {
      setAlertMess(Lang.t('no_plan'));
      setInformAlert(true);
    }
  };

  const updateASection = () => {
    if (section) {
      let data = {...section, places: places};
      setLoading(true);
      console.log('Section:', section);
      if (section.id)
        PlanCtrl.updateSection(token, data, section.id)
          .then(res => {
            if (res.data.success) {
              setSection(res.data.section);
              setInformAlert(true);
              setAlertMess(Lang.t('update_success'));
            } else {
              setInformAlert(true);
              setAlertMess(Lang.t('some_error'));
            }
          })
          .catch(err => {
            setInformAlert(true);
            setAlertMess(Lang.t('some_error'));
            console.log(err);
          })
          .finally(() => setLoading(false));
      else {
        data = {...data, plan: planId};
        PlanCtrl.createSection(token, data)
          .then(res => {
            if (res && res.data.success) {
              setSection(res.data.section);
              setInformAlert(true);
              setAlertMess(Lang.t('create_success'));
            } else {
              setInformAlert(true);
              setAlertMess(Lang.t('some_error'));
            }
          })
          .catch(err => console.log(err))
          .finally(() => setLoading(false));
      }
    } else {
      setInformAlert(true);
      setAlertMess(Lang.t('nothing_to_save'));
    }
  };

  useEffect(() => {
    if (goBack) {
      dispatch(clearGoBack());
      if (section) {
        setPlaces(
          section.places?.sort(
            (a, b) => new Date(a.visitedTime) - new Date(b.visitedTime),
          ),
        );
      }
    }

    return () => {};
  }, [goBack]);

  useEffect(() => {
    if (section) {
      updateSection({places: places});
    }
  }, [places]);
  /* -------------------------------------------------------------------------- */
  /*                                  RENDER UI                                 */
  /* -------------------------------------------------------------------------- */
  const renderButtonAddNewPlace = () => {
    return (
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('Collection', {
            screen: 'TripPlan',
            section: section,
            setSection: setSection,
            day: day,
            planId: planId,
          })
        }
        activeOpacity={0.7}
        style={{
          height: 100,
          backgroundColor: COLORS.lightGray,
          borderStyle: 'dashed',
          borderRadius: SIZES.radius,
          borderColor: COLORS.lightBlue1,
          justifyContent: 'center',
          alignItems: 'center',
          borderWidth: 1,
          marginTop: 20,
          marginBottom: 50,
          marginHorizontal: SIZES.radius,
        }}>
        <Text
          style={{
            color: COLORS.lightBlue1,
            ...FONTS.h4,
          }}>
          + {Lang.t('add_destination')}
        </Text>
      </TouchableOpacity>
    );
  };

  return isVisible ? (
    <Animatable.View
      easing="ease-in"
      animation="lightSpeedIn"
      duration={400}
      style={{
        flex: 1,
      }}>
      {!section ? (
        renderButtonAddNewPlace()
      ) : (
        <ScrollView
          style={{
            paddingVertical: SIZES.base,
            paddingHorizontal: SIZES.base,
            paddingBottom: 100,
          }}>
          <TouchableOpacity
            onPress={() => setShowNote(!showNote)}
            activeOpacity={0.7}
            style={{
              paddingHorizontal: SIZES.padding,
              paddingVertical: SIZES.radius,
              backgroundColor: COLORS.lightGray,
              borderRadius: SIZES.base,
              marginHorizontal: 8,
              flexDirection: 'row',
              justifyContent: 'flex-end',
              marginBottom: 2,
            }}>
            <InionIcon name={icons.pen} size={28} color={COLORS.lightBlue1} />
            <Text
              style={{
                color: COLORS.gray,
                ...FONTS.h4,
              }}>
              {Lang.t('note')}
            </Text>
          </TouchableOpacity>
          {showNote && (
            <Animatable.View
              animation="fadeInDown"
              duration={500}
              style={{
                paddingHorizontal: 8,
                paddingVertical: 5,
                backgroundColor: COLORS.lightGray,
                marginHorizontal: 8,
                marginBottom: 10,
              }}>
              <TextInput
                multiline
                maxLength={300}
                onChangeText={value => updateSection({note: value})}
                defaultValue={section.note}
                placeholder={Lang.t('some_note_at_here')}
                style={{
                  color: COLORS.darkGray,
                  ...FONTS.body3,
                  fontSize: 15,
                  lineHeight: 18,
                }}
              />
            </Animatable.View>
          )}

          {/* Place in section */}
          {section &&
            places
              ?.sort(
                (a, b) => new Date(a.visitedTime) - new Date(b.visitedTime),
              )
              .map((i, index) => {
                return (
                  <PlaceInSec
                    index={index}
                    key={index}
                    navigation={navigation}
                    item={i}
                    setPlaces={setPlaces}
                    places={places}
                  />
                );
              })}

          {renderButtonAddNewPlace()}
        </ScrollView>
      )}
      {/* Button save */}
      <SpeedDial
        buttonStyle={{
          height: 70,
          width: 70,
          borderRadius: 35,
          justifyContent: 'center',
          alignItems: 'center',
          padding: 0,
        }}
        containerStyle={{
          borderRadius: 35,
        }}
        color={COLORS.primary}
        isOpen={open}
        icon={{name: 'edit', color: '#fff'}}
        openIcon={{name: 'close', color: '#fff'}}
        onOpen={() => setOpen(!open)}
        onClose={() => setOpen(!open)}>
        <SpeedDial.Action
          buttonStyle={{
            width: 50,
            height: 50,
          }}
          titleStyle={{
            backgroundColor: COLORS.white2,
            color: COLORS.darkGray,
            ...FONTS.body4,
          }}
          color={COLORS.primary}
          icon={{name: 'add', color: '#fff'}}
          title={Lang.t('add_destination')}
          onPress={() => {
            setOpen(false);
            navigation.navigate('Collection', {
              screen: 'TripPlan',
              section: section,
              setSection: setSection,
              day: day,
              planId: planId,
            });
          }}
        />
        <SpeedDial.Action
          buttonStyle={{
            width: 50,
            height: 50,
          }}
          titleStyle={{
            backgroundColor: COLORS.white2,
            color: COLORS.darkGray,
            ...FONTS.body4,
          }}
          color={COLORS.primary}
          icon={{name: 'save', color: '#fff', type: 'antdesign'}}
          title={Lang.t('save_all')}
          onPress={() => {
            updateASection();
            setOpen(false);
          }}
        />
        <SpeedDial.Action
          buttonStyle={{
            width: 50,
            height: 50,
          }}
          titleStyle={{
            backgroundColor: COLORS.white2,
            color: COLORS.darkGray,
            ...FONTS.body4,
          }}
          color={COLORS.primary}
          icon={{name: 'delete', color: '#fff'}}
          title={Lang.t('delete_all')}
          onPress={() => {
            setComfirmDelete(true);
            setOpen(false);
          }}
        />
      </SpeedDial>
      <Animatable.View
        animation="pulse"
        iterationCount="infinite"
        //   iterationDelay={2000}
        easing="ease-in-out"
        style={{
          position: 'absolute',
          bottom: 15,
          right: 15,
        }}></Animatable.View>
      <InformModal
        isVisible={comfirmDelete}
        setIsVisible={setComfirmDelete}
        description={Lang.t('confirm_delete_all_section')}
        title=""
        titleStyle={{
          fontSize: 10,
        }}
        containerStyle={{backgroundColor: COLORS.white3}}
        descriptionStyle={{
          fontSize: 15,
          paddingHorizontal: 2,
          color: COLORS.darkGray1,
        }}
        actionComponent={
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              marginTop: 15,
            }}>
            <TextButton
              onPress={() => setComfirmDelete(false)}
              labelStyle={{
                fontSize: 15,
              }}
              label={Lang.t('cancel')}
              buttonContainerStyle={{
                height: 45,
                width: 120,
                backgroundColor: COLORS.orange,
                borderRadius: 7,
              }}
            />
            <TextButton
              onPress={() => {
                setComfirmDelete(false);
                handelDeleteSection();
              }}
              labelStyle={{
                fontSize: 15,
              }}
              label={Lang.t('delete')}
              buttonContainerStyle={{
                height: 45,
                width: 120,
                borderRadius: 7,
              }}
            />
          </View>
        }
      />
      {/* INFORM AND LOADING */}
      {loading && (
        <View
          style={{
            backgroundColor: COLORS.transparentBlack1,
            position: 'absolute',
            height: SIZES.height - 150,
            width: SIZES.width,
            alignItems: 'center',
            paddingTop: 200,
            top: 0,
            left: 0,
            zIndex: 10,
          }}>
          <Spinner type="WanderingCubes" color={COLORS.lightBlue1} size={50} />
        </View>
      )}
      {informAlert && (
        <Animatable.View
          animation="fadeOut"
          onAnimationEnd={() => setInformAlert(false)}
          delay={2000}
          style={{
            backgroundColor: COLORS.transparentBlack1,
            position: 'absolute',
            height: SIZES.height - 150,
            width: SIZES.width,
            paddingTop: 200,
            alignItems: 'center',
            top: 0,
            left: 0,
            zIndex: 10,
          }}>
          <Animatable.View
            animation="zoomIn"
            easing="ease-in-cubic"
            duration={500}
            style={{
              height: 100,
              width: 180,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: SIZES.radius,
              backgroundColor: COLORS.transparentBlack7,
              marginTop: -20,
            }}>
            <Text
              style={{
                color: COLORS.white,
                ...FONTS.body4,
                fontSize: 14,
                textAlign: 'center',
              }}>
              {alertMess}
            </Text>
          </Animatable.View>
        </Animatable.View>
      )}
    </Animatable.View>
  ) : (
    <View></View>
  );
};

export default BigSection;
