import moment from 'moment';
import React, {useEffect, useState} from 'react';
import {Image, Text, TextInput, TouchableOpacity, View} from 'react-native';
import DatePicker from 'react-native-date-picker';
import AntIcon from 'react-native-vector-icons/AntDesign';
import AwesIcon from 'react-native-vector-icons/FontAwesome';
import IonIcon from 'react-native-vector-icons/Ionicons';
import {InformModal, TextButton} from '../';
import {COLORS, FONTS, icons, images, SIZES} from '../../constants';
import Lang from '../../language';

const PlaceInSec = ({item, navigation, index, setPlaces, places}) => {
  const [timeModal, setTimeModal] = useState(false);
  const [showExpense, setShowExpense] = useState(false);
  const [comfirmDelete, setComfirmDelete] = useState(false);
  const [placeData, setPlaceData] = useState({
    visitedTime: new Date(item?.visitedTime),
    place: item?.place,
    expense: item?.expense,
    isVisited: false,
    _id: item._id,
  });

  const updateData = obj => {
    setPlaceData({...placeData, ...obj});
  };

  const updatePlaces = obj => {
    let placeData_ = {...placeData, ...obj};

    let newPlaces = places
      .map(el => (el._id === placeData_._id ? placeData_ : el))
      .sort((a, b) => new Date(b.visitedTime) - new Date(a.visitedTime));
    setPlaces(newPlaces);
  };

  //Delete place in section
  const handleDeletePlace = () => {
    let newPlaces = places.filter(el => el._id !== placeData._id);
    setPlaces(newPlaces);
    setComfirmDelete(false);
  };

  useEffect(() => {
    setPlaceData({
      visitedTime: new Date(item?.visitedTime),
      place: item?.place,
      expense: item?.expense,
      isVisited: false,
      _id: item._id,
    });
  }, [item]);

  return (
    <View
      style={{
        flexDirection: 'row',
        marginTop: 5,
      }}>
      {/* Timeline */}
      <View
        style={{
          width: SIZES.width * 0.1,
          alignItems: 'center',
        }}>
        <View
          style={{
            height: 40,
            width: 40,
            borderRadius: 20,
            backgroundColor: COLORS.supperLightBlue,
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 5,
          }}>
          <Text
            style={{
              ...FONTS.h4,
              color: COLORS.primary,
            }}>
            {index + 1}
          </Text>
        </View>
        <View
          style={{
            height: 170,
            width: 1,
            backgroundColor: COLORS.supperLightBlue,
          }}
        />
      </View>
      {/* Place list */}
      <View
        style={{
          width: SIZES.width * 0.85,
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingHorizontal: SIZES.radius + 5,
          }}>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}
            onPress={() => setTimeModal(true)}>
            <Text
              style={{
                color: COLORS.primary,
                ...FONTS.h4,
                marginRight: 10,
              }}>
              {moment.utc(placeData.visitedTime).format('LT')}
            </Text>
            <AntIcon name={icons.clock} color={COLORS.primary} size={20} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setComfirmDelete(true)}
            style={{
              height: 34,
              width: 34,

              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <AntIcon name={icons.delete_} color={COLORS.gray2} size={23} />
          </TouchableOpacity>
        </View>
        <DatePicker
          modal
          open={timeModal}
          androidVariant="nativeAndroid"
          mode="time"
          title={Lang.t('start_time')}
          timeZoneOffsetInMinutes={0}
          fadeToColor={COLORS.primary}
          dividerHeight={2}
          style={{
            borderRadius: 10,
            backgroundColor: COLORS.lightBlue,
          }}
          confirmText="Ok"
          cancelText={Lang.t('cancel')}
          textColor={COLORS.lightBlue1}
          date={placeData.visitedTime}
          onConfirm={time => {
            setTimeModal(false);
            updateData({
              visitedTime: new Date(time.getTime()),
            });
            updatePlaces({visitedTime: time});
          }}
          onCancel={() => {
            setTimeModal(false);
          }}
        />
        <View
          style={{
            paddingVertical: SIZES.radius,
          }}>
          <PlaceItem item={item?.place} navigation={navigation} />
          <View
            style={{
              marginTop: SIZES.base,
              marginHorizontal: SIZES.radius,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            {!showExpense && (
              <Text
                style={{
                  color: COLORS.darkGray,
                  ...FONTS.body4,
                  fontSize: 14,
                }}>
                {Lang.t('expense')} - {placeData.expense}VND
              </Text>
            )}

            {showExpense && (
              <TextInput
                style={{
                  backgroundColor: COLORS.white,
                  height: 45,
                  borderBottomWidth: 1,
                  borderColor: COLORS.lightGray2,
                  ...FONTS.body4,
                  width: '80%',
                }}
                onChangeText={value => {
                  updateData({expense: +value});
                  updatePlaces({expense: +value});
                }}
                clearButtonMode="while-editing"
                keyboardType="numeric"
                placeholder={Lang.t('estimate_expense')}
              />
            )}

            <TouchableOpacity
              onPress={() => setShowExpense(!showExpense)}
              style={{
                width: 40,
                height: 40,
                borderRadius: 20,
                backgroundColor: COLORS.white,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <AwesIcon name={icons.dollar} size={25} color={COLORS.yellow} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <InformModal
        isVisible={comfirmDelete}
        setIsVisible={setComfirmDelete}
        description={Lang.t('confirm_delete_place_section')}
        title=""
        titleStyle={{
          fontSize: 10,
        }}
        containerStyle={{backgroundColor: COLORS.white3}}
        descriptionStyle={{
          fontSize: 15,
          paddingHorizontal: 2,
          color: COLORS.darkGray1,
        }}
        actionComponent={
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              marginTop: 15,
            }}>
            <TextButton
              onPress={() => setComfirmDelete(false)}
              labelStyle={{
                fontSize: 15,
              }}
              label={Lang.t('cancel')}
              buttonContainerStyle={{
                height: 45,
                width: 120,
                backgroundColor: COLORS.orange,
                borderRadius: 7,
              }}
            />
            <TextButton
              onPress={() => handleDeletePlace()}
              labelStyle={{
                fontSize: 15,
              }}
              label={Lang.t('delete')}
              buttonContainerStyle={{
                height: 45,
                width: 120,
                borderRadius: 7,
              }}
            />
          </View>
        }
      />
    </View>
  );
};

export const PlaceItem = ({navigation, item}) => {
  return (
    item && (
      <View
        style={{
          backgroundColor: COLORS.white,
          // borderWidth: 1,
          marginVertical: 3,
          marginHorizontal: SIZES.radius,
          borderRadius: SIZES.radius,
          elevation: 3,
        }}>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            // alignItems: 'center',
          }}>
          <View
            style={{
              width: '30%',
              elevation: 5,
            }}>
            <Image
              source={item ? {uri: item.images[0]} : images.background5}
              resizeMode="cover"
              style={{
                height: 110,
                width: 100,
                borderTopLeftRadius: SIZES.radius,
                borderBottomLeftRadius: SIZES.radius,
              }}
            />
          </View>

          <TouchableOpacity
            onPress={() =>
              navigation.navigate('Main', {
                screen: 'DetailPlace',
                params: {item: item},
              })
            }
            activeOpacity={0.5}
            style={{
              width: '70%',
              paddingLeft: SIZES.radius,
              paddingRight: 4,
              justifyContent: 'space-between',
              // alignItems: 'flex-start',
              paddingVertical: SIZES.base,
            }}>
            <Text
              numberOfLines={1}
              style={{
                color: COLORS.darkGray1,
                ...FONTS.h3,
                fontSize: 16,
                lineHeight: 20,
                // paddingBottom: SIZES.radius,
              }}>
              {item.name}
            </Text>

            <View
              style={{
                flexDirection: 'row',
                paddingRight: 15,
                justifyContent: 'flex-start',
                alignItems: 'center',
                // marginBottom: SIZES.base,
              }}>
              <Text
                numberOfLines={1}
                style={{
                  color: COLORS.gray2,
                  ...FONTS.body5,
                  marginLeft: 2,
                  lineHeight: 15,
                }}>
                {item.address}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingRight: 5,
                    alignItems: 'center',
                    marginBottom: 5,
                  }}>
                  {/* <AntIcon name={icons.clock} size={16} color={COLORS.primary} /> */}
                  <Text
                    style={{
                      color: COLORS.darkGray,
                      ...FONTS.body5,
                      fontSize: 11,
                      marginLeft: 2,
                      lineHeight: 15,
                    }}>
                    {item.openTime} - {item.closeTime}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingRight: 5,
                    alignItems: 'center',
                  }}>
                  {/* <AwesIcon
                  name={icons.dollar}
                  size={16}
                  color={COLORS.primary}
                /> */}
                  <Text
                    style={{
                      color: COLORS.darkGray,
                      ...FONTS.body5,
                      fontSize: 11,
                      marginLeft: 2,
                      lineHeight: 15,
                    }}>
                    {Lang.t('price')} {item.price.start} - {item.price.end}
                  </Text>
                </View>
              </View>

              <TouchableOpacity
                style={{
                  paddingHorizontal: 3,
                  backgroundColor: COLORS.lightGray,
                  borderRadius: 30,
                  paddingVertical: 2,
                  marginRight: 3,
                  rotation: 25,
                }}>
                <IonIcon name={icons.plane} color={COLORS.primary} size={25} />
              </TouchableOpacity>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  );
};

export default PlaceInSec;
