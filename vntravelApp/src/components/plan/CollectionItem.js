import React, {useState} from 'react';
import {View, Text, TouchableOpacity, ImageBackground} from 'react-native';
import {SIZES, COLORS, FONTS, icons, images} from '../../constants';
import AntIcon from 'react-native-vector-icons/AntDesign';
import Ionicon from 'react-native-vector-icons/Ionicons';
import * as Animatable from 'react-native-animatable';
import Lang from '../../language';
const CollectionItem = ({
  item,
  onPress,
  addButtonVisible,
  navigation,
  deleteFavoritePlace,
}) => {
  const [showDeleteInform, setShowDeleteInform] = useState(false);

  return (
    <TouchableOpacity
      onLongPress={() => setShowDeleteInform(true)}
      onPress={() =>
        navigation.navigate('Main', {
          screen: 'DetailPlace',
          params: {item: item},
        })
      }
      activeOpacity={0.7}
      style={{
        width: SIZES.width * 0.47,
        height: SIZES.height * 0.3,
        marginVertical: 3,
      }}>
      <ImageBackground
        resizeMode="cover"
        source={item.images ? {uri: item.images[0]} : images.background5}
        imageStyle={{borderRadius: SIZES.radius}}
        style={{
          width: SIZES.width * 0.47,
          height: SIZES.height * 0.3,
          justifyContent: 'flex-end',
          padding: 6,
        }}>
        <View
          style={{
            width: SIZES.width * 0.47,
            height: SIZES.height * 0.3,
            position: 'absolute',
            top: 0,
            left: 0,
            backgroundColor: COLORS.black,
            opacity: 0.4,
            borderRadius: SIZES.radius,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        />
        <Text
          numberOfLines={2}
          style={{
            color: COLORS.white,
            ...FONTS.h4,
            fontSize: 16,
          }}>
          {item.name}
        </Text>
        <Text
          numberOfLines={1}
          style={{
            color: COLORS.lightGray,
            ...FONTS.body4,
            fontSize: 13,
          }}>
          {item.address}
        </Text>
      </ImageBackground>
      {addButtonVisible && (
        <View
          style={{
            width: SIZES.width * 0.47,
            height: SIZES.height * 0.3,
            position: 'absolute',
            top: 0,
            left: 0,
            borderRadius: SIZES.radius,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={onPress}
            style={{
              width: 40,
              height: 40,
              borderRadius: 20,
              backgroundColor: COLORS.transparentWhite4,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Ionicon name={icons.add} size={32} color={COLORS.white} />
          </TouchableOpacity>
        </View>
      )}
      {!addButtonVisible && showDeleteInform && (
        <TouchableOpacity
          onPressIn={() => setShowDeleteInform(false)}
          style={{
            backgroundColor: COLORS.transparentBlack1,
            position: 'absolute',
            width: SIZES.width * 0.47,
            height: SIZES.height * 0.3,
            alignItems: 'center',
            top: 0,
            borderRadius: SIZES.radius,

            left: 0,
            zIndex: 10,
            justifyContent: 'center',
          }}>
          <Animatable.View
            animation="zoomIn"
            duration={300}
            easing="ease-in-sine"
            style={{
              width: SIZES.width * 0.4,
              justifyContent: 'center',
              // alignItems: 'center',
              borderRadius: SIZES.base,
              backgroundColor: COLORS.white3,
            }}>
            <TouchableOpacity
              onPress={() => {
                deleteFavoritePlace(item.id);
                setShowDeleteInform(false);
              }}
              activeOpacity={0.5}
              style={{
                paddingVertical: SIZES.radius,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <AntIcon name={icons.delete_} color={COLORS.blue} size={23} />
              <Text
                style={{
                  color: COLORS.darkGray,
                  ...FONTS.body4,
                  textAlign: 'center',
                  marginLeft: SIZES.radius,
                }}>
                {Lang.t('delete')}
              </Text>
            </TouchableOpacity>
            {/* <Divider
              width={1}
              color={COLORS.lightGray1}
              style={{
                width: SIZES.width * 0.67,
              }}
            /> */}
          </Animatable.View>
        </TouchableOpacity>
      )}
    </TouchableOpacity>
  );
};

export default CollectionItem;
