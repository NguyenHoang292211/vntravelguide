import React from 'react';
import {
  Modal,
  ScrollView,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {Divider} from 'react-native-elements';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {FONTS, COLORS, icons, SIZES} from '../constants';
import IconAnt from 'react-native-vector-icons/AntDesign';
// const deviceHeight = Dimensions.get('window').height;

const BottomModal = ({
  onTouchOutSide,
  title,
  show,
  close,
  renderContent,
  refModal,
  showHeader = false,
}) => {
  /**
   * Can touch out side
   * @param {*} onTouch
   * @returns
   */
  const renderOutSideTouchable = onTouch => {
    const view = <View style={{flex: 1, width: '100%'}}></View>;
    if (!onTouch) return view;
    return (
      <TouchableWithoutFeedback
        onPress={onTouch}
        style={{flex: 1, width: '100%'}}>
        {view}
      </TouchableWithoutFeedback>
    );
  };

  return (
    <Modal
      ref={refModal}
      animationType="trade"
      transparent={true}
      visible={show}
      onRequestClose={() => close(false)}>
      <View
        style={{
          flex: 1,
          backgroundColor: '#000000AA',
          justifyContent: 'flex-end',
        }}>
        {renderOutSideTouchable(onTouchOutSide)}
        {/* /* Render title */}
        <View
          style={{
            backgroundColor: COLORS.white,
            width: '100%',
            borderTopLeftRadius: SIZES.base*2,
            borderTopRightRadius: SIZES.base*2,
            paddingHorizontal: 10,
            paddingBottom: 40,
            paddingTop:15
          }}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 20,
            }}>
            {showHeader && (
              <>
                <View style={{width: '50%'}}>
                  <Text
                    style={{
                      color: COLORS.primary,
                      margin: 16,
                      ...FONTS.h3,
                      marginBottom: 5,
                    }}>
                    {title}
                  </Text>
                  <Divider width={3} color={COLORS.primary} />
                </View>
                <TouchableOpacity
                  style={{
                    backgroundColor: COLORS.lightGray,
                    padding: 6,
                    borderRadius: 20,
                  }}
                  onPress={() => {
                    close(false);
                  }}>
                  <IconAnt
                    name={icons.close}
                    size={23}
                    color={COLORS.primary}
                  />
                </TouchableOpacity>
              </>
            )}
          </View>
          {/* /* Render content */}
          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{paddingBottom: 10}}>
            {renderContent}
          </ScrollView>
        </View>
      </View>
    </Modal>
  );
};

export default BottomModal;
