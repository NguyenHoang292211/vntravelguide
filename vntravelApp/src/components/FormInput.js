import React from 'react';
import {KeyboardAvoidingView, Text, TextInput, View} from 'react-native';
import {COLORS, FONTS, SIZES} from '../constants';

const FormInput = ({
  containerStyle,
  label,
  labelStyle,
  placeHolder,
  inputStyle,
  prependComponent,
  appendCommponent,
  onChange,
  secureTextEntry,
  keyboardType = 'default',
  autoCompleteType = 'off',
  autoCapitalize = 'none',
  errorMsg = '',
  multiline = false,
  numberOfLines = 1,
}) => {
  console.log('hello sss');
  return (
    <View
      style={{
        ...containerStyle,
      }}>
      {/* Label && errorMsg */}

      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Text
          style={{
            color: COLORS.lightGray1,
            ...FONTS.body4,
            ...labelStyle,
          }}>
          {label}
        </Text>
      </View>
      {/* Text input */}
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: SIZES.padding,
          marginTop: 5,
          borderRadius: SIZES.base,
          backgroundColor: COLORS.lightGray,
        }}>
        {prependComponent}
        <TextInput
          style={{
            flex: 1,
            ...FONTS.body4,
            ...inputStyle,
          }}
          placeholder={placeHolder}
          placeholderTextColor={COLORS.lightGray2}
          secureTextEntry={secureTextEntry}
          keyboardType={keyboardType}
          autoCapitalize={autoCapitalize}
          // onChangeText={text => onChange(text)}
          // onEndEditing={text => onChange(text)}
          autoComplete={autoCompleteType}
          multiline={multiline}
          numberOfLines={numberOfLines}
        />
        {appendCommponent}
      </View>

      <Text
        style={{
          color: COLORS.pink,
          ...FONTS.body4,
        }}>
        {errorMsg}
      </Text>
    </View>
  );
};

export default FormInput;
