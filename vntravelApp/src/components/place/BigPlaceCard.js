import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Icon1 from 'react-native-vector-icons/AntDesign';
import {useDispatch, useSelector} from 'react-redux';
import {COLORS, FONTS, icons, images, SIZES} from '../../constants';
import {UserCtrl} from '../../controller';
import {updateUserData} from '../../stores/authSlice';

const BigPlaceCard = ({navigation, place, updateFavoritePlace}) => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.auth.userData);
  const isAuthorized = useSelector(state => state.auth.isAuthorized);
  const token = useSelector(state => state.auth.token);

  const saveToRecentSearch = async () => {
    if (isAuthorized === true && user != null)
      await UserCtrl.saveRecentSearch(place.id ? place.id : place._id, token)
        .then(res => {
          if (res.data.success) {
            dispatch(updateUserData(res.data));
          }
        })
        .catch(err => {
          console.log(err);
        });
  };
  return (
    <View
      style={{
        backgroundColor: COLORS.white,
        // borderWidth: 1,
        marginVertical: 7,
        marginHorizontal: SIZES.radius - 2,
        borderRadius: SIZES.radius + 5,
      }}>
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          // alignItems: 'center',
        }}>
        <View
          style={{
            width: '40%',
            elevation: 5,
          }}>
          <Image
            source={place ? {uri: place.images[0]} : images.background1}
            resizeMode="cover"
            style={{
              height: 150,
              width: 150,
              borderTopLeftRadius: SIZES.radius + 5,
              borderBottomLeftRadius: SIZES.radius + 5,
            }}
          />
          <View
            style={{
              position: 'absolute',
              top: 7,
              left: 7,
              backgroundColor: COLORS.white,
              padding: 1,
              justifyContent: 'center',
              alignItems: 'center',
              paddingHorizontal: 2,
              borderRadius: SIZES.radius * 0.5,
              flexDirection: 'row',
            }}>
            <Icon1
              style={{marginRight: 2}}
              name="star"
              size={14}
              color={COLORS.orange}
            />
            <Text
              style={{color: COLORS.orange, ...FONTS.body5, lineHeight: 15}}>
              {place.rateVoting ? Math.round(place.rateVoting * 10) / 10 : 1}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              updateFavoritePlace(place.id);
              
            }}
            activeOpacity={0.7}
            style={{
              position: 'absolute',
              top: 7,
              right: 7,
              backgroundColor: 'rgba(214,214,214,0.5)',
              padding: 5,
              borderRadius: 20,
            }}>
            <Icon1
              name={place.isFavorite ? icons.solidHeart : icons.outlineHeart}
              size={18}
              color={COLORS.white}
            />
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          onPress={() => {
            saveToRecentSearch();
            navigation.navigate('DetailPlace', {item: place});
          }}
          activeOpacity={0.5}
          style={{
            width: '60%',
            paddingLeft: SIZES.radius,
            paddingRight: 4,
            justifyContent: 'space-between',
            // alignItems: 'flex-start',
            paddingVertical: SIZES.base,
          }}>
          <Text
            numberOfLines={2}
            style={{
              color: COLORS.darkGray1,
              ...FONTS.h3,
              fontSize: 16,
              lineHeight: 20,
              // paddingBottom: SIZES.radius,
            }}>
            {place.name}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}></View>
          <View
            style={{
              flexDirection: 'row',
              paddingRight: 15,
              justifyContent: 'flex-start',
              alignItems: 'center',
              marginBottom: SIZES.radius,
            }}>
            {/* <Icon name={icons.location} size={18} color={COLORS.primary} /> */}
            <Text
              numberOfLines={2}
              style={{
                color: COLORS.gray,
                ...FONTS.body5,
                marginLeft: 2,
                lineHeight: 15,
              }}>
              {place.address}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            {place.category.name && (
              <View
                style={{
                  borderColor: place.category.color,
                  borderWidth: 1,
                  paddingVertical: 3,
                  paddingHorizontal: 4,
                  borderRadius: 30,
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: place.category.color,
                    ...FONTS.body4,
                    fontSize: 11,
                  }}>
                  {place.category.name}
                </Text>
              </View>
            )}

            <View
              style={{
                flexDirection: 'row',
                paddingVertical: 3,
                paddingRight: 5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon1 name={icons.clock} size={16} color={COLORS.primary} />
              <Text
                style={{
                  color: COLORS.darkGray,
                  ...FONTS.body5,
                  fontSize: 10,
                  marginLeft: 5,
                  lineHeight: 15,
                }}>
                {place.openTime} - {place.closeTime}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default BigPlaceCard;
