import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
} from 'react-native';
import {COLORS, FONTS, images, SIZES} from '../../constants';

const ProvinceCard = ({cardStyle, item, onPress, navigation}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.6}
      onPress={onPress}
      style={{
        marginHorizontal: 8,
        marginVertical: SIZES.base - 2,
      }}>
      <ImageBackground
        imageStyle={{
          borderRadius: 20,
          ...cardStyle,
        }}
        source={item.image ? {uri: item.image} : images.background2}
        resizeMode="cover"
        style={{
          height: 170,
          width: 230,
          justifyContent: 'flex-end',
          alignItems: 'flex-start',
          //   paddingHorizontal: SIZES.base,
          ...cardStyle,
        }}>
        <View
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            backgroundColor: COLORS.transparentBlack2,
            borderRadius: 20,
            ...cardStyle,
          }}
        />
        <View
          style={{
            alignItems: 'flex-start',
            justifyContent: 'flex-end',
            borderRadius: 15,
            paddingBottom: SIZES.radius,
            paddingHorizontal: SIZES.radius,
          }}>
          <Text
            style={{
              color: COLORS.white,
              ...FONTS.h3,
              fontSize: 19,
            }}>
            {item.name}
          </Text>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );
};

export default ProvinceCard;
