import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Modal,
  Animated,
  TouchableWithoutFeedback,
  ScrollView,
  Text,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {SIZES, FONTS, icons, COLORS} from '../../constants';
import IconAnt from 'react-native-vector-icons/AntDesign';
import {Divider} from 'react-native-elements';
import TwoPointSlider from '../TwoPointSlider';

const FilterModal = ({isVisible, onClose, children}) => {
  const [showFilterModal, setShowFilterModal] = useState(isVisible);
  const modalAnimatedValue = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    if (showFilterModal) {
      Animated.timing(modalAnimatedValue, {
        toValue: 1,
        duration: 500,
        useNativeDriver: false,
      }).start();
    } else {
      Animated.timing(modalAnimatedValue, {
        toValue: 0,
        duration: 500,
        useNativeDriver: false,
      }).start(() => onClose());
    }
  }, [showFilterModal]);

  useEffect(() => {
    setShowFilterModal(isVisible);
    return () => {};
  }, [isVisible]);

  const modalY = modalAnimatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [SIZES.height, SIZES.height - 680],
  });

  const renderDistance = () => {
    return (
      <Section title="Distance">
        <View
          style={{
            alignItems: 'center',
            paddingHorizontal: 5,
          }}>
          <TwoPointSlider
            values={[1, 30]}
            min={1}
            max={100}
            postfix="km"
            onValuesChange={value => {
              console.log(value);
            }}
          />
        </View>
      </Section>
    );
  };

  const Section = ({containerStyle, title, children}) => {
    return (
      <View
        style={{
          marginTop: SIZES.padding,
          ...containerStyle,
        }}>
        <Text
          style={{
            ...FONTS.h4,
            color: COLORS.black,
          }}>
          {title}
        </Text>
        {children}
      </View>
    );
  };

  return (
    <Modal transparent={true} visible={isVisible} animationType="slide">
      <View
        style={{
          flex: 1,
          backgroundColor: COLORS.transparentBlack7,
        }}>
        <Animated.View
          style={{
            position: 'absolute',
            left: 0,
            top: modalY,
            width: '100%',
            height: '100%',
            padding: SIZES.radius,
            backgroundColor: COLORS.white,
            borderTopRightRadius: SIZES.padding,
            borderTopLeftRadius: SIZES.padding,
          }}>
          {/* Header */}

          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              paddingBottom: 150,
            }}>
            {/* Render distance */}
            {children}
          </ScrollView>
        </Animated.View>
      </View>
    </Modal>
  );
};

export default FilterModal;
