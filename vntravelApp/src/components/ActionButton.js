import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {TouchableHighlight} from 'react-native-gesture-handler';
import MyText from './MyText';
const ActionButton = ({text, color, textSize, weight, style, onPress_}) => {
  return (
    <TouchableOpacity activeOpacity={0.5} onPress={onPress_}>
      <View style={[styles.button, style]}>
        <MyText
          style={{fontSize: textSize, color: color}}
          weight={weight}
          text={text}
        />
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 12,
    paddingHorizontal: 15,
    borderRadius: 25,
    elevation: 7,
  },
});

export default ActionButton;
