import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {COLORS, FONTS, SIZES} from '../constants';

export default FlatListPlaceSuggestion = ({
  icon,
  name,
  address,
  onPress,
  data,
}) => {
  return (
    <TouchableOpacity style={styles.item} onPress={() => onPress(data)}>
      <Icon name={icon} size={16} color={COLORS.primary} style={styles.icon} />
      <View>
        <Text style={styles.name}>{name}</Text>
        <Text style={styles.address}>{address}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  icon: {
    backgroundColor: COLORS.transparentBlack2,
    borderRadius: 100,
    width: 27,
    height: 27,
    justifyContent: 'center',
    textAlign:'center',
    alignContent:'center',
    marginRight: SIZES.base,
    paddingTop:5,
  },
  item: {
    flexDirection: 'row',
    padding: 7,
    paddingRight: 0,
    alignItems: 'center',
    ...FONTS.body4,
    backgroundColor:COLORS.white
  },
  name: {
    ...FONTS.h4,
    color:COLORS.darkGray1
  },
  address: {
    ...FONTS.body4,
    color: COLORS.descText,
  },
});
