import React, {forwardRef, useImperativeHandle, useRef, useState} from 'react';
import {FlatList, StyleSheet, TextInput, TouchableOpacity} from 'react-native';
import {View, Text, ActivityIndicator} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {FONTS, SIZES, COLORS, constants} from '../constants';

import FlatListPlaceSuggestion from './FlatListPlaceSuggestion';
const AutoComplete = (props, ref) => {
  const [searchText, setSearchText] = useState(
    isPinLocation ? 'Pin location' : '',
  );
  const inputRef = useRef();
  useImperativeHandle(ref, () => ({
    pinLocation(message) {
      //TODO: update value in input to Pin Location
      setSearchText(message);
    },
  }));
  const {placeHolder, getLocation, isPinLocation, notifyFocus} = props;

  const [isFocus, setIsFocus] = useState(false);
  const [visiblePrediction, setVisiblePrediction] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [predictedPlaces, setPredictedPlaces] = useState([]);

  const handleFocus = () => {
    setIsFocus(true);
    notifyFocus(true);
  };

  const handleTextChange = value => {
    setSearchText(value);
    if (value === '') {
      setVisiblePrediction(false);
    } else {
      setVisiblePrediction(true);
      getPredictedPlaces(value);
    }
  };
  const getPredictedPlaces = input => {
    if (input !== '') {
      setIsLoading(true);
      const predictionUrl = `${constants.autoCompleteAPI}=${input}`;
      setPredictedPlaces([]);
      fetch(predictionUrl)
        .then(response => {
          if (response.ok) {
            return response.json();
          }
          throw response;
        })
        .then(data => {
          setPredictedPlaces(data.predictions);
          return data;
        })
        .catch(error => {
          console.log(error);
        })
        .finally(() => {
          setIsLoading(false);
        });
    }
  };
  const getPlaceById = (place_id, description) => {
    const url = `${constants.infoPlaceAPI}=${place_id}&api_key=${constants.APIPermissionKey}`;
    setSearchText(description);
    setPredictedPlaces([]);
    fetch(url)
      .then(response => {
        if (response.ok) return response.json();
      })
      .then(data => {
        getLocation(data);
      })
      .catch(error => {
        console.log(error);
      });
  };

  const handleSelectedPlace = data => {
    getPlaceById(data.place_id);
    setSearchText(data.description);
  };
  const handleClearInput = () => {
    setSearchText('');
    getLocation(null);
    setVisiblePrediction(false);
  };

  return (
    <View>
      <View>
        <TextInput
          style={styles.searchBox}
          placeholder={placeHolder}
          placeholderTextColor={COLORS.lightGray2}
          value={searchText}
          onFocus={() => handleFocus()}
          onChangeText={value => {
            handleTextChange(value);
          }}></TextInput>
        {!isLoading && visiblePrediction && isFocus && (
          <TouchableOpacity
            style={styles.iconClear}
            onPress={() => handleClearInput()}>
            <Icon name="close" size={18} />
          </TouchableOpacity>
        )}
      </View>

      <View style={styles.autoComplete}>
        {isLoading && (
          <View style={styles.loadingContainer}>
            <ActivityIndicator size="large" color={COLORS.mainBlue} />
          </View>
        )}
        {!isLoading && visiblePrediction && isFocus && (
          <FlatList
            data={predictedPlaces}
            renderItem={({item}) => {
              return (
                <FlatListPlaceSuggestion
                  icon="location-arrow"
                  name={item.structured_formatting.main_text}
                  address={item.structured_formatting.secondary_text}
                  data={item}
                  onPress={handleSelectedPlace}
                />
              );
              // </View>
            }}></FlatList>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  searchBox: {
    marginTop: 10,
    backgroundColor: COLORS.white,
    height: 60,
    paddingLeft: 30,
    paddingRight: 30,
    borderRadius: SIZES.radius,
    ...FONTS.body4,
    fontSize: 14,
    color: COLORS.darkGray1,
  },

  autoComplete: {
    top: 10,
    left: 0,
    backgroundColor: COLORS.white,
    zIndex: 100,
    flexDirection: 'row',
  },
  iconClear: {
    position: 'absolute',
    top: 28,
    right: 25,
    width: 25,
    height: 25,
    borderRadius: 30,
    backgroundColor: COLORS.darkGray,
    opacity: 0.2,
    color: COLORS.darkGray1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
});

export default forwardRef(AutoComplete);
