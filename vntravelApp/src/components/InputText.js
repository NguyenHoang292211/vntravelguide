import React from 'react';
import {StyleSheet, TextInput, View} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

const InputText = ({iconName, placeholder}) => {
  return (
    <View style={styles.inputText}>
      <Icon name={iconName} size={25} color="#1D6989" />
      <TextInput
        style={styles.input}
        placeholder={placeholder}
        placeholderTextColor="#79A5B7"
      />
    </View>
  );
};
const styles = StyleSheet.create({
  inputText: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 5,
    marginVertical: 10,
    borderRadius: 30,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#1D6989',
  },

  input: {
    paddingHorizontal: 15,
    fontSize: 14,
    width: '85%',
    textAlign: 'center',
  },
});
export default InputText;
