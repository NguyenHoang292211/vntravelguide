import React, {useEffect, useState} from 'react';
import {View, FlatList} from 'react-native';
import {ReviewItem, Skeleton, InformModal, TextButton} from '..';
import {SIZES, FONTS, COLORS, images} from '../../constants';
const ReviewList = ({reviews, overview = false, pageShown = 1, navigation}) => {
  const recordPerPage = 30;
  const [page, setPage] = useState(pageShown);
  const [reviewCurrent, setReviewCurrent] = useState([]);
  const [loginModalVisible, setLoginModalVisible] = useState(false);

  useEffect(() => {
    if (reviews) {
      if (overview) {
        setReviewCurrent(reviews.slice(0, 4));
      } else {
        setReviewCurrent(reviews.slice(0, page * recordPerPage));
      }
    }
  }, [reviews]);

  const renderSkeleton = item => {
    return (
      <View
        key={item}
        style={{
          marginBottom: 10,
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Skeleton
            containerStyle={{width: 60}}
            layout={[
              {
                width: 50,
                height: 50,
                borderRadius: 25,
                marginHorizontal: SIZES.radius,
                marginVertical: SIZES.base,
              },
            ]}>
            <View />
          </Skeleton>
          <View
            style={{
              paddingHorizontal: SIZES.base,
            }}>
            <Skeleton
              containerStyle={{width: SIZES.width - 100}}
              layout={[
                {
                  width: 150,
                  height: 20,
                },
                {
                  width: 100,
                  height: 15,
                  marginVertical: SIZES.base,
                  marginTop: 5,
                },
              ]}></Skeleton>
          </View>
        </View>
        <View
          style={{
            marginHorizontal: SIZES.base,
          }}>
          <Skeleton
            containerStyle={{width: SIZES.width - 100}}
            layout={[
              {
                width: 120,
                height: 15,
              },
              {
                width: 180,
                height: 18,
                marginTop: 5,
              },
              {
                width: SIZES.width - 50,
                height: 30,
                marginVertical: SIZES.base,
                marginTop: 5,
              },
            ]}></Skeleton>
        </View>
        <View
          style={{
            marginHorizontal: SIZES.base,
          }}>
          <Skeleton
            containerStyle={{width: SIZES.width - 50, flexDirection: 'row'}}
            layout={[
              {
                width: 100,
                height: 70,
                marginHorizontal: 2,
              },
              {
                width: 100,
                height: 70,
                marginHorizontal: 2,
              },
              {
                width: 100,
                height: 70,
                marginHorizontal: 2,
              },
            ]}></Skeleton>
        </View>
      </View>
    );
  };
  return (
    <View>
      {!reviewCurrent || reviewCurrent.length === 0
        ? [0, 1].map(item => renderSkeleton(item))
        : reviewCurrent.map((item, index) => (
            <ReviewItem
              setLoginModalVisible={setLoginModalVisible}
              navigation={navigation}
              key={`review-${index}`}
              item={item}
            />
          ))}
      <InformModal
        navigation={navigation}
        isVisible={loginModalVisible}
        setIsVisible={setLoginModalVisible}
        description="Maybe you forget to sign in. Join us now to start to review!"
        title="OPP!"
        source={images.signin}
        actionComponent={
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 30,
            }}>
            <TextButton
              label="Sign in"
              onPress={() => navigation.replace('Auth', {screen: 'Signin'})}
              buttonContainerStyle={{
                height: 50,
                width: 200,
                elevation: 3,
              }}
            />
          </View>
        }
      />
    </View>
  );
};

export default ReviewList;
