import moment from 'moment';
import React, {useEffect, useState} from 'react';
import {
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  Modal,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {Divider} from 'react-native-elements/dist/divider/Divider';
import Icon from 'react-native-vector-icons/AntDesign';
import IconFea from 'react-native-vector-icons/Feather';
import {Tooltip} from 'react-native-elements';
import Spinner from 'react-native-spinkit';
import {useSelector} from 'react-redux';
import {ImageModal, RatingCustom, TextButton} from '..';
import {COLORS, constants, FONTS, icons, images, SIZES} from '../../constants';
import {ReviewCtrl} from '../../controller';
import Lang from '../../language';
import {TextInput} from 'react-native';

const ReviewItem = ({item, setLoginModalVisible}) => {
  const [showImageModal, setShowImageModal] = useState(false);
  const [selectedImageIndex, setSelectedImageIndex] = useState(0);
  const [review, setReview] = useState(item);
  const [isReadMore, setIsReadMore] = useState(false);
  const [showReport, setShowReport] = useState(false);
  const token = useSelector(state => state.auth.token);
  const user = useSelector(state => state.auth.userData);
  const [isLiked, setIsLiked] = useState(false);
  const isAuthorized = useSelector(state => state.auth.isAuthorized);
  const [reportData, setReportData] = useState({
    reason: '',
    review: item.id,
    isLoading: false,
  });

  const [reportSuccess, setReportSuccess] = useState(false);
  const updateReport = obj => setReportData({...reportData, ...obj});
  const handleLike = reviewId => {
    if (isAuthorized) {
      ReviewCtrl.updateLikeStatus(reviewId, token)
        .then(data => {
          if (data.review) {
            setReview(data.review);
            if (data.review.likedUser.length > 0) {
              if (data.review.likedUser.find(id => id === user.id)) {
                setIsLiked(true);
              } else setIsLiked(false);
            } else setIsLiked(false);
          }
        })
        .catch(err => {
          console.log(err);
          alert('Some error happen!');
        });
    } else setLoginModalVisible(true);
  };

  const createReport = () => {
    if (isAuthorized) {
      if (reportData.reason.trim() !== '') {
        updateReport({isLoading: true});
        ReviewCtrl.createReport(reportData, token)
          .then(res => {
            if (res.success) {
              console.log(res);
              setReportSuccess(true);
            } else {
              console.log(res);
              setReportSuccess(false);
            }
          })
          .catch(err => {
            console.log(err);
          })
          .finally(() => {
            updateReport({isLoading: false});
          });
      }
    } else setLoginModalVisible(true);
  };

  const checkIsLiked = () => {
    if (review.likedUser.length > 0) {
      if (review.likedUser.find(id => id === user.id)) {
        setIsLiked(true);
      } else setIsLiked(false);
    }
  };

  useEffect(() => {
    if (isAuthorized) {
      checkIsLiked();
    } else setIsLiked(false);
  }, []);

  return (
    <View>
      {/* User's info */}
      <View
        style={{
          flexDirection: 'row',
          paddingVertical: SIZES.base,
          alignItems: 'center',
          width: '100%',
        }}>
        {/* Avatar */}
        <Image
          source={{uri: review.user?.image}}
          defaultSource={images.photo}
          style={{
            height: 40,
            width: 40,
            resizeMode: 'cover',
            borderRadius: 20,
          }}
        />
        {/* User info */}
        <View
          style={{
            marginLeft: 5,
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '88%',
            alignItems: 'center',
          }}>
          <View>
            <Text
              style={{
                color: COLORS.darkGray1,
                ...FONTS.h4,
              }}>
              {review.user.fullName}
            </Text>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  color: COLORS.gray,
                  ...FONTS.body4,
                }}>
                {Lang.t('visited')}{' '}
                {moment(review.visitedTime).utc(true).format('ll')}
              </Text>
              {/* <Moment date={review.visitedTime}/> */}
            </View>
          </View>
          <Tooltip
            withOverlay={false}
            pointerColor={COLORS.darkGray}
            backgroundColor={COLORS.white}
            withPointer={false}
            skipAndroidStatusBar={true}
            containerStyle={{
              paddingVertical: SIZES.radius,
              height: 50,
              color: COLORS.black,
              // marginTop: 40,
              marginLeft: 15,
              width: 130,
              elevation: 10,
            }}
            popover={
              <TouchableOpacity
                onPress={() => {
                  setShowReport(true);
                }}>
                <Text
                  style={{
                    color: COLORS.darkGray1,
                    ...FONTS.body4,
                  }}>
                  {Lang.t('report')}
                </Text>
              </TouchableOpacity>
            }>
            <IconFea name={icons.more} size={28} color={COLORS.gray3} />
          </Tooltip>
        </View>
      </View>
      {/* Rating */}
      <View
        style={{
          alignItems: 'flex-start',
        }}>
        <RatingCustom
          imageSize={20}
          tintColor={COLORS.white}
          value={review.rate ? Math.round(review.rate * 10) / 10 : 1}
        />
      </View>
      {/* Content */}
      <View style={{}}>
        {/* Title */}
        <Text
          style={{
            color: COLORS.darkGray1,
            ...FONTS.h4,
            paddingVertical: 3,
          }}>
          {review.title}
        </Text>
        {/* Review */}

        <Text
          // numberOfLines={10}
          style={{
            color: COLORS.darkGray,
            ...FONTS.body4,
            lineHeight: 17,
            textAlign: 'justify',
          }}>
          {!isReadMore
            ? review.content.length > +constants.NUMBER_WORD_OF_REVIEW
              ? review.content.slice(0, +constants.NUMBER_WORD_OF_REVIEW) +
                '... '
              : review.content
            : review.content}
        </Text>
        <TouchableOpacity onPress={() => setIsReadMore(!isReadMore)}>
          {review.content.length > +constants.NUMBER_WORD_OF_REVIEW && (
            <Text
              style={{
                ...FONTS.body4,
                color: COLORS.gray2,
                textAlign: 'right',
              }}>
              {isReadMore == true ? Lang.t('hide') : Lang.t('read_more')}
            </Text>
          )}
        </TouchableOpacity>
        {/* Time */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingVertical: 3,
          }}>
          <Text
            style={{
              color: COLORS.gray2,
              ...FONTS.h5,
            }}>
            {Lang.t('written')}{' '}
            {moment(review.createAt)
              .parseZone('Asia/Ho_Chi_Minh')
              .format('lll')}
          </Text>
          {/* <Moment date={review.createAt}></Moment> */}

          <TouchableOpacity
            onPress={() => handleLike(review.id)}
            style={{
              flexDirection: 'row',
              paddingRight: 2,
              alignItems: 'center',
            }}>
            <Icon
              name={isLiked ? icons.solidLike : icons.outlineLike}
              size={20}
              color={isLiked ? COLORS.primary : COLORS.gray}
            />
            <Text
              style={{
                color: COLORS.gray,
                ...FONTS.body4,
                paddingLeft: 3,
              }}>
              {review.likeCount}
            </Text>
          </TouchableOpacity>
        </View>
        {/* Image  */}
        {review.images && (
          <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={true}
            style={{
              marginVertical: SIZES.base,
              flexDirection: 'row',
            }}>
            {review.images.map((item, index) => (
              <TouchableOpacity
                activeOpacity={0.7}
                key={`image-${index}`}
                style={{
                  marginRight: 2,
                }}
                onPress={() => {
                  setShowImageModal(true);
                  setSelectedImageIndex(index);
                }}>
                <Image
                  source={{uri: item}}
                  style={{
                    height: 80,
                    width: SIZES.width / 3 - 20,
                    resizeMode: 'cover',
                  }}
                />
              </TouchableOpacity>
            ))}
          </ScrollView>
        )}
        <ImageModal
          visible={showImageModal}
          setShowImageModal={setShowImageModal}
          images={review.images}
          selectedIndex={selectedImageIndex}
        />
      </View>
      <Divider
        style={{marginVertical: SIZES.base, marginHorizontal: SIZES.base}}
        width={1}
        color={COLORS.lightGray1}
      />
      <Modal
        transparent={true}
        animationType="fade"
        presentationStyle="overFullScreen"
        visible={showReport}>
        <TouchableOpacity
          onPress={() => setShowReport(false)}
          style={{
            backgroundColor: COLORS.transparentBlack1,
            height: SIZES.height,
            width: SIZES.width,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              paddingHorizontal: SIZES.radius,
              paddingVertical: SIZES.padding,
              width: SIZES.width * 0.85,
              backgroundColor: COLORS.white2,
              borderRadius: 10,
            }}>
            <Text
              style={{
                color: COLORS.primary,
                ...FONTS.h4,
              }}>
              {Lang.t('report_title')}
            </Text>
            <TextInput
              clearButtonMode="always"
              placeholderTextColor={COLORS.gray2}
              numberOfLines={4}
              maxLength={300}
              multiline
              onChangeText={value => updateReport({reason: value})}
              placeholder={Lang.t('report_holder')}
              style={{
                color: COLORS.darkGray1,
                ...FONTS.body4,

                fontSize: 13,
                paddingHorizontal: SIZES.radius,
                marginTop: SIZES.base,
                paddingVertical: SIZES.base,
                borderWidth: 1,
                borderColor: COLORS.lightGray1,
                textAlignVertical: 'top',
                flexWrap: 'wrap',
              }}
            />

            {/* suggestion */}
            <View
              style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                marginTop: 10,
                justifyContent: 'center',
              }}>
              {/* {constants.reportSuggestion.map((item, i) => {
                return (
                  <TouchableOpacity
                    onPress={() => addToReason(Lang.t(item))}
                    style={{
                      borderRadius: 20,
                      padding: SIZES.base,
                      paddingVertical: 10,
                      borderWidth: 1,
                      borderColor: COLORS.gray3,
                      // backgroundColor: selectedReason.includes(Lang.t(item), 0)
                      //   ? COLORS.lightBlue
                      //   : COLORS.white2,
                      marginVertical: 3,
                    }}>
                    <Text
                      style={{
                        ...FONTS.body4,
                        color: COLORS.gray,
                        lineHeight: 13,
                        fontSize: 12,
                      }}>
                      {Lang.t(item)}
                    </Text>
                  </TouchableOpacity>
                );
              })} */}
              {reportData.isLoading ? (
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: '100%',
                    height: 70,
                  }}>
                  <Spinner type="Circle" color={COLORS.lightBlue1} size={30} />
                </View>
              ) : reportSuccess ? (
                <Animatable.View
                  animation="fadeOut"
                  easing="ease-in-cubic"
                  duration={400}
                  delay={1500}
                  onAnimationEnd={() => {
                    setShowReport(false);
                    setReportSuccess(false);
                  }}
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      color: COLORS.green,
                      ...FONTS.h4,
                    }}>
                    Done
                  </Text>
                  <Icon name={icons.check} color={COLORS.green} size={30} />
                </Animatable.View>
              ) : (
                <TextButton
                  label={Lang.t('send')}
                  onPress={() => createReport()}
                  labelStyle={{
                    fontSize: 14,
                  }}
                  buttonContainerStyle={{
                    padding: 8,
                    width: 120,
                    marginTop: 5,
                  }}
                />
              )}
            </View>
          </View>
        </TouchableOpacity>
      </Modal>
    </View>
  );
};

export default ReviewItem;
